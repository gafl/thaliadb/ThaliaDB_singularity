rm -f ./Postgres_Container/ctn.img
cd ./Postgres_Container
singularity  build ctn.img postgres.def

rm -f ../Mongo_Container/ctn.img 
cd ../Mongo_Container
singularity  build ctn.img mongodb.def

rm -f ../Thalia_Container_Ubuntu/ctn.img
cd ../Thalia_Container_Ubuntu
singularity  build ctn.img recipe.def
