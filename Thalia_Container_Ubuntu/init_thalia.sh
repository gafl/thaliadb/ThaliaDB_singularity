#recuperation du nom de l'image
nom_container_img=`ls | grep -o *\.img`

port_mongo=`cat ../Mongo_Container/config/params.conf | grep port: | grep -o [0-9]*`
ip=`cat ../Mongo_Container/config/params.conf | grep bindIp: | grep -o [0-9.]*`

port_postgres=`cat ../Postgres_Container/config/params.conf | grep "port = "| sed s/[^0-9]*//g`
listen_addresses=`cat ../Postgres_Container/config/params.conf | grep "listen_addresses = "| grep -o \'.*\' | sed s/\'//g`

#changment du PORT pour postgres
sed -e "s/'PORT':'.*'/'PORT':'$port_postgres'/g" -i ./thalia/settings.py

##changment du listen_addresses pour postgres
sed -e "s/'HOST':'.*'/'HOST':'$listen_addresses'/g" -i ./thalia/settings.py

##changment du port pour mongo
sed -e "s/"\"port\":[0-9]*"/"\"port\":$port_mongo"/g" -i ./thalia/settings.py

##changment de l'ip pour mongo
sed -e "s/\"host\": '.*'/\"host\": '$ip'/g" -i ./thalia/settings.py


singularity run --app init -B ./thalia:/var/www/django-apps/thalia $nom_container_img
