$(document).ready(
	function() {
		/*Show a datepicker when datefield*/
		$(".datePicker").datepicker();
		
		/*split the screen in tabs*/
		$("#tabs").tabs();
		
		/*add sort function to the table*/
		$("#basictable").tablesorter(); 
		
		$("#clear-btn").click(function() {
			//cas particulier du password pour User
			$(this).closest('form').find("#id_login").closest('tr').after('<tr><th><label for="id_password">Password:</label></th><td><input id="id_password" maxlength="128" name="password" type="text"></td></tr>');
			//cas des tabs (accession, locus, seedlot type)
			$.each($('#sortable > tr'),function(index, element){
				element.remove();
			});
			
			$(this).closest('form').find("input[type=text], input[type=password], textarea, select").val("");
		    $(this).closest('form').find("input[type=checkbox]").prop('checked', false);
		    $("input[name=hiddenid]").remove();
		    $('#submit-btn').val('Create');
		    
		});
	}
);
