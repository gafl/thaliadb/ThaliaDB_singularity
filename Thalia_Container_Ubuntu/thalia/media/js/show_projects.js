function ShowProjectsTab(button, id_projects, table_id, type_data){
	var compteur = "0";
	for(var i=0; i<id_projects.options.length; i++){
		if(id_projects.options[i].selected){
			table_id.style.display="block";
			var id_tab_proj = type_data + "_" + id_projects.options[i].value;
			var element = document.getElementById(id_tab_proj);
			element.style.display="table-row";
			compteur = "1";
		}
		else{
			var id_tab_proj = type_data + "_" + id_projects.options[i].value;
			var element = document.getElementById(id_tab_proj);
			element.style.display='none';
		}
	}
	if (compteur == "0"){
		alert("No project selected");
	}
};

function function_add_br(element,object_val) {
	var str=object_val; 
	var coma=','; 
	var dec = 1; 
	var newstr = ''; 
	for (var i = 0; i <= str.length - 1; i++) {
		var currentChar = str.charAt(i);
		newstr = newstr + currentChar;
		if (currentChar == coma){
    		dec=dec+1;
    	};
    	if (dec % 10 === 0){
    		newstr = newstr + '<br>';
    		dec = 1
    	};
	};
	element.innerHTML=newstr;
};

function tick_all(element, name_id){
	var element_to_tick = name_id;
    var case_all = element;
    if (case_all.checked==true){
    	for (var element_from_list in element_to_tick){
    		element_to_tick.item(element_from_list).selected=true;
    	}
    }
    else{
    	for (var element_from_list in element_to_tick){
    		element_to_tick.item(element_from_list).selected=false;
    	}
    }
};
