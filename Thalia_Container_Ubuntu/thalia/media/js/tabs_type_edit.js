$(document).ready(
	function() {
		//on initialise le tableau avec l'id 'sortable'
		//quand on passe la souris le curseur devient pointer
		$( "#sortable" ).hover(function(){
			$(this).css('cursor','pointer');
		});
		//les élément tr sont définits comme triables
		$( "#sortable" ).sortable({
		      items: "> tr",
		      cursor:"pointer"
	    });
		//on désactive la sélection
	    $( "#sortable" ).disableSelection();

	    
	    //Fonction d'édition du Type (indépendant des attributs)
		$('.edit2').on('click',function() {
			$("#clear-btn").click();
			
			object = $(this).next('.delete').next('[name=objectid]');
			classe = object.next('[name=classid]');
			
			$.ajax({
				url:ROOT_URL.concat("entitytypeedit/"),
				type:"POST",
				data: {'objectid':object.val(),"classname":$('#classname').val()},
				dataType: "json",
                success: function(json) {
                	for (var key in json[0]['fields']) {
                		$('#id_'+key).val(json[0]['fields'][key]);
                	}
                	$('#submit-btn').val('Update');
                	$('#type_form').append('<input type="hidden" name="hiddenid" value="'+object.val()+'" />');
                	$.each(json, function(index) {
                		if (index != 0) {
                			$("#sortable").append('<tr class="ui-state-default"><td>Edit/Delete</td><td align="center" width="250px">'+json[index]['fields']['attribute'][0]+'</td><td>'+types[json[index]['fields']['attribute'][1]-1]+'</td></tr>');
                		}
                	});
                },
                error: function(jqXHR, textStatus, errorThrown) {
                	alert("Edition failed !");
                }
			});
		});
		
		//Ajout d'attributs
		var attribute_number = 0
		$('#add-attr').on('click',function() {

			var myData = {"classname":$('#classname').val(),
						"attribute":$('#attributes_autocomplete').val(),
						"type":$('#id_type').val()};
			
			if ( $( "input[name=hiddenid]" ).length > 0 ) {
				myData['objectid'] = $( "input[name=hiddenid]" ).val();
			}
			$.ajax({
				url:ROOT_URL.concat("addattributes/"),
				type:"POST",
				data: myData, //classname doit être généric
				dataType: "json",
				success: function(json) {
					$('#sortable').append('<tr class="ui-state-default"><td>Edit/Delete</td><td align="center" width="250px">'+$('#attributes_autocomplete').val()+'</td><td>'+types[$('#id_type').val()-1]+'</td></tr>');
					$('#type_form').append('<input type="hidden" name="attribute'+attribute_number+'" value="'+$('#attributes_autocomplete').val()+';'+$('#id_type').val()+'" />');
					attribute_number = attribute_number+1;
					
					$('#attributes_autocomplete').val("");
					$('#id_type').val("");
					
				},
				error: function(jqXHR, textStatus, errorThrown) {
                	alert("Adding an attribute failed !");
                }
			});
			return false;
		});
	}
);