import os
import sys

"""
    CUSTOM SETTINGS
"""

ROOT_URL = "/"
#Apache settings

DJANGO_ROOT_SETTING = ''
#Database settings

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'HOST':'localhost',
        'PORT':'5437',
        'NAME': 'thalia',                  
        'USER': 'thaliadbadmin',                     
        'PASSWORD': 'db',                  
        'ATOMIC_REQUESTS': True,
    },      
}

MONGODB_DATABASES = {
    "default": {
        "name": "genotyping",
        "host": 'localhost',
        "port":27017,
        # "password": 'db',
        # "username": 'thaliadbadmin',
        #"tz_aware": True, # if you using timezones in django (USE_TZ = True)

    },
}

# Path to store heavy files
HEAVYFILES_URL = '/tmp/'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/var/www/django-apps/thalia/media/'

# Used by Django File Form module 
DEFAULT_FILE_STORAGE = MEDIA_ROOT
