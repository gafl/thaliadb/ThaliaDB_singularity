from django.conf.urls import include, url
# import notifications.urls
#from django.conf.urls import patterns, url, include
from django.contrib.auth.views import logout
from commonfct.genericviews import genericdelete, genericedit, getattributes, addattributes, typeedit, get_empty_file
from commonfct.filerenderer import filerenderer
from django.conf import settings
from django.conf.urls.static import static


from team.views import authentication, home, user_profile
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
print("reading urls")
urlpatterns = [
    # Examples:
    # url(r'^$', 'Thaliadbv3.views.home', name='home'),
    url(r'^home/$', home, name="home"),
    url(r'^userprofile/$', user_profile, name="user_profile"),
    #url(r'^team/', include('team.urls')),
    url(r'^$', authentication, name='login' ),
    url(r'^logout/$', logout, {'next_page': settings.ROOT_URL}, name='logout'),
    url(r'^contact/',include('team.urls')),
    url(r'^help/',include('team.urls')),
    url(r'^team/', include('team.urls')),
#     (r'^seedlot/', include('lot.urls')),
    url(r'^genealogy/', include('genealogy_managers.urls')),
    url(r'^lot/', include('lot.urls')),
    url(r'^phenotyping/', include('phenotyping.urls')),
    url(r'^accession/', include('accession.urls')),
    url(r'^images/', include('images.urls')),
    #Ajout Guy_Ross,
    url(r'^genotyping/', include('genotyping.urls')),
    url(r'^dataview/', include('dataview.urls')),
    url(r'^classification/', include('classification.urls')),
    
    #Appels JQuery
    url(r'^genericdelete/$', genericdelete, name='genericdelete' ),
    url(r'^genericedit/$', genericedit, name='genericedit' ),
    url(r'^getattributes/$', getattributes, name='getattributes' ),
    url(r'^addattributes/$', addattributes, name='addattributes' ),
    url(r'^entitytypeedit/$', typeedit, name='typeedit' ),
    
    url(r'^getemptyfile/(?P<classtype>[a-z]+)/(?P<object_id>[0-9]+)/$', get_empty_file, name='getemptyfile'),
    url(r'^filerenderer/(?P<filetype>\w+)/(?P<type_id>\d+)/$',filerenderer, name='filerenderer'),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

import os
import settings
