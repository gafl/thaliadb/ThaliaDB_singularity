# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import csv
import ast

from django.db.models import Q
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from accession.models import AccessionType, AccessionTypeAttribute, AccessionAttributePosition, Accession
from accession.forms import AccessionTypeForm, AccessionTypeAttForm, AccessionDataForm
from accession.forms import UploadFileForm
from genealogy_accession.models import AccessionRelation
from team.models import Project
from commonfct.genericviews import generic_management, get_fields
from commonfct.utils import recode
from commonfct.constants import ACCEPTED_TRUE_VALUES

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accessionhome(request):
    return render(request,'accession/accession_base.html',{"admin":True})
#=============================


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def accession_type(request):
    formatt = AccessionTypeAttForm()
    response = generic_management(AccessionType, AccessionTypeForm, request, 'accession/accession_type.html',{"attrib_update":True,"admin":True, 'title':'Accession Type Management','attributes':formatt,'classname':'Accession'},"Accession Type Management")
                
    if request.method == "POST":
        try :
            name = request.POST["name"]
            attribute = "attribute0"
            i = 0
            
            while attribute in request.POST:
                att_type = int(request.POST[attribute].split(';')[-1])
                att_name= ';'.join(request.POST[attribute].split(';')[:-1])
                fields_exist = [acc_field.name for acc_field in Accession._meta.get_fields()]
                for f in ['Parent Male','Parent Female']:
                    fields_exist.append(f)

                for j in fields_exist:
                    if att_name.lower() == j.lower() :
                        error_field="The field '{0}' already exists, please change the attribute name.".format(att_name)
                        response = generic_management(AccessionType, AccessionTypeForm, request, 'accession/accession_type.html',{"name":name,"error_field":error_field,"admin":True, 'title':'Accession Type Management','attributes':formatt,'classname':'Accession'},"Accession Type Management")
                        return response
#                         return render(request,'accession/accession_type.html',{"form":AccessionTypeForm,"error_field":error_field,"admin":True, 'title':'Accession Type Management','attributes':formatt,'classname':'Accession'})
                acc_type = AccessionType.objects.get(name=name)
                att, created = AccessionTypeAttribute.objects.get_or_create(attribute_name=att_name, type=att_type)
                position_qs = acc_type.accessionattributeposition_set.all().order_by('-position')
                if position_qs :
                    AccessionAttributePosition.objects.create(attribute=att,type=acc_type,position=position_qs[0].position+1)
                else :
                    AccessionAttributePosition.objects.create(attribute=att,type=acc_type,position=1)
                i+=1
                attribute = "attribute{0}".format(str(i))
        except Exception as e :
            print(e)
    return response
#=============================

# Getting excel file for inserting several data

    
@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
#@cache_page(60 * 3)
def accessiondata(request, type_id):
    #Getting AccessionType corresponding to type_id
    accessiontype = AccessionType.objects.get(id=int(type_id))
    #Getting all attributes of this AccessionType
    attributes = accessiontype.accessiontypeattribute_set.all()
    # Building the form
    formattype = AccessionDataForm(formstype=attributes)
    
    # Building the FileForm
    register_formFile = UploadFileForm()
    
    names, labels = get_fields(formattype)  
    names_genealogy = ['parent_male','parent_female']
    labels_genealogy = ['Parent Male','Parent Female']

            
    request.session['accession'] = 'accession'
    title = accessiontype.name+' Accession Management'
    
    template = 'accession/accession_data.html'   
    accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
    list_attributes=[]
#     print('\naccessions: ',accessions)
    list_attributes_id = []
    accessions_all = Accession.objects.filter(type=accessiontype).distinct()
    number_all = len(accessions_all)
    for i in ['name', 'doi', 'pedigree', 'description', 'projects','parent male','parent female']:
        list_attributes.append(i)
    for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
        list_attributes_id.append(i.id)
    for i in list_attributes_id:
        if str(AccessionTypeAttribute.objects.get(id=i)) not in list_attributes:
            list_attributes.append(str(AccessionTypeAttribute.objects.get(id=i)))
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "names_genealogy":names_genealogy,
                  "labels_genealogy":labels_genealogy,
                  "admin":True,
                  "list_attributes":list_attributes,
                  }
    
    if request.method == "GET":
        dico_get={}
        or_and = None
        ## refine search
        if "no_search" in request.GET:
            return render(request, 'accession/accession_data.html', {"refine_search":True,"admin":True,"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'accession','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names})

        elif "or_and" in request.GET:
            """
               Requete de filtre à placer dans un Manager (refine_search)
                   - accession
                   - lot
                   - sample
                   - 
            """
            
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10
#             if or_and == "or":
#                 proj=[]
#                 query=Q()
#                 list_name_in=[]
#                 list_attr=[]
#                 for dat, dico_type_data in dico_get.items():
#                     if "projects" in dico_type_data:
#                         if dico_type_data['projects']=='':
#                             print('Projects empty')
#                             query |= Q(projects__isnull=True)
#                         else:
#                             proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
#                             query |= Q(projects__in=proj)
#                     elif 'parent male' in dico_type_data:
#                         parent_relation = []
#                         for acc in Accession.objects.filter(name__icontains=dico_type_data['parent male']):
#                             acc_relation = acc.relations_as_parent.filter()
#                             for i in acc_relation:
#                                 parent_relation.append(i.child)
#                         query &= Q(name__in=parent_relation)
#                     elif 'parent female' in dico_type_data:
#                         parent_relation = []
#                         for acc in Accession.objects.filter(name__icontains=dico_type_data['parent female']):
#                             acc_relation = acc.relations_as_parent.filter()
#                             for i in acc_relation:
#                                 parent_relation.append(i.child)
#                         query &= Q(name__in=parent_relation)
#                     else:
#                         for data_type, value in dico_type_data.items():
#                             for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
#                                 if str(data_type) == str(i):
# #                                     print(str(i))
#                                     list_attr.append(str(i))
#                             if data_type in list_attr:
#                                 id_attribute = str(AccessionTypeAttribute.objects.get(attribute_name=data_type).id)
#                                 for i in Accession.objects.filter(type=accessiontype):
#                                     try:
# #                                         print(value)
#                                         if value in ast.literal_eval(i.attributes_values)[id_attribute]:
#                                             print(str(i))
#                                             list_name_in.append(str(i))
#                                     except:
#                                         if value in i.attributes_values[id_attribute]:
# #                                             print(str(i))
#                                             list_name_in.append(str(i))
#                                 query |= Q(name__in = list_name_in)
#                             else:
#                                 var_search = str( data_type + "__icontains" ) 
#                                 query |= Q(**{var_search : dico_type_data[data_type]})
#         
#             if or_and == "and":
#                 proj=[]
#                 query=Q()
#                 list_name_in=[]
#                 list_attr=[]
#                 for dat, dico_type_data in dico_get.items():
#                     if "projects" in dico_type_data:
#                         if dico_type_data['projects']=='':
#                             query &= Q(projects__isnull=True)
#                         else:
#                             proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
#                             query &= Q(projects__in=proj)
#                     elif 'parent male' in dico_type_data:
#                         parent_relation = []
#                         for acc in Accession.objects.filter(name__icontains=dico_type_data['parent male']):
#                             acc_relation = acc.relations_as_parent.filter()
#                             for i in acc_relation:
#                                 parent_relation.append(i.child)
#                         query &= Q(name__in=parent_relation)
#                     elif 'parent female' in dico_type_data:
#                         parent_relation = []
#                         for acc in Accession.objects.filter(name__icontains=dico_type_data['parent female']):
#                             acc_relation = acc.relations_as_parent.filter()
#                             for i in acc_relation:
#                                 parent_relation.append(i.child)
#                         query &= Q(name__in=parent_relation)
#                     else:
#                         for data_type, value in dico_type_data.items():
#                             for i in AccessionTypeAttribute.objects.filter(accession_type=accessiontype):
#                                 if str(data_type) == str(i):
#                                     list_attr.append(str(i))
#                             if data_type in list_attr:
#                                 id_attribute = str(AccessionTypeAttribute.objects.get(attribute_name=data_type).id)
#                                 for i in Accession.objects.filter(type=accessiontype):
#                                     try:
#                                         if value in ast.literal_eval(i.attributes_values)[id_attribute]:
#                                             list_name_in.append(str(i))
#                                     except:
#                                         if value in i.attributes_values[id_attribute]:
#                                             list_name_in.append(str(i))
#                                 query &= Q(name__in = list_name_in)
#                             else:
#                                 var_search = str( data_type + "__icontains" ) 
#                                 query &= Q(**{var_search : dico_type_data[data_type]})
#             
#             query = Q(query, type=accessiontype)  
#             accessions_init = Accession.objects.filter(query).distinct()
            accessions_init = Accession.objects.refine_search(or_and, accessiontype, dico_get)
            
            number_all = len(accessions_init)
            dico = {}
            download_not_empty = False
            dico_relation={}
            accessions=[]
            for acc in accessions_init:
                dico_relation[acc]={}
                for r in acc.relations_as_child.all() :
                    dico_relation[acc][r.parent_gender] = r.parent
#                 if AccessionRelation.objects.filter(child=i):
#                     relation = AccessionRelation.objects.filter(child=i)
#                     dico_relation[i]={}
#                     for j in relation:
#                         dico_relation[i][j.parent_gender]=j.parent
            for acc in accessions_init:
                list_relation = []
                for gender in ['M','F']:
                    list_relation = list_relation_append("accession",gender, dico_relation, acc, list_relation)
                accessions.append([acc,list_relation])

            if accessions:      
                list_col = [f.name for f in accessions[0][0]._meta.get_fields()]
                try:
                    ## si le dictionnaire contenant les attributs est vide, il est considéré comme un string, il faut donc le convertire en dictionnaire
                    get_attr_values = ast.literal_eval(getattr(accessions[0][0],"attributes_values")) 
                except:
                    get_attr_values = getattr(accessions[0][0],"attributes_values")
                for j in get_attr_values.keys():
                    list_col.append(AccessionTypeAttribute.objects.get(id=j).attribute_name)
                for j in ['parent male','parent female']:
                    list_col.append(j)
                for element_to_remove in ['relations_as_child','relations_as_parent','seedlot', 'id', 'type', 'attributes_values']:
                    list_col.remove(element_to_remove)
#                 print(list_col)
                with open("/tmp/result_search.csv", 'w', encoding='utf-8') as csv_file:
                    writer = csv.writer(csv_file)
                    writer.writerow(list_col)
                    for i in accessions_init:
                        row=[]
                        dico=model_to_dict(i)
                        try:
                            dico_attr_values = ast.literal_eval(getattr(i,"attributes_values"))
                        except:
                            dico_attr_values = getattr(i,"attributes_values")
                        for id_attr, val_attr in dico_attr_values.items():
                            dico[AccessionTypeAttribute.objects.get(id=id_attr).attribute_name]=val_attr
                        print(i)
                        try:
                            dico['parent male'] = [a.parent.name for a in AccessionRelation.objects.filter(parent_gender = "M", child=i)]
                            dico['parent female'] = [a.parent.name for a in AccessionRelation.objects.filter(parent_gender = "F", child=i)]
                            
                        except ObjectDoesNotExist:
                            print('doesnt exist')
                        
                        for key, value in dico.items():
                            if not dico[key]:
                                dico[key]=""
                            elif isinstance(dico[key], QuerySet):
                                list_qs = []
                                for queryset_i in dico[key]:
                                    list_qs.append(queryset_i.name)
                                dico[key]=', '.join(list_qs)
                        for col in list_col:
                            if col not in dico:
                                dico[col]=""
                            row.append(dico[col])
                        writer.writerow(row)    
                download_not_empty = True
                tag_fields.update({"download_not_empty":download_not_empty})
# >>>>>>> .merge-right.r614
        #Cas où veut afficher la base de données
        tag_fields.update({"search_result":dico_get,
                           "or_and":or_and,
                           "refine_search":True,
                           'all':accessions,
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'formfile':register_formFile,
                           'form':formattype,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'accession',
                           'typeid': type_id,
                           "list_attributes":list_attributes})
                           
        return render(request, template, tag_fields)

    elif request.method == "POST":
        #Cas où veut modifier la base de données  
        if "hiddenid" in request.POST.keys() :
            return _update_accession_from_form(request, attributes, template, tag_fields, accessiontype)

        
        else : #Ajout de nouvelles données        
            #Formulaire
            formattype = AccessionDataForm(data=request.POST,formstype=attributes)
#             print(formattype.errors.items()
            # EXCEL FILE             
            formf = UploadFileForm(request.POST, request.FILES) 
            try:
                separator = request.POST['delimiter']
            except: 
                pass
            if formf.is_valid():       
                tag_fields.update({'formfile':register_formFile,
                                   'filetype':'accession',
                                   "list_attributes":list_attributes,
                                   "number_all":number_all,
                                   'typeid': type_id,
                                   'form':AccessionDataForm(formstype=attributes),
                                   'creation_mode':True,
                                   'all':accessions,
                                   'nb_per_page':nb_per_page})
                return _create_accession_from_file(request, formf, attributes, accessiontype, template, tag_fields, separator)
            elif formattype.is_valid():
                tag_fields.update({'formfile':register_formFile,
                                  'filetype':'accession',"number_all":number_all,
                                  'typeid': type_id,"list_attributes":list_attributes,
                                  'excel_empty_file':True,
                                  'form':AccessionDataForm(formstype=attributes),
                                  'creation_mode':True, 
                                  'all':accessions,
                                  'nb_per_page':nb_per_page})
                return _create_accession_from_form(formattype, accessiontype, attributes, request, template, tag_fields)
                
            else :
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formattype.errors.items() ])
                return render(request, template, {'formfile':register_formFile,"list_attributes":list_attributes,'filetype':'accession','typeid': type_id,'nb_per_page':nb_per_page,'form':AccessionDataForm(formstype=attributes),'formfile':register_formFile,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names,'errormsg':errormsg})
                #return render(request, template, {'form':formattype,'creation_mode':False, 'all':accessions, 'fields_label':labels,'fields_name':names})    
            #return render(request, 'accession/accession_data.html', {'form':formattype,'creation_mode':True, 'all':accessions, 'fields_label':labels,'fields_name':names})
    else :
        return render(request, 'accession/accession_data.html', {"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'accession','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':accessions, 'fields_label':labels,'fields_name':names})
#=============================
def _create_accession_from_file(request, formf, attributes, accessiontype, template, tag_fields, separator):
    file_uploaded = _upload_file(request.FILES['file'], formf, attributes, accessiontype, request, separator)
    if type(file_uploaded) is str : #1er cas: l'insertion provoque des erreurs
        if file_uploaded.count(';') > 0 :
            counter = file_uploaded.count(';') + 1
            if counter == 1:
                errormsg = 'An accession from your file is already in the database: ' + file_uploaded
            else:
                errormsg=  str(counter) + ' accessions are already in your database: ' + file_uploaded
            tag_fields.update({'errormsg':errormsg})
            return render(request, template, tag_fields)
        else :
            tag_fields.update({'errormsg':file_uploaded})
            return render(request, template, tag_fields)
    elif type(file_uploaded) is list:
        #Cas où l'utilisateur tente un update mais que certaines données du CSV ne sont pas dans la BDD
        missing_accessions = file_uploaded[0]
        del file_uploaded[0]
        if file_uploaded !=[] and len(file_uploaded) ==1:
            missing_accessions= missing_accessions + ' ,' + file_uploaded[0]
        elif file_uploaded != [] and len(file_uploaded)>1:
            missing_accessions = missing_accessions + ' ,' +  '  ,'.join(file_uploaded)
        errormsg = 'Some accessions of your file aren\'t in your database: ' + missing_accessions + ' .No update was done.'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)
    else:
        #Cas d'une insertion où d'un update fonctionnel
        inserted = True
        accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
        accessions_all = Accession.objects.filter(type=accessiontype).distinct()
        number_all = len(accessions_all)
        tag_fields.update({'inserted': inserted,'all':accessions,"number_all":number_all,})
        return render(request, template, tag_fields)

def _create_accession_from_form(formattype, accessiontype, attributes, request, template, tag_fields):
    #Insertion depuis un forumulaire
    newaccession = formattype.save(commit=False)
    newaccession.type = accessiontype
    jsonvalues = {}
    for att in attributes:
        jsonvalues[att.id] = request.POST[att.attribute_name.lower().replace(' ','_')]
    newaccession.attributes_values = jsonvalues
    newaccession.save()
    if 'projects' in request.POST:
        projectss = request.POST.getlist('projects')
        for i in range(len(projectss)):
            project_id = projectss[i]
            project = Project.objects.get(id=project_id)
            newaccession.projects.add(project)
    accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)
    accessions_all = Accession.objects.filter(type=accessiontype).distinct()
    number_all = len(accessions_all)
    tag_fields.update({'all':accessions,"number_all":number_all,})
    return render(request, template, tag_fields)
 
def _update_accession_from_form(request, attributes, template, tag_fields, accessiontype):
#Edition des données
    try : 
        #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
        instance = Accession.objects.get(id=request.POST['hiddenid'])
        form = AccessionDataForm(instance=instance, data=request.POST, formstype=attributes)
        
        if form.is_valid():
            instance = form.save()
            jsonvalues = {}
            for att in attributes:
                jsonvalues[att.id] = request.POST[att.attribute_name.lower().replace(' ','_')]
            instance.attributes_values = jsonvalues
            instance.save()
            accessions, nb_per_page = _get_accessions(accessiontype, attributes, request)     
            accessions_all = Accession.objects.filter(type=accessiontype).distinct()
            number_all = len(accessions_all)                       
            tag_fields.update({'all':accessions,
                               "number_all":number_all,
                               'nb_per_page':nb_per_page,
                               'form':form,
                               'creation_mode':False,
                               'object':instance,
                               'hiddenid':instance.id})
            return render(request,template,tag_fields)
        else :
            #sinon on génère un message d'erreur qui sera affiché
            
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
            tag_fields.update({'form':form,
                               'creation_mode':False,
                               'hiddenid':instance.id,
                               'errormsg':errormsg})
            return render(request,template,tag_fields)
    except ObjectDoesNotExist :
        tag_fields.update({'form':form,
                            'creation_mode':True,
                            'errormsg':"The object you try to update doesn't exist in the database"})
        return render(request,template,tag_fields)

def _get_accessions(accessiontype, attributes, request, ):
    accessions_init = Accession.objects.filter(type=accessiontype).order_by('name')
#     print(accessions)
    dico_relation={}
    accessions=[]
    for i in accessions_init:
        if AccessionRelation.objects.filter(child=i):
            relation = AccessionRelation.objects.filter(child=i)
            dico_relation[i]={}
            for j in relation:
                dico_relation[i][j.parent_gender]=j.parent
    for i in accessions_init:
        list_relation = []
        for gender in ['M','F']:
            list_relation = list_relation_append("accession",gender, dico_relation, i, list_relation)
        accessions.append([i,list_relation])    
    # Ici, on ne peut pas utiliser accessions.iterator()
    # car sur notre ModelForm les attributs dynamiques ne font pas partie
    # de la liste d'attributs du Model
    #print(accessions
    for acc in accessions: 
        #print('acc', acc)
        for att in attributes:
            #print('attributs', att)
            #print('valeurs des attributs', acc.attributes_values)
            #On vérifie si au moins un des attributs a été rempli
            if acc[0].attributes_values :
                #Lorsque les données sont insérées depuis un .csv plutôt qu'à la main, acc.attributes_values est traité comme un String contenant le JSON
                #Il faut donc vérifier la nature de acc.attributes_values
                if type(acc[0].attributes_values) is dict:
                    attributes_values=acc[0].attributes_values
                else:
                    attributes_values =json.loads(acc[0].attributes_values)
                #si l'id de l'attribut "att" est présent dans les clés de attributes_values
                if str(att.id) in attributes_values.keys() :
                    try:
                        setattr(acc[0],att.attribute_name.lower().replace(' ','_'),attributes_values[str(att.id)])
                    except ValueError as e:
                        print(e)
                        setattr(acc[0],att.attribute_name.lower().replace(' ','_'),'')
                else :
                    setattr(acc[0],att.attribute_name.lower().replace(' ','_'),'')
            else :
                setattr(acc[0],att.attribute_name.lower().replace(' ','_'),'')
    
    # Adding Paginator  
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(accessions)
    except:
        nb_per_page=50
    
    
    paginator = Paginator(accessions, nb_per_page)
    page = request.GET.get('page')
#     print("paginator: ",paginator.object_list)
    try:
        accessions = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        accessions = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        accessions = paginator.page(paginator.num_pages)    
    return accessions, nb_per_page     
 #=============================

def list_relation_append(typedata, gender, dico_relation, seed_or_accession, list_relation):
    if not list_relation:
        list_relation = []
    if seed_or_accession not in dico_relation and typedata == "accession":
        list_relation = ['','']
    elif seed_or_accession not in dico_relation and typedata == "seedlot":
        list_relation = ['','','']
    else:
        if gender in dico_relation[seed_or_accession]:
            list_relation.append(dico_relation[seed_or_accession][gender])
        else:
            list_relation.append('')
    return(list_relation)

def _upload_file(files, form, attributess, accestype, request, separator):
    if files != None:
        dictline = {}
        i = 0
        for line in files:
            #On récurère chaque ligne du fichier: la case i contiendra la i-1 ème ligne du fichier
            dictline[i] = line.strip()
            i+=1
        headers = recode(dictline[0])
        #la première ligne du tableau donne attributs du fichier; on les conserve dans un autre tableau et on efface la première ligne de dictvalues
#         print(type(headers))
        theaders = headers.split(separator)
        dictvalues_final = {}
        del dictline[0]
        t=0
        try:
            for key in dictline:
                #On récupère les valeurs correspondant aux clés
                v = recode(dictline[key])
                val = v.split(separator)
                dictvalues = {}
                for i in range(len(theaders)): 
                    #On remplit dictvalues avec les clés et valeur              
                    dictvalues[theaders[i]] = val[i]
                #On stocke le dictionnaire obtenu dans un autre dictionnaire: la case i contiendra la i+1 ème ligne du fichier
                dictvalues_final[t] = dictvalues
                t+=1
        except IndexError: #Cas où certaines lignes seraient vides, par exemple, si l'utilisateur est passé à la ligne après avoir inséré la dernière donnée de son .csv
            return 'There are empty lines in your file. Please fill or remove them.'
        else:
#             print(request.POST)
            if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
                return _submit_upload_file(dictvalues_final, attributess, accestype)
            elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
                return _update_upload_file(dictvalues_final, attributess, theaders)
            else:
                database=Accession.objects.all().values_list('name') 
                return 'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.'
         
#=============================

def _submit_upload_file(dictvalues_final, attributess, accestype):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins un doublon, aucune donnée n'est insérée : soit l'utilisateur voulait faire un update, soit il ne s'est pas apercu des doublons
    Tout d'abord, on extrait les noms des accessions (clé primaire) de la BDD'''
#     print("---   ",dictvalues_final)
    try:
        database=Accession.objects.all().values_list('name') 
        print(database)
        keys_in_database=[i[0] for i in database]
        #Maintenant, on récupère les noms des accessions du CSV, contenues dans dictvalues_final
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
    except KeyError:
        return 'The headers of your files do not correspond to those in the database. Please check them'
    else:
        #On va maintenant comparer les deux listes
        accessions_doublons='' #Ce String stockera les éventuels doublons
        for m in range(len(keys_inserted)):
            for n in range(len(keys_in_database)):
                if keys_inserted[m]==keys_in_database[n]:
                    if accessions_doublons =='':
                        accessions_doublons = keys_inserted[m]
                    else:
                        accessions_doublons = accessions_doublons + ' ;' + keys_inserted[m]
        if set(keys_in_database) == set(keys_inserted):
            return 'All the accessions in your CSV file are already in your database. Maybe you are trying to update former data ?'
        else:
            if accessions_doublons != '': 
                return accessions_doublons
            else: #Cas de l'insertion
                jsonvalues={}
                try:
                    for k in dictvalues_final:
                        #On récupère la k+1 ème ligne du fichier
                        dval = dictvalues_final[k]
                        try :
                            for att in attributess:
                                value = dval[str(att.attribute_name)]
                                if value != None or value !='':
                                    jsonvalues[att.id] = value   
                                else:
                                    jsonvalues[att.id] = ''
                            if dval["Latitude"] != "" and dval["Longitude"] != "":
                                a  = Accession.objects.create(name = str(dval['Name']),
                                                         type=accestype,
                                                         doi = str(dval['DOI']),
                                                         pedigree = str(dval['Pedigree']),
                                                         description = str(dval['Description']),
                                                         latitude = dval['Latitude'],
                                                         longitude = dval['Longitude'],
                                                         is_obsolete = (str(dval['Is Obsolete']) in ACCEPTED_TRUE_VALUES ),
                                                         private_doi = (str(dval['Private DOI']) in ACCEPTED_TRUE_VALUES ),
                                                         attributes_values = json.dumps(jsonvalues)
                                                         )
                            else:
                                a  = Accession.objects.create(name = str(dval['Name']),
                                                         type=accestype,
                                                         doi = str(dval['DOI']),
                                                         pedigree = str(dval['Pedigree']),
                                                         description = str(dval['Description']),
                                                         is_obsolete = (str(dval['Is Obsolete']) in ACCEPTED_TRUE_VALUES ),
                                                         private_doi = (str(dval['Private DOI']) in ACCEPTED_TRUE_VALUES ),
                                                         attributes_values = json.dumps(jsonvalues)
                                                         )
                            project_list = [i.strip() for i in str(dval['Projects']).split('|')]
                            projects = Project.objects.filter(name__in = project_list)
                            if projects.exists():
                                a.projects.add(*list(projects))
                        except IntegrityError as ie :
                            return str(ie)
                        
                except KeyError:
                    return 'The headers of your files do not correspond to those in the database. Please check them'
                
                except IntegrityError as ie:
                    return str(ie)
            
def _update_upload_file(dictvalues_final, attributess, theaders):
    #Il faut s'assurer que l'utilisateur ne s'est pas trompé de bouton et voulait bien faire un update et pas une insertion
    #Pour cela, on va d'abord vérifier si le CSV et la BDD ont des accessions en commun
    try:
        database=Accession.objects.all().values_list('name')
        keys_in_database=[i[0] for i in database]
#         print("<<=====>>>>>> ",dictvalues_final)
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
        common_keys =[x for x in keys_in_database if x in keys_inserted]
        lacking_keys = [x for x in keys_inserted if x not in keys_in_database]
    except KeyError:
        raise
#         return 'The headers of your files do not correspond to those in the database. Please check them'
    else:
        if common_keys == []:
            return 'Your file shares no accession with the database. Please make sure you weren\'t trying to insert new data instead of updating some. '
        elif lacking_keys != []:
            return lacking_keys
        else:
            #1er cas : il s'agit d'une mise à jour des DOI et de leur statut
            if len(dictvalues_final[0]) == 3: #3 pour le nom de l'accession, le DOI et son statut
                for k in range(len(dictvalues_final)):
                    doi=str(dictvalues_final[k]['DOI'])
                    private_doi=True
                    if str(dictvalues_final[k]['Private DOI']) == 'False':
                        private_doi=False
                    Accession.objects.select_related().filter(name=dictvalues_final[k]['Name']).update(doi=doi)
                    Accession.objects.select_related().filter(name=dictvalues_final[k]['Name']).update(private_doi=private_doi)
            else: #2nd cas : on veut mettre à jour l'intégralité des données, SAUF LE NOM
                #Il est possible que l'utilisateur n'ait par mégarde modifié aucune donnée
                #Pour s'en assurer, on va stocker les modifications effectuées dans une liste. Si cette liste est vide, on affiche un message d'erreur
                del theaders[0] #Le nom, clé primiare, ne peut être modifié
                attributes_names = [att.attribute_name for att in attributess]
                j=0
                while theaders[j] not in attributes_names: #A modifier
                    j+=1
                #j est la position du premier champ non statique
                static_headers = [theaders[p] for p in range(j)]
                #On ne peut pas utiliser Accession._meta car il renvoie des champs que l'utilisateur ne peut pas modifier (l'id par exemple)
                #On convertit les en-têtes des colonnes du CSV en les noms des entrées modifiables
                fields_names=[]
                for field_csv in static_headers:
                    field_name = field_csv.lower()
                    field_name=field_name.replace(" ","_")
                    fields_names.append(field_name)
                #pour chaque ligne du CSV
                for k in range(len(dictvalues_final)):
                    jsonvalues ={}
                    keys_updated=[]
                    accession_to_update = Accession.objects.get(name=keys_inserted[k]) #On récuère le nom de l'accession à modifier
                    fields_values = [getattr(accession_to_update, field_to_update) for field_to_update in fields_names] #Puis les valeurs des champs dans la BDD pour cette accession
                    for field_csv, field, l in zip(static_headers, fields_names, range(len(fields_values))):
                        if field == 'projects':
                            if "|" in dictvalues_final[k]['Projects']:
                                project_list = dictvalues_final[k]['Projects'].split('|') #On récupère la liste des projets du CSV
                            else:
                                project_list = [dictvalues_final[k]['Projects']]
                            project_qs = Project.objects.filter(name__in=project_list)
                            accession_to_update.projects.set(project_qs)

                        else:       
                            field_type = Accession._meta.get_field(field).get_internal_type() #Les booléens doivent être traités différemment 
                            new_value = dictvalues_final[k][field_csv] #Pour une accession et un champ donnés, on récupère sa valeur dans le CSV
                            if field_type=='BooleanField' and (new_value == 'False' or new_value == ''):
                                if fields_values[l] == True:
                                    setattr(accession_to_update, field, False)
                                    keys_updated.append([keys_inserted[k], field_csv, False])
                                    accession_to_update.save()
                            elif field_type == 'BooleanField' and new_value != 'False':
                                if fields_values[l] == False:
                                    setattr(accession_to_update, field, True)
                                    keys_updated.append([keys_inserted[k], field_csv, True])
                                    accession_to_update.save()
                            else: 
                                if new_value != fields_values[l]:
                                    setattr(accession_to_update, field, new_value)
                                    keys_updated.append([keys_inserted[k], field_csv, new_value])
                    #On s'occupe désormais des attributs    
                    for att in attributess:
                        value = dictvalues_final[k][str(att.attribute_name)]
                        if value != None or value !='':
                            jsonvalues[att.id] = value   
                        else:
                            jsonvalues[att.id] = ''
                    if type(accession_to_update.attributes_values) is dict:
                        attributes_in_database = accession_to_update.attributes_values
                    else:    
                        attributes_in_database = json.loads(accession_to_update.attributes_values)
                    shared_items = set(jsonvalues.items()) & set(attributes_in_database.items()) #Cette liste contient les valeurs en commun pour chaque clé
                    if len(shared_items) < len(attributes_in_database):
                        accession_to_update.attributes_values = jsonvalues
                        accession_to_update.save()
                if keys_updated == []:
                    return 'All the entries are identical to those previously inserted. No update was done.'