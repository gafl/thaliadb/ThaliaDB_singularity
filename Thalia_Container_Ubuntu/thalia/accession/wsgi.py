"""
WSGI config for Thaliadbv3 project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
#import site

# Add the site-packages of the chosen virtualenv to work with
# site.addsitedir('/var/www/django-apps/venv/lib/python3.5/site-packages')

# # Add the app's directory to the PYTHONPATH
# sys.path.append('/var/www/django-apps/venv')
# sys.path.append('/var/www/django-apps/venv/thalia')

# os.environ['DJANGO_SETTINGS_MODULE'] = 'thalia.settings'

# # Activate your virtual env
# activate_env=os.path.expanduser("/var/www/django-apps/venv/bin/activate_this.py")
# #execfile(activate_env, dict(__file__=activate_env))
# exec(open(activate_env).read())

# import django.core.handlers.wsgi
# application = django.core.handlers.wsgi.WSGIHandler()

# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
# os.environ["DJANGO_SETTINGS_MODULE"] = "Thaliadbv3.settings"
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")



# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# def application(environ, start_response):
#     status = '200 OK'
#     output = b'Hello World!'

#     response_headers = [('Content-type', 'text/plain'),
#                         ('Content-Length', str(len(output)))]
#     start_response(status, response_headers)

#     return [output]


# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
