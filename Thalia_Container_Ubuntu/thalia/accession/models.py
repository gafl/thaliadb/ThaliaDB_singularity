# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from jsonfield import JSONField

from django.db import models
from django.utils.encoding import force_bytes, force_text
from django.core.exceptions import ValidationError
from accession.managers import AccessionManager

from commonfct.constants import ATTRIBUTE_TYPES
from commonfct.managers import EntityTypeManager

# Create your models here.
class AccessionType(models.Model):
    """
      The AccessionType model gives information about the type of accessions.
      
      :var CharField name: name of the accession type
      :var TextField description: description of the accession type
    """
    
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    def __str__(self):
        return self.name
    
    def attribute_set(self):
        return self.accessiontypeattribute_set

class Accession(models.Model):
    """
      The Accession model gives information about the accessions.
      
      :var ForeignKey type: type of the accession, defined in the AccessionType model
      :var CharField name: name of the accession
      :var CharField doi: doi of the accession
      :var BooleanField private_doi: check if the doi of the accession is a private doi or not
      :var TextField pedigree: pedigree of the accession
      :var BooleanField is_obsolete: check if the accession is obsolete or not
      :var TextField description: description of the accession
      :var JSONField attributes_values: dictionary containing the attributes values of the accession
      :var FloatField latitude: latitude of the accession
      :var FloatField longitude: longitude of the accession
      :var ManyToManyField projects: project(s) linked to the accession
    """
    
    type = models.ForeignKey('AccessionType', db_index=True)
    name = models.CharField(max_length=100, unique=True, db_index=True)
    doi = models.CharField(max_length=100, null=True, unique=True)
    private_doi= models.BooleanField(default=True)
    pedigree = models.TextField(max_length=500, blank=True, null=True)
    is_obsolete = models.BooleanField(default= False)
    description = models.TextField(max_length=500, blank=True, null=True)
    #variety = models.CharField(max_length=100, null=True)
    attributes_values = JSONField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    objects = AccessionManager() 
    
    #images = models.ManyToManyField('team.Documents', blank = True) 
    projects = models.ManyToManyField('team.Project', blank = True)
    
    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.doi == "" :
            self.doi=None
        super(Accession, self).save(*args, **kwargs)
        
    def relation_set(self):
        return self.accessionrelation_set 
    
class AccessionTypeAttribute(models.Model):
    """
      The AccessionTypeAttribute model gives information about the attributes linked to the accession type
      
      :var CharField attribute_name: name of the attribute
      :var IntegerField type: type of the attribute
      :var ManyToManyField accession_type: accession type linked to the attribute, defined in the AccessionType model
    """
    attribute_name = models.CharField(max_length=100, blank=True)
    type = models.IntegerField(choices=ATTRIBUTE_TYPES, blank=True)
    accession_type = models.ManyToManyField('AccessionType', through='AccessionAttributePosition')

    objects = EntityTypeManager()
    
    def natural_key(self):
        return (self.attribute_name, self.type)

    def __str__(self):
        return self.attribute_name

    class Meta:
        unique_together = ("attribute_name","type")
    
    def clean(self):
        data = self.clean_fields()
     
    def save(self,*args, **kwargs):
        self.clean()
        super(AccessionTypeAttribute,self).save(*args, **kwargs)

class AccessionAttributePosition(models.Model):
    """
      The AccessionAttributePosition model gives information about the position of the attribute. 
      It allows the movement of the attribute thanks to a drag and drop system and the memorization of the position of this attribute.
      
      :var ForeignKey attribute: name of the attribute, defined with the AccessionTypeAttribute model
      :var ForeignKey type: type of the accession which is linked to the attribute, defined with the AccessionType model
      :var IntegerField position: integer rendering the position number
    """
    
    attribute = models.ForeignKey('AccessionTypeAttribute')
    type = models.ForeignKey('AccessionType')
    position = models.IntegerField()  
    
    class Meta:
        unique_together = ("type","position")        
        