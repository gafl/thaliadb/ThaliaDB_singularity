
# Django settings for Thaliadbv3 project.
import os
import sys

"""
    CUSTOM SETTINGS
"""

PROFILE_LOG_BASE = ""

EMAIL_USE_TLS = True
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587


#ROOT_URL = '/thalia-dev/'
ROOT_URL = '/'
#Apache settings

#DJANGO_ROOT_SETTING = 'thalia-dev.'
DJANGO_ROOT_SETTING = ''
#Database settings
MONGO_DB_NAME = 'genotyping'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'HOST':'localhost',
        'PORT':'5437',
        'NAME': 'thalia',                  
        'USER': 'thaliadbadmin',                     
        'PASSWORD': 'tHalia.6',                  
        'ATOMIC_REQUESTS': True,
    },      
}

MONGODB_DATABASES = {
    "default": {
        "name": "genotyping",
        "host": 'localhost',
        "password": 'tHalia.6',
        "username": 'thaliadbadmin',
        "tz_aware": True, # if you using timezones in django (USE_TZ = True)

    },
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

DEFAULT_FILE_STORAGE = ''


"""
    STATIC SETTINGS
    modify it at your own risk
"""

DEBUG = True
#TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = ROOT_URL+"media/"

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))

#we use sqlite if test are run
if 'test' in sys.argv:
    DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost', '127.0.0.1','pac-sm-gafl01' , 'pac-sm-gafl01.avignon.inra.fr', '147.100.168.2', '[::1]']
#ALLOWED_HOSTS = ['pac-sm-gafl01.avignon.inra.fr']

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''
STATIC_URL = "/media/"
HEAVYFILES_URL = '/tmp/'
# HEAVYFILES_URL = "/var/lib/mongodb/tmpfiles/" ##stockage des fichiers lourds sur morpheus sur partition mongodb qui a plus d'espace

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, "media"),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '(8bxrv!ldw1z)j#(a7d5&d0_vpq1f)2l&49r9p*qq*4g#bb8_m'

# # List of callables that know how to import templates from various sources.
# TEMPLATE_LOADERS = (
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader',
# #     'django.template.loaders.eggs.Loader',
# )

MIDDLEWARE_CLASSES = (      
    #'django.middleware.cache.UpdateCacheMiddleware',    # This must be first on the list
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware', # This must be last
)

ROOT_URLCONF = DJANGO_ROOT_SETTING+'urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [PROJECT_PATH+"/templates/",],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                'django_settings_export.settings_export',
                "django.contrib.messages.context_processors.messages",
                DJANGO_ROOT_SETTING+"accession.processor.accession_menu",
                DJANGO_ROOT_SETTING+"accession.processor.locus_menu",
                DJANGO_ROOT_SETTING+"accession.processor.seedlot_menu",
                DJANGO_ROOT_SETTING+'commonfct.processor.rooturl'
            ],
            'debug':True
        },
    },
]


# TEMPLATE_DIRS = (
#     PROJECT_PATH+"/templates/",
#      Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
#      Always use forward slashes, even on Windows.
#      Don't forget to use absolute paths, not relative paths.
# )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'team.apps.TeamConfig',
    'accession.apps.AccessionConfig',
    'lot.apps.LotConfig',
    'phenotyping.apps.PhenotypingConfig',
    'classification.apps.ClassificationConfig',
    'genotyping.apps.GenotypingConfig',
    'commonfct.apps.CommonfctConfig',
    'dataview.apps.DataviewConfig',
    'django_mongoengine',
    'django_bootstrap_typeahead',
    'dal',
    'dal_select2',
    'django.contrib.admin',
)

GRAP_MODELS={
    'all_applications':True,
    'group_models':True,
}

CHANNEL_LAYERS={
    "default":{
        "BACKEND":"asgiref.inmemery.ChannelLayer",
        "ROUTING":"genotyping.routing.channel_routing",
    },
}


SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

AUTH_USER_MODEL = 'team.User'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/'


#26/8/2015
# I use this in the 'upload_to' arg for FileFields
# and ImageFields, hence making it a setting.
UPLOAD_PATH = 'uploads/%Y/%m'

FILE_UPLOAD_HANDLERS = ("django.core.files.uploadhandler.MemoryFileUploadHandler",                      
                        "django.core.files.uploadhandler.TemporaryFileUploadHandler")


VERSION = '3.3'
SETTINGS_EXPORT = [
    'VERSION',
]
