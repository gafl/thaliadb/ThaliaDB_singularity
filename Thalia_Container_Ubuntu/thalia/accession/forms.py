# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django import forms

from accession.models import AccessionType, AccessionTypeAttribute, Accession
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect

requests=[('name','name'),
          ('project','project'),
          ('doi','doi')]
or_and_choices=[('and','and'),('or','or')]

class AccessionRefineSearchForm(forms.Form):
    """
      Form used for refined queries on the existing tables.
      
      :var ChoiceField or_and: Radio button allowing the choice between 'or' and 'and', which will be condition for the query
      :var ChoiceField request_by: menu containing the choices of columns on which the user can apply its query
      :var list requests: list of tuples that defines the fields on which the user can refine his search
      :var list or_and_choices: list of tuples that defines the choices between 'and' and 'or' conditions
    """
    or_and = forms.ChoiceField(widget=forms.RadioSelect(), choices=or_and_choices, required=True, initial="and")
    request_by = forms.ChoiceField(choices=requests)

class AccessionTypeForm(forms.ModelForm):
    """
      Form based on the AccessionType model managing data from this model
    """
    class Meta:
        model = AccessionType
        fields = '__all__'

class AccessionTypeAttForm(forms.ModelForm):
    """
      Form based on the AccessionTypeAttribute model managing data from this model
    """
    class Meta:
        model = AccessionTypeAttribute
        exclude = ['accession_type']
        #exclude = ['accession']#test guy
        widgets = {'attribute_name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}
        

class AccessionDataForm(forms.ModelForm):  
    """
      Form based on the Accession model, allowing the creation of new accessions into the database
    """  
    class Meta:
        model = Accession
        exclude = ['attributes_values', 'type']
        

    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(AccessionDataForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(AccessionDataForm, self).__init__(data=kwargs['data'])
            else :
                super(AccessionDataForm, self).__init__()
            for at in attrs:
                self.fields[at.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)

        else :
            if 'data' in kwargs.keys():
                super(AccessionDataForm, self).__init__(data=kwargs['data'])
            else :
                super(AccessionDataForm, self).__init__()
        
        self.fields['doi'].required=False
    
    
SepChoices=[(',',' , '),
            (';',' ; ')]
class UploadFileForm(forms.Form):
    """
      Form allowing the uploading of a file, querying a delimiter to be readable
      
      :var FileField file: required field, file to be uploaded
      :var ChoiceField delimiter: delimiter of the file, required so that the developer knows how to read the file
      :var list SepChoices: list of tuples that defines the delimiters that can be used by the user
    """
    file = forms.FileField(required=True, label='Main files')
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")
    #delimiter = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=SepChoices, required=True, initial=",")
