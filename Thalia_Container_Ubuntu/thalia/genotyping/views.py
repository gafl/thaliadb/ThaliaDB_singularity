# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import transaction
import pymongo
from pymongo import MongoClient
#from commonfct import filerenderer

from genotyping.models import AttributeValue, Experiment, Referential, Position,LocusType, LocusTypeAttribute, LocusAttributePosition, Locus, Sample, GenotypingID, RefXpGenotype
from genotyping.forms import UploadFileForm_Matrix, PositionForm, ExperimentForm, ReferentialForm, LocusTypeForm, LocusTypeAttForm,LocusDataForm,SampleForm, LocusDataFileForm,UploadGenoTypValueFileForm, ViewGenotypingValuesForm, DropGenoValuesForm,UploadFileForm, ChoiceRefExpForm, ChoiceIndOrFreq, GenotypingIDForm, UploadFileFormGenotypingID

from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm,\
    PhenoViewerEnvironmentForm,\
    GenoViewerAccessionForm, GenoViewerLocusForm, GenoViewerExperimentForm,\
    DataviewGenotypingValuesForm, GenoViewerReferentialForm, GenoViewerChrForm,\
    UploadLocusFileForm
from team.models import Project, Institution, Person
from accession.models import Accession
from django.contrib import messages
from django.http import HttpResponse
import time
from genotyping.forms import UploadFileForm
from commonfct.genericviews import generic_management
from commonfct.utils import get_fields
import json
from django.core.exceptions import ObjectDoesNotExist

from lot.models import Seedlot
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from mongoengine.queryset.visitor import Q
from django.db.models import Q as Query_dj
import pandas as pd
import csv
import os
import io
import numpy as np
import sys
import re
import datetime
import subprocess
from django.conf import settings

from django.db import IntegrityError
from django.db.models import Count
from collections import Counter


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genotypinghome(request):
    return render(request,'genotyping/genotyping_base.html',{"admin":True,})

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def experiment_management(request):
    number_all = len(Experiment.objects.all())
    
    list_attributes = ['Name','Institution','Person','Date','Comments','Projects']

#     adds= {'special_case':True,"list_attributes":list_attributes, "number_all":number_all,"admin":True,
#            "noheader":True,'excel_empty_file':True,'filetype':'experiment','typeid':0}

    adds= {"list_attributes":list_attributes, "number_all":number_all,"admin":True,
           "noheader":True,'excel_empty_file':True,'filetype':'experiment','typeid':0, "refine_search":True}
    
    if 'or_and' in request.GET and "no_search" not in request.GET:
        dico_get={}
        or_and = None
        or_and=request.GET['or_and']
        length_dico_get = len(dico_get)
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]}
        
        if or_and == "or":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query |= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person" or data_type == "institution":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            else:
                                data_filter = Institution.objects.filter(name__icontains=value)
                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query |= Query_dj(**{var_search : search_value})
        elif or_and == "and":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query &= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person" or data_type == "institution":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            else:
                                data_filter = Institution.objects.filter(name__icontains=value)
                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query &= Query_dj(**{var_search : search_value})
        experiments = Experiment.objects.filter(query).distinct()
        number_all = len(experiments)
        nb_per_page = 10
        adds.update({"query":query,"number_all":number_all,"all":experiments,'search_result':dico_get,})
    return generic_management(Experiment, ExperimentForm, request, 'genotyping/genotyping_generic.html',
                              adds, "Experiment Management")


@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def sample_management(request):
    inserted = False
    register_formFile = UploadFileForm()
    form = SampleForm()
    names, labels = get_fields(form) 
    template = 'genotyping/genotyping_generic.html'
    title = ' Sample Management'
    list_attributes = ['Name','Seedlot','Description','Codelabo','Tubename','Projects']
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "refine_search":True,
                  'list_attributes':list_attributes,
                  }
    if request.method == "GET":
        if 'or_and' in request.GET and 'no_search' not in request.GET:
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            length_dico_get = len(dico_get)
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
            if or_and == "or":       
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "Projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            data_type = data_type[0].lower()+data_type[1:]
                            var_search = str(data_type + "__icontains" ) 
                            if data_type == "seedlot":
                                var_search = str(data_type+"__in")
                                data_filter = Seedlot.objects.filter(name__icontains=value)
                                data_id_list = []
                                for i in data_filter:
                                    data_id_list.append(i.id)
                                search_value = data_id_list
                            query |= Query_dj(**{var_search : search_value})
                            
            elif or_and == "and":       
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "Projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            data_type = data_type[0].lower()+data_type[1:]
                            var_search = str(data_type + "__icontains" ) 
                            if data_type == "seedlot":
                                var_search = str(data_type+"__in")
                                data_filter = Seedlot.objects.filter(name__icontains=value)
                                data_id_list = []
                                for i in data_filter:
                                    data_id_list.append(i.id)
                                search_value = data_id_list
                            query &= Query_dj(**{var_search : search_value})
            samples = Sample.objects.filter(query).distinct()
            number_all = len(samples)
            nb_per_page = 10
            tag_fields.update({'search_result':dico_get})
    # affichage de la bdd
        else:
            samples, number_all, nb_per_page = _get_samples(request)
            
        tag_fields.update({'all':samples,
                           "number_all":number_all,
                           "nb_per_page":nb_per_page,
                           'formfile':register_formFile,
                           'form':form,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'sample',
                           'typeid': 0,})
        return render(request, template, tag_fields)

    # modif de la bdd
    elif request.method == "POST":     
        samples, number_all, nb_per_page = _get_samples(request)   
        formf = UploadFileForm(request.POST, request.FILES)
        if formf.is_valid():
            files = request.FILES['file']
            error = uploadSampleFile(files, request)
            if type(error) is str:
                if error == 'All the samples you\'re trying to insert are already in the db. Update?':
                    return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':error})
                elif error == 'Your file share no samples with the database. Please make sure you weren\'t trying to insert new samples':
                    return render(request, template, {"refine_search":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True, "number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes,'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':error})
                elif error == 'There were no changes in your CSV file compared to your db. No update was done.':
                    return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':error})
                else:
                    errormsg = 'The following samples are already in the db or are in double in your file: ' + error
                    return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True, "number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
            elif type(error) is list:
                if type(error[0]) is list:
                    sdlots_not_in_database = ','.join(error[0])
                    samples_concerned = ','.join(error[1])
                    errormsg = 'The file you try to upload relies on seedlots which aren\'t in your db: ' + sdlots_not_in_database \
                    + '. The samples concerned are: ' + samples_concerned + '.'
                    return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                else:
                    lacking_samples = error[0]
                    del error[0]
                    if error != []:
                        lacking_samples = lacking_samples + ' ,'.join(error)
                    errormsg = 'Some samples you\'re trying to update aren\'t in the db: ' + lacking_samples
                    return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True, "number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':samples,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
            elif type(error) is type(None):
                inserted = True
                return render(request, template, {"refine_search":True,"admin":True,'form':SampleForm(), 'formfile':register_formFile,'creation_mode':True, "number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes,'all':samples,'fields_label':labels,'fields_name':names, 'inserted':inserted})

        
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ]) 
            return generic_management(Sample, SampleForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,'errormsg':errormsg, "number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes,'formfile':register_formFile}, "Sample Management")       
    else:
        samples, number_all, nb_per_page = _get_samples(request)   
        return generic_management(Sample, SampleForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,'formfile':register_formFile,"list_attributes":list_attributes, "number_all":number_all,
                           "nb_per_page":nb_per_page,}, "Sample Management")

def uploadSampleFile(files, request):
    if files != None:
        try:
            reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
        except:
            try:
                reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
            except:
                reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])
        database = Sample.objects.all().values_list('name') #tuple de tuple
        # list de ts les samples ds la bdd
        samples_in_database = [i[0] for i in database]

        sdlots_csv = [] # ls de ts les sdlots dont dépendent les samples
        samples_inserted = [] # ls de ts les samples inseres

        for row in reader:
            sdlots_csv.append(row['Seedlot'])
            samples_inserted.append(row['Name'])

        missing_sdlot, samples_concerned = [], []
        sdlots_in_database = []
        sdlots_not_in_database = []
        sdlots = Seedlot.objects.all().values_list('name')
        sdlots_in_database = [i[0] for i in sdlots] 
        
        sdlots_not_in_database = [x for x in sdlots_csv if x not in sdlots_in_database]
        if sdlots_not_in_database != [''] and sdlots_not_in_database != []:
            missing_sdlot = sdlots_not_in_database
            for l in range(len(sdlots_not_in_database)):
                for k in range(len(sdlots_csv)):
                    if sdlots_csv[k] == sdlots_not_in_database[l]:
                        try:
                            samples_concerned.append(samples_inserted[k])
                        except IndexError:
                            pass
            errors = [sdlots_not_in_database, samples_concerned]
            return errors
        

        if request.POST.get('submit'):
            samples_doublon = ''
            for i in range(len(samples_inserted)):
                for j in range(len(samples_in_database)):
                    if samples_inserted[i] == samples_in_database[j]:
                        samples_doublon = '  '.join([samples_doublon, samples_inserted[i]])
            if set(samples_in_database) == set(samples_inserted):
                return 'All the samples you\'re trying to insert are already in the db. Update?'
            elif samples_doublon != '' and set(samples_in_database) != set(samples_inserted):
                return samples_doublon
            else:
                files.seek(0)
                try:
                    reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
                except:
                    try:
                        reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
                    except:
                        reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])
                for row in reader:
                    newsample = Sample()
                    newsample.name = row['Name']
                    newsample.description = row['Description']
                    if row['Is Obsolete'] == 'False':
                        newsample.is_obsolete = False
                    elif row['Is Obsolete'] == 'True':
                        newsample.is_obsolete = True
                    else:
                        newsample.is_obsolete = False
                    seedlot_name = row['Seedlot']
                    seedlot = Seedlot.objects.get(name=seedlot_name)
                    newsample.seedlot = seedlot
                    try:
                        newsample.save()
                    except IntegrityError:
                        return newsample.name
 

        elif request.POST.get('update'):
            common_samples = [x for x in samples_in_database if x in samples_inserted]
            lacking_samples = [x for x in samples_inserted if x not in samples_in_database]
            if common_samples == []:
                return 'Your file share no samples with the database. Please make sure you weren\'t trying to insert new samples'
            elif lacking_samples != []:
                return lacking_samples
            else:
                files.seek(0)
                try:
                    reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
                except:
                    try:
                        reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
                    except:
                        reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])
                headers = reader.fieldnames
                fields_names = []
                for field_csv in headers:
                    field_name = field_csv.lower()
                    field_name = field_name.replace(" ","_")
                    fields_names.append(field_name)
                while fields_names[-1] != 'description':
                    del fields_names[-1]
                for row in reader:
                    sample_to_update = Sample.objects.get(name=row['Name'])
                    samples_updated = []
                    fields_values = [getattr(sample_to_update, field_to_update) for field_to_update in fields_names]
                    for field_csv, field, l in zip(headers, fields_names, range(len(fields_values))):
                        if field_csv == 'Seedlot':
                            seedlot_name = row['Seedlot']
                            seedlot_instance = Seedlot.objects.get(name=seedlot_name)
                            if sample_to_update.seedlot != seedlot_instance:
                                sample_to_update.seedlot = seedlot_instance
                                samples_updated.append([sample_to_update, seedlot_name])
                                sample_to_update.save()
                        else:
                            field_type = Sample._meta.get_field(field).get_internal_type()
                            new_value = row[field_csv]
                            if field_type == 'BooleanField' and (new_value == 'False' or new_value == ''):
                                if fields_values[l] == True:
                                    setattr(sample_to_update, field, False)
                                    samples_updated.append([sample_to_update, False])
                                    sample_to_update.save()
                            elif field_type == 'BooleanField' and new_value == 'True':
                                if fields_values[l] == False:
                                    setattr(sample_to_update, field, True)
                                    samples_updated.append([sample_to_update, True])
                                    sample_to_update.save()
                            else:
                                if new_value != fields_values[l]:
                                    setattr(sample_to_update, field, new_value)
                                    samples_updated.append([sample_to_update, new_value])

                if samples_updated == []:
                    return 'There were no changes in your CSV file compared to your db. No update was done.'             

#==========================GENOTYPING ID=============================
def _get_genotypingid(request):
    genotypingids = GenotypingID.objects.all()
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(locuss)
    except:
        nb_per_page=50
        
    paginator = Paginator(genotypingids, nb_per_page)
    page = request.GET.get('page')
    try:
        genotypingids = paginator.page(page)
    except PageNotAnInteger:
        genotypingids = paginator.page(1)
    except EmptyPage:
        genotypingids = paginator.page(paginator.num_pages)
    number_all = (paginator.num_pages-1)*int(nb_per_page) + len(paginator.page(paginator.num_pages))
    return genotypingids, number_all, nb_per_page

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genotypingid_management(request):
    inserted = False
    register_formFile = UploadFileFormGenotypingID()
    form = GenotypingIDForm()
    names, labels = get_fields(form)
    

    template = 'genotyping/genotyping_generic.html'
    title = 'GenotypingID Management'

    list_attributes = ['Name','Sample','Experiment','Referential','Sentrixbarcode a','Sentrixposition a','Funding','Projects']
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label': labels,
                  "admin":True,
                  "refine_search":True,
                  "list_attributes":list_attributes,
                  }
    if request.method == 'GET':
        
        if 'or_and' in request.GET and 'no_search' not in request.GET:
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            length_dico_get = len(dico_get)
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]}
            
            if or_and == "or":       
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "Projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            data_type = data_type[0].lower()+data_type[1:]
                            var_search = str(data_type + "__icontains" ) 
                            if data_type == "sample" or data_type == "experiment" or data_type == "referential":
                                var_search = str(data_type+"__in")
                                if data_type == "sample":
                                    data_filter = Sample.objects.filter(name__icontains=value)
                                elif data_type == "experiment":
                                    data_filter = Experiment.objects.filter(name__icontains=value)
                                else:
                                    data_filter = Referential.objects.filter(name__icontains=value)
                                data_id_list = []
                                for i in data_filter:
                                    data_id_list.append(i.id)
                                search_value = data_id_list
                            query |= Query_dj(**{var_search : search_value})
            elif or_and == "and":       
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "Projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            data_type = data_type[0].lower()+data_type[1:]
                            var_search = str(data_type + "__icontains" ) 
                            if data_type == "sample" or data_type == "experiment" or data_type == "referential":
                                var_search = str(data_type+"__in")
                                if data_type == "sample":
                                    data_filter = Sample.objects.filter(name__icontains=value)
                                elif data_type == "experiment":
                                    data_filter = Experiment.objects.filter(name__icontains=value)
                                else:
                                    data_filter = Referential.objects.filter(name__icontains=value)
                                data_id_list = []
                                for i in data_filter:
                                    data_id_list.append(i.id)
                                search_value = data_id_list
                            query &= Query_dj(**{var_search : search_value})
            genotypingids = GenotypingID.objects.filter(query).distinct()
            number_all = len(genotypingids)
            nb_per_page = 10
            tag_fields.update({'search_result':dico_get})
        else:
            genotypingids, number_all, nb_per_page = _get_genotypingid(request)
        tag_fields.update({'all':genotypingids,
                           'formfile':register_formFile,
                           'form':form,
                           'creation_mode':True,
                           "number_all":number_all,
                           "nb_per_page":nb_per_page,
                           'excel_empty_file':True,
                           'filetype':'genotypingid',
                           'typeid':0,})
        return render(request, template, tag_fields)

    elif request.method == 'POST':
        formf = UploadFileFormGenotypingID(request.POST, request.FILES)
        genotypingids, number_all, nb_per_page = _get_genotypingid(request)
        if formf.is_valid():
            files = request.FILES['file']
            error = UploadGenotypingIDFile(files,request)
            if type(error) is str:
                if error == 'All the genotyping ids you\'re trying to insert are already in the db. Update?':
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
                elif error == 'Your file shares no genotyping ids w/ the db. Please make sur you weren\'t trying to insert new genotyping ids':
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
                elif error == 'There were no changes in your CSV file compared to your db. No update was done.':
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
                elif error == 'The format of the csv file is not right.':
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
                else:
                    errormsg = 'The following genotyping ids are already in the db or in double in your file: ' + error
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
            elif type(error) is list:
                if type(error[0]) is list:
                    if len(error) == 3:
                        # missing sample
                        if error[2] == 1:
                            samples_not_in_database = ','.join(error[0])
                            genotypingids_concerned = ','.join(error[1])
                            errormsg = 'The file you try to upload relies on samples which are not in your db: ' + samples_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        # missing exp
                        elif error[2] == 2:
                            exps_not_in_database = ','.join(error[0])
                            genotypingids_concerned = ','.join(error[1])
                            errormsg = 'The file you try to upload relies on experiments which are not in your db: ' + exps_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page, "list_attributes":list_attributes,'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        # missing ref
                        elif error[2] == 3:
                            refs_not_in_database = ','.join(error[0])
                            genotypingids_concerned = ','.join(error[1])
                            errormsg = 'The file you try to upload relies on referentials which are not in your db: ' + refs_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                    elif len(error) == 4:
                        # missing sample and exp
                        if error[3] == 4:
                            samples_not_in_database = ','.join(error[0])
                            exps_not_in_database = ','.join(error[1])
                            genotypingids_concerned = ','.join(error[2])
                            errormsg = 'The file you try to upload relies on samples which are not in your db: ' + samples_not_in_database\
                            + ' and on experiments which are not in your db: ' + exps_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        # missing sample and ref
                        elif error[3] == 5:
                            samples_not_in_database = ','.join(error[0])
                            refs_not_in_database = ','.join(error[1])
                            genotypingids_concerned = ','.join(error[2])
                            errormsg = 'The file you try to upload relies on samples which are not in your db: ' + samples_not_in_database\
                            + ' and on referentials which are not in your db: ' + refs_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        elif error[3] == 6:
                            refs_not_in_database = ','.join(error[1])
                            exps_not_in_database = ','.join(error[0])
                            genotypingids_concerned = ','.join(error[2])
                            errormsg = 'The file you try to upload relies on referentials which are not in your db: ' + refs_not_in_database\
                            + ' and on experiments which are not in your db: ' + exps_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        else:
                            exps_not_in_database = ','.join(error[0])
                            refs_not_in_database = ','.join(error[1])
                            samples_not_in_database = ','.join(error[2])
                            genotypingids_concerned = ','.join(error[3])
                            errormsg = 'The file you try to upload relies on referentials which are not in your db: ' + refs_not_in_database\
                            + ' and on experiments which are not in your db: ' + exps_not_in_database\
                            + ' and on samples which are not in your db: ' + samples_not_in_database\
                            +'. The genotyping ids concerned are: ' + genotypingids_concerned + '.'
                            return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':errormsg})

                else:
                    lacking_genotypingids = error[0]
                    del error[0]
                    if error != []:
                        lacking_genotypingids = lacking_genotypingids + ' ,'.join(error)
                    errormsg = 'Some genotyping ids you\'re trying to update are not in the db: ' + lacking_genotypingids
                    return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'errormsg':error})
            elif type(error) is type(None):
                inserted = True
                return render(request, template, {"refine_search":True,"admin":True,'form':GenotypingIDForm(), 'formfile':register_formFile,'creation_mode':True,"number_all":number_all,
                           "nb_per_page":nb_per_page,"list_attributes":list_attributes, 'all':genotypingids,'fields_label':labels,'fields_name':names, 'inserted':inserted})

        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
            return generic_management(GenotypingID, GenotypingIDForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"admin":True,"list_attributes":list_attributes,'errormsg':errormsg, 'formfile':register_formFile}, "GenotypingID Management")       

    else:
        genotypingids, number_all, nb_per_page = _get_genotypingid(request)
        return generic_management(GenotypingID, GenotypingIDForm, request, 'genotyping/genotyping_generic.html',{"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile}, "GenotypingID Management")       

def UploadGenotypingIDFile(files, request):
    if files != None:
        try:
            reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
        except:
            try:
                reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
            except:
                try:
                    reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])
                finally:
                    return 'The format of the csv file is not right.'
        database = GenotypingID.objects.all().values_list('name')
        genotypingids_in_database = [i[0] for i in database]

        samples_csv = [] # ls de ts les samples dont dependent les genotyping id
        exps_csv = []
        refs_csv = []
        genotypingids_inserted = []
        #headers = reader.fieldnames
        #print(headers)
        for row in reader:
            samples_csv.append(row['Sample'])
            genotypingids_inserted.append(row['Name'])
            exps_csv.append(row['Experiment'])
            refs_csv.append(row['Referential'])
            
        missing_sample, genotypingids_concerned = [], []
        samples_in_database = []
        samples_not_in_database = []
        samples = Sample.objects.all().values_list('name')
        samples_in_database = [i[0] for i in samples]

        missing_exps, missing_refs, genotypingids_concerned = [], [], []

        exps_in_database = []
        refs_in_database = []

        exps_not_in_database = []
        refs_not_in_database = []

        exps = Experiment.objects.all().values_list('name')
        refs = Referential.objects.all().values_list('name')

        refs_in_database = [i[0] for i in refs]
        exps_in_database = [i[0] for i in exps]

        exps_not_in_database = [x for x in exps_csv if x not in exps_in_database]
        refs_not_in_database = [x for x in refs_csv if x not in refs_in_database]

        samples_not_in_database = [x for x in samples_csv if x not in samples_in_database]

        #===========MISSING SAMPLE===============

        if samples_not_in_database != [] and refs_not_in_database == [] and exps_not_in_database == []:
            missing_sample = samples_not_in_database
            for l in range(len(samples_not_in_database)):
                for k in range(len(samples_csv)):
                    if samples_csv[k] == samples_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [samples_not_in_database, genotypingids_concerned, 1]
            return errors

        #===========MISSING EXP===============

        elif exps_not_in_database != [] and refs_not_in_database == [] and samples_not_in_database == []:
            missing_exps = exps_not_in_database
            for l in range(len(exps_not_in_database)):
                for k in range(len(exps_csv)):
                    if exps_csv[k] == exps_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [exps_not_in_database, genotypingids_concerned, 2]
            return errors

        #===========MISSING REF===============

        elif refs_not_in_database != [] and exps_not_in_database == [] and samples_not_in_database == []:
            missing_refs = refs_not_in_database
            for l in range(len(refs_not_in_database)):
                for k in range(len(refs_csv)):
                    if refs_csv[k] == refs_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [refs_not_in_database, genotypingids_concerned, 3]
            return errors

        #===========MISSING SAMPLES AND EXP===============

        elif samples_not_in_database != [] and exps_not_in_database != [] and refs_not_in_database == []:
            missing_sample = samples_not_in_database
            missing_exps = exps_not_in_database
            for l in range(len(samples_not_in_database)):
                for k in range(len(samples_csv)):
                    if samples_csv[k] == samples_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass

            for l in range(len(exps_not_in_database)):
                for k in range(len(exps_csv)):
                    if exps_csv[k] == exps_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [samples_not_in_database, exps_not_in_database, genotypingids_concerned, 4]
            return errors


        #===========MISSING SAMPLES AND REF===============

        elif samples_not_in_database != [] and refs_not_in_database != [] and exps_not_in_database == []:
            missing_sample = samples_not_in_database
            missing_refs = refs_not_in_database
            for l in range(len(samples_not_in_database)):
                for k in range(len(samples_csv)):
                    if samples_csv[k] == samples_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass

            for l in range(len(refs_not_in_database)):
                for k in range(len(refs_csv)):
                    if refs_csv[k] == refs_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [samples_not_in_database, refs_not_in_database, genotypingids_concerned, 5]
            return errors

        #===========MISSING EXPS AND REF===============

        elif exps_not_in_database != [] and refs_not_in_database != [] and samples_not_in_database == []:
            missing_exps = exps_not_in_database
            for l in range(len(exps_not_in_database)):
                for k in range(len(exps_csv)):
                    if exps_csv[k] == exps_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            for l in range(len(refs_not_in_database)):
                for k in range(len(refs_csv)):
                    if refs_csv[k] == refs_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            errors = [exps_not_in_database, refs_not_in_database, genotypingids_concerned, 6]
            return errors

        #===========MISSING EXP AND REF AND SAMPLE===============
        
        elif exps_not_in_database != [] and refs_not_in_database != [] and samples_not_in_database != []:
            missing_exps = exps_not_in_database
            missing_refs = refs_not_in_database
            for l in range(len(exps_not_in_database)):
                for k in range(len(exps_csv)):
                    if exps_csv[k] == exps_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass

            for l in range(len(refs_not_in_database)):
                for k in range(len(refs_csv)):
                    if refs_csv[k] == refs_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass

            for l in range(len(samples_not_in_database)):
                for k in range(len(samples_csv)):
                    if samples_csv[k] == samples_not_in_database[l]:
                        try:
                            genotypingids_concerned.append(genotypingids_inserted[k])
                        except IndexError:
                            pass
            
            errors = [exps_not_in_database, refs_not_in_database, samples_not_in_database, genotypingids_concerned]
            return errors


        if request.POST.get('submit'):
            genotypingids_doublon = ''
            for i in range(len(genotypingids_inserted)):
                for j in range(len(genotypingids_in_database)):
                    if genotypingids_inserted[i] == genotypingids_in_database:
                        genotypingids_doublon = '  '.join([genotypingids_doublon, genotypingids_inserted[i]])
            if set(genotypingids_in_database) == set(genotypingids_inserted):
                return 'All the genotyping ids you\'re trying to insert are already in the db. Update?'
            elif genotypingids_doublon != '' and set(genotypingids_in_database) != set(genotypingids_inserted):
                return genotypingids_doublon
            else:
                files.seek(0)
                try:
                    reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
                except:
                    try:
                        reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
                    except:
                        reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])

                for row in reader:
                    newgenotypingid = GenotypingID()
                    newgenotypingid.name = row['Name']
                    sample_name = row['Sample']
                    newgenotypingid.sample = Sample.objects.get(name=sample_name)
                    experiment_name = row['Experiment']
                    newgenotypingid.experiment = Experiment.objects.get(name=experiment_name)
                    referential_name = row['Referential']
                    newgenotypingid.referential = Referential.objects.get(name=referential_name)
                    sentrixbarcode = row['SentrixBarcode_A']
                    newgenotypingid.sentrixbarcode_a = sentrixbarcode
                    sentrixposition = row['SentrixPosition_A']
                    newgenotypingid.sentrixposition_a = sentrixposition
                    funding = row['Funding']
                    newgenotypingid.funding = funding
                    try:
                        with transaction.atomic():
                            newgenotypingid.save()
                    except IntegrityError:
                        return 'All the genotyping ids you\'re trying to insert are already in the db. Update?'

        elif request.POST.get('update'):
            common_genotypingids = [x for x in genotypingids_in_database if x in genotypingids_inserted]
            lacking_genotypingids = [x for x in genotypingids_inserted if x not in genotypingids_in_database]
            if common_genotypingids == []:
                return 'Your file shares no genotyping ids w/ the db. Please make sur you weren\'t trying to insert new genotyping ids'
            elif lacking_genotypingids != []:
                return lacking_genotypingids
            else:
                headers = reader.fieldnames
                fields_names = []
                for field_csv in headers:
                    field_name = field_csv.lower()
                    field_name = field_name.replace(" ","_")
                    fields_names.append(field_name)
                files.seek(0) #on remet files à 0 pour lire à nouveau le fichier
                try:
                    reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
                except:
                    try:
                        reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
                    except:
                        reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter']) 
                for row in reader:
                    ref_id = Referential.objects.get(name=row['Referential']).id
                    genotypingid_to_update = GenotypingID.objects.get(name=row['Name'], referential=ref_id)
                    genotypingids_updated = []
                    fields_values = [getattr(genotypingid_to_update, field_to_update) for field_to_update in fields_names]
                    for field_csv, field, l in zip(headers, fields_names, range(len(fields_values))):
                        if field_csv == 'Sample':
                            sample_name = row['Sample']
                            sample_instance = Sample.objects.get(name=sample_name)
                            if genotypingid_to_update.sample != sample_instance:
                                genotypingid_to_update.sample = sample_instance
                                genotypingids_updated.append([genotypingid_to_update, sample_name])
                                genotypingid_to_update.save()
                        if field_csv == 'Referential':
                            referential_name = row['Referential']
                            referential_instance = Referential.objects.get(name=referential_name)
                            if genotypingid_to_update.referential != referential_instance:
                                genotypingid_to_update.referential = referential_instance
                                genotypingids_updated.append([genotypingid_to_update, referential_name])
                                genotypingid_to_update.save()
                        elif field_csv == 'Experiment':
                            experiment_name = row['Experiment']
                            experiment_instance = Experiment.objects.get(name=experiment_name)
                            if genotypingid_to_update.experiment != experiment_instance:
                                genotypingid_to_update.experiment = experiment_instance
                                genotypingids_updated.append([genotypingid_to_update, experiment_name])
                                genotypingid_to_update.save()                                
                        else:
                            field_type = GenotypingID._meta.get_field(field).get_internal_type()
                            new_value = row[field_csv]
                            if field_type == 'BooleanField' and (new_value == 'False' or new_value == ''):
                                if fields_values[l] == True:
                                    setattr(genotypingid_to_update, field, False)
                                    genotypingids_updated.append([genotypingid_to_update, False])
                                    genotypingid_to_update.save()
                            elif field_type == 'BooleanField' and new_value == 'True':
                                if fields_values[l] == False:
                                    setattr(genotypingid_to_update, field, True)
                                    genotypingids_updated.append([genotypingid_to_update, True])
                                    genotypingid_to_update.save()
                            else:
                                if new_value != fields_values[l] and new_value != '':
                                    setattr(genotypingid_to_update, field, Sample.objects.get(name=new_value))
                                    genotypingids_updated.append([genotypingid_to_update, new_value])
                if genotypingids_updated == []:
                    return 'There were no changes in your CSV file compared to your db. No update was done.'

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def referential_management(request):
    number_all = len(Referential.objects.all())
    list_attributes = ['Name','Person','Date','Comments','Projects']
    adds= {"refine_search":True, "list_attributes":list_attributes, "number_all":number_all,"admin":True,"noheader":True,'excel_empty_file':True,'filetype':'referential','typeid':0}
    if 'or_and' in request.GET and 'no_search' not in request.GET:
        dico_get={}
        or_and = None
        or_and=request.GET['or_and']
        length_dico_get = len(dico_get)
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]}
        
        if or_and == "or":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query |= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person":
                            var_search = str(data_type+"__in")
                            person_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            person_id_list = []
                            for i in person_filter:
                                person_id_list.append(i.id)
                            search_value = person_id_list
                        query |= Query_dj(**{var_search : search_value})
        elif or_and == "and":       
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "Projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['Projects']))
                    query &= Query_dj(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person":
                            var_search = str(data_type+"__in")
                            person_filter = Person.objects.filter(Query_dj(first_name__icontains=value)|Query_dj(last_name__icontains=value))
                            person_id_list = []
                            for i in person_filter:
                                person_id_list.append(i.id)
                            search_value = person_id_list
                        query &= Query_dj(**{var_search : search_value})
        referentials = Referential.objects.filter(query).distinct()
        number_all = len(referentials)
        nb_per_page = 10
        adds.update({"query":query,"number_all":number_all,"all":referentials,'search_result':dico_get,})
    return generic_management(Referential, ReferentialForm, request, 'genotyping/genotyping_generic.html', adds, "Referential Management")

#================================================   

#================================================

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def genotypingvalue_management(request):
    
    if request.method == 'POST':
        form = UploadFileForm_Matrix(request.POST,request.FILES)
        ref_and_exp = ChoiceRefExpForm(request.POST)
        
        if form.is_valid():
            file = request.FILES['file']
            delimiter = form.cleaned_data['delimiter']
            matrix_orientation = form.cleaned_data['matrix_orientation']

            if ref_and_exp.is_valid():
                ref = ref_and_exp.cleaned_data['ref']
# <<<<<<< .working
# #                 if "50k" in ref.name:
# #                     fichier = format_file(file)
# #                     if fichier != None and fichier != []:
# #                         msg_error = export_to_json(ref.name, fichier, delimiter, ref.id)
# #                     else:
# #                         msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
# #                     if msg_error != None and msg_error != []:
# #                         return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter,"msg_error":msg_error})     
# #         
#                 if 'GBS' in ref.name:
#                     list_file = split_file_GBS(file, delimiter)
#                     print(list_file)
#                     if list_file != None and list_file != []:
#                         msg_error =  export_to_json(ref.name, list_file, delimiter, ref.id, matrix_orientation)
#                     else:
#                         msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
# #                     print(msg_error)
#                     if msg_error != None and msg_error != []:
#                         return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter,"msg_error":msg_error})     
#         
#                 elif '50k' in ref.name or '600k' in ref.name or '50K' in ref.name or '600K' in ref.name:
# =======
#                 if "50k" in ref.name:
#                     fichier = format_file(file)
#                     export_to_json(ref.name, fichier, delimiter, ref.id, matrix_orientation)
#                 elif ref.name == 'GBS_V8':
#                 elif 'GBS' in ref.name :
                    #list_file = split_file_GBS(file, delimiter)
#                     if list_file != None and list_file != []:
#                         msg_error =  export_to_json(ref.name, list_file, delimiter, ref.id, matrix_orientation)
#                     else:
#                         msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
                    #msg_error =  export_to_json(ref.name, [file], delimiter, ref.id, matrix_orientation)
                if 'GBS' in ref.name :
                    msg_error = process_gbs(file,delimiter,ref.id)
                    if msg_error != None and msg_error != []:
                        return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter,"msg_error":msg_error})     
        
                elif '600k' in ref.name or '50k' in ref.name:
# >>>>>>> .merge-right.r614
                    fichier = request.FILES['file']
                    output = open(settings.HEAVYFILES_URL + "matrix.csv","w")
                    try:
                        input = fichier.read().decode('utf-8').split('\n')
                    except:
                        input = fichier.read().split('\n')
                    headers = input[0]
                    headers_split = headers.split(delimiter)
                    
#                     print(len(input[1].split(delimiter)))
                    if headers_split[-1]=="":
#                         print('oui')
                        headers = headers[:-1]
                    if len(headers.split(delimiter))+1 == len(input[1].split(delimiter)):
                        headers = "genoid_name"+delimiter+headers
                    output.write(headers+"\n")
                    for i in range(1, len(input)):
                        output.write(input[i]+'\n')
                    output.close()
                    if output != None and output != []:
                        file = settings.HEAVYFILES_URL + "matrix.csv"
                        msg_error = export_to_json(ref.name, file, delimiter, ref.id, matrix_orientation)
                    else:
                        msg_error = "Some errors were detected. The format of your file is not right. Please check that you're trying to upload the right file."
                    if msg_error != None and msg_error != []:
                        return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter,"msg_error":msg_error})     
        
                else:
                    msg_error = "The chosen referential doesn't contain the name of a known referential: '50k','600k', or 'GBS', please contact an administrator."
                    return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter,"msg_error":msg_error})     
        
                return render(request,'genotyping/genotyping_values_success.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter})
            else:
                msg_error = "The form containing the referential is not valid."
                return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp,"delimiter":delimiter, "msg_error":msg_error})
        else: 
            msg_error = "The form containing the file is not valid."
            return render(request, 'genotyping/genotyping_values_fail.html',{"file":file,"admin":True,'form':form,"ref_and_exp":ref_and_exp, "msg_error":msg_error})        
    else:
        form = UploadFileForm_Matrix()
        ref_and_exp = ChoiceRefExpForm()
    return render(request,'genotyping/genotyping_values.html',{"admin":True,'form':form,"ref_and_exp":ref_and_exp})
        
def format_file(file):
    #quand première ligne non alignée
    inter = settings.HEAVYFILES_URL + 'output.txt'
    formatted_file = open(inter,'w')
    f = []
    for line in file:
        try:
            line=line.decode('utf-8')
        except:
            pass
        if '"' in line:
            line = line.replace('"','')
        f.append(line)

    if not f[0].startswith("\t"):
        f[0] = "\t" + f[0]
    formatted_file.write(f[0])
    
    for el in f[1:]:
        formatted_file.write(el)
    
    return inter

# def split_file_GBS(file, delimiter):
#     list_file = []
#     
#     splitLen = 500         # 50 lines per file
#     outputBase = settings.HEAVYFILES_URL + 'GBS' # GBS1.txt, GBS2.txt, etc.
#     
#     # This is shorthand and not friendly with memory
#     # on very large files (Sean Cavanagh), but it works.
#     try:
#         try:
#             input = file.read().decode('utf-8').split('\n')
#         except:
#             input = file.read().split('\n')
#     except TypeError:
#         return list_file
#     header = input[0]
#     print("Header = \n")
#     print(header)
#     header = header.split(delimiter)
# #     print(header)
#     for h in header:
#         if h == '':
#             header.remove(h)
#     at = 1
#     for lines in range(1, len(input), splitLen):
#         # First, get the list slice
#         outputData = input[lines:lines+splitLen]
#         #if len(header[0].split(';')) == len(outputData[0].split(';'))-1:
#         
#         if len(header) == len(outputData[0].split(';'))-1:
#             #header[0] = "genoid_name;" + header[0]
#             header = ["genoid_name"] + header
#         #header = [header[0]]
#         # Now open the output file, join the new slice with newlines
#         # and write it out. Then close the file.
#         output = open(outputBase + str(at) + '.txt', 'w')
#         list_file.append(outputBase + str(at) + '.txt')
#         #output.write(header+'\n')
#         output.write(';'.join(header) + "\n")
#         output.write('\n'.join(outputData))
# #         print('\n'.join(header + outputData))
#         output.close()
#     
#         # Increment the counter
#         at += 1
#     return list_file

# def split_file_GBS(file, delimiter):
#     list_file = []
#     
#     splitLen = 50         # 50 lines per file
#     outputBase = settings.HEAVYFILES_URL + 'GBS' # GBS1.txt, GBS2.txt, etc.
#     
#     try:
#         try:
#             input = file.read().decode('utf-8').split('\n')
#         except:
#             input = file.read().split('\n')
#     except TypeError:
#         return list_file
#     input = [i.split(';') for i in input]
#     header = input[0]
#     print("input00", input[0][0])
#     print("input01", input[0][1])
#     print("input10", input[1][0])
#     print("Header = \n")
#     print(header)
#     #header = header.split(delimiter)
# #     print(header)
#     for h in header:
#         if h == '':
#             header.remove(h)
#     at = 1
#     
#     genoid_names = input[0]
#     print("voici les genoid : ", genoid_names)
#     
#     for cols in range(1, len(input[0]), splitLen):
#         
#         outputData = input[1:][cols:cols+splitLen]
#         outputData = [';'.join(i) for i in outputData]
#         headerData = header[cols:cols+splitLen]
# 
#         output = open(outputBase + str(at) + '.txt', 'w')
#         list_file.append(outputBase + str(at) + '.txt')
#         output.write("genoid_name;" + ';'.join(headerData) + "\n")
#         output.write('\n'.join(outputData))
#         
#         output.close()
#         
#         # Increment the counter
#         at += 1
#     return list_file



def split_file_GBS(file, delimiter):
    list_file = []
    
    splitLen = 100         # 50 lines per file
    outputBase = settings.HEAVYFILES_URL + 'GBS' # GBS1.txt, GBS2.txt, etc.
    # J'ouvre le fichier pour recuperer le header
    try:
        input = file.read().decode('utf-8').split('\n')
    except:
        input = file.read().split('\n')
    headers = input[0]
    headers_split = headers.split(delimiter)
    # J'ouvre le fichier sans la premiere ligne de header
    try:
        input = np.genfromtxt(file, delimiter=delimiter, dtype=np.str, skip_header=1)
        #input = pd.read_csv(file, sep=delimiter, dtype=np.str,header=None)
        #input = np.genfromtxt(("\t".join(i) for i in csv.reader(open(file, newline=''))), delimiter=delimiter)
    except ValueError as ve:
        print(ve)
    #Si un loci est vide on l'enleve
    for h in headers_split:
        if h == '':
            headers_split.remove(h)
            
    print("len header", len(headers_split))
    print("len input", len(input[0]))
    if len(headers_split) == len(input[0])-1:
        headers_split.insert(0,"genoid_names")
    
    #print("header = ",headers_split)
    #print("input = ", input)
    at = 1
    #On parcours les colonnes avec le pas donne plus haut
    for cols in range(1, len(input[0]), splitLen):
        output_data = input[:,cols:cols+splitLen]
        header_data = headers_split[cols:cols+splitLen]
        #on recupere les noms des genoids
        genoid = input[:,0]
        #et le nom du premier champs des loci
        loci = headers_split[0]
        #print("loci = ", loci)
        # on concatene le nom des genoids avec les colonnes de donnees
        output_data = np.concatenate((np.reshape(genoid,(len(genoid),1)),output_data),axis=1)
        # et on ajoute genoid_names en face des header aussi pour avoir
        # le meme nombre de colonnes
        header_data.insert(0,loci)
        
        #print("header data", header_data)
        #print("output_data", output_data)
        #print("genoid = ",genoid)
        
        output = open(outputBase + str(at) + '.txt', 'w')
        list_file.append(outputBase + str(at) + '.txt')
        if loci != "genoid_names" :
            output.write("genoid_names;" + ';'.join(header_data) + "\n")
        else :
            output.write(';'.join(header_data) + "\n")
        
        for line in output_data :
            a = 0
            for char in line :
                output.write(char.replace("'", ''))
                if a != len(line)-1 :
                    output.write(';')
                a = a+1
            #line.tofile(output,sep=';')
            output.write("\n")
        #output_data.tofile(output,sep=';')
        #np.savetxt(output, output_data, delimiter=delimiter, fmt='%s', newline='\n')
        #output_data.tofile(output, sep=delimiter, format='%s')
        #output.write('\n'.join(output_data))
        output.close()
        print("on a ecrit dans = ", output)
     
        # Increment the counter
        at += 1
    return list_file

def process_gbs(gbsfile, delimiter, ref_id):
    # Nombre de colonnes par fichier
    # Mettre 1 de moins que le nombre de colonnes souhaite ex: pour 1000 mettre 999 
    # (du a la gestion des intervalles par le cut)
    split_len = 9999
    output_base = settings.HEAVYFILES_URL + 'GBS'
    #file_path = '/home/melanie/Fichiers_Donnees/Fichiers_test/GBS/'+gbsfile.name
    # Chemin du fichier temporaire cree
    file_path = gbsfile.temporary_file_path()
    # A ameliorer pour les petits fichiers ?
    print('Chemin du fichier = ',file_path)
    
    head = subprocess.Popen(['head','-1', file_path], stdout=subprocess.PIPE)
    select = subprocess.Popen(['sed','s/[^;]//g'], stdin=head.stdout, stdout=subprocess.PIPE)
    count = subprocess.Popen(['wc','-c'], stdin=select.stdout, stdout=subprocess.PIPE)
    
    nb_col_header = int(count.communicate()[0])
    print("nombre colonnes header ", nb_col_header)
    
    head2 = subprocess.Popen(['head','-2', file_path], stdout=subprocess.PIPE)
    select2 = subprocess.Popen(['sed','s/[^;]//g'], stdin=head2.stdout, stdout=subprocess.PIPE)
    count2 = subprocess.Popen(['wc','-c'], stdin=select2.stdout, stdout=subprocess.PIPE)
    
    nb_col_line = int(count2.communicate()[0])
    print("nombre colonnes 2 premieres lignes ", nb_col_line)
    
    if nb_col_header != nb_col_line-nb_col_header :
        sed = subprocess.Popen(['sed','-i','1s/^/"genoid_names";/',file_path],stdout=subprocess.PIPE)
        nb_col_header += 1
    nb = 0
    dec = 0
    for cols in range(2, nb_col_header, split_len+1) :
        cut_file = cut_gbs_file(file_path, cols, cols+split_len, output_base)
        result = transform_gbs_in_json(cut_file, delimiter, ref_id, nb)

        if result[0] == None or result[0] == [] :
            import_in_mongo(result[1])
        else :
            return result
        nb += 1
    return result[0]

def cut_gbs_file(file_path, start, end, output_base):
    
    output_name = output_base + '_' + str(start) +'-'+ str(end) + '.txt'
    output = open(output_name, 'w')
    
    cut = subprocess.Popen(['cut','-d',';','-f1,'+str(start)+'-'+str(end),file_path],stdout=output)
    cut_status = cut.communicate()
    output.close()
    
    return output_name

def transform_gbs_in_json(cutfile, delimiter, ref_id, nb):
    
    msg_error = []
    output = settings.HEAVYFILES_URL + 'interGBS_' + str(nb) + '.json'
    res = open(output,'w')
    v=0
    
    print("f: ",cutfile)
    if delimiter == '\\t':
        delimiter = '\t'
            
    try:
        fichier = np.genfromtxt(cutfile, delimiter=delimiter, dtype=np.str)
    except ValueError:
        msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
        return msg_error
    
    #print(fichier)
    genoid_doesnt_exist = []
    locusid_doesnt_exist = []
    #print(fichier[0,0:])
    nb_lo = np.count_nonzero(fichier[0,0:])

    row_loc0 = fichier[0,1:nb_lo]
    row_loc = [s.replace('"','') for s in row_loc0]

    nb_genoid = np.count_nonzero(fichier[1:,0])
    #print("Nombre genoid : ", nb_genoid)
    col_genoid0 = fichier[1:,0]
    col_genoid = [s.replace('"', '') for s in col_genoid0]
    sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid)
    sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
    listgenoid = []

    for genoid in col_genoid:
        try:
            listgenoid.append(sorted_listgenoid[genoid])
        except KeyError:
            print('GenoID ' + genoid + ' does not exist.')
            genoid_doesnt_exist.append(genoid)
            continue
    if v ==1:
        for i in range(len(row_loc)-1):
            try:
                len_locus = len(Locus.objects.filter(name = row_loc[i]))
            except:
                locusid_doesnt_exist.append(row_loc[i])
                continue

    if genoid_doesnt_exist != []:
        msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
        return msg_error
    if locusid_doesnt_exist != []:
        msg_error = "Some errors were detected. The following loci you are trying to updload are not in the database: {0}. No data will be inserted.".format(locusid_doesnt_exist)
        return msg_error    
        v=0

    listsample = []
    listsample_exp = {} #création d'un dictionnaire vide
    print('RECUP FINIE')
            
    for genoid in listgenoid:
        listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
    
        
    for i in range(len(row_loc)):
        j = 0
        if i%1000 == 0:
            print(i)

        variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": ['
        res.write(variable)

        k=0
        for genoid in listgenoid :

            val_geno0 = str(fichier[j+1,i+1])
            val_geno1 = [s.replace('"', '') for s in val_geno0]
            val_geno = ''.join(val_geno1)

            if k < len(listgenoid)-1:
                variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
                variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
                res.write(variable_2 + variable_3)

            elif k == len(listgenoid)-1:
                variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ',' 
                variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
                res.write(variable_2 + variable_3)
            k+=1
            j += 1
        res.write(']}\n')
    os.remove(cutfile)
    res.close()
    
    return [msg_error, output]
    
def import_in_mongo(json_file):
    
    db_name = settings.MONGODB_DATABASES['default']['name']
    appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + json_file + ' --batchSize 1' #ajouter -vvvv pour verbose, pour avoir plus de renseignements mais plus lent et ça plombe les log...
    #print("appel_mongo: ",appel_mongo)
    os.system(appel_mongo)
    os.remove(json_file)



def export_to_json(ref_name, file, deli, ref_id, matrix_orientation):
    client = MongoClient()
    db = client[settings.MONGODB_DATABASES['default']['name']]
    i = 0
    msg_error = []
    if matrix_orientation == "loci_col" and "GBS" not in ref_name:
        if deli == '\\t':
            deli = '\t'
        try:
            fichier = np.genfromtxt(file, delimiter=deli, dtype=np.str)
        except ValueError:
            msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
            return msg_error
        
        output = settings.HEAVYFILES_URL + 'inter.json'
        res = open(output,'w')
        
        nb_lo = np.count_nonzero(fichier[0,1:])
#         print(( "nb lo: "+str(nb_lo)
        row_loc0 = fichier[0,1:nb_lo+1]
        row_loc = [s.replace('"','') for s in row_loc0]

#         print(( "row lo: "+str(row_lo)
        genoid_doesnt_exist = []
    
        ref = Referential.objects.get(name=ref_name)
    
        nb_genoid = np.count_nonzero(fichier[1:,0])
        col_genoid0 = fichier[1:,0]
        col_genoid = [s.replace('"', '') for s in col_genoid0]
        sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid, referential=ref)
        dict_genoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
        #dict_exp = dict([(genoid.name, genoid.experiment) for genoid in sorted_listgenoid])
        
        
        listgenoid = []
        for genoid in col_genoid:
            try:
                listgenoid.append(dict_genoid[genoid])
            except KeyError:
                print('GenoID ' + genoid + ' does not exist')
                genoid_doesnt_exist.append(genoid)
                continue
        if genoid_doesnt_exist != []:
            msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
            return msg_error
        listsample_exp = {} #création d'un dictionnaire vide
        
        print('RECUP FINIE')
        
#         print(listgenoid)
        for genoid in listgenoid:
            listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
#             print('genoid: ',genoid)
#             print('list sample exp: ',listsample_exp)
        
        for i in range(len(row_loc)):
            j = 0
            if i%1000 == 0:
                print(i)
#             print('locus: ',row_loc[i])
            #recuperation de l'id du marqueur grace au nom du marqueur
            variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": [' #on remplace name par l'id
            res.write(variable)
            k=0
            for genoid in listgenoid :
             #on  récupère la clé: le sample_id et la valeur associée: l'expérience dans le dictionnaire précédemment créé
#                 print(j)
                val_geno0 = str(fichier[j+1,i+1])
                val_geno1 = [s.replace('"', '') for s in val_geno0]
                val_geno = ''.join(val_geno1)
#                     print('valgeno: ',val_geno)
#                     print(k, '  len: ',len(listsample_exp)-1)
                if k < len(listgenoid)-1:
                    variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
                    variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
                    res.write(variable_2 + variable_3)
                elif k == len(listgenoid)-1:    
                    variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ',' 
                    variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
                    res.write(variable_2 + variable_3)
                k+=1
                j += 1
#                 print("res:   ",variable,variable_2,variable_3)
            res.write(']}\n')
        # verif que tout a ete parse
        
#         if i != len(row_loc)-1 and j != len(listsample_exp) or i != len(row_loc)-1 or j != len(listsample_exp):
#             print('Erreur')
        res.close()
        db_name = settings.MONGODB_DATABASES['default']['name']
        appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1' #ajouter -vvvv pour verbose, pour avoir plus de renseignements mais plus lent
#         print("appel_mongo: ",appel_mongo)
        os.system(appel_mongo)
        
#         print_mongoimport = subprocess.getoutput(appel_mongo)
#         if 'Failed:' in print_mongoimport:
#             msg_error = "There is an error in the mongo import module, please contact a competent person."
#             appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'
#         
        
        os.remove(output)
        os.remove(file)
        return msg_error
        
    elif matrix_orientation == "genoid_col" and "GBS" not in ref_name:
        output = settings.HEAVYFILES_URL +'inter600.json'
        res = open(output,'w')
        genoid_doesnt_exist = []
        if deli == '\\t':
            deli = '\t'
        try:
            fichier = np.genfromtxt(file, delimiter=deli, dtype=np.str)
            print("ok")
        except ValueError as ve:
            print(ve)
            msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
            return msg_error
        # lecture du fichier
        #nb_genoid = np.count_nonzero(fichier[0,1:])-4 #récupération du nombre de genoID (sans les 4 valeurs non genoID: probesetID, chr, chrpos et affyID)
        nb_genoid = np.count_nonzero(fichier[0,1:])
        row_genoid0 = fichier[0,1:nb_genoid+1]
        row_genoid = [s.replace('"', '') for s in row_genoid0]
    
        nb_lo = np.count_nonzero(fichier[1:,0])
        col_loc0 = fichier[1:,0]
        col_loc = [s.replace('"', '') for s in col_loc0]
        
        ref = Referential.objects.get(name=ref_name)
        
        sorted_listgenoid = GenotypingID.objects.filter(name__in=row_genoid, referential=ref)
        sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
        listgenoid = []
        for genoid in row_genoid:
            try:
                listgenoid.append(sorted_listgenoid[genoid])
            except KeyError:
                genoid_doesnt_exist.append(genoid)
                continue
        if genoid_doesnt_exist != []:
            msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
            return msg_error
        # recup tous les samples dont le nom est present dans le fichier
        listsample_exp = {} #création d'un dictionnaire vide
        for genoid in listgenoid:
            listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante

        # pour chaque locus
        for i in range(len(col_loc)):
            if i%1000 == 0:
                print(i)
            #chr = fichier[i+1,nb_genoid+2]
            #pos = fichier[i+1, nb_genoid+3]
            # pour obtenir les valeurs de genotypage par colonne
            f = fichier[i+1,1:nb_genoid+1]
                    
            #variable = '{"name": "' + str(col_loc[i]) + '", "positions": [{"genomeversion": '\
            #                + str(3) + ', "chromosome": "' + str(chr) + '", "position": '\
            #                + str(pos) + '}], "genotyping": ['
            variable = '{"name": "' + str(col_loc[i]) + '", "genotyping": ['
            res.write(variable)

            j = 0
            # pour chaque sample d'un locus
            for sample, experiment in listsample_exp.items():
                val_geno0 = f[j]   
                val_geno1 = [s.replace('"', '') for s in val_geno0]
                val_geno = ''.join(val_geno1) 
                
                variable_2 = '{"sample_id": ' + str(sample) + ','
                if j < len(listsample_exp)-1:
                    variable_3 = ' "genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(experiment)+\
                                 ', "referential_id":' + str(ref_id) + '}]},'
                    res.write(variable_2+variable_3)
                elif j == len(listsample_exp)-1: #si on arrive à la derniere ligne, il n'y a plus besoin de virgule à la fin
                    variable_3 = ' "genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(experiment)+\
                                 ', "referential_id":' + str(ref_id) + '}]}'
                    res.write(variable_2+variable_3)
                else:
                    print("Else")   
                j += 1
                 
            res.write(']}\n')
            
        res.close()
        
        # verif que tout a ete parse
#         print(i 
#         print(j
#         print(len(col_loc)
#         print(len(listsample)
        if i != len(col_loc)-1 and j != len(listsample_exp) or i != len(col_loc)-1 or j != len(listsample_exp):
            print('Erreur')
        
        
        db_name = settings.MONGODB_DATABASES['default']['name']
        appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1 '
        os.system(appel_mongo)
        
        # On utilise subprocess pour voir les messages d'erreur
        #cmd = ["mongoimport","--collection", "locus", "--db", "thaliadb-trunk", "--file", "/tmp/inter600.json"]
        #r = subprocess.run(cmd, stderr=subprocess.PIPE)
        #print(r.stderr)
        
#         print_mongoimport = subprocess.getoutput(appel_mongo)
#         if 'Failed:' in print_mongoimport:
#             msg_error = "There is an error in the mongo import module, please contact a competent person."
#             appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'
#         
        #os.remove(output)
        os.remove(file)
        return msg_error

    elif "GBS" in ref_name:
        #VERIFICATION pour tous les fichiers que les genoids et les loci existent bien dans la base
        output = settings.HEAVYFILES_URL + 'interGBS.json'
        res = open(output,'w')
        v=0
        for f in file:
            print("f: ",f)
            v=+1
            if deli == '\\t':
                deli = '\t'
            
            try:
                fichier = np.genfromtxt(f, delimiter=deli, dtype=np.str)
#<<<<<<< .working
            except ValueError:
                msg_error = "Some errors were detected. The format of your file is not right. Please check you try to upload the right file."
                return msg_error

            genoid_doesnt_exist = []
            locusid_doesnt_exist = []
            print(fichier[0,0:])
            nb_lo = np.count_nonzero(fichier[0,0:])
            #row_loc0 = fichier[0,0:nb_lo]
            row_loc0 = fichier[0,1:nb_lo]
            row_loc = [s.replace('"','') for s in row_loc0]
#            print('row loc: ',row_loc)
            nb_genoid = np.count_nonzero(fichier[1:,0])
            print("Nombre genoid : ", nb_genoid)
            col_genoid0 = fichier[1:,0]
            col_genoid = [s.replace('"', '') for s in col_genoid0]
            sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid)
            sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
            listgenoid = []
#                 print("col_genoid: ",col_genoid)
            for genoid in col_genoid:
                try:
                    listgenoid.append(sorted_listgenoid[genoid])
                except KeyError:
                    print('GenoID ' + genoid + ' does not exist.')
                    genoid_doesnt_exist.append(genoid)
                    continue
            if v ==1:
                for i in range(len(row_loc)-1):
# =======
#         
#                 nb_lo = np.count_nonzero(fichier[0,1:])
#                 row_loc0 = fichier[0,1:nb_lo+1]
#                 row_loc = [s.replace('"','') for s in row_loc0]
#         
#                 nb_genoid = np.count_nonzero(fichier[1:,0])
#                 col_genoid0 = fichier[1:,0]
#                 col_genoid = [s.replace('"', '') for s in col_genoid0]
#                 sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid)
#                 sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
#         
#                 listgenoid = []
#                 for genoid in col_genoid:
# >>>>>>> .merge-right.r614
                    try:
                        len_locus = len(Locus.objects.filter(name = row_loc[i]))
                    except:
                        locusid_doesnt_exist.append(row_loc[i])
                        continue
#<<<<<<< .working
            if genoid_doesnt_exist != []:
                msg_error = "Some errors were detected. The following genotypingID you are trying to updload are not in the database: {0}. No data will be inserted.".format(genoid_doesnt_exist)
                return msg_error
            if locusid_doesnt_exist != []:
                msg_error = "Some errors were detected. The following loci you are trying to updload are not in the database: {0}. No data will be inserted.".format(locusid_doesnt_exist)
                return msg_error
# =======
#         
#                 listsample = []
#                 listsample_exp = {} #création d'un dictionnaire vide
#                 for genoid in listgenoid:
#                     listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
# >>>>>>> .merge-right.r614
                v=0
#                 print(row_loc)
#<<<<<<< .working
            listsample = []
            listsample_exp = {} #création d'un dictionnaire vide
            print('RECUP FINIE')
            
            for genoid in listgenoid:
                listsample_exp[genoid.sample_id]=genoid.experiment_id #remplissage du dictionnaire: pour chaque sample_id on lui associe l'expérience correspondante
    #             print('genoid: ',genoid)
    #             print('list sample exp: ',listsample_exp)
            
            #for i in range(len(row_loc)):
            for i in range(len(row_loc)):
                j = 0
                if i%1000 == 0:
                    print(i)
    #             print('locus: ',row_loc[i])
                variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": ['
                res.write(variable)
                #print("variable = ", variable)
                k=0
                for genoid in listgenoid :
                 #on  récupère la clé: le sample_id et la valeur associée: l'expérience dans le dictionnaire précédemment créé
    #                 print(j)
                    val_geno0 = str(fichier[j+1,i+1])
                    val_geno1 = [s.replace('"', '') for s in val_geno0]
                    val_geno = ''.join(val_geno1)
    #                     print('valgeno: ',val_geno)
    #                     print(k, '  len: ',len(listsample_exp)-1)
                    #if k < len(listgenoid)-1:
                    if k < len(listgenoid)-1:
                        variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
                        variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
                        res.write(variable_2 + variable_3)
                    #elif k == len(listgenoid)-1: 
                    elif k == len(listgenoid)-1:
                        variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ',' 
                        variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
                        res.write(variable_2 + variable_3)
                    k+=1
                    j += 1
                    #print("variable 2 et 3 = ", variable_2, variable_3)
    #                 print("res:   ",variable,variable_2,variable_3)
                #print("]}")
                res.write(']}\n')
            #os.remove(f)
        res.close()
        db_name = settings.MONGODB_DATABASES['default']['name']
        appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1' #ajouter -vvvv pour verbose, pour avoir plus de renseignements mais plus lent et ça plombe les log...
#         print("appel_mongo: ",appel_mongo)
        os.system(appel_mongo)
        
# =======
#                 for i in range(len(row_loc)):
#                     j = 0
# #                     print(row_loc[i])
#                     variable = '{"name": "' + row_loc[i] + '"", "genotyping": ['
#                     res.write(variable)
#                     k=0
#                     for genoid in listgenoid :
#                      #on  récupère la clé: le sample_id et la valeur associée: l'expérience dans le dictionnaire précédemment créé
#         #                 print(j)
#                         val_geno = fichier[j+1,i+1]
# #                         print('val geno: ',val_geno,"  type: ",type(val_geno))
#         #                     print(k, '  len: ',len(listsample_exp)-1)
#                         if val_geno == "NA":
#                             val_geno = '"'+val_geno+'"'
#                         if k < len(listgenoid)-1:
#                             variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
#                             variable_3 = '"genotyping":[{"genotype": ' + val_geno + ',"experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]},'
#                             res.write(variable_2 + variable_3)
#                         elif k == len(listgenoid)-1:    
#                             variable_2 = '{"sample_id": ' + str(genoid.sample_id) + ','
#                             variable_3 = '"genotyping":[{"genotype": ' + val_geno + ',"experiment_id":' + str(genoid.experiment_id) + ', "referential_id": ' + str(ref_id) + '}]}'
#                             res.write(variable_2 + variable_3)
#                         k+=1
#                         v+=1
#                         j+=1
# >>>>>>> .merge-right.r614
#         print_mongoimport = subprocess.getoutput(appel_mongo)
#         if 'Failed:' in print_mongoimport:
#             msg_error = "There is an error in the mongo import module, please contact a competent person."
#             appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'    
#<<<<<<< .working

        #os.remove(output)
        return msg_error

@login_required
def readFile(line):
    name=settings.MEDIA_ROOT_ANALYSIS+'mongo_insert.log'
    f = open(name, 'r')
    return HttpResponse(f.readlines()[0:])
# =======
#                 
#                 db_name = settings.MONGODB_DATABASES['default']['name']
#                 appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'
#                 os.system(appel_mongo)
# #                 print(os.system(appel_mongo).stdout.read())
#                 print_mongoimport = subprocess.getoutput(appel_mongo)
#                 if 'Failed:' in print_mongoimport:
#                     msg_error = "There is an error in the mongo import module, please contact a competent person."
#                     appel_mongo = 'mongoimport -c locus -d ' + db_name + ' < ' + output + ' --batchSize 1'
#                 
#                 os.remove(output)
#                 os.remove(f)
#                 return msg_error
#             i += 1
# >>>>>>> .merge-right.r614

def deleteGenotypicValues(experimentsids, locisids, samplesids):
    allLocus = Locus.objects.all()
    for locus in allLocus:
        if locus.id in locisids:
            Locus.objects.filter(id=locus.id).delete()
                       
def insertGenoValues(request,tag_fields, template):
    mode = request.POST['select_mode']
    if mode == "1":
        buildDataBaseAlleleMode(request,tag_fields, template)
    else:
        buildDataBaseStandardMode(request, tag_fields, template)       

def buildDataBaseStandardMode(request, tag_fields, template):
    ref = int(request.POST["referential"]) #recupere les id en base postgres
    manip = int(request.POST["experiment"]) 
    main_f = request.FILES['main_file']  
    try:
        reader_mf = csv.DictReader(io.StringIO(main_f.read().decode('utf-8')), delimiter=request.POST['delimiter'])
    except:
        try:
            reader_mf = csv.DictReader(io.StringIO(main_f.read()), delimiter=request.POST['delimiter'])
        except:
            reader_mf = csv.DictReader(io.BytesIO(main_f.read()), delimiter=request.POST['delimiter'])
    
    for rowv in reader_mf:        
        lenkeys = rowv.keys()        
        #Traitement recherche "Sample"
        loc_count =0
        sample =None
        
        for loc in lenkeys:
            loc_count+=1
            locus=None                    
            if loc == None or loc =='':
                samplesearched = rowv.get(loc)     
                try:
                    sample = Sample.objects.get(name=samplesearched)
                except Sample.DoesNotExist:
                    messages.add_message(request, messages.INFO, "Sample named: " + samplesearched+ " not found in Database.")                
            else :
                try:
                    locus = Locus.objects.get(name=str(loc))
                except Locus.DoesNotExist:                   
                    messages.add_message(request, messages.INFO, "Locus named:  " +loc +" not found in Database.")

            if locus != None and sample != None:
                allele = rowv.get(locus.name) 
                if ("m" != allele) and ("[" not in allele) and ("-" != allele) and ("--" != allele) : 
                    #print('allele cas à supprimer: ',allele, 'not "[" in allele ?: ',not "[" in allele,'    ==>allele== -- ?: ',allele=='--','    ==> ellele== - ?: ',allele=='-', '    ==>allele==m ?: ',allele=='m'         
                    #GenotypingValue.objects.create(sample=sample.id,locus=locus,experiment=manip, referential=ref, allele=allele, frenquency=1)
                    Locus.objects.create(genotyping__sample_id=sample.id, genotyping__genotyping__experiment_id=manip, \
                                         genotyping__genotyping__referential_id=ref, genotyping__genotyping__genotype=allele, \
                                         genotyping__genotyping__frequency=frequency)
                    messages.add_message(request, messages.INFO, "la valeur géno du sample name:  " + str(sample.name)+ "  et du locus: " +str(locus.name)+"==> allele = "+allele+"==> frequence = 1 d'office,  a bien été insérée en BD.")

                elif "[" in allele :
                    liste = allele.split('][')
                    for li in liste:
                        length = len(li)
                        coma = li.find(',')
                        if "[" in li:
                            allele = li[1:coma]#li[1:(len(li))-1]
                            if "-" != allele:                              
                                freq = li[coma+1:length]
                                frequency = freq.replace(",", ".")
                                Locus.objects.create(genotyping__sample_id=sample.id, genotyping__genotyping__experiment_id=manip, \
                                        genotyping__genotyping__referential_id=ref, genotyping__genotyping__genotype=allele, \
                                        genotyping__genotyping__frequency=frequency)
                                messages.add_message(request, messages.INFO, "la valeur géno du sample:  " + str(sample.name)+ "  et du locus: " +str(locus.name)+"==> allele = "+allele+"    ==> frequency = "+frequency+ "  a bien été insérée en BD.")
                        elif "]" in li:
                            allele = li[0:coma]
                            if "-" != allele:                                
                                freq = li[coma+1:length-1]
                                frequency = freq.replace(",", ".")
                                Locus.objects.create(genotyping__sample_id=sample.id, genotyping__genotyping__experiment_id=manip, \
                                        genotyping__genotyping__referential_id=ref, genotyping__genotyping__genotype=allele, \
                                        genotyping__genotyping__frequency=frequency)
                                messages.add_message(request, messages.INFO, "la valeur géno du sample:  " + str(sample.name)+ "  et du locus: " +str(locus.name)+"==> allele = "+allele+"    ==> frequency = "+frequency+ "  a bien été insérée en BD.")
                        else:
                            allele = li[0:coma]
                            freq = li[coma+1:length]
                            frequency = freq.replace(",", ".")
                            Locus.objects.create(genotyping__sample_id=sample.id, genotyping__genotyping__experiment_id=manip, \
                                    genotyping__genotyping__referential_id=ref, genotyping__genotyping__genotype=allele, \
                                    genotyping__genotyping__frequency=frequency)
                            messages.add_message(request, messages.INFO, "la valeur géno du sample:  " + str(sample.name)+ "  et du locus: " +str(locus.name)+"==> allele = "+allele+"    ==> frequency = "+frequency+ "  a bien été insérée en BD.")
                                           
def buildDataBaseAlleleMode(request,tag_fields, template):
    
    verif_f = request.FILES['verication_file']
    main_f = request.FILES['main_file']
    ref = int(request.POST["referential"])   
    manip = int(request.POST["experiment"])
    dict_alleles = getTableVerif(verif_f)
    
#     #test
#     for key, value in dict_alleles.items():
#         print('locusname: ',key, '    value: ',value
    #fin test
    
    try:
        reader_mf = csv.DictReader(io.StringIO(main_f.read().decode('utf-8')), delimiter=request.POST['delimiter'])
    except:
        try:
            reader_mf = csv.DictReader(io.StringIO(main_f.read()), delimiter=request.POST['delimiter'])
        except:
            reader_mf = csv.DictReader(io.BytesIO(main_f.read()), delimiter=request.POST['delimiter'])
    
       
    for rowv in reader_mf:
        vsample = str(rowv["Sample"])
        try:
            
            spdb = Sample.objects.get(name=vsample)
            isample = spdb.id
        except Sample.DoesNotExist:
            messages.add_message(request, messages.INFO, "Sample named:  " +vsample +' does not exist in database.')

        vlocus = rowv["Locus"]
        vfreq = float(rowv["Frequency"])
        vallele = rowv["Allele"]
        if vallele != 'm':
            vallele = float(vallele)
        updated_values={'frenquency':vfreq}
        trouve = isValidCodeAllele(dict_alleles, vlocus, vallele) 
        if trouve == True:
            locusfind = Locus.objects.get(name=vlocus)
            try:
                # Ici, soit on crée l'objet, soit on fait la MÀJ
#                 obj = GenotypingValue.objects.get(sample=isample,locus=locusfind,experiment=manip, referential=ref, allele=vallele)
                obj = Locus.objects.get(genotyping__sample_id=isample, genotyping__genotyping__experiment_id=manip, \
                                        genotyping__genotyping__referential_id=ref, genotyping__genotyping__genotype=vallele)
                sp = Sample.objects.get(id=isample)
                freqbd = obj.frenquency
                if freqbd != vfreq:                  
                    for key, value in updated_values.items():
                        setattr(obj, key, value)                        
                    obj.save()
                    messages.add_message(request, messages.INFO, "Locus named:  " +vlocus +" et l'allele: "+vallele+" ont été mis à jour pour le sample "+str(sp.name)+".") 
                #tag_fields.update({'success':'All insertion was done !'                              
                #                    })  
            except Locus.DoesNotExist:
                sp = Sample.objects.get(id=isample)
                updated_values.update({'genotyping__genotyping__sample':isample,'genotyping__genotyping__experiment':manip,\
                                       'genotyping__genotyping__referential':ref, 'genotyping__genotyping__genotype':vallele})
                obj = Locus(**updated_values)
                obj.save()
                messages.add_message(request, messages.INFO, "Locus named:  " +vlocus +" et l'allele: "+str(vallele)+" ont été insérés pour le sample "+str(sp.name)+".") 
                    #tag_fields.update({'success':'Update was done !'  })                             
        else:
            messages.add_message(request, messages.INFO, "Locus named:  " +vlocus +" et l'allele  "+str(vallele)+" ne sont pas valides.")                            
    
def isValidCodeAllele (dictAllele, locus, vallele):
    trouve = False
    if dictAllele != None:
        listallele = None
        try:            
            listallele = dictAllele[locus]
            if listallele != None:
                if vallele in listallele:
                    trouve = True                    
        except Exception as e :
            print((e))
    return trouve            


#getting a dict that contains all alleles for every Locus

def getTableVerif(verif_file):
    try:
        reader = csv.DictReader(io.StringIO(verif_file.read().decode('utf-8')), delimiter=request.POST['delimiter'])
    except:
        try:
            reader = csv.DictReader(io.StringIO(verif_file.read()), delimiter=request.POST['delimiter'])
        except:
            reader = csv.DictReader(io.BytesIO(verif_file.read()), delimiter=request.POST['delimiter'])
    
    listlocus = []       
    for row in reader:
        locusname = row["NomLocus"]
        checker = False # not necessary
        if locusname in listlocus:
            checker = True
        if checker == False:            
            listlocus.append(locusname)
    dict_loc_allele = {}
    for locusname in listlocus:
        allelelist = []
        verif_file.seek(0)
        try:
            reader = csv.DictReader(io.StringIO(verif_file.read().decode('utf-8')), delimiter=request.POST['delimiter'])
        except:
            try:
                reader = csv.DictReader(io.StringIO(verif_file.read()), delimiter=request.POST['delimiter'])
            except:
                reader = csv.DictReader(io.BytesIO(verif_file.read()), delimiter=request.POST['delimiter'])
    
        for row1 in reader :
            if locusname == row1["NomLocus"]:
                vallele = int(row1["TailleAllele"])
                allelelist.append(vallele)
        dict_loc_allele[locusname] = allelelist
    return dict_loc_allele        
        




#=============================================

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def locustype(request):
    formatt = LocusTypeAttForm()
    response = generic_management(LocusType, LocusTypeForm, request, 'genotyping/genotyping_type.html',{"attrib_update":True,"admin":True,'title':'Locus Type Management','attributes':formatt,'classname':'Locus'},"Locus Type Management")  
    if request.method == "POST":
            try :
                name = request.POST["name"]
                acc_type = LocusType.objects.get(name=name)
                attribute = "attribute0"
                i = 0
                while attribute in request.POST:
                    att_type = int(request.POST[attribute].split(';')[-1])
                    att_name= ';'.join(request.POST[attribute].split(';')[:-1])
                    att, created = LocusTypeAttribute.objects.get_or_create(attribute_name=att_name, type=att_type)
                    
                    position_qs = acc_type.locusattributeposition_set.all().order_by('-position')
                    if position_qs :
                        LocusAttributePosition.objects.create(attribute=att,type=acc_type,position=position_qs[0].position+1)
                    else :
                        LocusAttributePosition.objects.create(attribute=att,type=acc_type,position=1)
                    i+=1
                    attribute = "attribute{0}".format(str(i))
#                     print(attribute
                    
            except Exception as e :
                print((e))
    return response

#from commonfct.profiler import profile
#@profile("locusdata.prof")

@login_required
@user_passes_test(lambda u: u.is_genotyping_admin_user(), login_url='/login/')
def locusdata(request, type_id):
    # On récupère l'accessionType correspondant à l'id = type_id 
    locustype = LocusType.objects.get(id=int(type_id))
    # On récupère tous les attributs du type
    attributes = locustype.locustypeattribute_set.all()
#     print('attrs: ',attributes)
    # On récupère tous les accession du type dans "formattype"
    formattype = LocusDataForm(formstype=attributes)
    formposition = PositionForm(parent_document=Locus())
    
    register_formFile = UploadFileForm()
    
    names, labels = get_fields(formattype) 
    labels.remove('Name')
    labels = ['Name','  chromosome  ','  position  ',  '  genomeversion  '] + labels 
#     print('labels: ',labels)
    title = locustype.name+' Locus Management'
    
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "locus_special":True,
                  "refine_search":True,
                  }

    template = 'genotyping/locus_data.html'
#     print("test1")
#     print(request.method)
    
    if "or_and" in request.GET and "no_search" not in request.GET and request.method!='POST':
        dico_get={}
        or_and = None
        or_and=request.GET['or_and']
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                
        #                 if or_and in request.GET:
        #                     dico_get[]
        length_dico_get = len(dico_get)
        nb_per_page = 10
#         print('====> HERE <====')
        df_test = pd.read_csv("/tmp/locus_management.csv", sep=",", dtype=object)
        if or_and == 'and':
            
            df_empty = 1
            
            for dat, dico_type_data in dico_get.items():
                for data_type, value in dico_type_data.items():
                    if df_empty == 1:
                        df_or_and = df_test[data_type].str.contains(value,case=False,na=False)
                        df_empty = 2
                    else:
                        df_or_and = df_test[data_type].str.contains(value,case=False,na=False) & df_or_and
        elif or_and == 'or':
            df_empty = 1
            for dat, dico_type_data in dico_get.items():
                for data_type, value in dico_type_data.items():
                    if df_empty == 1:
                        df_or_and = df_test[data_type].str.contains(value,case=False) 
                        df_empty = 2
                    else:
                        df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
        
#         print(df_or_and)
        df = df_test[df_or_and]
        list_attributes = list(df.columns.values)
        dico_locus = {}
#         print('test1')
        df.index.names = ['name']
        df = df.to_dict('records')
#         print(list_attributes)
        newlist=[]
        for i in df:
            newlist.append(sorted(i.items(), key=lambda pair: list_attributes.index(pair[0])))
        
        try:
            if request.method == "GET":
                nb_per_page=request.GET['nb_per_page']  
            elif request.method == "POST":
                nb_per_page=request.POST["nb_per_page"]   
            if nb_per_page=="all":
                nb_per_page = number_all
        except:
            nb_per_page=50
        
        #print("1.05"
        page = request.GET.get('page')
        if page==None:
            page=1
  
        paginator = Paginator(newlist, nb_per_page)
    
        try:
            #print(paginator)
            locuss = paginator.page(page)
            #print(locuss)
        except PageNotAnInteger:
            # If page number is not an interger then page one
            locuss = paginator.page(1)
        except EmptyPage:
            # invalid page number show the last
            locuss = paginator.page(paginator.num_pages)  
        number_all = len(df)

         
        tag_fields.update({'all':locuss,
                           'fields_label':list_attributes,
                           'formfile':register_formFile,
                           'form':formattype,
                           'formposition':formposition,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'locus',
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           'typeid': type_id,
                            "or_and":or_and,
                            "list_attributes":list_attributes,
                            "locustypeid":locustype.id,
                            })
#         print('4')
        return render(request, template, tag_fields)   
    
    if request.method == "GET":
#         print('0')
        locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustype.id, attributes, request)
#         print('3: ',locuss)
#         print('formposition: ',formposition)
        tag_fields.update({'all':locuss,
                           'formposition':formposition,
                           'formfile':register_formFile,
                           'form':formattype,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'locus',
                           "number_all":number_all,
                           'nb_per_page':nb_per_page,
                           "list_attributes":list_attributes,
                           "locustypeid":locustype.id,
                           'typeid': type_id,})
#         print('4')
        return render(request, template, tag_fields) 
    
    elif request.method == "POST":     
#         print("---->  ",request.POST)
        if "hiddenid" in request.POST.keys() :
            try :
                #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
                try:
                    locus_id = int(request.POST['hiddenid'])  ## si le post est un string contenant l'id
                except:
                    loci = Locus.objects.filter(name=request.POST['hiddenid'])  ## si le post est un string non numérique, il passe dans le except, il s'agit du nom du locus
                    for loc in loci:
                        if loc.type != None:
                            locus_id = loc.id
                    

                instance = Locus.objects.get(id=locus_id)
                form = LocusDataForm(formstype=attributes, instance=instance, data=request.POST)
                formposition = PositionForm(parent_document=Locus(), data=request.POST)
                locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustype.id, attributes, request)              
#                 print('request post: ',request.POST)
                tag_fields.update({"list_attributes":list_attributes, 
                                   'all':locuss,
                                   "number_all":number_all,
                                   'nb_per_page':nb_per_page,
                                   })
                if form.is_valid() and formposition.is_valid():
                    instance = form.save(commit=False)
                    jsonvalues = []
                    values={}
                    
                    for pos in instance.positions:
                        pos.genomeversion = request.POST['genomeversion']
                        pos.chromosome = request.POST['chromosome']
                        pos.position = request.POST['position']
                        pos.save()
                        
                    #instance.attributes = json.dumps(jsonvalues)    
                    for att in attributes:
                        values[att.id] = request.POST[att.attribute_name.lower().replace(' ','_')]
                        
                    for attribute in instance.attributes:
                        attribute.value = values[attribute.attribute_id]
                    instance.save()
                    #Ajout le 11/05/15
                    
                    tag_fields.update({'form':form,
                                       'formposition':formposition,
                                       'creation_mode':False,
                                       'object':instance,
                                       'hiddenid':instance.id,
                                       })
                    return render(request,template,tag_fields)
                else :
                    #sinon on génère un message d'erreur qui sera affiché
                    #errormsg = "Some fields of the form are incorrect, nothing has been updated."
                    errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                    errormsg = errormsg + '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formposition.errors.items() ])
                    tag_fields.update({'form':form,
                                       'formposition':formposition,
                                       #'creation_mode':False,
                                       'hiddenid':instance.id,
                                       'errormsg':errormsg})
                    return render(request,template,tag_fields)
            except ObjectDoesNotExist :
                tag_fields.update({'form':form,
                                    'creation_mode':True,
                                    'errormsg':"The object you try to update doesn't exist in the database",
                                       })
                return render(request,template,tag_fields)
        else :                    
            formattype = LocusDataForm(data=request.POST,formstype=attributes)      
            formposition = PositionForm(data=request.POST,parent_document=Locus())
            formf = UploadFileForm(request.POST, request.FILES)
            if formf.is_valid():
                locustype = LocusType.objects.get(name=locustype)
                locustypeid = locustype.id
                uploadLocusFile(request.FILES['file'], attributes, locustypeid, str(request.POST['delimiter']))
                locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustypeid, attributes, request)  
                tag_fields.update({"list_attributes":list_attributes, 
                                   "locustypeid":locustype.id,
                                   'nb_per_page':nb_per_page,
                                   "number_all":number_all, 
                                   'formfile':register_formFile,
                                    'form':formattype,
                                    'formposition':formposition,
                                    'creation_mode':True, 
                                    'all':locuss,
                                    })
                return render(request, template, tag_fields)
            elif formattype.is_valid() and formposition.is_valid(): 
                newlocus = Locus()    
                newlocus.name = request.POST['name']
                newlocus.comment = request.POST['comment'] 
                locustype = LocusType.objects.get(name=locustype)
                locustypeid = locustype.id
                newlocus.type = locustypeid
                position = Position()
                position.position = request.POST['position']
                position.genomeversion = request.POST['genomeversion']
                position.chromosome = request.POST['chromosome']
                newlocus.positions =[position,]
                
                jsonvalues = []
                if Locus.objects.filter(name=request.POST['name']).exists():
                    for locus_pos in Locus.objects.filter(name = request.POST['name']):
                        if locus_pos.positions:
                            errormsg = 'Error : The object {0} already exists in the database.'.format(request.POST['name'])
                            locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustype.id, attributes, request)       
                            tag_fields.update({'all':locuss,
                                               "list_attributes":list_attributes,
                                               'nb_per_page':nb_per_page,
                                               "locustypeid":locustype.id,
                                       'formfile':register_formFile,
                                       'form':formattype,
                                       'formposition':formposition,
                                       'errormsg':errormsg,
                                       'creation_mode':True,
                                       "number_all":number_all,
                                       'excel_empty_file':True,
                                       'filetype':'locus',
                                       'typeid': type_id})
                            return render(request, template, tag_fields) 
                
                print(request.POST)
                for att in attributes:
                    attribute_value=AttributeValue()
                    attribute_value.attribute_id=att.id
                    attribute_value.value=request.POST[att.attribute_name.lower().replace(' ','_')]
                    jsonvalues.append(attribute_value)
                newlocus.attributes = jsonvalues
                newlocus.save()
                
                #Ajout 11/05/15
                locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustype.id, attributes, request)
                tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "locus_special":True,
                  }
                loc_filter = Locus.objects.filter(name=request.POST['name'])
                for locus_name in loc_filter:
                    if locus_name.positions:
                        object_locus = locus_name
                
                tag_fields.update({"number_all":number_all,
                                   "list_attributes":list_attributes,
                                   "locustypeid":locustype.id,
                                   'all':locuss,
                                   'nb_per_page':nb_per_page,
                                   'creation_mode':True,
                                   'object':object_locus,
                                   'form':LocusDataForm(formstype=attributes),
                                   'formposition':PositionForm(parent_document=Locus()),
                                   })
                return render(request, template, tag_fields)
            else :                
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formattype.errors.items() ])    
                locuss, number_all, nb_per_page, list_attributes  = _get_locus(locustype.id, attributes, request)       
                tag_fields.update({'all':locuss,
                                   "list_attributes":list_attributes,
                                   "locustypeid":locustype.id,
                                   'nb_per_page':nb_per_page,
                           'formfile':register_formFile,
                           'form':formattype,
                           'formposition':formposition,
                           'errormsg':errormsg,
                           'creation_mode':True,
                           "number_all":number_all,
                           'excel_empty_file':True,
                           'filetype':'locus',
                           'typeid': type_id})
                return render(request, template, tag_fields) 
            return render(request, template, {"number_all":number_all, "admin":True,'form':formattype,'creation_mode':True,"locustypeid":locustype.id, "list_attributes":list_attributes, 'all':locuss, 'fields_label':labels,'fields_name':names},"Locus Management")
    else :
        return render(request, template, {"number_all":number_all,"admin":True,'formposition':formposition,'form':formattype,"locustypeid":locustype.id, 'creation_mode':True,"list_attributes":list_attributes, 'all':locuss, 'fields_label':labels,'fields_name':names},"Locus Management")

def _get_samples(request):
    samples = Sample.objects.all()
        # Adding Paginator        
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(locuss)
    except:
        nb_per_page=50
    paginator = Paginator(samples, nb_per_page)
    page = request.GET.get('page')
    try:
        samples = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        samples = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        samples = paginator.page(paginator.num_pages)   
    number_all = (paginator.num_pages-1)*int(nb_per_page) + len(paginator.page(paginator.num_pages))
    return samples, number_all, nb_per_page

#=======================================================  

def uploadLocusFile(files, attributess, locustypeid, delimiter):
    if files != None:
        try:
            reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=delimiter)
        except:
            try:
                reader = csv.DictReader(io.StringIO(files.read()), delimiter=delimiter)
            except:
                reader = csv.DictReader(io.BytesIO(files.read()), delimiter=delimiter)

        i = 0
        for row in reader:
            print("---Rwo ",row)
            if i%100000 == 0 :
                print(i)
            position = Position()
            newlocus = Locus()
            newlocus.type = locustypeid
            if 'Comment' in row.keys():
                newlocus.comment = row['Comment'] 
            else:
                newlocus.comment = row['comment']
            if 'Name' in row.keys():
                newlocus.name = row['Name']
            else:  
                newlocus.name = row['name']
            try:
                if 'Chromosome' in row.keys():
                    chromoval = row['Chromosome']
                else:
                    chromoval = row['  chromosome  ']
                if chromoval != None or chromoval !='':
                    position.chromosome = chromoval
                else: 
                    position.chromosome = None
            except Exception as e :
                print((e))
            try:  
                if 'Position' in row.keys():
                    posival = row['Position']
                else:        
                    posival = row['  position  ']  
                if posival != None or posival != '':
                    if isinstance(posival,int):
                        position.position = posival
                    else:
                        position.position = int(posival)
                else:
                    position.position = None             
            except Exception as e :
                position.position = 0
                print((e))
                
            try:
                if 'Genome version' in row.keys():
                    genoval = row['Genome version']
                else: 
                    genoval = row['  genomeversion  '] 
                if genoval != None:
                    if isinstance(genoval,str):
                        position.genomeversion = genoval
                    else:
                        position.genomeversion = str(genoval)
                else: position.genomeversion = None
            except Exception as e :
                print((e))
                                
            jsonvalues = []
#             print("attributess: ",attributess)
            for att in attributess:
                    attribute_value=AttributeValue()
                    #print("att: "+str(att))
                    attribute_value.attribute_id=att.id
                    attribute_value.value=row[str(att.attribute_name)]
                    #print("att.attribute_name: "+str(att.attribute_name))
                    jsonvalues.append(attribute_value)
                    
            newlocus.attributes = jsonvalues
            newlocus.positions =[position,]
            newlocus.save()
            i += 1
    else: 
        print('file est nul !!!')

def _get_locus(locustypeid, attributes, request):
    import pymongo
    from pymongo import MongoClient
    client = MongoClient()
    db = client[settings.MONGODB_DATABASES['default']['name']]
    locus_restricted_type = db.locus.find({'type':locustypeid},{'name':1,'attributes':1,'comment':1,"positions.genomeversion":1,"positions.chromosome":1,"positions.position":1})
    number_all = locus_restricted_type.count()
    dico_locus={}
    dico_positions={}
    dico_genomeversion={}
    dico_chromosome = {}

    for loc_type in locus_restricted_type:
        dico_attributes={}
        try:
            dico_attributes['comment']=loc_type['comment']
        except:
            dico_attributes['comment']=" "
        for i in attributes:
            for j in loc_type['attributes']:
                if i.id == j['attribute_id']:
                    dico_attributes[i.attribute_name]=str(j['value'])
                elif i.attribute_name not in dico_attributes.keys():
                    dico_attributes[i.attribute_name]=' '
            if i.attribute_name not in dico_attributes.keys():
                dico_attributes[i.attribute_name]=' '
        for i in ['genomeversion','position','chromosome']:
            for j in loc_type['positions']:
                if i not in j:
                    dico_attributes["  "+i+"  "] = ""
                else:
                    dico_attributes["  "+i+"  "] = str(j[i])
                
            if not loc_type['positions']:
                dico_attributes["  "+i+"  "] = ""
        dico_locus[loc_type['name']]=dico_attributes
    
    list_loc_type = ['  chromosome  ','  position  ','  genomeversion  ','comment']
    for i in attributes:
        list_loc_type.append(i.attribute_name)
    if 'nb_per_page' not in request.GET:
        df = pd.DataFrame(dico_locus).T
        if not df.empty:
            df=df[list_loc_type]
            df.columns.name="name"
            df.index.names = ['name']
            df.to_csv("/tmp/locus_management.csv", index=True, encoding='utf-8')  
            list_attributes = ['name'] + list_loc_type
            request.session['list_attributes']=list_attributes
    
    for key, value in dico_locus.items():
        dico_locus[key] = sorted(dico_locus[key].items(), key= lambda pair: list_loc_type.index(pair[0]))
    
    try:
        list_attributes = request.session['list_attributes']
    except:
        list_attributes = ['name'] + list_loc_type
    
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = number_all
    except:
        nb_per_page=50
       
    paginator = Paginator(tuple(dico_locus.items()), nb_per_page)
    page = request.GET.get('page')
    if page==None:
        page=1


    try:
        locuss = paginator.page(page)
    except PageNotAnInteger:
        locuss = paginator.page(1)
    except EmptyPage:
        locuss = paginator.page(paginator.num_pages)  


    return locuss, number_all, nb_per_page, list_attributes
    
   
        #=================================================

def getGenodata(export, etape, chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode, chromosome_only):
    listIdExp = []
    listIdLoc = []
    listIdRef = []
    listSamples = []
    samples = []
    header = []
    genotypingvalues = []
    data = {}
    datadict = {}
    dictvalues = {}
    sample_name = ["Genome Version","Chromosome","Position","Referential","Experiment",]
    sd_name = ["Genome Version","Chromosome","Position","Referential","Experiment",]
    acc_name = ["Genome Version","Chromosome","Position","Referential","Experiment",]
    genotypingid_name = ["Genome Version","Chromosome","Position",]
    seed_lots = []
    ls_samples = []
    nb_columns=0
    # si aucune accession n'est selectionnee, ttes le st par defaut pareil pr exp et red
    if not listaccessions:
        listaccessions = Accession.objects.all()
    if not listexperiments:
        listexperiments = Experiment.objects.all()
    if not listreferentials:
        listreferentials = Referential.objects.all()
    
    # recup des id des listes demandees par l'usr pr les request
    for exp in listexperiments:
        listIdExp.append(exp.id)
    for ref in listreferentials:
        listIdRef.append(ref.id)
    for acc in listaccessions:
        acc_name.append(acc.name)
        seed = Seedlot.objects.filter(accession=acc)
        if seed != None:
            for sd in seed:
                seed_lots.append(sd)
                sd_name.append(sd.name)
                samples = Sample.objects.filter(seedlot=sd)
                if samples != None:
                    for sp in samples:
                        sample_name.append(sp.name)
                        listSamples.append(sp.id)
                        ls_samples.append(sp)

#     print(listSamples)
    d1 = datetime.datetime.now() 
    if name!=None:     
        name = [x.decode() for x in name]
    if chromosome==None:    
        for i in name:
            listlocus=Locus.objects(name=i)
        listlocus = Locus.objects(name__in=name)
    else:               
        if chromosome != None and position_minimale != None and position_maximale != None:
            listlocus = Locus.objects(Q(positions__chromosome__in=chromosome) \
                & Q(positions__position__gte=position_minimale) \
                & Q(positions__position__lte=position_maximale) \
                & Q(positions__genomeversion__in=genomeversion))
        elif chromosome !=None:
            listlocus = Locus.objects(positions__chromosome__in=chromosome)
       
    listlocus2=listlocus.distinct('name')
    if len(listlocus2) > 300 and chromosome_only==False:
        df_pd_test = None
        nb_columns =0
    else:            
        locus_names = []
        for locus in listlocus2:
            locus_names.append(locus)
        listref = []
        i = 0
        dico_tableau={}
        d2 = datetime.datetime.now()
        
        import pymongo
        from pymongo import MongoClient
        client = MongoClient()
        db = client[settings.MONGODB_DATABASES['default']['name']]
        list_test_csv=[]
        list2=[]
        num_list=0
        dico2={}
        position_locus_counter=0
        request=[]
        dico_loc={}
        try:
            if etape=="2":
                if chromosome_only==True:
                    df_pd_test=pd.read_csv('/tmp/view_geno_chromosome'+chromosome+'.csv', index_col=0)
                else:
                    df_pd_test=pd.read_csv('/tmp/view_geno.csv', index_col=0)
                nb_columns=len(locus_names)
                list_column_names=list(df_pd_test.columns.values)
                list_row_names=list(df_pd_test.index.values)
                dico_locus_column_names={}
                df_pd_etape1=pd.DataFrame(index=list_row_names)
                for i in listlocus2:
                    dico_locus_column_names[i]=[]
                    for k in list_column_names:
                        if i == k.replace(' ',''):
                            dico_locus_column_names[i].append(k)
                    if dico_locus_column_names[i]==[]:
                        del dico_locus_column_names[i]
                print("dico_locus_column_names : " ,dico_locus_column_names)
                for key,value in dico_locus_column_names.items():
                    df_pd_etape1[key]=""
                    for v in value:
                        print("key: ",key)
                        print("value: ",value)
                        if v==value[-1]:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]
                        else:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]+"/"
#<<<<<<< .working
                        for i in list_row_names:
                            element_splited = df_pd_etape1[key][i].split('/')
                            for j in df_pd_etape1[key][i].split('/'):
                                if j == "--":
                                    element_splited.remove(j)
                            element_df_pd = "/".join(element_splited)  
                            df_pd_etape1[key][i] = element_df_pd
                                
                        df_pd_etape1.ix["Genome Version",key]=df_pd_test.loc["Genome Version",key]
                        df_pd_etape1.ix["Chromosome",key]=df_pd_test.loc["Chromosome",key]
                        df_pd_etape1.ix["Position",key]=df_pd_test.loc["Position",key]
                
                
                try:
                    df_pd_etape1=df_pd_etape1.T.sort('Position', ascending=True).T
                    df_pd_etape1=df_pd_etape1.dropna(axis=1, how='all')   
                except: 
                    pass     
                return df_pd_etape1, len(listlocus), nb_columns   
            
            elif etape == "1":
                if chromosome_only==True:
                    df_pd_test=pd.read_csv('/tmp/view_geno_chromosome'+chromosome+'.csv', index_col=0)
                else:
                    df_pd_test=pd.read_csv('/tmp/view_geno.csv', index_col=0)
                nb_columns=len(locus_names)
                list_column_names=list(df_pd_test.columns.values)
                list_row_names=list(df_pd_test.index.values)
                dico_locus_column_names={}
                df_pd_etape1=pd.DataFrame(index=list_row_names)
                for i in listlocus2:
                    dico_locus_column_names[i]=[]
                    for k in list_column_names:
                        if i == k.replace(' ',''):
                            dico_locus_column_names[i].append(k)
                    if dico_locus_column_names[i]==[]:
                        del dico_locus_column_names[i]
                for key,value in dico_locus_column_names.items():
                    df_pd_etape1[key]=""
                    for v in value:
                        if v==value[-1]:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]
                        else:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]+"/"
# =======
#                         for i in list_row_names:
#                             element_splited = df_pd_etape1[key][i].split('/')
#                             for j in df_pd_etape1[key][i].split('/'):
#                                 if j == "--":
#                                     element_splited.remove(j)
#                             element_df_pd = "/".join(element_splited)  
#                             df_pd_etape1[key][i] = element_df_pd
#                         df_pd_etape1.ix["Genome Version",key]=df_pd_test.loc["Genome Version",key]
#                         df_pd_etape1.ix["Chromosome",key]=df_pd_test.loc["Chromosome",key]
#                         df_pd_etape1.ix["Position",key]=df_pd_test.loc["Position",key]
#                 try:
#                     df_pd_etape1=df_pd_etape1.T.sort('Position', ascending=True).T
#                     df_pd_etape1=df_pd_etape1.dropna(axis=1, how='all')   
#                 except: 
#                     pass     
#                 return df_pd_etape1, len(listlocus), nb_columns   
#             
#             elif etape == "1":
#                 if chromosome_only==True:
#                     df_pd_test=pd.read_csv('/tmp/view_geno_chromosome'+chromosome+'.csv', index_col=0)
#                 else:
#                     df_pd_test=pd.read_csv('/tmp/view_geno.csv', index_col=0)
#                 nb_columns=len(locus_names)
#                 list_column_names=list(df_pd_test.columns.values)
#                 list_row_names=list(df_pd_test.index.values)
#                 dico_locus_column_names={}
#                 df_pd_etape1=pd.DataFrame(index=list_row_names)
#                 for i in listlocus2:
#                     dico_locus_column_names[i]=[]
#                     for k in list_column_names:
#                         if i == k.replace(' ',''):
#                             dico_locus_column_names[i].append(k)
#                     if dico_locus_column_names[i]==[]:
#                         del dico_locus_column_names[i]
#                 for key,value in dico_locus_column_names.items():
#                     df_pd_etape1[key]=""
#                     for v in value:
#                         if v==value[-1]:
#                             df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]
#                         else:
#                             df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]+"/"
# >>>>>>> .merge-right.r614
                        df_pd_etape1.ix["Genome Version",key]=df_pd_test.loc["Genome Version",key]
                        df_pd_etape1.ix["Chromosome",key]=df_pd_test.loc["Chromosome",key]
                        df_pd_etape1.ix["Position",key]=df_pd_test.loc["Position",key]
                
                
                try:
                    df_pd_etape1=df_pd_etape1.T.sort('Position', ascending=True).T
                    df_pd_etape1=df_pd_etape1.dropna(axis=1, how='all')   
                except: 
                    pass     
                return df_pd_etape1, len(listlocus), nb_columns               
            
            elif etape=="0":
                if chromosome_only==True:
                    df_pd_etape2=pd.read_csv('/tmp/view_geno_chromosome'+chromosome+'.csv', index_col=0)
                else:
                    df_pd_etape2=pd.read_csv('/tmp/view_geno.csv', index_col=0)
                nb_columns=len(locus_names)
                try:
                    df_pd_etape2=df_pd_etape2.T.sort('Position', ascending=True).T
                    df_pd_etape2=df_pd_etape2.dropna(axis=1, how='all')        
                except:
                    pass
                return df_pd_etape2, len(listlocus), nb_columns               
            
        except Exception as e:
            print("\n\n\n===>",e,"\n\n\n")
            pass
        #ACCESSION
        if mode != "4":
            name_test_csv=['']   
            for loc in locus_names:
                req_position=db.locus.find({"$and": [{'genotyping':[]},{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                exp_list=[]
                liste_loc=[loc]
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    for i in range(len(geno)):
                        if position_locus_counter == 0:
                            try:
                                dico_genome_version={'':'Genome Version',loc:req_position['positions'][0]['genomeversion']}
                                dico_chromosome={'':'Chromosome',loc:req_position['positions'][0]['chromosome']}
                                dico_position={'':'Position',loc:req_position['positions'][0]['position']}
                                dico_referential={'':'Referential',loc:Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name}
                                dico_experiment={'':'Experiment',loc:Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name}                               
                                position_locus_counter=1
                                list_test_csv.append(dico_genome_version)
                                list_test_csv.append(dico_chromosome)
                                list_test_csv.append(dico_position)
                                list_test_csv.append(dico_referential)
                                list_test_csv.append(dico_experiment)
                            except:
                                pass
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']

                        if geno[i]['sample_id'] in listSamples and geno[i]['genotyping'][0]['experiment_id'] in listIdExp and geno[i]['genotyping'][0]['referential_id'] in listIdRef :
                            Sample_name=Sample.objects.get(id=geno[i]['sample_id']).name
                            sample_id=Sample.objects.get(id=geno[i]['sample_id']).id
                            print("geno i: ",geno[i]['genotyping'][0])
                            experiment_id=geno[i]['genotyping'][0]['experiment_id']
                            referential_id=geno[i]['genotyping'][0]['referential_id']
                            print("exp_list: ",exp_list)
                            if experiment_id not in exp_list:
                                exp_list.append(experiment_id) 
                                print("id: ",experiment_id)
                                print("name: ",Experiment.objects.get(id=experiment_id).name)
                                if len(exp_list)>1:
                                    liste_loc.append(str(liste_loc[-1])+' ')
                            compteur=0
                            for g in exp_list:
                                if g == experiment_id : 
                                    numero_locus=compteur
                                compteur = compteur + 1
                            loc=liste_loc[numero_locus]
                            if loc not in name_test_csv :
                                name_test_csv.append(loc)
                            dico={'':sample_id,loc:geno[i]['genotyping'][0]['genotype'],'sample_id':sample_id,'experiment_id':experiment_id,'loc':loc}
                            seedlot=Seedlot.objects.get(sample=sample_id)
                            accession=Accession.objects.get(seedlot=seedlot.id)
                            test_csv=1
                            list_test_csv[3][loc]=Referential.objects.get(id=referential_id).name
                            list_test_csv[4][loc]=Experiment.objects.get(id=experiment_id).name
                            if mode == "1":
                                list_test_csv = create_list_csv(accession, list_test_csv, loc, geno, i)
                            elif mode == "2":
                                list_test_csv = create_list_csv(seedlot, list_test_csv, loc, geno, i)
                            elif mode == "3":
                                list_test_csv = create_list_csv(Sample_name, list_test_csv, loc, geno, i)
                            
        #GENOTYPINGID
        else:
            name_test_csv=['','Accession','SeedLot','Sample','Experiment','Referential'] 
            dico2={}  
            count=0
            len_loc=len(locus_names)
            for loc in locus_names:
                count=count+1
                req_position=db.locus.find({"$and": [{'genotyping':[]},{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    for i in range(len(geno)):
#                         print('list_test_csv : ', list_test_csv)
                        if position_locus_counter == 0:
                            dico_genome_version={'':'Genome Version','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['genomeversion']}
                            list_test_csv.append(dico_genome_version)
                            dico_chromosome={'':'Chromosome','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['chromosome']}
                            list_test_csv.append(dico_chromosome)
                            dico_position={'':'Position','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['position']}
                            list_test_csv.append(dico_position)
                            position_locus_counter=1
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']
    
                        if geno[i]['sample_id'] in listSamples and geno[i]['genotyping'][0]['experiment_id'] in listIdExp and geno[i]['genotyping'][0]['referential_id'] in listIdRef :
                            try:
                                #genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'])
                                genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'], referential=geno[i]['genotyping'][0]['referential_id'])
                                dico={'':genotypingID,'Sample':geno[i]['sample_id'],'Experiment':geno[i]['genotyping'][0]['experiment_id'],'Referential':geno[i]['genotyping'][0]['referential_id'],loc:geno[i]['genotyping'][0]['genotype']}
                                test_csv=1
                                for k in range(len(list_test_csv)):
                                    if genotypingID in list_test_csv[k].values():
                                        num_list = k
                                        list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                        test_csv=0
                                if test_csv==1:
                                    seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
                                    accession=Accession.objects.get(seedlot=seedlot.id)
                                    dico_names={'Accession':accession,'SeedLot':seedlot,'':genotypingID,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':genotypingID.experiment, 'Referential':genotypingID.referential ,loc:geno[i]['genotyping'][0]['genotype']}
                                    list_test_csv.append(dico_names)
                            except:
                                #genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'])
                                genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'], referential=geno[i]['genotyping'][0]['referential_id'])
                                test_csv = 1
                                for genotyping_num in genotypingID:
                                    dico={'':genotyping_num,'Sample':geno[i]['sample_id'],'Experiment':geno[i]['genotyping'][0]['experiment_id'],'Referential':geno[i]['genotyping'][0]['referential_id'],loc:geno[i]['genotyping'][0]['genotype']}
                                    test_csv=1
                                    for k in range(len(list_test_csv)):
                                        if genotyping_num in list_test_csv[k].values():
                                            num_list = k
                                            list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                            test_csv=0
                                if test_csv==1:
                                    for genotyping_num in genotypingID:
                                        seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
                                        accession=Accession.objects.get(seedlot=seedlot.id)
                                        dico_names={'Accession':accession,'SeedLot':seedlot,'':genotyping_num,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':genotyping_num.experiment, 'Referential':genotyping_num.referential ,loc:geno[i]['genotyping'][0]['genotype']}   
                                        list_test_csv.append(dico_names)
        for i in list_test_csv:        
            for names in name_test_csv:  
                if not names in i:
                    i[names]='--'
        
        if chromosome_only==True:
            open_csv='/tmp/view_geno_chromosome'+chromosome+'.csv'
        else:
            open_csv='/tmp/view_geno.csv'
        with open(open_csv,'w') as f:                     
            writer=csv.DictWriter(f, fieldnames=name_test_csv)
            writer.writeheader()
            for i in list_test_csv:
                writer.writerow(i)
        
        df_pd_test=None    
        d2 = datetime.datetime.now()
            
        if export=="table_web":
            try:
                df_pd_test=pd.read_csv(open_csv, index_col=0)
                df_pd_test=df_pd_test.T.sort('Position', ascending=True).T
            except:
                pass
            d2 = datetime.datetime.now()
            
        nb_columns=len(locus_names)
    
    return df_pd_test, len(listlocus), nb_columns

def create_list_csv(data_name, list_test_csv, loc, geno, i):
    test_csv=1
    for k in range(len(list_test_csv)):
        if data_name in list_test_csv[k].values():
            num_list = k
            list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
            test_csv=0
    if test_csv==1:
        dico_names={'':data_name,loc:geno[i]['genotyping'][0]['genotype']}
        list_test_csv.append(dico_names)
    return list_test_csv

def viewlocusvalues(request):
    formacc = GenoViewerAccessionForm(request.POST)
    formlocus = GenoViewerLocusForm(request.POST)
    formexp = GenoViewerExperimentForm(request.POST)
    formref = GenoViewerReferentialForm(request.POST)
    formchr = GenoViewerChrForm(request.POST)
    formfile = UploadLocusFileForm(request.POST,request.FILES)
    template = 'genotyping/viewgenovalues.html'
#     formpage = PaginatorLocusForm(request.POST)
#     print(request.method)
    #headers = []
    if request.method == 'POST' or "or_and" in request.GET:
        if "or_and" in request.GET and "no_search" not in request.GET:
            mode = request.session['mode']
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "experiment" and 'test' not in request.GET:
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                    
            #                 if or_and in request.GET:
            #                     dico_get[]
            length_dico_get = len(dico_get)
            nb_per_page = 10
            df_test = pd.DataFrame.from_csv("/tmp/view_geno.csv",index_col=None)
            if or_and == 'and':
                df_empty = 1
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if mode != '4':
                            if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                data_type = 'Unnamed: 0'
                        elif data_type == 'Genotyping ID':
                            data_type = 'Unnamed: 0'
                        data_type = data_type[0].upper()+data_type[1:]
                        if mode == "4":
                            if df_empty == 1:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                df_empty = 2
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
                        else:
                            if df_empty == 1:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                df_empty = 2
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
            elif or_and == 'or':
                df_empty = 1
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if mode != '4':
                            if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                data_type = 'Unnamed: 0'
                        elif data_type == 'Genotyping ID':
                            data_type = 'Unnamed: 0'
                        data_type = data_type[0].upper()+data_type[1:]
                        if mode == "4":
                            if df_empty == 1:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                df_empty = 2
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
                        else:
                            if df_empty == 1:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                df_empty = 2
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
            df = df_test[df_or_and]
            list_attributes = list(df.columns.values)
            
            df = df.set_index('Unnamed: 0')
            df.index.names = [None] #permet d'avoir la premiere colonne du dataframe en index
            df.to_csv("/tmp/view_geno_refine_search.csv", index=False, encoding='utf-8')  
            html = df.to_html(index=True,justify={'center'})
            html = html.split('\n')
            html2 = refine_html(request, html, mode)
            if mode in ['1','2','3']:
                if mode == '1':
                    mode = 'Accession'
                elif mode == '2':
                    mode = 'Seed lot'
                else:
                    mode = 'Sample'
                if len(df)>0:
                    nb_rows = len(df)-5
                    nb_marqueurs = len(df.columns)
                else:
                    nb_rows = 0
                    nb_marqueurs = 0
            elif mode == '4':
                if len(df)>0:
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                else:
                    nb_rows = 0
                    nb_marqueurs = 0
                
            for i in list_attributes : 
                if "Unnamed:" in i:
                    list_attributes.remove(i)
                    list_attributes = [mode] + list_attributes
            
            return render(request, template, {"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2, "or_and":or_and,})  
        
        elif "no_search" in request.GET:
            df = pd.DataFrame.from_csv('/tmp/view_geno.csv',index_col=None)
            list_attributes = list(df.columns.values)
            df = df.set_index('Unnamed: 0')
            df.index.names = [None]
            html = df.to_html(index=True,justify={'center'})
            html = html.split('\n')
            mode = request.session['mode']
            if mode in ['1','2','3']:
                if mode == '1':
                    mode = 'Accession'
                elif mode == '2':
                    mode = 'Seed lot'
                else:
                    mode = 'Sample'
                if len(df)>0:
                    nb_rows = len(df)-5
                    nb_marqueurs = len(df.columns)
                else:
                    nb_rows = 0
                    nb_marqueurs = 0
            elif mode == '4':
                if len(df)>0:
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                else:
                    nb_rows = 0
                    nb_marqueurs = 0
            for i in list_attributes : 
                if "Unnamed:" in i:
                    list_attributes.remove(i)
                    list_attributes = [mode] + list_attributes
            html2 = refine_html(request, html, mode)
            
            return render(request, template, {"mode":mode,"list_attributes":list_attributes,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,})  
            
        d1 = datetime.datetime.now()
        name = []
            
        if formacc.is_valid() and formexp.is_valid() and formref.is_valid():
            #cas ou l'on requete sur chromosome, region
            name = []                
            if formchr.is_valid() and not request.FILES:
                genomeversion = formchr.cleaned_data['genome_version']
                if not genomeversion:
                    message_error = "Step 2 : Please, enter a genome version or submit a file"
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                chromosome = formchr.cleaned_data['chromosome']
                if not chromosome:
                    message_error = "Step 2 : Please, enter a chromosome"
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                position_minimale = formchr.cleaned_data['position_minimale']
                if not position_minimale:
                    message_error = "Step 2 : Please, enter a position minimale"
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                position_maximale = formchr.cleaned_data['position_maximale']
                if not position_maximale:
                    message_error = "Step 2 : Please, enter a position maximale"
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                name = None
                print("gnn -- ", genomeversion)
                for i in [",",";","/"]:
                    if i in genomeversion:
                        genomeversion=genomeversion.split(i)
                    if i in chromosome:
                        chromosome=chromosome.split(i)
                print("list ",list, " gev ",genomeversion," len ",len(genomeversion), " type ",type)
                if type(genomeversion)==str:
                    genomeversion=genomeversion.split()
                elif type(genomeversion)==list:
                    genomeversion=genomeversion
                else:
                    message_error="Step 2 : Please use the right delimiters for the genome version."
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                if type(chromosome)==str:
                    chromosome=chromosome.split()
                elif type(chromosome)==list:
                    chromosome=chromosome
                else:
                    message_error="Step 2 : Please use the right delimiters for the chromosome."
                    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
            #cas ou on choisit une liste de locus
            elif formlocus.is_valid():
                chromosome = None
                genomeversion = None
                position_minimale = None
                position_maximale = None
                name = formlocus.cleaned_data['locus_name']
            
            #file upload avec des noms de locus un par ligne
            elif formfile.is_valid() and request.FILES:
                chromosome = None
                genomeversion = None
                position_minimale = None
                position_maximale = None
                name = uploadFile(request.FILES['file'])
                    
            else:
                message_error="Please enter some locus values."
                return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
            
            if "mode" not in request.POST:
                etape="4"
                listaccessions = formacc.cleaned_data['name']
                if not listaccessions:
                    listaccessions = formacc.cleaned_data['accession']
                listaccessions_post=[i.name for i in listaccessions]
                listexperiments = formexp.cleaned_data['experiment']
                listexperiments_post = [i.name for i in listexperiments]
                listreferentials = formref.cleaned_data['referential']
                listreferentials_post = [i.name for i in listreferentials]
                mode = request.POST["choose_visualization_mode"]
                export_or_view_in_web_browser = request.POST['export_or_view_in_web_browser']
                ind_or_freq = request.POST.getlist('ind_or_freq')
        
        if "mode" in request.POST:
            ####### OK
            etape=request.POST['etape']
            mode=request.POST['mode']
            if mode=="Accession":
                mode="1"
            elif mode=="Seed lot":
                mode="2"
            elif mode=="Sample":
                mode="3"
            elif mode=="Genotyping ID":
                mode="4"
            request.session['mode']=mode
            ind_or_freq=request.POST["ind_or_freq"]
            export_or_view_in_web_browser=request.POST["export_or_view_in_web_browser"]
            if len(name) == 0:
                chromosome_post=request.POST["chromosome"]
                chromosome=[]
                for c in chromosome_post:
                    if c.isdigit() or c==",":
                        chromosome.append(c)
                chromosome=''.join(chromosome)
                chromosome=chromosome.split(',')
                position_minimale=request.POST["position_minimale"]
                position_minimale=int(position_minimale)
                position_maximale=request.POST["position_maximale"]
                position_maximale=int(position_maximale)          
                genomeversion_post=request.POST["genomeversion"]
                genomeversion=[]
                for g in genomeversion_post:
                    if g.isdigit() or g==',':
                        genomeversion.append(g)
                genomeversion="".join(genomeversion)
                genomeversion=genomeversion.split(',')
            
            ####### OK
            
            listaccessions_post=request.POST["listaccessions"]
            listaccessions=listaccessions_post.strip('[').strip(']').split("'")
            listexperiments_post=request.POST["listexperiments"]
            listexperiments=listexperiments_post.strip('[').strip(']').split("'")
            listreferentials_post=request.POST["listreferentials"]
            listreferentials=listreferentials_post.strip('[').strip(']').split("'")
            for i in ['','u',', u']:
                for j in listaccessions:
                    if i==j:
                        listaccessions.remove(j)
                for j in listexperiments:
                    if i==j:
                        listexperiments.remove(j)
                for j in listreferentials:
                    if i==j:
                        listreferentials.remove(j)        
                        
            listaccessions=Accession.objects.filter(name__in=listaccessions)
            listexperiments=Experiment.objects.filter(name__in=listexperiments)
            listreferentials=Referential.objects.filter(name__in=listreferentials)
            # niveau individu
        if ind_or_freq == ['1'] or ind_or_freq=="['1']":
            # export CSV
            if export_or_view_in_web_browser == '1':
                export='export_csv'
                chromosome_only=False
                df, nb_locus, nb_marqueurs = getGenodata(export,etape, chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode, chromosome_only)
                void = False
                request.session['mode']=mode
                if mode == '1':
                    nb_rows = len(df) - 5
                    mode = 'Accession'
                elif mode == '2':
                    nb_rows = len(df) - 5
                    mode = 'Seed lot'
                elif mode == '3':
                    nb_rows = len(df) - 5
                    mode = 'Sample'
                elif mode == '4':
                    nb_rows = len(df) - 3
                    mode = 'Genotyping ID'
                if df is not None:
                    return render(request, 'genotyping/viewgenovalues_csv.html', {"admin":True,'void':void, 'mode':mode, "nb_marqueurs":nb_marqueurs, "nb_rows":nb_rows})
                else:
                    formacc = GenoViewerAccessionForm()
                    formlocus = GenoViewerLocusForm()
                    formexp = GenoViewerExperimentForm()
                    formref = GenoViewerReferentialForm()
                    formchr = GenoViewerChrForm()
                    formfile = UploadLocusFileForm()
                    msg_error = 'nb loci found: ' + str(nb_locus) + ', too many loci found : refine the query'
                    return render(request,'genotyping/genotyping_viewer_admin.html',{'message_error':msg_error,"admin":True,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                
            else:
                export="table_web"
                chromosome_only=False
                df, nb_locus, nb_marqueurs = getGenodata(export,etape, chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode, chromosome_only)
                d2 = datetime.datetime.now()
                request.session['mode']=mode
                # pour afficher le niveau
                
                if df is not None:
                    if mode == '1':
                        nb_rows = len(df) - 5
                        mode = 'Accession'
                    elif mode == '2':
                        nb_rows = len(df) - 5
                        mode = 'Seed lot'
                    elif mode == '3':
                        nb_rows = len(df) - 5
                        mode = 'Sample'
                    elif mode == '4':
                        nb_rows = len(df) - 3
                        mode = 'Genotyping ID'
                    print("etape: ",etape)
                    list_attributes = list(df.columns.values)
                    list_attributes = [mode] + list_attributes
                    if etape == "1" or etape == "2":
                        df.to_csv('/tmp/view_geno_concat.csv')
                    else:
                        df.to_csv('/tmp/view_geno.csv')
                    html = df.to_html()    
                    html = html.split('\n')
#                         for i in html:
#                             print(i)
                    html2 = refine_html(request, html, mode)
                    if df.empty:
                        html = None
                        html2 = None
                    d2 = datetime.datetime.now()
                    if mode=="Accession" or mode=="Seed lot" or mode=="Sample":
                        type_table=1
                    elif mode=="Genotyping ID":
                        type_table=0
                    dico_genovalue={'mode':mode,
                                    "ind_or_freq":ind_or_freq,
                                    "export_or_view_in_web_browser":export_or_view_in_web_browser,
                                    "chromosome":chromosome,
                                    "position_minimale":position_minimale,
                                    "position_maximale":position_maximale,
                                    "genomeversion":genomeversion,
                                    "listaccessions":listaccessions_post,
                                    "name":name,
                                    "list_attributes":list_attributes,
                                    "html":html2,
                                    "listexperiments":listexperiments_post,
                                    "listreferentials":listreferentials_post,
                                    "type_table":type_table,
                                    "nb_marqueurs":nb_marqueurs,
                                    "nb_rows":nb_rows,
                                    "etape":etape,
                                    "admin":True,
                                    }
                    return render(request, 'genotyping/viewgenovalues.html', dico_genovalue)
                    
                else:
                    formacc = GenoViewerAccessionForm()
                    formlocus = GenoViewerLocusForm()
                    formexp = GenoViewerExperimentForm()
                    formref = GenoViewerReferentialForm()
                    formchr = GenoViewerChrForm()
                    formfile = UploadLocusFileForm()
                    msg_error = 'nb loci found: ' + str(nb_locus) + '. Too many loci found : refine the query'
                    return render(request,'genotyping/genotyping_viewer_admin.html',{'message_error':msg_error,"admin":True,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

        # niveau frequency
        elif ind_or_freq == ['2'] or ind_or_freq == "[u'2']":
            formacc = GenoViewerAccessionForm()
            formlocus = GenoViewerLocusForm()
            formexp = GenoViewerExperimentForm()
            formref = GenoViewerReferentialForm()
            formchr = GenoViewerChrForm()
            formfile = UploadLocusFileForm()
            msg_error = 'Step 4 : Frequency level were chosen. For now, only the individual level can be used.'
            return render(request,'genotyping/genotyping_viewer_admin.html',{'message_error':msg_error,"admin":True,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

        elif ind_or_freq == ['1','2'] or ind_or_freq == "[u'1',u'2']":
            formacc = GenoViewerAccessionForm()
            formlocus = GenoViewerLocusForm()
            formexp = GenoViewerExperimentForm()
            formref = GenoViewerReferentialForm()
            formchr = GenoViewerChrForm()
            formfile = UploadLocusFileForm()
            msg_error = 'Step 4 : Individual and frequency level were chosen. For now, only the individual level can be used.'
            return render(request,'genotyping/genotyping_viewer_admin.html',{'message_error':msg_error,"admin":True,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

    else:
        formacc = GenoViewerAccessionForm()
        formlocus = GenoViewerLocusForm()
        formexp = GenoViewerExperimentForm()
        formref = GenoViewerReferentialForm()
        formchr = GenoViewerChrForm()
        formfile = UploadLocusFileForm()
    return render(request,'genotyping/genotyping_viewer_admin.html',{"admin":True,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

def getDictValues (headers, heads, myDictData, mode):    
    liste = []                  
    for onekey, geno in myDictData.items():
        dicts = {}
        if 'Accession' in headers:
            dicts['Accession']=onekey
        elif 'Seedlot' in headers:
            dicts['Seedlot']=onekey
        else :
            dicts['Sample']=onekey
            
        for key in heads:
            value = None                
            try:
                value = geno[key] 
            except:
                print('except lancé, donc value = forcément: ',value )                       
            if value != None:
                dicts[key]=value
            else:
                print(' else lancé donc= value incorrecte,donc value = ',value)
        liste.append(dicts)
    return liste
                      
def fromDictToCSV(headers, heads, myDictData):
    try:
        #myFilePath = os.path.join(filePath, 'MyDictionary.csv')
        myFilePath = "/tmp/MyDictionary2.csv"
        with open(myFilePath, 'w') as myCSVFILE:
            csvWriter = csv.DictWriter(myCSVFILE, fieldnames=headers, dialect='excel', delimiter = ';')
            csvWriter.writeheader()
            for data in myDictData:
                csvWriter.writerow(data)
    except IOError as errno:
        print("I/O error({0}): {1}".format(errno, errno))
    return   

# def phnoval_management(request):
#     return generic_management(Institution, InstitutionForm, request, 'team/team_generic.html',{},"Institution Management")

def refine_html(request, html, mode):
    v=0
    tbody=0
    html2=[]
    if mode =="4":
        mode = "Genotyping ID"
    html2.append('<div class="outer">')
    for i in html:                            
        
        if "<table" in i:
            new_table="<table id='basictable' class='result alternate tablesorter'><thead>"
            html2.append(new_table)
            v=2
        if "</tr>" in i or "</th>" in i:
            v=0

        if "<th>Accession</th>" in i:
            new_title='<th class="header">Accession<br></center></th>'
            html2.append(new_title)
            v=2
        if "<th>SeedLot</th>" in i:
            new_title='<th class="header"><center>SeedLot<br></center></th>'
            html2.append(new_title)
            v=2
        if "<th>Sample</th>" in i:
            new_title='<th class="header"><center>Sample<br></center></th>'
            html2.append(new_title)
            v=2
        if "<th>Referential</th>" in i and mode=="Genotyping ID":
            new_title='<th class="header"><center>Referential<br></center></th>'
            html2.append(new_title)
            v=2
        elif "<th>Experiment</th>" in i and mode=="Genotyping ID":
            new_title='<th class="header"><center>Experiment<br></center></th>'
            html2.append(new_title)
            v=2
            
#                         if mode == 'Genotyping ID':
        elif "<th>Position</th>" in i or "<th>Chromosome</th>" in i or "<th>Genome Version</th>" in i or "<th>Experiment</th>" in i or "<th>Referential</th>" in i:
            new_th="<th style='background-color:#848484; border:1px solid #848484;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
            html2.append(new_th)
            v=1
        elif v==1:
            new_td="<td style='background-color:#BDBDBD; border:1px solid #848484;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
            html2.append(new_td)
        elif v==0:
            html2.append(i)
        if "thead" in i:
            tbody=tbody+1
    html2.append("</div>")               
    return html2         

def uploadFile(files):
    if files != None:
        tabline = []
        i=0
        for line in files:
            tabline.append(line.strip())
            i+=1
        return tabline   
