# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import sys
import re
import datetime
import random
import subprocess
from subprocess import call
import os
from string import rstrip
from genotyping.models import Referential, Experiment, Sample, Locus


def format_file(file):
	inter = 'output.txt'
	formatted_file = open(inter,'w')
	f = []
	for line in file:
		f.append(line)

	if not f[0].startswith("\t"):
		f[0] = "\t" + f[0]
	formatted_file.write(f[0])
	
	for el in f[1:]:
		formatted_file.write(el)
	
	return inter

def export50k(file, deli, ref_id, exp_id):
	print('EXPORT 50')
	from genotyping.models import GenotypingID
	if deli == '\\t':
		deli = '\t'
	fichier = np.genfromtxt(file, delimiter=deli, dtype=np.str)
	output = 'inter50.json'
	res = open(output,'w')

	nb_lo = np.count_nonzero(fichier[0,1:])
	row_loc = fichier[0,1:nb_lo+1]

	nb_genoid = np.count_nonzero(fichier[1:,0])
	col_genoid = fichier[1:,0]

	sorted_listgenoid = GenotypingID.objects.filter(name__in=col_genoid)
	sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
	
	listgenoid = []
	for genoid in col_genoid:
		try:
			listgenoid.append(sorted_listgenoid[genoid])
		except KeyError:
			print('GenoID ' + genoid + ' does not exist')
			continue
	listsample = []
	print('RECUP FINIE')
	print(listgenoid)
	for genoid in listgenoid:
		print(genoid) 
		listsample.append(genoid.sample)
	
	for i in range(len(row_loc)):
		variable = '{"name": "' + str(row_loc[i]) + '", "genotyping": ['
		res.write(variable)

		j = 0
		for sample in listsample:
			val_geno = fichier[j+1,i+1]
			variable_2 = '{"sample_id": ' + str(sample.id) + ','

			if j < len(listsample)-1:
				variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(exp_id) + ', "referential_id": ' + str(ref_id) + ', "frequency": 0.5}]},'
				res.write(variable_2 + variable_3)
		 	if j ==len(listsample)-1:
		 		variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(exp_id) + ', "referential_id": ' + str(ref_id) + ', "frequency": 0.5}]}'
				res.write(variable_2 + variable_3)
 			
 			j += 1

 		res.write(']}\n')
#  		print i 
#  		print j
#  		if i != len(row_loc)+1 and j != len(listsample)-1 or i != len(row_loc)+1 or j != len(listsample)-1:
#  			print 'Erreur'

		
# 	appel_mongo = 'mongoimport -c locus -d insert-test < ' + output + ' --batchSize 1 '
#   	
# 	call(appel_mongo, shell=True)
# 	os.remove(output)
#   	print appel_mongo
	
def export600k(input,deli,ref_id,exp_id):
	from genotyping.models import GenotypingID
	if deli == '\\t':
		deli = '\t'
	fichier = np.genfromtxt(input, delimiter=deli, dtype=np.str)
	output = 'inter600.json'
	res = open(output,'w')
	
	# lecture du fichier
	nb_genoid = np.count_nonzero(fichier[0,1:])-4
	row_genoid = fichier[0,1:nb_genoid+1]

	nb_lo = np.count_nonzero(fichier[1:,0])
	col_loc = fichier[1:,0]
	
	
	sorted_listgenoid = GenotypingID.objects.filter(name__in=row_genoid)
	sorted_listgenoid = dict([(genoid.name, genoid) for genoid in sorted_listgenoid])
	listgenoid = []
	for genoid in row_genoid:
		try:
			listgenoid.append(sorted_listgenoid[genoid])
		except KeyError:
			print('GenoID ' + genoid + ' does not exist')
			continue
	# recup tous les samples dont le nom est present dans le fichier
	listsample = []
	for genoid in listgenoid:
		listsample.append(genoid.sample)
		
	
# 	for sample in sorted_listsample:
# 		print sample
 	# pour chaque locus
	for i in range(len(col_loc)):
		chr = fichier[i+1,nb_genoid+2]
		pos = fichier[i+1, nb_genoid+3]
# pour obtenir les valeurs de genotypage par colonne
		f = fichier[i+1,1:nb_genoid+1]
    			
		variable = '{"name": "' + str(col_loc[i]) + '", "positions": [{"genomeversion": '\
						+ str(3) + ', "chromosome": "' + str(chr) + '", "position": '\
						+ str(pos) + '}], "genotyping": ['
		res.write(variable)
  			
  			
		j = 0
		# pour chaque sample d'un locus
		for sample in listsample:
			val_geno = f[j]	
			variable_2 = '{"sample_id": ' + str(sample.id) + ','
    					
			if j < len(listsample)-1:
				variable_3 = ' "genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(exp_id)+\
		 					', "referential_id":' + str(ref_id) + ', "frequency": 0.5}]},'
		 		res.write(variable_2+variable_3)
		 	# derniere partie differente sinon beug dans l'import
		 	if j == len(listsample)-1:
		 		variable_3 = '"genotyping":[{"genotype": "' + val_geno + '","experiment_id":' + str(exp_id)+\
		 					', "referential_id":' + str(ref_id) + ', "frequency": 0.5}]}'
		 		res.write(variable_2+variable_3)
     			
 			j += 1
     		
 	 	res.write(']}\n')
 	
 	# verif que tout a ete parse
 	print(i)
 	print(j)
 	if i != len(col_loc)+1 and j != len(listsample)-1 or i != len(col_loc)+1 or j != len(listsample)-1:
 		print('Erreur')
 	
# 	appel_mongo = 'mongoimport -c locus -d insert-test < ' + output + ' --batchSize 1'
	
# 	call(appel_mongo, shell=True)
# 	print appel_mongo
