#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url
from genotyping.views import genotypinghome, experiment_management, sample_management, referential_management, genotypingvalue_management, locustype
from genotyping.views import locusdata, viewlocusvalues, genotypingid_management, readFile
from dataview.views import return_csv

urlpatterns = [
                       url(r'^home/$', genotypinghome, name='genotypinghome'),
                       url(r'^experiment/$', experiment_management, name='experiment_management'),
                       url(r'^sample/$', sample_management, name='sample_management'),
                       url(r'^referential/$', referential_management, name='referential_management'),
                       url(r'^genotypingvalue/$', genotypingvalue_management, name='genotypingvalue_management'),
                       url(r'^types/$', locustype, name='locustype'),
                       url(r'^locusdata/(?P<type_id>\d+)/$', locusdata, name='locusdata'),                       
                       url(r'^vlocusvalues/$', viewlocusvalues, name='viewlocusvalues'), 
                       url(r'^genotypingid/$', genotypingid_management, name='genotypingid_management'),
                       url(r'^return_csv/(?P<filename>[\w]+)/$', return_csv, name='return_csv'),   
                       url(r'^readFile/$', readFile, name='readFile'),
                       ]

