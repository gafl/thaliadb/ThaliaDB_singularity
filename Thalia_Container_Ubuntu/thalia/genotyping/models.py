# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import unicode_literals
import json
from bson import ObjectId
from django_mongoengine import Document, EmbeddedDocument, fields

from django.db import models
from jsonfield import JSONField
from django.utils.encoding import force_bytes
from commonfct.managers import EntityTypeManager
from genotyping.managers import SampleManager, ReferentialManager, ExperimentManager

from commonfct.constants import ATTRIBUTE_TYPES

class Sample(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    seedlot = models.ForeignKey('lot.Seedlot', db_index=True)
    description = models.TextField(blank=True, null=True)
    is_bulk = models.BooleanField(default=False)
    is_obsolete = models.BooleanField(default=False)
    codelabo = models.CharField(max_length=100,blank=True, null=True)
    tubename = models.CharField(max_length=100,blank=True, null=True)
    objects = SampleManager() 
    
    #projects = models.ManyToManyField(Project)
    projects = models.ManyToManyField('team.Project', blank=True)
    def __str__(self):
        return self.name
    
class Referential(models.Model):
    name  = models.CharField(max_length=100, unique=True)
    person = models.ForeignKey('team.Person')
    date = models.DateField()
    comments = models.TextField(blank=True, null=True)
    linkedfiles = models.ManyToManyField('team.Documents', blank=True, null=True)
    objects = ReferentialManager()
    
    projects = models.ManyToManyField('team.Project', blank = True)
    
    def __str__(self):
        return self.name

class Experiment(models.Model):
    name  = models.CharField(max_length=100, unique=True)
    institution = models.ForeignKey('team.Institution')
    person = models.ForeignKey('team.Person')
    date = models.DateField()
    comments = models.TextField(blank=True, null=True)
    objects = ExperimentManager()

    projects = models.ManyToManyField('team.Project',blank=True)
    def __str__(self):
        return self.name

class GenotypingID(models.Model):
# <<<<<<< .working
    name = models.CharField(max_length=100)
    sample = models.ForeignKey('Sample')
    experiment = models.ForeignKey('Experiment')
    referential = models.ForeignKey('Referential')
# =======
#     name = models.CharField(max_length=100, null=True)
#     sample = models.ForeignKey('Sample', null=True)
#     experiment = models.ForeignKey('Experiment', null=True)
#     referential = models.ForeignKey('Referential', null=True)
# >>>>>>> .merge-right.r614
    sentrixbarcode_a = models.CharField(max_length=100, null=True,blank=True)
    sentrixposition_a = models.CharField(max_length=100, null=True,blank=True)
    funding = models.CharField(max_length=100, null=True,blank=True)
    projects = models.ManyToManyField('team.Project',blank=True)

    def __str__(self):
        return self.name
     
    class Meta:
        unique_together = ("name","referential")
    
class LocusType(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    def __str__(self):
        return self.name
    
    def attribute_set(self):
        return self.locustypeattribute_set
    

class LocusTypeAttribute(models.Model):
    attribute_name = models.CharField(max_length=100, unique=True, blank=True)
    type = models.IntegerField(choices=ATTRIBUTE_TYPES, blank=True, null=True)
    locus_type = models.ManyToManyField('LocusType', through='LocusAttributePosition')
    def natural_key(self):
        return (self.attribute_name, self.type)
    
    class Meta:
        unique_together = ("attribute_name","type")

    def __str__(self):
        return self.attribute_name     
        
        

class LocusAttributePosition(models.Model):
    
    attribute = models.ForeignKey('LocusTypeAttribute')
    type = models.ForeignKey('LocusType')
    position = models.IntegerField()
    
    class Meta:
        unique_together = (("type",'attribute'),('type',"position"))
        

    
    
"""Mongodb model classes"""
class Position(EmbeddedDocument):
    genomeversion = fields.StringField(blank=True)
    chromosome = fields.StringField(blank=True)
    position = fields.LongField(blank=True)

class AttributeValue(EmbeddedDocument):
    attribute_id = fields.IntField()
    value = fields.StringField()

class RefXpGenotype(EmbeddedDocument):
    referential_id = fields.IntField()
    experiment_id = fields.IntField()
    genotype = fields.StringField()
    frequency = fields.FloatField()
    
class SampleGenotype(EmbeddedDocument):
    sample_id = fields.IntField()
    genotyping = fields.ListField(fields.EmbeddedDocumentField('RefXpGenotype'))

class Locus(Document):
    name = fields.StringField()
    type = fields.IntField(blank=True)
    positions = fields.ListField(fields.EmbeddedDocumentField('Position'), blank=True)
    comment = fields.StringField(blank=True)
    sequence = fields.StringField(blank=True, null=True)
    attributes = fields.ListField(fields.EmbeddedDocumentField('AttributeValue'), blank=True)
    genotyping = fields.ListField(fields.EmbeddedDocumentField('SampleGenotype'), blank=True)

    #referential = fields.StringField(blank=True)
    meta = {
        'indexes': [
            'name',
            '$genotyping',
            'attributes.attribute_id',
            ('positions.chromosome','-positions.genomeversion'),
            'type'
        ],
        'strict': False,
        'collection':'locus',
        'unique':'name', #unicite du nom a definir ici et non pas dans les () sinon erreur
            }
    
    #pour afficher leur nom sur l'interface et non "<Locus object>"
    def __str__(self):
        return self.name
    