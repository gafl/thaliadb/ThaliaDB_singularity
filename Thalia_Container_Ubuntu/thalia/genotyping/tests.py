"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
#<<<<<<< .working
from django.apps import apps
from django.core.urlresolvers import reverse
from django.test import Client
from django.core.files.uploadedfile import SimpleUploadedFile
from genotyping.forms import ExperimentForm, ReferentialForm, SampleForm, GenotypingIDForm, LocusTypeForm, LocusTypeAttForm, ChoiceIndOrFreq, UploadFileForm, ChoiceRefExpForm
import datetime
from django.db import IntegrityError, transaction

str110 = "a"*110

class ModelsTest(TestCase):
    def setUp(self):
        Accession = apps.get_model('accession','accession')
        AccessionType = apps.get_model('accession','accessiontype')
        self.acctype1 = AccessionType.objects.create(name='type1')
        self.acc1 = Accession.objects.create(name = "acc1",type=self.acctype1)
        
        Lot = apps.get_model('lot','seedlot')
        SeedlotType = apps.get_model('lot','seedlottype')
        self.lottype1 = SeedlotType.objects.create(name='type1')
        self.lot1 = Lot.objects.create(name="lot1",
                                  accession = self.acc1,
                                  type=self.lottype1)
        
        LocusTypeAttribute = apps.get_model('genotyping','locustypeattribute')
        LocusType = apps.get_model('genotyping','locustype')
        LocusAttributePosition = apps.get_model('genotyping','locusattributeposition')
#         Locus = apps.get_model('genotyping','locus')
        self.attr = LocusTypeAttribute.objects.create(attribute_name = "attribut1",
                                                     type = 1,
                                                     )
        self.attr2 = LocusTypeAttribute.objects.create(attribute_name = "attribut1.1",
                                                     type = 1,
                                                     )
        self.attr3 = LocusTypeAttribute.objects.create(attribute_name = "attribut1.2",
                                                     type = 2,
                                                     )
        self.attr4 = LocusTypeAttribute.objects.create(attribute_name = "attribut1.3",
                                                     type = 3,
                                                     )
        self.loctype = LocusType.objects.create(name = "Type1",
                                               description = "type 1 description")
        self.loctype2 = LocusType.objects.create(name = "Type2",
                                               description = "type 2 description")
        self.loctype3 = LocusType.objects.create(name = "Type3",
                                               description = "type 3 description")
        self.loctype4 = LocusType.objects.create(name = "Type4",
                                               description = "type 4 description")
        self.locattr = LocusTypeAttribute.objects.create(attribute_name = "locattr1",
                                                     type = 1,
                                                     )
        self.locattr2 = LocusTypeAttribute.objects.create(attribute_name = "locattr2",
                                                     type = 1,
                                                     )
        self.locattr3 = LocusTypeAttribute.objects.create(attribute_name = "locattr3",
                                                     type = 2,
                                                     )
        self.locattr4 = LocusTypeAttribute.objects.create(attribute_name = "locattr4",
                                                     type = 3,
                                                     )
        self.locpostype = LocusType.objects.create(name = "locpostype1",
                                               description = "type 1 description")
        self.locpostype2 = LocusType.objects.create(name = "locpostype2",
                                               description = "type 2 description")
        self.locpostype3 = LocusType.objects.create(name = "locpostype3",
                                               description = "type 3 description")
        self.locpostype4 = LocusType.objects.create(name = "locpostype4",
                                               description = "type 4 description")
        LocusAttributePosition.objects.create(attribute=self.attr,type=self.loctype2,position=1)
        LocusAttributePosition.objects.create(attribute=self.attr2,type=self.loctype2,position=2)
        LocusAttributePosition.objects.create(attribute=self.attr,type=self.loctype3,position=1)
        LocusAttributePosition.objects.create(attribute=self.attr3,type=self.loctype3,position=2)
        LocusAttributePosition.objects.create(attribute=self.attr4,type=self.loctype3,position=3)
        Institution = apps.get_model('team','institution')
        Person = apps.get_model('team','person')
        self.institution0 = Institution.objects.create(name = "institution0")
        self.person0 = Person.objects.create(first_name = "Alice",
                                             last_name = "Beaugrand")
        Experiment = apps.get_model('genotyping','experiment')
        self.exp = Experiment.objects.create(name = "experiment3", 
                                        institution = self.institution0, 
                                        person = self.person0,
                                        date=datetime.date(2017, 1, 12))
        
        Referential = apps.get_model('genotyping','referential')
        self.ref = Referential.objects.create(name = "referential1",
                                         person = self.person0,
                                         date = datetime.date(2017, 1, 12))
        self.ref2 = Referential.objects.create(name = "referential2",
                                         person = self.person0,
                                         date = datetime.date(2017, 1, 12))
        
        Sample = apps.get_model('genotyping','sample')
        self.sple = Sample.objects.create(name = "sample1",
                                          seedlot = self.lot1)
        
        GenotypingID = apps.get_model('genotyping','genotypingid')
        self.genoid = GenotypingID.objects.create(name = "genoid",
                                             sample = self.sple,
                                             experiment = self.exp,
                                             referential = self.ref)
        
    def testAttribute_set(self):
        LocusType = apps.get_model('genotyping','locustype')
        loctype2 = LocusType.objects.get(name='Type2')
        
        self.assertEqual(loctype2.attribute_set(),loctype2.locustypeattribute_set)
    
    def testExperiment_unique(self):
        Experiment = apps.get_model('genotyping','experiment')
        
        with self.assertRaises(IntegrityError):
            exp1 = Experiment.objects.create(name = "experiment3", 
                                        institution = self.institution0, 
                                        person = self.person0,
                                        date=datetime.date(2017, 1, 12) )        
    
    def testReferential_unique(self):
        Referential = apps.get_model('genotyping','referential')
        
        with self.assertRaises(IntegrityError):
            ref1 = Referential.objects.create(name = "referential1", 
                                        person = self.person0,
                                        date=datetime.date(2017, 1, 12) )
            
    def testSample_unique(self):
        Sample = apps.get_model('genotyping','sample')
        
        with self.assertRaises(IntegrityError):
            sple1 = Sample.objects.create(name = "sample1", 
                                        seedlot = self.lot1 )
    
    def testGenoid_unique(self):
        GenotypingID = apps.get_model('genotyping','genotypingid')
        #mm ref, nom genoid different
        genoid1 = GenotypingID.objects.create(name="genoid1",
                                              sample = self.sple,
                                              experiment = self.exp,
                                              referential = self.ref)
        self.assertEqual(genoid1.name,'genoid1')
        self.assertEqual(genoid1.referential,self.ref)
        #mm nom, referentiel different
        genoid2 = GenotypingID.objects.create(name="genoid1",
                                              sample = self.sple,
                                              experiment = self.exp,
                                              referential = self.ref2)
        self.assertEqual(genoid2.name,'genoid1')
        self.assertEqual(genoid2.referential,self.ref2)
        #mm nom, mm referentiel => erreur
        with self.assertRaises(IntegrityError):
            GenotypingID.objects.create(name="genoid1",
                                      sample = self.sple,
                                      experiment = self.exp,
                                      referential = self.ref2)
            
    def testLocusType_unique(self):
        LocusType = apps.get_model('genotyping','locustype')
        with self.assertRaises(IntegrityError):
            LocusType.objects.create(name="Type1")   
            
    def testLocusTypeAttribute_unique(self):
        LocusTypeAttribute = apps.get_model('genotyping','locustypeattribute')
        with self.assertRaises(IntegrityError):
            LocusTypeAttribute.objects.create(attribute_name="attribut1")      
            
    def testLocusAttributePosition_unique_together(self):
        LocusAttributePosition = apps.get_model('genotyping','locusattributeposition')        
               
        #base sur laquelle on se base pour les existants, n'existant pas avant, donc tout marche
        locusattpos1 = LocusAttributePosition.objects.create(attribute = self.locattr,
                                                             type = self.locpostype,
                                                             position = 1)    
        self.assertEqual(locusattpos1.attribute,self.locattr)
        self.assertEqual(locusattpos1.type,self.locpostype)
        self.assertEqual(locusattpos1.position,1)
        
        #nom existant, pour un type et une position différents ok
        locusattpos2 = LocusAttributePosition.objects.create(attribute = self.locattr,
                                                             type = self.locpostype2,
                                                             position = 2)    
        self.assertEqual(locusattpos2.attribute,self.locattr)
        self.assertEqual(locusattpos2.type,self.locpostype2)
        self.assertEqual(locusattpos2.position,2)
        
        #un type existant, avec un attribut différent à une position différente ok
        locusattpos3 = LocusAttributePosition.objects.create(attribute = self.locattr2,
                                                             type = self.locpostype,
                                                             position = 3)    
        self.assertEqual(locusattpos3.attribute,self.locattr2)
        self.assertEqual(locusattpos3.type,self.locpostype)
        self.assertEqual(locusattpos3.position,3)
        
        #une position existante, pour un type différent et un attribut différent ok 
        locusattpos4 = LocusAttributePosition.objects.create(attribute = self.locattr3,
                                                             type = self.locpostype3,
                                                             position = 1)    
        self.assertEqual(locusattpos4.attribute,self.locattr3)
        self.assertEqual(locusattpos4.type,self.locpostype3)
        self.assertEqual(locusattpos4.position,1)
                
        #un attribut existant, une position existante pour un type différent ok
        locusattpos5 = LocusAttributePosition.objects.create(attribute = self.locattr,
                                                             type = self.locpostype4,
                                                             position = 1)    
        self.assertEqual(locusattpos5.attribute,self.locattr)
        self.assertEqual(locusattpos5.type,self.locpostype4)
        self.assertEqual(locusattpos5.position,1)
                
        #une position existante, un type existant, avec un attribut différent Error
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                LocusAttributePosition.objects.create(attribute = self.locattr4,
                                                 type = self.locpostype,
                                                 position = 1) 
            
        #attribut existant, type existant pour une position différente Error
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                LocusAttributePosition.objects.create(attribute = self.locattr,
                                                 type = self.locpostype,
                                                 position = 4)   
            
        #attribut existant, type existant, position existante Error
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                LocusAttributePosition.objects.create(attribute = self.locattr,
                                                 type = self.locpostype,
                                                 position = 1)      
    
class FormsTest(TestCase):
    def setUp(self):
        Institution = apps.get_model('team','institution')
        Person = apps.get_model('team','person')
        self.institution1 = Institution.objects.create(name = "institution1")
        self.person1 = Person.objects.create(first_name = "Yannick",
                                             last_name = "De Oliveira")

        Accession = apps.get_model('accession','accession')
        AccessionType = apps.get_model('accession','accessiontype')
        self.acctype1 = AccessionType.objects.create(name='type2')
        self.acc1 = Accession.objects.create(name = "acc2",type=self.acctype1)
        Lot = apps.get_model('lot','seedlot')
        SeedlotType = apps.get_model('lot','seedlottype')
        self.lottype1 = SeedlotType.objects.create(name='type2')
        self.lot1 = Lot.objects.create(name="lot2",
                                  accession = self.acc1,
                                  type=self.lottype1)
        
        Experiment = apps.get_model('genotyping','experiment')
        self.exp = Experiment.objects.create(name = "experiment3", 
                                        institution = self.institution1, 
                                        person = self.person1,
                                        date=datetime.date(2017, 1, 12))
        
        Referential = apps.get_model('genotyping','referential')
        self.ref = Referential.objects.create(name = "referential3",
                                         person = self.person1,
                                         date = datetime.date(2017, 1, 12))
        self.ref2 = Referential.objects.create(name = "referential4",
                                         person = self.person1,
                                         date = datetime.date(2017, 1, 12))
        
        Sample = apps.get_model('genotyping','sample')
        self.sple = Sample.objects.create(name = "sample1",
                                          seedlot = self.lot1)
        
        LocusType = apps.get_model('genotyping','locustype')
        self.locustype1 = LocusType.objects.create(name="locustype1")
        

        
    def test_experimentForm(self):
        experimentForm = ExperimentForm({'name':"experiment1", 
                                        'institution':self.institution1.id, 
                                        'person':self.person1.id, 
                                        'date':"2017-01-12"})
        experimentForm2 = ExperimentForm({'name':"experiment2", 
                                        'institution':self.institution1.id, 
                                        'person':self.person1.id, 
                                        'date':"2017-15-12", 
                                        'comments':"comments"})
        experimentForm3 = ExperimentForm({'name':str110, 
                                        'institution':self.institution1.id, 
                                        'person':self.person1.id, 
                                        'date':"2017-01-12", 
                                        'comments':"comments"})
        self.assertTrue(experimentForm.is_valid())
        save_object = experimentForm.save()
        self.assertEqual(save_object.name, "experiment1")
        self.assertEqual(save_object.institution, self.institution1)
        self.assertEqual(save_object.person, self.person1)
        self.assertEqual(save_object.date,datetime.date(2017, 1, 12))
        self.assertEqual(save_object.comments, "")
        
        self.assertFalse(experimentForm2.is_valid())
        self.assertFalse(experimentForm3.is_valid())
        
    def test_referentialForm(self):
        referentialForm = ReferentialForm({'name':"referential2", 
                                        'person':self.person1.id, 
                                        'date':"2017-01-12"})
        referentialForm2 = ReferentialForm({'name':"referential3", 
                                        'person':self.person1.id, 
                                        'date':"2017-15-12", 
                                        'comments':"comments"})
        referentialForm3 = ReferentialForm({'name':str110, 
                                        'person':self.person1.id, 
                                        'date':"2017-15-12", 
                                        'comments':"comments"})
        self.assertTrue(referentialForm.is_valid())
        save_object = referentialForm.save()
        self.assertEqual(save_object.name, "referential2")
        self.assertEqual(save_object.person, self.person1)
        self.assertEqual(save_object.date,datetime.date(2017, 1, 12))
        self.assertEqual(save_object.comments, "")
        
        self.assertFalse(referentialForm2.is_valid())
        self.assertFalse(referentialForm3.is_valid())
    
    def test_sampleForm(self):
        sampleForm = SampleForm({'name':"sample2", 
                                        'seedlot':self.lot1.id})
        sampleForm3 = SampleForm({'name':str110, 
                                        'seedlot':self.lot1.id})
        sampleForm4 = SampleForm({'name':"sample4",
                                        'seedlot':self.lot1.id,
                                        'codelabo':str110,})
        sampleForm5 = SampleForm({'name':"sample5",
                                        'seedlot':self.lot1.id,
                                        'tubename':str110,})
        
        self.assertTrue(sampleForm.is_valid())
        save_object = sampleForm.save()
        self.assertEqual(save_object.name, "sample2")
        self.assertEqual(save_object.seedlot, self.lot1)
        self.assertFalse(save_object.is_bulk)
        self.assertFalse(save_object.is_obsolete)
        self.assertEqual(save_object.codelabo, None)
        
        self.assertFalse(sampleForm3.is_valid())
        self.assertFalse(sampleForm4.is_valid())
        self.assertFalse(sampleForm5.is_valid())
    
    def test_genoidForm(self):
        genoidForm = GenotypingIDForm({'name':'genoid2',
                                       'sample':self.sple.id,
                                       'experiment':self.exp.id,
                                       'referential':self.ref.id})
        genoidForm2 = GenotypingIDForm({'name':str110,
                                       'sample':self.sple.id,
                                       'experiment':self.exp.id,
                                       'referential':self.ref.id})
        genoidForm3 = GenotypingIDForm({'name':'genoid3',
                                       'sample':self.sple.id,
                                       'experiment':self.exp.id,
                                       'referential':self.ref.id,
                                       'sentrixbarcode_a':str110})
        genoidForm4 = GenotypingIDForm({'name':'genoid4',
                                       'sample':self.sple.id,
                                       'experiment':self.exp.id,
                                       'referential':self.ref.id,
                                       'sentrixposition_a':str110})
        self.assertTrue(genoidForm.is_valid())
        save_object = genoidForm.save()
        self.assertEqual(save_object.name, "genoid2")
        self.assertEqual(save_object.sample, self.sple)
        self.assertEqual(save_object.experiment, self.exp)
        self.assertEqual(save_object.referential, self.ref)
        self.assertEqual(save_object.sentrixbarcode_a, None)
        self.assertEqual(save_object.sentrixposition_a, None)
        self.assertEqual(save_object.funding, None)
        
        self.assertFalse(genoidForm2.is_valid())
        self.assertFalse(genoidForm3.is_valid())
        self.assertFalse(genoidForm4.is_valid())
        
    def test_locusTypeForm(self):
        locusTypeForm = LocusTypeForm({'name':'locustype2'})
        locusTypeForm2 = LocusTypeForm({'name':''})
        locusTypeForm3 = LocusTypeForm({'name':None})
        self.assertTrue(locusTypeForm.is_valid())
        save_object = locusTypeForm.save()
        self.assertEqual(save_object.name, "locustype2")
        self.assertFalse(locusTypeForm2.is_valid())
        self.assertFalse(locusTypeForm3.is_valid())
        
    
    def test_locusTypeAttForm(self):
        locusTypeAttForm = LocusTypeAttForm({'attribute_name':'locustypeatt1'})
        locusTypeAttForm2 = LocusTypeAttForm({'attribute_name':''})
        locusTypeAttForm3 = LocusTypeAttForm({'attribute_name':None,'locus_type':self.locustype1})
        
        self.assertTrue(locusTypeAttForm.is_valid())
        save_object = locusTypeAttForm.save()
        self.assertEqual(save_object.attribute_name, "locustypeatt1")
        self.assertFalse(locusTypeAttForm2.is_valid())
        self.assertFalse(locusTypeAttForm3.is_valid())

    def test_choiceIndOrFreqForm(self):
        choiceIndOrFreq = ChoiceIndOrFreq({'choice':'1'})
        choiceIndOrFreq2 = ChoiceIndOrFreq({'choice':'3'})
        choiceIndOrFreq3 = ChoiceIndOrFreq({'choice':''})
        
        self.assertTrue(choiceIndOrFreq.is_valid())        
        self.assertFalse(choiceIndOrFreq2.is_valid())
        self.assertFalse(choiceIndOrFreq3.is_valid())
    
    def test_uploadFileForm(self):
        file_test = SimpleUploadedFile('/tmp/test_uploadfileform.txt',b'text test')
        uploadfile = UploadFileForm({'delimiter':';'},{'file':file_test})
        uploadfile2 = UploadFileForm({'delimiter':';'},{'file':None})
        uploadfile3 = UploadFileForm({'delimiter':''},{'file':file_test})
        uploadfile4 = UploadFileForm({'delimiter':None},{'file':file_test})
        
        self.assertTrue(uploadfile.is_valid())
        self.assertFalse(uploadfile2.is_valid())
        self.assertFalse(uploadfile3.is_valid())
        self.assertFalse(uploadfile4.is_valid())
        
    def test_choiceRefExpForm(self):
        choiceRefExpForm = ChoiceRefExpForm({'ref':self.ref.id})
        choiceRefExpForm2 = ChoiceRefExpForm({'ref':18})
        choiceRefExpForm3 = ChoiceRefExpForm({'ref':''})
        choiceRefExpForm4 = ChoiceRefExpForm({'ref':None})
        
        self.assertTrue(choiceRefExpForm.is_valid())
        self.assertFalse(choiceRefExpForm2.is_valid())
        self.assertFalse(choiceRefExpForm3.is_valid())
        self.assertFalse(choiceRefExpForm4.is_valid())