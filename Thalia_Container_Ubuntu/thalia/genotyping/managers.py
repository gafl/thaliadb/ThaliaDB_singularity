"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.db import models
from team.models import User, Project


class SampleManager(models.Manager):
    """
    Manager for sample model.
    """
    def by_username(self,username):
        """
        Returns only the samples from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    

class ExperimentManager(models.Manager):
    """
    Manager for experiment model.
    """
    def by_username(self,username):
        """
        Returns only the experiments from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    


class ReferentialManager(models.Manager):
    """
    Manager for referential model.
    """
    def by_username(self,username):
        """
        Returns only the referentials from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()












