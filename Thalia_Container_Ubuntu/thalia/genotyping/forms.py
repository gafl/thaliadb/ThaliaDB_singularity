# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from mongodbforms import DocumentForm, EmbeddedDocumentForm
from genotyping.models import Experiment, Referential, LocusType, LocusTypeAttribute,\
    Locus, Sample, GenotypingID, Position
from accession.models import Accession
from cProfile import label
from django.contrib.admin.widgets import FilteredSelectMultiple
# Import pour aligner les radioboutons
from django.utils.safestring import mark_safe
from phenotyping.models import Trait, Environment
from commonfct.forms_utils import HorizontalRadioSelect

#from django_bootstrap_typeahead.fields import *

class ExperimentForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = '__all__'
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'}),
                  }
        
class SampleForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Sample
        blank = True
        
class ReferentialForm(forms.ModelForm):
    class Meta:
        model = Referential
        fields = '__all__'
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'}),
                  }

class LocusTypeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = LocusType
        

class LocusTypeAttForm(forms.ModelForm):
    class Meta:
        model = LocusTypeAttribute
        exclude = ['locus_type']
        widgets = {'attribute_name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}

class PositionForm(EmbeddedDocumentForm):
    class Meta:
        document = Position
        embedded_field_name = "positions"
        fields = ['position','genomeversion','chromosome']
        widgets = {'chromosome': forms.TextInput(),}

class LocusDataForm(DocumentForm):
    class Meta:
        model = Locus
        exclude = ['attributes', 'type']
        

    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            print(kwargs.keys())
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(LocusDataForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(LocusDataForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataForm, self).__init__()
            for at in attrs:
                self.fields[at.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)

        else :
            if 'data' in kwargs.keys():
                super(LocusDataForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataForm, self).__init__()
    #=======================
    
    
                
class LocusDataFileForm(DocumentForm):
    class Meta:
        model = Locus
        exclude = ['attributes_values', 'type']
#         widgets = {'attribute_name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}
    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(LocusDataFileForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(LocusDataFileForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataFileForm, self).__init__()
            for at in attrs:
                self.fields[at.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)
        else :
            if 'data' in kwargs.keys():
                super(LocusDataFileForm, self).__init__(data=kwargs['data'])
            else :
                super(LocusDataFileForm, self).__init__()
    #=============================================================================================

#==================================================================
#==================================================================            

#from django.core.validators import validate

    
CHOICES1 = (('1', 'Allele mode',), ('2', 'Standard mode',))

class UploadGenoTypValueFileForm(forms.Form):
    
    experiment = forms.ModelChoiceField(queryset=Experiment.objects.all(),empty_label="Choose Experiment")
    referential = forms.ModelChoiceField(queryset=Referential.objects.all(),empty_label="Choose Referential")    
    select_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICES1)
    verication_file = forms.FileField(label='Verification file', required=False)
    main_file = forms.FileField(label='Main file')


class UploadFileForm(forms.Form):
    file = forms.FileField(label='Enter input file ')
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")
   
MATRIX_CHOICES = [('loci_col','Columns of the file = LOCI'),
                  ('genoid_col','Columns of the file = GENOTYPINGID')]
class UploadFileForm_Matrix(forms.Form):
    file = forms.FileField(label='Enter input file ')
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")
    matrix_orientation = forms.ChoiceField(widget=HorizontalRadioSelect(), choices = MATRIX_CHOICES, required = True, initial = 'loci_col')
    
CHOIX = [('1', 'Au niveau individu'),
         ('2', 'Au niveau frequence')]

class ChoiceIndOrFreq(forms.Form):
    choice = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOIX)
    
class ChoiceRefExpForm(forms.Form):
    ref = forms.ModelChoiceField(queryset=Referential.objects.all(),
                                 label='Choose a referential')
    #exp = forms.ModelChoiceField(queryset=Experiment.objects.all(),
      #                           label='Choose an experiment')

    
CHOICES = [('1', 'View considering Accession',), 
           ('2', 'View considering Seed lot',), 
           ('3', 'View considering Sample',)]

class ViewGenotypingValuesForm(forms.Form):
    
    accessions = forms.ModelMultipleChoiceField(queryset=Accession.objects.all(),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'10'}))
    locus = forms.FileField(label="Enter a loci file")
    experiments = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all())
    referentials = forms.ModelMultipleChoiceField(queryset=Referential.objects.all())
    missing_data = forms.CharField(max_length=30, initial='missing')
    check = forms.BooleanField(label="View considering correspondence", required=False)
    choose_visualization_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICES)
    #forms.ChoiceField(widget=forms.RadioSelect(renderer=MyCustomRenderer), choices=CHOICES1)
    

class DropGenoValuesForm (forms.Form):
    #experiments = forms.CharField( widget=forms.Textarea)
    experiments = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all(),
                                                widget=FilteredSelectMultiple("Experiment",False,attrs={'rows':'100'}))
    samples = forms.ModelMultipleChoiceField(queryset=Sample.objects.all(),
                                                widget=FilteredSelectMultiple("Sample",False,attrs={'rows':'100'}))
#     locis = forms.ModelMultipleChoiceField(queryset=Locus.objects.all(),
#                                                 widget=FilteredSelectMultiple("Locus",False,attrs={'rows':'100'}))
    locis = forms.FileField(label="Enter a loci file to delete")
    
class GenotypingIDForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = GenotypingID
        blank = True
    
class UploadFileFormGenotypingID(forms.Form):
    file = forms.FileField(label='Enter input file ')
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")

