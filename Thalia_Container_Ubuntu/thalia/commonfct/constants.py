# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

""" Router constants """
MONGO_TABLES = ["locus","genotypingvalue","locusproject"]

""" Models constants """
ATTRIBUTE_TYPES = ((1,"Short Text"),
                   (2,"Long Text"),
                   (3,"Number"),
                   (4,"URL"),
                   (5,"File")
                   )

METHOD_TYPE = (
               (1,"phenotyping"),
               (2,"classification"),
               (3,"accession"),
               (4,"lot")
               )

DOCUMENT_TYPE = (
                 (1,"Project"),
                 (2,"AccessionImage"),
                 (3,"SeedlotImage"),
                 (4,"Referential"),
                 (5,"Phenotyping"),
                 )

CROSSING_METHOD_TYPE = ((1,'Crossing Method'),
                        (2,'Multiplication Method'),
                        )

ACCEPTED_TRUE_VALUES = ('True', 'true', 't', 'T', 'yes', 'TRUE')