# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.http import HttpResponse
#import StringIO
#import xlsxwriter 
import json
import collections
from team.models import Method
from accession.models import AccessionType, Accession, AccessionTypeAttribute
from genotyping.models import LocusType, Locus, Sample, GenotypingID, Position, Experiment, Referential

from classification.models import Classification, Classe
from lot.models import SeedlotType, Seedlot, SeedlotTypeAttribute
from genealogy_seedlot.models import SeedlotRelation
from genealogy_accession.models import AccessionRelation
from genealogy_managers.models import Reproduction, Reproduction_method
from images.models import Image
import ast
from phenotyping.models import Environment, SpecificTreatment, Trait
from types import *
import ast
from phenotyping.models import Environment, SpecificTreatment, Trait
import csv
from django.core.exceptions import ObjectDoesNotExist
def filerenderer(request, filetype, type_id):
    #output = StringIO.StringIO()      
    #workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    #test
    #csv.writer(open('eggs.csv', 'w'), delimiter=';',quotechar="", quoting=csv.QUOTE_MINIMAL)
    
#     with open("the_new_csv.csv", "w+") as to_file:
#         writer = csv.writer(to_file, delimiter=";")
#         for new_row in data:
#             writer.writerow(new_row)
    response = HttpResponse(content_type='text/csv')
    writer = csv.writer(response,delimiter=';')
    headers = []
    filename = None
    if request.method == "GET":        
        if filetype == 'sample':
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Seedlot')
            headers.append('Description')
            headers.append('Labo code')
            headers.append('Tube name') 
            headers.append('Is Obsolete')
            writer.writerow(headers)
            if request.GET["requesttype"] == "all":
                fields_names = []
                for csv_field in headers:
                    field_name = csv_field.lower()
                    field_name = field_name.replace(" ","_")
                    if field_name=="labo_code":
                        field_name="codelabo"
                    if field_name=="tube_name":
                        field_name="tubename"
                    fields_names.append(field_name)
                    
                database = Sample.objects.all().values_list('name')
                database_list = [i[0] for i in database]
                #Cette liste contient toutes clés primaires de la BDD
                for entry_name in database_list:
                    entry_value = Sample.objects.get(name=entry_name) #On récupère l'entrée à l'aide de son nom
                    line = [getattr(entry_value, field_name) for field_name in fields_names]
#                     if type(entry_value.attributes_values) is dict:
#                         attributes = entry_value.attributes_values
#                     else:
#                         attributes=json.loads(entry_value.attributes_values)
#                     for att in attributes:
#                         line.append(attributes[att])
                    writer.writerow(line)

        elif filetype == 'genotypingid':
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Sample')
            headers.append('Referential')
            headers.append('Experiment')
            headers.append('SentrixBarcode_A')
            headers.append('SentrixPosition_A')
            headers.append('Funding')
            writer.writerow(headers)
            if request.GET["requesttype"] == "all":
                fields_names = []
                for csv_field in headers:
                    field_name = csv_field.lower()
                    field_name = field_name.replace(" ","_")
                    fields_names.append(field_name)
        #         while fields_names[-1] != 'description':
        #             del fields_names[-1]
                
                database = GenotypingID.objects.all().values_list('name','referential')
                for entry in database:
                    entry_value = GenotypingID.objects.get(name=entry[0], referential=entry[1])
                    line = [getattr(entry_value, field_name) for field_name in fields_names]
                    writer.writerow(line)

    
        elif filetype =='accession':
            accessiontype = AccessionType.objects.get(id=int(type_id))
            filename = accessiontype.name+'.csv'
            arg = accessiontype.accessiontypeattribute_set.all()
            headers.append('Name')
            headers.append('DOI')
            headers.append('Private DOI')
            headers.append('Pedigree')
            headers.append('Is Obsolete')
            headers.append('Description')
            headers.append('Latitude')
            headers.append('Longitude')
            headers.append('Projects')
            for at in arg:
                headers.append(str(at.attribute_name))
            if request.GET['requesttype'] == 'all':
                for p in ['Parent Male','Parent Female']:
                    headers.append(p)
            writer.writerow(headers)
            #Une fois les en-tête récupérés, on vérifie si l'utilisateur veut récupérer toutes les données ou non
            if request.GET['requesttype'] == 'all': #Si l'utilisateur a cliqué sur 'Get CSV file, alors on récupère toutes les données
                attributess = [att.attribute_name for att in arg]
                attributes_names = []
                fields_names = []
                #Les champs statiques ne sont pas orthographiés de la même manière dans le CSV et dans le code (majuscules, pas d'underscore), il faut donc le réorthographier
                for csv_field in headers:
                    if csv_field == "Parent Male" or csv_field == "Parent Female":
                        pass
                    else:
                        field_name = csv_field.lower() #Les majuscules deviennent des minuscules
                        field_name = field_name.replace(" ","_") #underscores à la place des espaces
                        fields_names.append(field_name)
                for att in attributess: #On fait de même pour les attributs
                    attribute_name = att.lower()
                    attribute_name = att.replace(" ","_")
                    attributes_names.append(attribute_name)
                #Il faut maintenat récupérer la liste des champs statiques uniquement
                #Mais il faut que cette méthode fonctionne même si de nouveux champs staitques sont rajoutés
                staticfields_names = []
                j=0
                if attributess == []:
                    staticfields_names = [fields_names[i]for i in range(len(fields_names))]
                else:
                    while fields_names[j] not in attributes_names:
                        j+=1
                        #j est donc la position du premier champ non statique
                        staticfields_names = [fields_names[i]for i in range(j)]
                        
                #j est donc la position du premier champ non statique
                #Cette liste contient les noms des champs statiques associées à la classe
                database = Accession.objects.filter(type=accessiontype).values_list('name')
                database_list = [i[0] for i in database]
                #Cette liste contient toutes clés primaires de la BDD
                for entry_name in database_list:
                    #entry_name=entry_name
                    entry_name=entry_name
                    line = [] #Cette liste représente la future nouvelle ligne du tableau
                    entry_value = Accession.objects.get(name=entry_name) #On récupère l'entrée à l'aide de son nom
                    fetched_values = [getattr(entry_value, field_name) for field_name in staticfields_names] #On récupere la vaeur de chacun des champs statiques
                    fields_types = [Accession._meta.get_field(field_name).get_internal_type() for field_name in staticfields_names]
                    for k, field_type in zip(range(len(fetched_values)), fields_types):
                        if field_type == 'BooleanField':
                            line.append(fetched_values[k])
                        elif field_type == 'ManyToManyField':
                            list_name = list(map(lambda x:x.name, fetched_values[k].all()))
                            if list_name == []:
                                line.append('')
                            elif len(list_name) == 1:
                                project_name = list_name[0]
                                line.append(project_name)
                            else:
                                projects_names = '|'.join(project_name for project_name in list_name)
                                line.append(projects_names)
                        else:
                            if fetched_values[k]==None:
                                fetched_values[k]=""
                            line.append(fetched_values[k])
                    if type(entry_value.attributes_values) is dict:
                        attributes = entry_value.attributes_values
                        
                        for h in headers[1:]:
                            check=0
                            for i, j in attributes.items():
                                if AccessionTypeAttribute.objects.get(id=int(i)).attribute_name==h:
                                    line.append(j)
                                    check=1
                            if check==0:
                                print(j)
                    else:
                        attributes = ast.literal_eval(entry_value.attributes_values)
                        #Dans ce cas, on force la conversion
                        for h in headers[1:]:
                            check=0
                            for i, j in attributes.items():
                                if AccessionTypeAttribute.objects.get(id=int(i)).attribute_name==h:
                                    line.append(j)
                                    check=1
                    try: 
                        line.append(AccessionRelation.objects.get(parent_gender = "M", child=entry_value).parent)
                        line.append(AccessionRelation.objects.get(parent_gender = "F", child=entry_value).parent)
                    except ObjectDoesNotExist as e:
                        print(e)
                    writer.writerow(line) #Cette ligne est finalement rajoutée au fichier CSV
                
        elif filetype =='locus':
            locustype = LocusType.objects.get(id=int(type_id))
            filename = locustype.name+'.csv'
            arg = locustype.locustypeattribute_set.all()
            headers.append('Name')
            headers.append('Chromosome') 
            headers.append('Position')   
            headers.append('Genome version')   
            headers.append('Comment') 
            for at in arg:
                headers.append(str(at.attribute_name))
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Locus.objects.filter(type=type_id).values_list('name')

                for entry_name in database:
                    line=[]
                    entry_value = Locus.objects.filter(name=entry_name)[0] #On récupère l'entrée à l'aide de son nom
                    line.append((entry_value.name))
                    positions=entry_value.positions
                    for i in positions:
                        chromosome=i.chromosome
                        genomeversion=i.genomeversion
                        position=i.position
                    if positions==[]:
                        chromosome=""
                        genomeversion=""
                        position=""
                    line.append(chromosome)
                    line.append(position)
                    line.append(genomeversion)
                    attributes=entry_value.attributes
                    for attr in attributes:
                        line.append((attr.value))
                    writer.writerow(line)
        
        elif filetype == "experiment":
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Institution') 
            headers.append('Person')          
            headers.append('Date') 
            headers.append('Comments')   
            headers.append('Projects')   
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Experiment.objects.values_list('name')

                for entry_name in database:
                    line=[]
                    entry_value = Experiment.objects.filter(name=entry_name[0])[0] #On récupère l'entrée à l'aide de son nom
                    line.append((entry_value.name))
                    line.append((entry_value.institution.name))
                    line.append((entry_value.person.first_name)+" "+(entry_value.person.last_name))
                    line.append((entry_value.date.strftime("%x")))
                    line.append((entry_value.comments))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
                    
        elif filetype == "referential":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Person')          
            headers.append('Date') 
            headers.append('Comments')   
            headers.append('Projects')   
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Referential.objects.values_list('name')

                for entry_name in database:
                    line=[]
                    entry_value = Referential.objects.filter(name=entry_name[0])[0] #On récupère l'entrée à l'aide de son nom
                    line.append((entry_value.name))
                    line.append((entry_value.person.first_name)+" "+(entry_value.person.last_name))
                    line.append((entry_value.date.strftime("%x")))
                    line.append((entry_value.comments))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)                    
        elif filetype == "treatment":
            filename = filetype+'.csv'
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = SpecificTreatment.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = SpecificTreatment.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
                    
        elif filetype == "trait":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Abbreviation')
            headers.append('Measure Unit')
            headers.append('Comments')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Trait.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Trait.objects.filter(name = entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.abbreviation))
                    line.append((entry_value.measure_unit))
                    line.append((entry_value.comments))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
                    
        elif filetype == "environment":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Date')
            headers.append('Location')
            headers.append('Description')
            headers.append('Specific treatment')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Environment.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Environment.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.date))
                    line.append((entry_value.location))
                    line.append((entry_value.description))
                    line.append((entry_value.specific_treatment))
                    line.append((', '.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
        elif filetype == "method":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Method.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Method.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
                    
        elif filetype == "image":
            filename = filetype + ".csv"
            headers.append('IMAGE_NAME')
            headers.append('DATE')
            headers.append('COMMENT')
            headers.append('ACCESSION')
            headers.append('SEEDLOT')
            headers.append('ENVIRONMENT')
            writer.writerow(headers)


        elif filetype == "seedlothybridcross":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Crossing Method')
            headers.append('Site')
            headers.append('Start Date')
            headers.append('End Date')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 2)))
                database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 3)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        dico_interm[entry_name.child.name]['parent_male'] = entry_name.parent.name
                    else:
                        dico_interm[entry_name.child.name]['parent_female'] = entry_name.parent.name
                    dico_interm[entry_name.child.name]['crossing_method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['site']=entry_name.site
                    dico_interm[entry_name.child.name]['start_date']=entry_name.reproduction.start_date
                    dico_interm[entry_name.child.name]['end_date']=entry_name.reproduction.end_date
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent_male'])
                    line.append(j['parent_female'])
                    line.append(j['crossing_method'])
                    line.append(j['site'])
                    line.append(j['start_date'])
                    line.append(j['end_date'])
                    line.append(j['comments'])
                    writer.writerow(line)
        
        elif filetype == "seedlotmultiplication":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent')
            headers.append('Multiplication Method')
            headers.append('Site')
            headers.append('Start Date')
            headers.append('End Date')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 1)))
                database = SeedlotRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 1)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    dico_interm[entry_name.child.name]['parent'] = entry_name.parent
                    dico_interm[entry_name.child.name]['multiplication_method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['site']=entry_name.site
                    dico_interm[entry_name.child.name]['start_date']=entry_name.reproduction.start_date
                    dico_interm[entry_name.child.name]['end_date']=entry_name.reproduction.end_date
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent'])
                    line.append(j['multiplication_method'])
                    line.append(j['site'])
                    line.append(j['start_date'])
                    line.append(j['end_date'])
                    line.append(j['comments'])
                    writer.writerow(line)
                    
        elif filetype == "accessionhybridcross":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Crossing Method')
            headers.append('Synonymous')
            headers.append('First Production')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 2)))
                database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 2)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        dico_interm[entry_name.child.name]['parent_male'] = entry_name.parent.name
                    else:
                        dico_interm[entry_name.child.name]['parent_female'] = entry_name.parent.name
                    dico_interm[entry_name.child.name]['crossing_method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['synonymous']=entry_name.synonymous
                    dico_interm[entry_name.child.name]['first_production']=entry_name.first_production
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent_male'])
                    line.append(j['parent_female'])
                    line.append(j['crossing_method'])
                    line.append(j['synonymous'])
                    line.append(j['first_production'])
                    line.append(j['comments'])
                    writer.writerow(line)    
                    
        elif filetype == "otheraccessionpedigree":
            filename = filetype+".csv"
            headers.append('Name')
            headers.append('Parent Male')
            headers.append('Parent Female')
            headers.append('Crossing Method')
            headers.append('Synonymous')
            headers.append('First Production')
            headers.append('Comments')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                dico_interm = {}
                #database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(type = 2)))
                database = AccessionRelation.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = 4)))
                for entry_name in database:
                    line=[]
                    
                    if entry_name.child.name not in dico_interm:
                        dico_interm[entry_name.child.name]={}
                    if entry_name.parent_gender == "M":
                        dico_interm[entry_name.child.name]['parent_male'] = entry_name.parent.name
                    else:
                        dico_interm[entry_name.child.name]['parent_female'] = entry_name.parent.name
                    dico_interm[entry_name.child.name]['crossing_method'] = entry_name.reproduction.reproduction_method.name
                    dico_interm[entry_name.child.name]['comments']=entry_name.reproduction.description
                    dico_interm[entry_name.child.name]['synonymous']=entry_name.synonymous
                    dico_interm[entry_name.child.name]['first_production']=entry_name.first_production
                for i, j, in dico_interm.items():
                    line=[]
                    line.append(i)
                    line.append(j['parent_male'])
                    line.append(j['parent_female'])
                    line.append(j['crossing_method'])
                    line.append(j['synonymous'])
                    line.append(j['first_production'])
                    line.append(j['comments'])
                    writer.writerow(line)          

        elif filetype == "method":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Method.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Method.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    writer.writerow(line)
        elif filetype == "classification":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Date')
            headers.append('Reference')
            headers.append('Method')
            headers.append('Person')
            headers.append('Description')
            headers.append('Projects')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Classification.objects.values_list('name')
                for entry_name in database:
                    line = []
                    entry_value = Classification.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.date))
                    line.append((entry_value.reference))
                    line.append((entry_value.method))
                    line.append((entry_value.person))
                    line.append((entry_value.description))
                    line.append(('|'.join([i.name for i in entry_value.projects.filter()])))
                    writer.writerow(line)
                    
        elif filetype == "classe":
            filename = filetype + ".csv"
            headers.append('Name')
            headers.append('Description')
            headers.append('Classification')
            writer.writerow(headers)
            if request.GET['requesttype'] == 'all':
                database = Classe.objects.all()
                for entry_value in database:
                    line = []
                    #entry_value = Classe.objects.filter(name=entry_name[0])[0]
                    line.append((entry_value.name))
                    line.append((entry_value.description))
                    line.append(entry_value.classification)
                    writer.writerow(line)  
        else :
            seedlottype = SeedlotType.objects.get(id=int(type_id))
            filename = seedlottype.name+'.csv'
            arg = seedlottype.seedlottypeattribute_set.all()
            headers.append('Name')
            headers.append('Accession')
            headers.append('Is Obsolete')
            headers.append('Description')
            headers.append('Projects')
            for at in arg:
                headers.append(str(at.attribute_name))
            if request.GET['requesttype'] == 'all':
                for p in ['Parent Male','Parent Female','Parent X']:
                    headers.append(p)
            writer.writerow(headers)

            if request.GET['requesttype'] == 'all': #Si l'utilisateur a cliqué sur 'Get CSV file, alors on récupère toutes les données
                attributess = [at.attribute_name for at in arg]
                attributes_names = []
                fields_names = []
                for p in ['Parent Male','Parent Female','Parent X']:
                    headers.remove(p)
                #Les champs statiques ne sont pas orthographiés de la même manière dans le CSV et dans le code (majuscules, pas d'underscore), il faut donc le réorthographier
                for csv_field in headers:
                    field_name = csv_field.lower() #Les majuscules deviennent des minuscules
                    field_name = field_name.replace(" ","_") #underscores à la place des espaces
                    fields_names.append(field_name)
                for att in attributess: #On fait de même pour les attributs
                    att=att
                    att = att.lower()
                    att = att.replace(" ","_")
                    attributes_names.append(att)
                #Il faut maintenat récupérer la liste des champs statiques uniquement
                #Mais il faut que cette méthode fonctionne même si de nouveux champs staitques sont rajoutés
                staticfields_names = []
                j=0
                if attributess == []:
                    staticfields_names = [fields_names[i] for i in range(len(fields_names))]
                else:
                    while fields_names[j] not in attributes_names:
                        j+=1
                        #j est donc la position du premier champ non statique
                        staticfields_names = [fields_names[i] for i in range(j)]
                #Cette liste contient les noms des champs statiques associées à la classe, sauf l'Accession
                database = Seedlot.objects.filter(type=seedlottype).values_list('name')
                database_list = [i[0] for i in database]
                #Cette liste contient toutes clés primaires de la BDD
                for entry_name in database_list:
                    entry_name=entry_name
                    line = [] #Cette liste représente la future nouvelle ligne du tableau
                    entry_value = Seedlot.objects.get(name=entry_name) #On récupère l'entrée à l'aide de son nom
                    print('staticfields: ',staticfields_names)
                    fetched_values = [getattr(entry_value, field_name) for field_name in staticfields_names] #On récupere la vaeur de chacun des champs statiques
                    fields_types = [Seedlot._meta.get_field(field_name).get_internal_type() for field_name in staticfields_names]
                    for k, field_type in zip(range(len(fetched_values)), fields_types):
                        if field_type == 'BooleanField':
                            line.append(fetched_values[k])
                        elif field_type == 'ManyToManyField':
                            print('PROJET', fetched_values[k].name)
                            list_name = list(map(lambda x:x.name, fetched_values[k].all()))
                            if list_name == []:
                                line.append('')
                            elif len(list_name) == 1:
                                project_name = list_name[0]
                                line.append(project_name)
                            else:
                                projects_names = '|'.join(project_name for project_name in list_name)
                                line.append(projects_names)
                        elif field_type == 'ForeignKey':
                            accession_name = fetched_values[k].name
                            line.append(accession_name)
                        else:
                            line.append(fetched_values[k])
                    #line = [x.encode("utf-8") for x in fetched_values if (type(x) is not BooleanType] #Conversion en UTF-8
                    #On récupère ensuite le dictionnaire d'attributs, mais il arrive qu'il soit considére comme un Sring plutôt que comme un dictionnaire
                    if type(entry_value.attributes_values) is dict:
                        attributes = entry_value.attributes_values
                        
                        for h in headers[1:]:
                            check=0
                            for i, j in attributes.items():
                                if SeedlotTypeAttribute.objects.get(id=int(i)).attribute_name==h:
                                    line.append(j)
                                    check=1
#                                 line.append(' ')   
                    else:
                        #Dans ce cas, on force la conversion
                        attributes=json.loads(entry_value.attributes_values)
                        for h in headers[1:]:
                            check=0
                            for i, j in attributes.items():
                                if SeedlotTypeAttribute.objects.get(id=int(i)).attribute_name==h:
                                    line.append(j)
                                    print(j)
                                    check=1
                    try: 
                        line.append(SeedlotRelation.objects.get(parent_gender = "M", child=entry_value).parent)
                        line.append(SeedlotRelation.objects.get(parent_gender = "F", child=entry_value).parent)
                    except ObjectDoesNotExist as e:
                        try:
                            line.append('')
                            line.append('')
                            line.append(SeedlotRelation.objects.get(parent_gender = "X", child=entry_value).parent)
                        except:
                            print(e)
                    writer.writerow(line) #Cette ligne est finalement rajoutée au fichier CSV
        # Create the HttpResponse object with the appropriate CSV header.
        response['Content-Disposition'] = 'attachment; filename=%s' %filename  
        writer = csv.writer(response)
    return response
    
