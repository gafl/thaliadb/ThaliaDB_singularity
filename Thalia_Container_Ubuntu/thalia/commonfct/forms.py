"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals
from django import forms
from django.utils.translation import ugettext as _

import dal_queryset_sequence
from dal import autocomplete
import dal_select2_queryset_sequence
from team.models import User, Project

from accession.models import Accession
from classification.models import Classification
from lot.models import Seedlot
from genotyping.models import Sample, Locus, Referential, Experiment
from phenotyping.models import Trait, Environment
from team.models import Project

#from django.contrib.auth.models import User
    
class GlobalSearchForm(forms.Form):
    """
    This class represents a form for a global search.
    
    :var queryset search : contains all objects from Samples, Seedlots, Accessions, Traits, Environments, Referentials, Experiments and Projects
    """

    search = dal_queryset_sequence.fields.QuerySetSequenceModelField(label=_("Search in database"),
                                       queryset=autocomplete.QuerySetSequence(
                                                                                Sample.objects.all(),
                                                                                Seedlot.objects.all(),
                                                                                Accession.objects.all(),
                                                                                Trait.objects.all(),
                                                                                Environment.objects.all(),
                                                                                Referential.objects.all(),
                                                                                Experiment.objects.all(),
                                                                                Classification.objects.all(),
                                                                                Project.objects.all(),
                                                                                ),
                                       #required=False,
                                       widget=dal_select2_queryset_sequence.widgets.QuerySetSequenceSelect2(url='search-autocomplete', attrs={'style': 'width:100px'}))
    
    
