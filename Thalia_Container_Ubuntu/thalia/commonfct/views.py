# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from __future__ import unicode_literals
from lot.models import Seedlot
from django import http
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render, redirect
from dal import autocomplete
from commonfct.forms import GlobalSearchForm 
from accession.models import Accession
from classification.models import Classification
from genotyping.models import Sample, Locus, Referential, Experiment
from phenotyping.models import Trait, Environment
from team.models import User, Project
from dataview import views
from django.http.request import HttpRequest

 
def global_search(request):
    """
        Redirects to the searched object datacard

        :var form form : search form
        :var int content_type_id : content_type's id
        :var string content_type : object's type
        :var int object_id : object's id
        :var Object obj : searched object
        :var string link : datacard url of this specific object
        :var string modelName : obj's type
        :var string modelNameUrl : obj's type in lower case
    """

    if request.method=="POST":
        
        form = GlobalSearchForm(data=request.POST)
        if form.is_valid():
#            if request.POST.get('search') is None:
#                return render(request, "dataview/data_card.html", {'dataview':True, "error":"This search is invalid, you need to enter the name and select an accession, a sample or a seedlot."})   
            content_type_id = request.POST['search'].split('-')[0]
            content_type = ContentType.objects.get(id=content_type_id)
            object_id = request.POST['search'].split('-')[1]
            
            modelName = content_type.model_class()
            obj=modelName.objects.get(id__iexact=object_id)
            modelNameUrl = str(modelName).lower().split('.')[2][:-2]
            
            if(str(content_type)=="project") : 
                mutable = request.POST._mutable
                request.POST._mutable = True
                request.POST['fiche'] = obj.name
                request.POST._mutable = mutable
                return views.project_viewer(request)
            else :     
                link = "/dataview/data_card/?"+modelNameUrl+'=' + str(obj.name)
                return redirect(link)

    else :
        return http.HttpResponseRedirect(reverse('home'))
    
     

class GlobalSearchAutocomplete(autocomplete.Select2QuerySetSequenceView):
    
    def get_queryset(self):
        """
        Creates a list of accessions, seedlots and samples corresponding to the search, the user can access every object from this list
    
        :var string username : username
        :var list qs_accession : accessions from projects linked to this user and containing the searched char
        :var list qs_seedlot : seedlot from projects linked to this user and containing the searched char
        :var list qs_sample : sample from projects linked to this user and containing the searched char
        :var list qs_trait : trait from projects linked to this user and containing the searched char
        :var list qs_environment : environments from projects linked to this user and containing the searched char
        :var list qs_experiment : experiments from projects linked to this user and containing the searched char
        :var list qs_referential : referentials from projects linked to this user and containing the searched char
        :var list qs_project : projects linked to this user and containing the searched char
        """
        
        if not self.request.user.is_authenticated():
            return autocomplete.QuerySetSequence()
        username = self.request.user.login
        username_id=User.objects.get(login=username).id
        
        qs_accession = Accession.objects.by_username(username)
        qs_seedlot = Seedlot.objects.by_username(username)
        qs_sample = Sample.objects.by_username(username)
        qs_trait = Trait.objects.by_username(username)
        qs_environment = Environment.objects.by_username(username)
        qs_experiment = Experiment.objects.by_username(username)
        qs_referential = Referential.objects.by_username(username)
        qs_classif = Classification.objects.by_username(username)
        qs_project = Project.objects.filter(users=username_id)
        
        if self.q:
            qs_accession = qs_accession.filter(name__icontains = self.q)
            qs_seedlot = qs_seedlot.filter(name__icontains =self.q)
            qs_sample = qs_sample.filter(name__icontains =self.q)
            qs_trait = qs_trait.filter(name__icontains =self.q)
            qs_environment = qs_environment.filter(name__icontains =self.q)
            qs_experiment = qs_experiment.filter(name__icontains =self.q)
            qs_referential = qs_referential.filter(name__icontains =self.q)
            qs_classif = qs_classif.filter(name__icontains =self.q)
            qs_project = qs_project.filter(name__icontains =self.q)
        
        return autocomplete.QuerySetSequence(
            qs_accession,
            qs_seedlot,
            qs_sample,
            qs_trait, 
            qs_environment,
            qs_experiment,
            qs_referential,
            qs_classif,
            qs_project
        )  
        
        
    