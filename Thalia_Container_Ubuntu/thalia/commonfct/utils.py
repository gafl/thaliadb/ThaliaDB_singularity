# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


import sys

from django.conf import settings
from django.apps import apps

def get_fields(form):
    """
    get_fields est une fonction qui permet à partir d'un formulaire de récupérer le nom
    des champs du Model, ainsi que les labels qui leur sont associés
    """
    fields_name = []
    fields_label = []
    
    for field in form.fields.keys():
        fields_name.append(field)
        fields_label.append(form[field].label)
        
    return fields_name, fields_label

def get_model_from_name(classname):
    """Warning : this function doesn't work if two classes has the same name in two different apps"""
    for model in apps.get_models():
        if model._meta.model_name == classname.lower() :
            return model
    raise Exception("No model found for Class name : {0}".format(classname))

#     all_models = []
#     for module in settings.INSTALLED_APPS :
#         if not module.startswith('django') :
#             packagedir = module+'.models'
#             try :
#                 model = getattr(sys.modules[packagedir], classname)
#                 if model not in all_models :
#                     all_models.append(model)
#             except :
#                 continue
#     
#     if len(all_models) == 1 :
#         return all_models[0]
#     else :
#         raise Exception

def recode(string):
    encoding = ['ascii', 'utf-8','iso-8859-1','utf-16']
    for e in encoding :
        try :
            return string.decode(e)
        except :
            pass
    raise UnicodeDecodeError("%s can't be decoded"%string)



