import psycopg2
import traceback

from django.db import transaction
from django.core.files import File 
from django.apps import apps
import django
django.setup()
#from team.models import Institution, Person, User, Project, Documents, DataFile, Method
#from accession.models import AccessionType, AccessionTypeAttribute, AccessionAttributePosition, Accession
#from lot.models import Seedlot, SeedlotType, SeedlotTypeAttribute, SeedlotAttributePosition
#from genotyping.models import Sample, Experiment, Referential, LocusType, LocusTypeAttribute, LocusAttributePosition
from genotyping.models import Locus, AttributeValue, SampleGenotype, RefXpGenotype, Position
#from phenotyping.models import SpecificTreatment, Trait, Environment, PhenotypicValue
#from classification.models import Classification, Classe, ClasseValue

METHODS = {'phenotyping':1,
           'classification':2,
           'genealogy':3,
           'lot':4
           }


DB_NAME = "thalia"
DB_USER = "thaliadbadmin"
DB_PWD = "pheeLee3"
DB_HOST = "ganesh"

ATTRIBUTE_TYPES = {'C':1,'N':3,'T':2}
V2DIR = "/var/store/thalia/maize/"

""" dict that maps v2 id of objects with v3 objects """
insitution_map = {}
person_map = {}
project_map = {}

accessiontype_map = {}
accessionatt_map = {}
accession_map = {}

seedlottype_map = {}
seedlot_map = {}
seedlotatt_map = {}

sample_map = {}
treatment_map = {}
trait_map = {}
env_map = {}
phenofile_map = {}

classification_map = {}
classe_map = {}

method_map = {}
experiment_map = {}
referential_map = {}

locustype_map = {}
locustypeatt_map = {}


conn = psycopg2.connect("dbname={0} user={1} host={2} password={3}".format(DB_NAME, DB_USER, DB_HOST, DB_PWD))
cur = conn.cursor()

"""
Team migration
"""

def migrate_institutions(cursor):
    cursor.execute("SELECT * FROM team.institution;")
    for record in cursor :
        try :
            institution = Institution()
            institution.name = record[1]
            institution.acronym = record[2]
            institution.address = record[3]
            institution.post_box = record[4]
            institution.city = record[5]
            institution.web_site = record[6]
            institution.email_adresse = record[7]
            institution.save()
            insitution_map[record[0]] = institution
        except Exception as e :
            print("probleme with this record : {0}".format(record))
            raise
            
def migrate_persons(cursor):
    cursor.execute("SELECT * from team.person;")
    for record in cursor :
        try:
            try:
                p = Person.objects.get(first_name=record[1], last_name=record[2])
                person_map[record[0]] = p
            except Exception as e:
                person = Person()
                person.first_name = record[1]
                person.last_name = record[2]
                person.initial = record[3]
                person.title_of_person = record[4]
                person.other_langage = record[5]
                person.work_phone = record[9]
                person.work_extension = record[6]
                person.fax_number = record[10]
                person.email_adresse = record[7]
                person.note_on_person = record[8]
                person.save()
                person_map[record[0]] = person
            else:
                print("This person {0} {1} already exists and is ignored".format(record[1], record[2]))
                 
        except Exception as e :
            print("probleme with this record : {0}".format(record))
            raise
     
    cursor.execute("SELECT * from team.institution_has_person;")
    for record in cursor :
        try :
            person_map[record[1]].institutions.add(insitution_map[record[0]])
        except Exception as e : 
            print("probleme with this record : {0}".format(record))
            raise
 
def migrate_users(cursor):
    print(person_map)
    cursor.execute("SELECT * from team.user;")
    for record in cursor :
        try:
            try :
                User.objects.get(login=record[2])
            except :
                try :
                    User.objects.get(person=person_map[record[1]])
                except :
                    user = User()
                    user.person = person_map[record[1]]
                    user.login = record[2]
                    user.start_date = record[4]
                    user.end_date = record[5]
                    user.is_team_admin = record[6].endswith('w')
                    user.is_accession_admin = record[7].endswith('w')
                    user.is_seedlot_admin = record[8].endswith('w')
                    user.is_genotyping_admin = record[9].endswith('w')
                    user.is_phenotyping_admin = record[10].endswith('w')
                    user.is_classification_admin = record[11].endswith('w')
                    user.set_password(record[3])
                    user.save()
                else :
                    print("The person with id {0} is already used and user is ignored".format(record[1]))
            else :
                print("User {0} already exists and is ignored".format(record[2]))
        except Exception as e : 
            print("probleme with this record : {0}".format(record))
            raise
 
def migrate_projects(cursor):
    cursor.execute("SELECT * from team.projects;")
    for record in cursor :
        project = Project()
        project.name = record[1]
        project.authors = record[2]
        project.start_date = record[3]
        project.end_date = record[4]
        project.description = record[5]
        project.save()
        project_map[record[0]] = project
     
 
"""
Accession migration
"""
 
def migrate_accessions(cursor):
    migrate_accessiontypes(cursor)
    cursor.execute("SELECT * from genealogy.accession;")
    for record in cursor :
        accession = Accession()
        accession.name = record[1]
        accession.pedigree = record[2]
        accession.is_obsolete = record[3]
        accession.description = record[4]
        accession.type = accessiontype_map[record[5]]
        accession.save()
        accession_map[record[0]] = accession
     
    for v2id in accession_map.keys():
        cursor.execute("SELECT * from genealogy.accession_value where id_accession={0};".format(v2id))
        v3accession = accession_map[v2id]
        attvalues = {}
        for record in cursor :
            if record[2] :
                attvalues[int(accessionatt_map[record[0]].id)] = record[2]
            elif record[3] :
                attvalues[int(accessionatt_map[record[0]].id)] = record[3]
            elif record[4] :
                attvalues[int(accessionatt_map[record[0]].id)] = record[4]
        #v3accession.attributes_values = json.dumps(attvalues)
        v3accession.attributes_values = attvalues
        v3accession.save()
    
    cursor.execute("SELECT * from team.list_accession;")
    for record in cursor :
        accession_map[record[1]].projects.add(project_map[record[0]])

def migrate_accessiontypes(cursor):
    cursor.execute("SELECT * from genealogy.accession_type;")
    for record in cursor :
        accessiontype = AccessionType()
        accessiontype.name = record[1]
        accessiontype.description = record[2]
        accessiontype.save()
        accessiontype_map[record[0]] = accessiontype
     
    cursor.execute("SELECT * from genealogy.accession_type_attribute;")
    for record in cursor :
        try :
            attribute = AccessionTypeAttribute.objects.get(attribute_name=record[1], type=ATTRIBUTE_TYPES[record[2]])
        except :
            attribute = AccessionTypeAttribute()
            attribute.attribute_name = record[1]
            attribute.type = ATTRIBUTE_TYPES[record[2]]
            attribute.save()
        accessionatt_map[record[0]] = attribute
     
    cursor.execute("SELECT * from genealogy.accession_type_has_attribute;")
    for record in cursor :
        position = AccessionAttributePosition()
        position.attribute = accessionatt_map[record[1]]
        position.type = accessiontype_map[record[0]]
        position.position = record[2]
        position.save()
        
    
"""
Seedlot migration
"""
 
def migrate_seedlots(cursor):
    migrate_seedlottypes(cursor)
    cursor.execute("SELECT * from lot.seedlot;")
    for record in cursor :
        seedlot = Seedlot()
        seedlot.name = record[3]
        seedlot.descriptions = record[4]
        seedlot.is_obsolete = record[5]
 
        seedlot.type = seedlottype_map[record[2]]
        seedlot.accession = accession_map[record[1]]
        seedlot.save()
        seedlot_map[record[0]] = seedlot
     
    for v2id in seedlot_map.keys():
        cursor.execute("SELECT * from lot.seedlot_value where id_seedlot={0};".format(v2id))
        v3seedlot = seedlot_map[v2id]
        attvalues = {}
        for record in cursor :
            if record[2] :
                attvalues[int(seedlotatt_map[record[1]].id)] = record[2]
            elif record[3] :
                attvalues[int(seedlotatt_map[record[1]].id)] = record[3]
            elif record[4] :
                attvalues[int(seedlotatt_map[record[1]].id)] = record[4]
        #v3seedlot.attributes_values = json.dumps(attvalues)
        v3seedlot.attributes_values = attvalues
        v3seedlot.save()
    
    cursor.execute("SELECT * from team.list_seedlot;")
    for record in cursor :
        seedlot_map[record[1]].projects.add(project_map[record[0]])

def migrate_seedlottypes(cursor):
    cursor.execute("SELECT * from lot.seedlot_type;")
    for record in cursor :
        seedlottype = SeedlotType()
        seedlottype.name = record[1]
        seedlottype.description = record[2]
        seedlottype.save()
        seedlottype_map[record[0]] = seedlottype
     
    cursor.execute("SELECT * from lot.seedlot_type_attribute;")
    for record in cursor :
        try :
            attribute = SeedlotTypeAttribute.objects.get(attribute_name=record[1], type=ATTRIBUTE_TYPES[record[2]])
        except :
            attribute = SeedlotTypeAttribute()
            attribute.attribute_name = record[1]
            attribute.type = ATTRIBUTE_TYPES[record[2]]
            attribute.save()
        seedlotatt_map[record[0]] = attribute
     
    cursor.execute("SELECT * from lot.seedlot_type_has_attribute;")
    for record in cursor :
        position = SeedlotAttributePosition()
        position.attribute = seedlotatt_map[record[1]]
        position.type = seedlottype_map[record[0]]
        position.position = record[2]
        position.save()
 
"""
Sample and Marker migration
"""
def migrate_samples(cursor):
    cursor.execute("SELECT * from genotyping.sample;")
    for record in cursor :
        sample = Sample()
        sample.name = record[2]
        sample.description = record[3]
        sample.is_bulk = record[4]
        sample.is_obsolete = record[5]
        sample.codelabo = record[6]
        sample.tubename = record[7]
        sample.seedlot = seedlot_map[record[1]]
        sample.save()
        sample_map[record[0]] = sample
    
    cursor.execute("SELECT * from team.list_sample;")
    for record in cursor :
        sample_map[record[1]].projects.add(project_map[record[0]])
#def methods(cursor):
 
def migrate_phenotyping(cursor):
    #migration treatments
    cursor.execute("SELECT * from phenotyping.specific_treatment;")
    for record in cursor :
        treatment = SpecificTreatment()
        treatment.name = record[1]
        treatment.description = record[2]
        treatment.save()
        treatment_map[record[0]] = treatment
    #source data 
    cursor.execute("SELECT * from phenotyping.source_data;")
    allrecords = cursor.fetchall()
    for record in allrecords :
        try :
            source_data = Documents()
            source_data.type = 5
            cursor.execute("SELECT * FROM team.file WHERE id_file = {0} ;".format(record[2]))
            file_rec = cursor.fetchone()
            source_data.name = file_rec[5]
            source_data.save()
            source_file = DataFile()
            source_file.document = source_data
            v2filename = V2DIR+file_rec[1]
            handler = open(v2filename,"r")
        except :
            pass
        else :
            source_file.datafile = File(handler)
            source_file.source =  person_map[record[1]]
            source_file.date = file_rec[2]
            source_file.comments = file_rec[4]
            source_file.version = 'v1'
            source_file.save()
            phenofile_map[record[0]] = source_data
    #trait
    cursor.execute("SELECT * from phenotyping.trait;")
    for record in cursor :
        trait = Trait()
        trait.name = record[1]
        trait.abbreviation = record[2]
        trait.measure_unit = record[3]
        trait.comments = record[4]
        trait.save()
        trait_map[record[0]] = trait
     
    cursor.execute("SELECT * FROM team.list_trait;")
    for record in cursor :
        trait_map[record[1]].projects.add(project_map[record[0]])
     
    #environement
    cursor.execute("SELECT * from phenotyping.environment;")
    for record in cursor :
        env = Environment()
        env.name = record[2]
        env.date = record[3].year
        env.location = record[4]
        env.description = record[5]
        env.specific_treatment = treatment_map[record[8]]
        env.save()
        env_map[record[0]] = env
     
    cursor.execute("SELECT * FROM team.list_environment;")
    for record in cursor :
        env_map[record[1]].projects.add(project_map[record[0]])
    #pheno
    cursor.execute("SELECT * from phenotyping.phenotypic_value;")
    for record in cursor :
        pheno = PhenotypicValue()
        if record[1] in phenofile_map :
            pheno.source_data = phenofile_map[record[1]]
        else :
            pheno.source_data = None
        pheno.environment = env_map[record[2]]
        pheno.method = method_map[record[0]]
        pheno.seedlot = seedlot_map[record[3]]
        pheno.trait = trait_map[record[4]]
        pheno.value = record[5]
        pheno.save()
 
def migrate_classification(cursor) :
    cursor.execute("SELECT * from classification.classification;")
    for record in cursor :
        classification = Classification()
        classification.name = record[3]
        classification.date = record[4]
        classification.reference = record[5]
        classification.person = person_map[record[1]]
        classification.method = method_map[record[2]]
        classification.save()
        classification_map[record[0]] = classification
     
    cursor.execute("SELECT * from classification.classe;")
    for record in cursor :
        classe = Classe()
        classe.classification = classification_map[record[1]]
        classe.name = record[2]
        classe.description = record[3]
        classe.save()
        classe_map[record[0]] = classe
         
    cursor.execute("SELECT * from classification.classe_value;")
    for record in cursor :
        classevalue = ClasseValue()
        classevalue.value = record[2]
        classevalue.seedlot = seedlot_map[record[1]]
        classevalue.classe = classe_map[record[0]]
        classevalue.save()
     
    cursor.execute("SELECT * from team.list_classification;")
    for record in cursor :
        classification_map[record[1]].projects.add(project_map[record[0]])
 
def migrate_method(cursor):
    cursor.execute("SELECT * from team.method;")
    for record in cursor :
        method = Method()
        method.name = record[1]
        method.description = record[3]
        method.type = METHODS[record[2]]
        method.save()
        method_map[record[0]] = method
         
def migrate_experiments(cursor):
    cursor.execute("SELECT * from team.manip;")
    for record in cursor:
        xp = Experiment()
        xp.name = record[5]
        xp.institution = insitution_map[record[2]]
        xp.person = person_map[record[1]]
        xp.date = record[3]
        xp.comments = record[4]
        xp.save()
        experiment_map[record[0]] = xp
     
    cursor.execute("SELECT * from team.list_experiment;")
    for record in cursor:
        experiment_map[record[1]].projects.add(project_map[record[0]])
 
def migrate_referential(cursor):
    cursor.execute("SELECT * from genotyping.referential;")
    for record in cursor:
        referential = Referential()
        referential.name = record[2]
        referential.person = person_map[record[1]]
        referential.date = record[3]
        referential.comments = record[4]
        referential.save()
        referential_map[record[0]] = referential
    cursor.execute("SELECT * from team.list_referential;")
    for record in cursor:
        referential_map[record[1]].projects.add(project_map[record[0]]) 
 
def migrate_locustype(cursor) :
    cursor.execute("SELECT * from genotyping.locus_type;")
    for record in cursor:
        locus_type = LocusType()
        locus_type.name = record[1]
        locus_type.decription = record[2]
        locus_type.save()
        locustype_map[record[0]] = locus_type
     
    cursor.execute("SELECT * from genotyping.locus_type_attribute;")
    for record in cursor:
        try :
            attribute = LocusTypeAttribute.objects.get(attribute_name=record[1], type=ATTRIBUTE_TYPES[record[2]])
        except :
            attribute = LocusTypeAttribute()
            attribute.attribute_name = record[1]
            attribute.type = ATTRIBUTE_TYPES[record[2]]
            attribute.save()
        locustypeatt_map[record[0]] = attribute
     
    cursor.execute("SELECT * from genotyping.locus_type_has_attribute;")
    for record in cursor :
        position = LocusAttributePosition()
        position.attribute = locustypeatt_map[record[1]]
        position.type = locustype_map[record[0]]
        position.position = record[2]
        position.save()

def migrate_genotypes(cursor) :
#     Locus = apps.get_model('genotyping','locus')
#     RefXpGenotype = apps.get_model('genotyping','RefXpGenotype')
#     SampleGenotype = apps.get_model('genotyping','SampleGenotype')
#     AttributeValue = apps.get_model('genotyping','AttributeValue')
    cursor.execute("SELECT * from genotyping.locus WHERE id_locus_type != 10;")
    all_locus = cursor.fetchall()
    for record in all_locus:
        locus = Locus()
        locus.name = record[2]
        locus.type = locustype_map[record[1]].id
        locus.comment = record[3]
        locus.positions = None
        locus.genotyping = []
        locus.attributes = []
        print("SELECT * FROM genotyping.locus_has_allele WHERE id_locus={0}".format(record[0]))
        cursor.execute("SELECT * FROM genotyping.locus_has_allele WHERE id_locus={0}".format(record[0]))
        all_genotypes = cursor.fetchall()
        sample_genotype = {}
        for genotype in all_genotypes :
            #print("SELECT value FROM genotyping.code_allele WHERE id_code_allele={0}".format(genotype[1]))
            cursor.execute("SELECT value FROM genotyping.code_allele WHERE id_code_allele={0}".format(genotype[1]))
            code_allele = cursor.fetchone()
            xp = RefXpGenotype()
            xp.referential_id = referential_map[genotype[4]].id
            xp.experiment_id = experiment_map[genotype[3]].id
            xp.genotype = code_allele[0]
            xp.frequency = genotype[5]
            if genotype[0] in sample_genotype.keys() :
                sample_genotype[genotype[0]].genotyping.append(xp)
            else :
                s = SampleGenotype()
                s.sample_id = sample_map[genotype[0]].id
                s.genotyping = [xp,]
                sample_genotype[genotype[0]] = s
        
        for s in sample_genotype.keys() :
            locus.genotyping.append(sample_genotype[s])
        
        print("SELECT * FROM genotyping.locus_value WHERE id_locus={0}".format(record[0]))
        cursor.execute("SELECT * FROM genotyping.locus_value WHERE id_locus={0}".format(record[0]))
        all_attributes = cursor.fetchall()
        for record in all_attributes :
            av = AttributeValue()
            av.attribute_id = locustypeatt_map[record[1]].id
            if locustypeatt_map[record[1]].type == 3:
                if record[2] != "":
                    try :
                        av.value = str(record[2])
                        locus.attributes.append(av)
                    except :
                        print("impossible")
            elif locustypeatt_map[record[1]].type == 1:
                if record[3] != "":
                    try :
                        av.value = str(record[3])
                        locus.attributes.append(av)
                    except :
                        print("impossible")
            elif locustypeatt_map[record[1]].type == 2:
                if record[4] != "":
                    try :
                        av.value = str(record[4])
                        locus.attributes.append(av)
                    except :
                        print("impossible")
            else :
                print("ici")
        locus.save()
        del(locus.genotyping)
        del(locus.attributes)
        del(locus)


def get_previous_data(cursor):
    LocusType = apps.get_model('genotyping','locustype')
    cursor.execute("SELECT * FROM genotyping.locus_type")
    locus_types = cursor.fetchall()
    for record in locus_types :
        lt = LocusType.objects.get(name=record[1])
        locustype_map[record[0]] = lt
    
    Referential = apps.get_model('genotyping','referential')
    cursor.execute("SELECT * FROM genotyping.referential")
    refs = cursor.fetchall()
    for record in refs :
        ref = Referential.objects.get(name=record[2])
        referential_map[record[0]] = ref
    
    Sample = apps.get_model('genotyping','sample')
    cursor.execute("SELECT * FROM genotyping.sample")
    samples = cursor.fetchall()
    for record in samples :
        s = Sample.objects.get(name=record[2])
        sample_map[record[0]] = s
    
    try :
        Experiment = apps.get_model('genotyping', 'experiment')
        cursor.execute("SELECT * FROM team.manip")
        exps = cursor.fetchall()
        for record in exps :
            e = Experiment.objects.get(name=record[5])
            experiment_map[record[0]] = e
    except :
        pass
    
    LocusTypeAttribute = apps.get_model('genotyping','locustypeattribute')
    cursor.execute("SELECT * FROM genotyping.locus_type_attribute")
    att = cursor.fetchall()
    for record in att :
        a = LocusTypeAttribute.objects.get(attribute_name=record[1])
        locustypeatt_map[record[0]] = a

def __main__(cur):
#     migrate_institutions(cur)
#     migrate_persons(cur)
#     migrate_users(cur)
#     migrate_projects(cur)
#     migrate_method(cur)
#     migrate_accessions(cur)
#     migrate_seedlots(cur)
#     migrate_samples(cur)
#     migrate_phenotyping(cur)
#     migrate_classification(cur)
#     migrate_experiments(cur)
#     migrate_referential(cur)
#     migrate_locustype(cur)
    get_previous_data(cur)
    migrate_genotypes(cur)
    
    
try:
    with transaction.atomic():
        __main__(cur)
except Exception as e:
    traceback.print_exc()
