# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from lot.models import SeedlotType, SeedlotTypeAttribute, Seedlot
from accession.models import Accession
from genealogy_managers.models import Reproduction_method, Reproduction
from genealogy_accession.models import AccessionRelation
from genealogy_seedlot.models import SeedlotRelation
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect
from django.contrib.admin.widgets import FilteredSelectMultiple
from dal import autocomplete
import datetime

class AccessionHybridCrossForm(forms.Form):
    hybrid_accession = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_male = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_female = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    crossing_method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=2))
    synonymous = forms.CharField(required=False)
    first_production = forms.DateField(initial=datetime.date.today)
    comments = forms.CharField(required=False)

    def create_hybrid_cross(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['crossing_method'], entity_type = 0, description = self.cleaned_data['comments'])
        male_relation = AccessionRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_male'], child = self.cleaned_data['hybrid_accession'], parent_gender = "M",
                                                     synonymous = self.cleaned_data['synonymous'], first_production = self.cleaned_data['first_production'])
        female_relation = AccessionRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_female'], child = self.cleaned_data['hybrid_accession'], parent_gender = "F",
                                                     synonymous = self.cleaned_data['synonymous'], first_production = self.cleaned_data['first_production'])
        return repro.id, male_relation.child, male_relation.parent, female_relation.parent, self.cleaned_data['crossing_method'].name, male_relation.synonymous, female_relation.first_production, repro.description
    
    
class OtherAccessionPedigreeForm(forms.Form):
    hybrid_accession = forms.ModelChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    parent_male = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    parent_female = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    crossing_method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=4))
    synonymous = forms.CharField(required=False)
    first_production = forms.DateField(initial=datetime.date.today)
    comments = forms.CharField(required=False)

    def create_other_accession_pedigree(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['crossing_method'], entity_type = 0, description = self.cleaned_data['comments'])
        for parent_male_i in self.cleaned_data['parent_male']:
            male_relation = AccessionRelation.objects.create(reproduction=repro, parent = parent_male_i, child = self.cleaned_data['hybrid_accession'], parent_gender = "M",
                                                     synonymous = self.cleaned_data['synonymous'], first_production = self.cleaned_data['first_production'])
        for parent_female_i in self.cleaned_data['parent_female']:
            female_relation = AccessionRelation.objects.create(reproduction=repro, parent = parent_female_i, child = self.cleaned_data['hybrid_accession'], parent_gender = "F",
                                                     synonymous = self.cleaned_data['synonymous'], first_production = self.cleaned_data['first_production'])
        return repro.id, male_relation.child, male_relation.parent, female_relation.parent, self.cleaned_data['crossing_method'].name, male_relation.synonymous, female_relation.first_production, repro.description
    
    
    
    
    
    
    
    
    
    
    
    
    
    