# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.db import models
from django.utils.encoding import force_bytes
from phenotyping.managers import TraitManager, EnvironmentManager


#****************** 3/06/15
import datetime

class SpecificTreatment(models.Model):
    name = models.CharField(max_length = 100, unique = True)
    description = models.TextField(max_length=500, blank=True, null=True)
    def __str__(self):
        return self.name

class Environment(models.Model) :
    name = models.CharField(max_length = 100, unique = True)
    #********3/06/15 **********************************
    date = models.IntegerField()
    location = models.CharField(max_length = 100)
    description = models.TextField(max_length=500, blank=True, null=True)
    specific_treatment = models.ForeignKey('SpecificTreatment', blank=True, null=True)
    objects = EnvironmentManager() 
    
    projects = models.ManyToManyField('team.Project')
    def __str__(self):
        return self.name

class Trait(models.Model):
    name = models.CharField(max_length = 100, unique = True)
    abbreviation = models.CharField(max_length = 20, blank=True, null=True)
    measure_unit = models.CharField(max_length = 50, blank=True, null=True)
    comments = models.TextField(max_length=500, blank=True, null=True)
    objects = TraitManager() 
    
    projects = models.ManyToManyField('team.Project')
    def __str__(self):
        return self.name

class PhenotypicValue(models.Model):
    sourcedata = models.ForeignKey('team.Documents', blank=True, null=True)
    environment = models.ForeignKey('Environment')
    method = models.ForeignKey('team.Method')
    seedlot = models.ForeignKey('lot.Seedlot')
    trait = models.ForeignKey('Trait')
    value = models.CharField(max_length = 100)
    class Meta:
        unique_together = ("sourcedata","environment","seedlot","trait")
        index_together = [["environment","trait","seedlot"],]
        
class FileWithUniversalNewLine(object):

    def __init__(self, file_obj):
        self.file = file_obj

    def lines(self):
        buff = ""  # In case of reading incomplete line, buff will temporarly keep the incomplete line
        while True:
            line = self.file.read(2048)
            if not line:
                if buff:
                    yield buff
                raise StopIteration

            # Convert all new lines into linux new line
            line = buff + line.replace("\r\n", "\n").replace("\r", "\n")
            lines = line.split("\n")
            buff = lines.pop()
            for sline in lines:
                yield sline

    def close(self):
        self.file.close()

    def __exit__(self, *args, **kwargs):
        return self.file.__exit__(*args, **kwargs)

    def __enter__(self, *args, **kwargs):
        return self

    def __iter__(self):
        return self.lines()
