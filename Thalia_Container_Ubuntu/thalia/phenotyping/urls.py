#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url
from phenotyping.views import phenotypinghome, environment_management, treatment_management, trait_management, phenotypingvalue_management
from phenotyping.views import method_management, viewphenovalues
from dataview.views import return_csv

urlpatterns = [
                       url(r'^home/$', phenotypinghome, name='phenotypinghome'),
                       url(r'^environment/$', environment_management, name='environment_management'),
                       url(r'^treatment/$', treatment_management, name='treatment_management'),
                       url(r'^trait/$', trait_management, name='trait_management'),
                       url(r'^phenotypingvalue/$', phenotypingvalue_management, name='phenotypingvalue_management'),
                       url(r'^method/$', method_management, name='method_management'),
                       #url(r'^sources/$', 'sources_management', name='sources_management'),
                       url(r'^vphenovalues/$', viewphenovalues,name='viewphenovalues'),
                       url(r'^return_csv/(?P<filename>[\w]+)/$', return_csv, name='return_csv')
                    ]