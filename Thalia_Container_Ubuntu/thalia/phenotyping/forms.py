# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms
from django.forms import ModelForm

from phenotyping.models import Environment, SpecificTreatment, Trait,\
    PhenotypicValue
from team.models import Method, Documents
from accession.models import Accession
from django.contrib.admin.widgets import FilteredSelectMultiple

#=== Viewer ================================================================






class EnvironmentForm(ModelForm):
    class Meta:
        fields = '__all__'
        model = Environment

class SpecificTreatmentForm(ModelForm):
    class Meta:
        fields = '__all__'
        model = SpecificTreatment

class TraitForm(ModelForm):
    class Meta:
        fields = '__all__'
        model = Trait

#**************3/06/15************************************
   
class PhenotypingValueForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = PhenotypicValue
    

class UploadPhenoTypValueFileForm(forms.Form):
    
    environment = forms.ModelChoiceField(queryset=Environment.objects.all(),empty_label="Choose Environment")
    method = forms.ModelChoiceField(queryset=Method.objects.all(),empty_label="Choose Method")  
    main_file = forms.FileField(label='Main file')
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")

#====================================================================================================

CHOICES = [('1', 'View considering Accession',), 
           ('2', 'View considering Seed lot',), 
           ('3', 'View considering Sample',)]

class ViewPhenotypingValuesForm(forms.Form):
    
    accessions = forms.ModelMultipleChoiceField(queryset=Accession.objects.all(),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'10'}))
    traits = forms.ModelMultipleChoiceField(queryset=Trait.objects.all(), 
                                           widget=FilteredSelectMultiple("Trait",False,attrs={'rows':'10'})) #,widget=FilteredSelectMultiple("Subjects",False,attrs={'rows':'10'})
    environments = forms.ModelMultipleChoiceField(queryset=Environment.objects.all(), 
                                           widget=FilteredSelectMultiple("Environment",False,attrs={'rows':'10'})) 
    missing_data = forms.CharField(max_length=30, initial='missing')

    #forms.ChoiceField(widget=forms.RadioSelect(renderer=MyCustomRenderer), choices=CHOICES1)
