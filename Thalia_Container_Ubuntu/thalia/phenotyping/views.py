# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from datetime import date
import os
import cProfile
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.conf import settings

import csv
import io
import shlex
from django.http import HttpResponse
from accession.models import Accession
from commonfct.genericviews import generic_management
from phenotyping.models import Environment, SpecificTreatment, Trait,\
    PhenotypicValue
from phenotyping.forms import EnvironmentForm, SpecificTreatmentForm, TraitForm,\
    UploadPhenoTypValueFileForm, PhenotypingValueForm, ViewPhenotypingValuesForm
from commonfct.utils import get_fields
from team.models import Method, Documents, Project
from team.forms import MethodForm
from lot.models import Seedlot
from images.models import Image
from team.views import write_csv_search_result
from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm, PhenoViewerEnvironmentForm, DataviewPhenotypingValuesForm
from commonfct.utils import recode
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import pandas as pd
from django.db.models import Q
from django.db import IntegrityError
from django.apps import apps
from accession.forms import UploadFileForm
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def phenotypinghome(request):
    return render(request,'phenotyping/phenotyping_base.html',{"admin":True,})

@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def environment_management(request):
    form = EnvironmentForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name', 'date', 'location', 'description', 'specific treatment', 'projects']
    list_exceptions = ['id','phenotypicvalue']
    title = "Environment Management"
    model = Environment
    environments, nb_per_page = _get_data(request, model)
    filetype = "environment"
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        try:
            separator = request.POST['delimiter']
        except: 
            pass
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, separator, model)
        else:
            return generic_management(Environment, EnvironmentForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Environment Management")

    return refine_search(request, form, formf, environments, template, list_attributes, list_exceptions, title, model, filetype, typeid)
       
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def treatment_management(request):
    register_formFile = UploadFileForm()
    form = SpecificTreatmentForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name', 'decription']
    list_exceptions = ['environment','id']
    title = "Treatment Management"
    model = SpecificTreatment
    specific_treatments, nb_per_page = _get_data(request, model)
    filetype = 'treatment'
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        try:
            separator = request.POST['delimiter']
        except: 
            pass
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, separator, model)
        else:
            return generic_management(SpecificTreatment, SpecificTreatmentForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Treatment Management")

    return refine_search(request, form, formf, specific_treatments, template, list_attributes, list_exceptions, title, model, filetype, typeid)

        
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def trait_management(request):
    form = TraitForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name','abbreviation','measure unit','comments', 'projects']
    list_exceptions = ['id','phenotypicvalue',]
    title = "Trait Management"
    model = Trait
    traits, nb_per_page = _get_data(request, model)
    filetype = 'trait'
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        try:
            separator = request.POST['delimiter']
        except: 
            pass
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, separator, model)
        else:
            return generic_management(Trait, TraitForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Trait Management")

    return refine_search(request, form, formf, traits, template, list_attributes, list_exceptions, title, model, filetype, typeid)
     
@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def method_management(request):
    form = MethodForm()
    formf = UploadFileForm()
    template = 'phenotyping/phenotyping_generic.html'
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']
    title = "Method Management"
    model = Method
    methods, nb_per_page = _get_data(request, model)
    filetype = 'method'
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm(request.POST, request.FILES) 
        try:
            separator = request.POST['delimiter']
        except: 
            pass
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, separator, model)
        else:
            return generic_management(Method, MethodForm, request, 'phenotyping/phenotyping_generic.html',{"admin":True,},"Method Management")

    return refine_search(request, form, formf, methods, template, list_attributes, list_exceptions, title, model, filetype, typeid)
     
#*************3/06/15 ***************************************

def handle_uploads(request, keys):
    saved = []   
    upload_dir = date.today().strftime(settings.UPLOAD_PATH)
    upload_full_path = os.path.join(settings.ROOT_URL, upload_dir)

    if not os.path.exists(upload_full_path):
        os.makedirs(upload_full_path, 0o755)

    for key in keys:
        if key in request.FILES:
            upload = request.FILES[key]
            while os.path.exists(os.path.join(upload_full_path, upload.name)):
                upload.name = '_' + upload.name
            dest = open(os.path.join(upload_full_path, upload.name), 'wb')
            for chunk in upload.chunks():
                dest.write(chunk)
            dest.close() 
            saved.append((key, os.path.join(upload_dir, upload.name)))
    return saved


@login_required
@user_passes_test(lambda u: u.is_phenotyping_admin_user(), login_url='/login/')
def phenotypingvalue_management(request):
    phenotypingvalues = PhenotypicValue.objects.all()
    formattype = PhenotypingValueForm()
    register_formFile = UploadPhenoTypValueFileForm()
     
    names, labels = get_fields(formattype)
 
    template = 'phenotyping/phenotyping_values.html'
#     
    tag_fields = {'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  }
    
    if request.method == "GET":
        count = PhenotypicValue.objects.count()
        if count <1:
            count = 'no phenotypic data in database'

        warning = 'The field separator into your CSV file must be a "Semi-colons (;)"'
        tag_fields.update({'all':phenotypingvalues,
                           'form':register_formFile,
                           'creation_mode':True,
                           'allentries':count,
                           'warning':warning})
        return render(request, template, tag_fields)
        
    elif request.method == 'POST':   
        formf = UploadPhenoTypValueFileForm(request.POST, request.FILES)        
        if formf.is_valid():                             
            tag_fields.update({'form':formf,
                               'fields_label':labels,
                               'fields_name':names,
                               'creation_mode':True}) 
            return getphenoData(request, tag_fields, template)            
    else:
        return generic_management(PhenotypicValue, formf, request, template,{}, "PhenotypingValue Management")
    return render(request, template, {"admin":True,'form':formf,'creation_mode':True, 'all':phenotypingvalues, 'fields_label':labels,'fields_name':names})


#========================================================================


def getphenoData(request, tag_fields, template):
    main_f = request.FILES['main_file']
    #Source file
#     scdata = request.POST["source_data"]
    #Environment
    envi = request.POST["environment"]
    #Method
    meth = request.POST["method"]
    doc = None
    env = None
    method = None
     

#     try:
#         doc = Documents.objects.get(id=scdata)
#     except :
#         print()

    try:        
        env = Environment.objects.get(id=envi)
    except Environment.DoesNotExist :
        print()
        #messages.add_message(request, messages.INFO, 'Environment not found in database')
        errormsg='Environment not found in database'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)
    try:
        method = Method.objects.get(id=meth)
    except Method.DoesNotExist :
        print()
    separator = request.POST['delimiter']
    try:
        
        reader = csv.DictReader(io.StringIO(main_f.read().decode('utf-8')), delimiter=separator)
    except:
        try:
            reader = csv.DictReader(io.StringIO(main_f.read()), delimiter=separator)
        except:
            reader = csv.DictReader(io.BytesIO(main_f.read()), delimiter=separator)
    for row in reader:
        phenotypingvalues = PhenotypicValue.objects.all()
        print(row)
        try:
            seedlot = Seedlot.objects.get(name = row['Accession'])
        except:#modif TODO dico error
            errormsg = "This seedlot doesn't exist in the database : "+row['Accession']
            return render(request, 'phenotyping/phenotyping_values.html', {"phenotypingvalues":phenotypingvalues,"admin":True,'form':UploadPhenoTypValueFileForm, 'errormsg':errormsg})
            
        traits_db = Trait.objects.all()
        keys_row = [i for i in row.keys()]
        keys_row.remove('Accession')
        
        for traits_file in keys_row:
            try:
                trait = Trait.objects.get(name=traits_file)
                value = row[traits_file]
                phenoval, created = PhenotypicValue.objects.get_or_create(sourcedata=doc,environment=env,method=method,seedlot=seedlot,trait=trait,defaults={'value':value})
            except:
                errormsg = "This trait doesn't exist in the database : "+traits_file
                return render(request, 'phenotyping/phenotyping_values.html', {"phenotypingvalues":phenotypingvalues,"admin":True,'form':UploadPhenoTypValueFileForm, 'errormsg':errormsg})
            
    phenotypingvalues = PhenotypicValue.objects.all()
    count = PhenotypicValue.objects.count()
    if count<1 :
        count = 'no phenotypic data in database'
    tag_fields.update({'all':phenotypingvalues, 'allentries':count}) 
    return render(request, template, tag_fields)

def viewphenovalues(request):
    #headers = []
    error_msg=""
    headername = None
    if request.method=="POST":
        form2 = PhenoViewerAccessionForm(request.POST)
        formtrait = PhenoViewerTraitForm(request.POST)
        formenv = PhenoViewerEnvironmentForm(request.POST)
        if form2.is_valid() and formenv.is_valid() and formtrait.is_valid():
    #             print(request.POST
                if request.POST.getlist("name"):
                    id_autocomplete = request.POST.getlist("name")
                    listaccessions = Accession.objects.filter(id__in=id_autocomplete)   
                else:   
                    listaccessions = form2.cleaned_data['accession']
                listraits = formtrait.cleaned_data['trait']
                listenvs = formenv.cleaned_data['environment']   
    #             print(request.POST
                #============================
                mode = request.POST["choose_visualization_mode"]
                if mode == "1":
                    headername = 'Accession'
                else:
                    headername = 'Seedlot'     
                #===========================================================           
    
                if listaccessions!= None and listraits!= None and listenvs!= None :
                    df = getPhenodata(listaccessions, listraits, listenvs, headername)
                    if df.empty:
                        html=None
                    else:
                        html = df.to_html()    
                        html = html.split('\n')
    
                    pheno_html="phenotyping/phenotyping_base.html"
                    if request.POST["export_or_view_in_web_browser"]=="2":
                        return render(request, 'phenotyping/dtviewphenovalues.html', {"admin":True,"pheno_html":pheno_html,"html":html})
                    else:
                        return return_csv_pheno(request)
                        
                else:
                    if listaccessions==None:
                        error_msg="Please choose at least one accession (step 1)."
                    elif listraits==None:
                        error_msg="Please choose at least one trait (step 2)."
                    elif listenvs==None:
                        error_msg="Please choose at least one environment (step 3)."
                                        
                    return render(request, 'phenotyping/viewphenovalues.html', {"admin":True,'error_msg':error_msg,'accesform':PhenoViewerAccessionForm, 'traitform':PhenoViewerTraitForm, 'enviform':PhenoViewerEnvironmentForm, 'visumodeform':DataviewPhenotypingValuesForm})       
                          
                #========================================================
        else:
            if not request.POST.getlist("name") and not request.POST.getlist('accession'):
                error_msg="Please choose at least one accession (step 1)."
            elif not request.POST.getlist("trait"):
                error_msg="Please choose at least one trait (step 2)."
            elif not request.POST.getlist('environment'):   
                error_msg="Please choose at least one environment (step 3)."
    accesform = PhenoViewerAccessionForm()
    traitform = PhenoViewerTraitForm()
    enviform = PhenoViewerEnvironmentForm()
    visumodeform = DataviewPhenotypingValuesForm()
    return render(request,'phenotyping/viewphenovalues.html',{"admin":True,'error_msg':error_msg,'accesform':accesform, 'traitform':traitform, 'enviform':enviform, 'visumodeform':visumodeform})

def getPhenodata(listaccessions, listraits, listenvs, headername):
    datadict ={}
    traitslist = [exp for exp in listraits if exp != None]        
    envilist = [env for env in listenvs if env != None]
    acclist = [acc for acc in listaccessions if acc != None]
    headers = []
    seedlist=[]
    df_pheno=pd.DataFrame()
#     print(traitslist)
#     print (acclist)
    for trait in traitslist:
        for e in envilist:
            tr = str(trait.name) +'\n'+ str(e.name)
            for acces in acclist:
                seedlots=Seedlot.objects.filter(accession=acces)
                for seed in seedlots:
                    phenos=PhenotypicValue.objects.filter(seedlot=seed,environment=e,trait=trait)
                    for pheno in phenos:
                        print(acces,pheno.value)
                        if headername == 'Accession':
                            df_pheno.ix[seed,tr]=str(pheno.value).replace(',', '.')
                        else: 
                            df_pheno.ix[acces,tr]=str(pheno.value).replace(',', '.')
    df_pheno.to_csv("/tmp/view_pheno.csv", sep=';')
    return df_pheno

def FileToCSV(headers, datadict):
    try:
        myFilePath = "/tmp/view_pheno.csv"
        with open(myFilePath, 'w') as myCSVFILE:
            csvWriter = csv.writer(myCSVFILE, dialect='excel', delimiter = ';')
            csvWriter.writerow(headers)
            for key, value in datadict.items():
                line=[]
                line.append(key)                      
                for h in headers[1:]:
                    check=0
                    for i,j in value.items():
                        if i==h:
                            print("j: "+str(j))
                            line.append(j)
                            check=1
                    if check==0:
                        line.append('X')
                csvWriter.writerow(line)
    except IOError as errno:
        print("I/O error({0}): {1}".format(errno, errno))
    return  

def return_csv_pheno(request):
    tmpfile=open('/tmp/view_pheno.csv','r')  
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename=view_pheno.csv'
# <<<<<<< .working
#     return response
# 
# def refine_search(request, form, formf, data, template, list_attributes, list_exceptions, title, model, filetype, typeid):
#     number_all = len(data)
#     nb_per_page = 10
#     names, labels = get_fields(form)
#     tag_fields = {'all':data,
#               'fields_name':names,
#               'fields_label':labels,
#               'title': title, "admin":True,}
#     dico_get={}
#     or_and = None
#     if "no_search" in request.GET:
#         tag_fields.update({"search_result":dico_get,
#                        "or_and":or_and,
#                        'all':data,
#                        "number_all":number_all,
#                        'nb_per_page':nb_per_page,
#                        'creation_mode':True,
#                        'form':form,
#                        "list_attributes":list_attributes})
# 
#         return render(request,template,tag_fields)
#     
#     elif "or_and" in request.GET:
#         or_and=request.GET['or_and']
#         for i in range(1,10):
#             data="data"+str(i)
#             text_data="text_data"+str(i)
#             if data in request.GET and text_data in request.GET:
#                 dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
# #                 if or_and in request.GET:
# #                     dico_get[]
#         length_dico_get = len(dico_get)
#         nb_per_page = 10
#         if or_and == "or":
#             proj=[]
#             query=Q()
#             list_name_in=[]
#             list_attr=[]
#             for dat, dico_type_data in dico_get.items():
#                 if "projects" in dico_type_data:
#                     if dico_type_data['projects']=='':
#                         print('Projects empty')
#                         query &= Q(projects__isnull=True)
#                     else:
#                         proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
#                         query &= Q(projects__in=proj)
#                 else:
#                     for data_type, value in dico_type_data.items():
#                         var_search = str( data_type + "__icontains" ) 
#                         var_search = var_search.replace(' ','_')
#                         query |= Q(**{var_search : dico_type_data[data_type]})
#             
#         if or_and == "and":
#             proj=[]
#             query=Q()
#             list_name_in=[]
#             list_attr=[]
#             for dat, dico_type_data in dico_get.items():
#                 if "projects" in dico_type_data:
#                     if dico_type_data['projects']=='':
#                         print('Projects empty')
#                         query &= Q(projects__isnull=True)
#                     else:
#                         proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
#                         query &= Q(projects__in=proj)
#                 else:
#                     for data_type, value in dico_type_data.items():
#                         var_search = str( data_type + "__icontains" ) 
#                         var_search = var_search.replace(' ','_')
#                         print("----------",var_search)
#                         query &= Q(**{var_search : dico_type_data[data_type]})
# 
#         data = model.objects.filter(query).distinct()
#         print(data)
#         number_all = len(data)
#         download_not_empty = False
#         if data:
#             download_not_empty = write_csv_search_result(data, list_exceptions)
#         tag_fields.update({"download_not_empty":download_not_empty})
#     #Cas où veut afficher la base de données
#     #accessions = _get_accessions(accessiontype, attributes, request)
#     tag_fields.update({"search_result":dico_get,
#                        "or_and":or_and,
#                        'all':data,
#                        "number_all":number_all,
#                        'nb_per_page':nb_per_page,
#                        'creation_mode':True,
#                        'form':form,
#                        "list_attributes":list_attributes,
#                        "filetype":filetype,
#                        'excel_empty_file':True,
#                        "typeid":typeid,
#                        "formfile":formf})  
#     return render(request, template, tag_fields) 
# 
# def _get_data(request, model):
#     data = model.objects.all().order_by('name')
#     print(data)
# 
#     # Adding Paginator  
#     try:
#         if request.method == "GET":
#             nb_per_page=request.GET['nb_per_page']  
#         elif request.method == "POST":
#             nb_per_page=request.POST["nb_per_page"]   
#         if nb_per_page=="all":
#             nb_per_page = len(data)
#     except:
#         nb_per_page=50
#     
#         
#     paginator = Paginator(data, nb_per_page)
#     page = request.GET.get('page')
#     try:
#         data = paginator.page(page)
#     except PageNotAnInteger:
#         # If page number is not an interger then page one
#         data = paginator.page(1)
#     except EmptyPage:
#         # invalid page number show the last
#         data = paginator.page(paginator.num_pages)    
#     return data, nb_per_page     
#  #=============================
# 
# def _create_phenodata_from_file(request, template, tag_fields, separator, modelname):
#     file_uploaded = _upload_file(request.FILES['file'], request, separator, modelname)
#     if type(file_uploaded) is str : #1er cas: l'insertion provoque des erreurs
#         if file_uploaded.count(';') > 0 :
#             counter = file_uploaded.count(';') + 1
#             if counter == 1:
#                 errormsg = 'A {0} from your file is already in the database: '.format(str(modelname.__name__)) + file_uploaded
#             else:
#                 errormsg=  str(counter) + ' {0}s are already in your database: '.format(str(modelname.__name__)) + file_uploaded
#             tag_fields.update({'errormsg':errormsg})
#             return render(request, template, tag_fields)
#         else :
#             tag_fields.update({'errormsg':file_uploaded})
#             return render(request, template, tag_fields)
#     elif type(file_uploaded) is list:
#         #Cas où l'utilisateur tente un update mais que certaines données du CSV ne sont pas dans la BDD
#         missing_data = file_uploaded[0]
#         del file_uploaded[0]
#         if file_uploaded !=[] and len(file_uploaded) ==1:
#             missing_data= missing_data + ' ,' + file_uploaded[0]
#         elif file_uploaded != [] and len(file_uploaded)>1:
#             missing_data = missing_data + ' ,' +  '  ,'.join(file_uploaded)
#         errormsg = 'Some {0}s of your file aren\'t in your database: '.format(str(modelname)) + missing_data + ' .No update was done.'
#         tag_fields.update({'errormsg':errormsg})
#         return render(request, template, tag_fields)
#     else:
#         #Cas d'une insertion où d'un update fonctionnel
#         inserted = True
#         data, nb_per_page = _get_data(request,modelname)
#         data_all = modelname.objects.all().distinct()
#         number_all = len(data_all)
#         tag_fields.update({'inserted': inserted,'all':data, "nb_per_page":nb_per_page, "number_all":number_all})
#         return render(request, template, tag_fields)
# 
# 
# def _upload_file(files, request, separator, modelname):
#     if files != None:
#         dictline = {}
#         i = 0
#         for line in files:
#             #On récurère chaque ligne du fichier: la case i contiendra la i-1 ème ligne du fichier
#             dictline[i] = line.strip()
#             i+=1
#         headers = recode(dictline[0])
#         #la première ligne du tableau donne attributs du fichier; on les conserve dans un autre tableau et on efface la première ligne de dictvalues
# #         print(type(headers))
#         theaders = headers.split(separator)
#         dictvalues_final = {}
#         del dictline[0]
#         t=0
#         try:
#             for key in dictline:
#                 #On récupère les valeurs correspondant aux clés
#                 v = recode(dictline[key])
#                 v = io.StringIO(v)
#                 csv_val = csv.reader(v, delimiter = separator)
#                 for val in csv_val:
#                     list_val = val
# 
#                 dictvalues = {}
#                 for i in range(len(theaders)): 
#                     #On remplit dictvalues avec les clés et valeur              
#                     dictvalues[theaders[i]] = list_val[i]
#                 #On stocke le dictionnaire obtenu dans un autre dictionnaire: la case i contiendra la i+1 ème ligne du fichier
#                 dictvalues_final[t] = dictvalues
#                 print(dictvalues)
#                 t+=1
#         except IndexError: #Cas où certaines lignes seraient vides, par exemple, si l'utilisateur est passé à la ligne après avoir inséré la dernière donnée de son .csv
#             return 'There are empty lines in your file. Please fill or remove them.'
#         else:
#             if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
#                 return _submit_upload_file(dictvalues_final, modelname)
#             elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
#                 return _update_upload_file(dictvalues_final, theaders)
#             else:
#                 database=Accession.objects.all().values_list('name') 
#                 return 'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.'
#             
# def _submit_upload_file(dictvalues_final, modelname):
#     '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
#     S'il y a au moins un doublon, aucune donnée n'est insérée : soit l'utilisateur voulait faire un update, soit il ne s'est pas apercu des doublons
#     Tout d'abord, on extrait les noms des accessions (clé primaire) de la BDD'''
#     try:
#         database=modelname.objects.all().values_list('name') 
#         keys_in_database=[i[0] for i in database]
#         #Maintenant, on récupère les noms des METHODS du CSV, contenues dans dictvalues_final
#         keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
#         print(keys_inserted)
#     except KeyError:
#         return '1: The headers of your files do not correspond to those in the database. Please check them'
#     else:
#         #On va maintenant comparer les deux listes
#         data_doublons='' #Ce String stockera les éventuels doublons
#         for m in range(len(keys_inserted)):
#             for n in range(len(keys_in_database)):
#                 if keys_inserted[m]==keys_in_database[n]:
#                     if data_doublons =='':
#                         data_doublons = keys_inserted[m]
#                     else:
#                         data_doublons = data_doublons + ' ;' + keys_inserted[m]
# #         print('data_doublons')
#         if set(keys_in_database) == set(keys_inserted):
#             return 'All the methods in your CSV file are already in your database. Maybe you are trying to update former data ?'
#         else:
#             if data_doublons != '': 
#                 return data_doublons
#             else: #Cas de l'insertion
#                 jsonvalues={}
#                 try:
#                     for k in dictvalues_final:
#                         #On récupère la k+1 ème ligne du fichier
#                         dval = dictvalues_final[k]
#                         try :
#                             if modelname == Method or modelname == SpecificTreatment:
#                                 a  = modelname.objects.create(name = str(dval['Name']),
#                                                        description = str(dval['Description'])
#                                                      )
#                             elif modelname == Trait:
#                                 a = modelname.objects.create(name = str(dval['Name']),
#                                                          abbreviation = str(dval['Abbreviation']),
#                                                          measure_unit = str(dval['Measure Unit']),
#                                                          comments = str(dval['Comments']),
#                                                          )
#                                 project_list = [i.strip() for i in str(dval['Projects']).split('|')]
#                                 projects = Project.objects.filter(name__in = project_list)
#                                 if projects.exists():
#                                     a.projects.add(*list(projects))
#                             elif modelname == Environment:
#                                 a  = modelname.objects.create(name = str(dval['Name']),
#                                                               date = str(dval['Date']),
#                                                               location = str(dval['Location']),
#                                                               specific_treatment = SpecificTreatment.objects.get(name = str(dval['Specific treatment'])),
#                                                               description = str(dval['Description'])
#                                                               )
#                                 project_list = [i.strip() for i in str(dval['Projects']).split('|')]
#                                 projects = Project.objects.filter(name__in = project_list)
#                                 if projects.exists():
#                                     a.projects.add(*list(projects))
#                         except IntegrityError as ie :
#                             return str(ie)
#                         
#                 except KeyError:
#                     return '2: The headers of your files do not correspond to those in the database. Please check them and mind the uppercase and the lowercase.'
#                 
#                 except IntegrityError as ie:
#                     return str(ie)
#             =======
    return response

def refine_search(request, form, formf, data, template, list_attributes, list_exceptions, title, model, filetype, typeid):
    number_all = len(data)
    nb_per_page = 10
    names, labels = get_fields(form)
    tag_fields = {'all':data,
              'fields_name':names,
              'fields_label':labels,
              'title': title, "admin":True,}
    dico_get={}
    or_and = None
    if "no_search" in request.GET:
        tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes})

        return render(request,template,tag_fields)
    
    elif "or_and" in request.GET:
        or_and=request.GET['or_and']
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
        length_dico_get = len(dico_get)
        nb_per_page = 10
        if or_and == "or":
            proj=[]
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Q(**{var_search : dico_type_data[data_type]})
            
        if or_and == "and":
            proj=[]
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        print("----------",var_search)
                        query &= Q(**{var_search : dico_type_data[data_type]})

        data = model.objects.filter(query).distinct()
        print(data)
        number_all = len(data)
        download_not_empty = False
        if data:
            download_not_empty = write_csv_search_result(data, list_exceptions)
        tag_fields.update({"download_not_empty":download_not_empty})
    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       "filetype":filetype,
                       'excel_empty_file':True,
                       "typeid":typeid,
                       "formfile":formf})  
    return render(request, template, tag_fields) 

def _get_data(request, model):
    if model.__name__ == "Image":
        data = model.objects.all().order_by('image_name')
    else:
        data = model.objects.all().order_by('name')

    # Adding Paginator  
    try:
        if request.method == "GET":
            nb_per_page=request.GET['nb_per_page']  
        elif request.method == "POST":
            nb_per_page=request.POST["nb_per_page"]   
        if nb_per_page=="all":
            nb_per_page = len(data)
    except:
        nb_per_page=50
    
        
    paginator = Paginator(data, nb_per_page)
    page = request.GET.get('page')
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an interger then page one
        data = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        data = paginator.page(paginator.num_pages)    
    return data, nb_per_page     
 #=============================

def _create_phenodata_from_file(request, template, tag_fields, separator, modelname):
    file_uploaded = _upload_file(request.FILES['file'], request, separator, modelname)
    if type(file_uploaded) is str : #1er cas: l'insertion provoque des erreurs
        if file_uploaded.count(';') > 0 :
            counter = file_uploaded.count(';') + 1
            if counter == 1:
                errormsg = 'A {0} from your file is already in the database: '.format(str(modelname.__name__)) + file_uploaded
            else:
                errormsg=  str(counter) + ' {0}s are already in your database: '.format(str(modelname.__name__)) + file_uploaded
            tag_fields.update({'errormsg':errormsg})
            return render(request, template, tag_fields)
        else :
            tag_fields.update({'errormsg':file_uploaded})
            return render(request, template, tag_fields)
    elif type(file_uploaded) is list:
        #Cas où l'utilisateur tente un update mais que certaines données du CSV ne sont pas dans la BDD
        missing_data = file_uploaded[0]
        del file_uploaded[0]
        if file_uploaded !=[] and len(file_uploaded) ==1:
            missing_data= missing_data + ' ,' + file_uploaded[0]
        elif file_uploaded != [] and len(file_uploaded)>1:
            missing_data = missing_data + ' ,' +  '  ,'.join(file_uploaded)
        errormsg = 'Some {0}s of your file aren\'t in your database: '.format(str(modelname.__name__)) + missing_data + ' .No update was done.'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)
    else:
        #Cas d'une insertion où d'un update fonctionnel
        inserted = True
        data, nb_per_page = _get_data(request,modelname)
        data_all = modelname.objects.all().distinct()
        number_all = len(data_all)
        tag_fields.update({'inserted': inserted,'all':data, "nb_per_page":nb_per_page, "number_all":number_all})
        return render(request, template, tag_fields)


def _upload_file(files, request, separator, modelname):
    if files != None:
        dictline = {}
        i = 0
        for line in files:
            #On récurère chaque ligne du fichier: la case i contiendra la i-1 ème ligne du fichier
            dictline[i] = line.strip()
            i+=1
        headers = recode(dictline[0])
        #la première ligne du tableau donne attributs du fichier; on les conserve dans un autre tableau et on efface la première ligne de dictvalues
#         print(type(headers))
        theaders = headers.split(separator)
        dictvalues_final = {}
        del dictline[0]
        t=0
        try:
            for key in dictline:
                #On récupère les valeurs correspondant aux clés
                v = recode(dictline[key])
#                 val = shlex.shlex(v,posix=True)
#                 val.whitespace = separator
#                 val.whitespace_split = True
#                 val = list(val)
                val = v.split(separator)
                dictvalues = {}
                for i in range(len(theaders)): 
                    #On remplit dictvalues avec les clés et valeur              
                    dictvalues[theaders[i]] = val[i]
                #On stocke le dictionnaire obtenu dans un autre dictionnaire: la case i contiendra la i+1 ème ligne du fichier
                dictvalues_final[t] = dictvalues
                t+=1
        except IndexError: #Cas où certaines lignes seraient vides, par exemple, si l'utilisateur est passé à la ligne après avoir inséré la dernière donnée de son .csv
            return 'There are empty lines in your file. Please fill or remove them.'
        else:
            if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
                return _submit_upload_file(dictvalues_final, modelname)
            elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
                return _update_upload_file(dictvalues_final, modelname)
                #return _update_upload_file(dictvalues_final, theaders)
                #return 'This function is not available at the moment.'
            else:
                database=Accession.objects.all().values_list('name') 
                return 'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.'
            
def _submit_upload_file(dictvalues_final, modelname):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins un doublon, aucune donnée n'est insérée : soit l'utilisateur voulait faire un update, soit il ne s'est pas apercu des doublons
    Tout d'abord, on extrait les noms des accessions (clé primaire) de la BDD'''
    try:
        database=modelname.objects.all().values_list('name') 
        keys_in_database=[i[0] for i in database]
        #Maintenant, on récupère les noms des METHODS du CSV, contenues dans dictvalues_final
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
        print(keys_inserted)
    except KeyError:
        return '1: The headers of your files do not correspond to those in the database. Please check them'
    else:
        #On va maintenant comparer les deux listes
        data_doublons='' #Ce String stockera les éventuels doublons
        for m in range(len(keys_inserted)):
            for n in range(len(keys_in_database)):
                if keys_inserted[m]==keys_in_database[n]:
                    if data_doublons =='':
                        data_doublons = keys_inserted[m]
                    else:
                        data_doublons = data_doublons + ' ;' + keys_inserted[m]
#         print('data_doublons')
        if set(keys_in_database) == set(keys_inserted):
            return 'All the methods in your CSV file are already in your database. Maybe you are trying to update former data ?'
        else:
            if data_doublons != '': 
                return data_doublons
            else: #Cas de l'insertion
                jsonvalues={}
                try:
                    for k in dictvalues_final:
                        #On récupère la k+1 ème ligne du fichier
                        dval = dictvalues_final[k]
                        try :
                            if modelname == Method or modelname == SpecificTreatment:
                                a  = modelname.objects.create(name = str(dval['Name']),
                                                       description = str(dval['Description'])
                                                     )
                            elif modelname == Trait:
                                a = modelname.objects.create(name = str(dval['Name']),
                                                         abbreviation = str(dval['Abbreviation']),
                                                         measure_unit = str(dval['Measure Unit']),
                                                         comments = str(dval['Comments']),
                                                         )
                                project_list = [i.strip() for i in str(dval['Projects']).split('|')]
                                projects = Project.objects.filter(name__in = project_list)
                                if projects.exists():
                                    a.projects.add(*list(projects))
                            elif modelname == Environment:
                                a  = modelname.objects.create(name = str(dval['Name']),
                                                              date = str(dval['Date']),
                                                              location = str(dval['Location']),
                                                              specific_treatment = SpecificTreatment.objects.get(name = str(dval['Specific treatment'])),
                                                              description = str(dval['Description'])
                                                              )
                                project_list = [i.strip() for i in str(dval['Projects']).split('|')]
                                projects = Project.objects.filter(name__in = project_list)
                                if projects.exists():
                                    a.projects.add(*list(projects))
                        except IntegrityError as ie :
                            return str(ie)
                        
                except KeyError:
                    return '2: The headers of your files do not correspond to those in the database. Please check them and mind the uppercase and the lowercase.'
                
                except IntegrityError as ie:
                    return str(ie)
                
def _update_upload_file(dictvalues_final, modelname):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins une nouvelle donnee, rien n'est mis à jour
    '''
    try:
        database=modelname.objects.all().values_list('name') 
        keys_in_database=[i[0] for i in database]
        #Maintenant, on récupère les noms des METHODS du CSV, contenues dans dictvalues_final
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
        print(keys_inserted)
    except KeyError:
        return '1: The headers of your files do not correspond to those in the database. Please check them'
    else:
        #Ce hash stocke les donnees en commun entre le fichier et la base
        data = {} 
        for m in range(len(keys_inserted)):
            for n in range(len(keys_in_database)):
                if keys_inserted[m]==str(keys_in_database[n]):
                        if not keys_inserted[m] in data:
                            data[keys_inserted[m]] = '1'
        #Cette liste stocke les donnees qui n'existent pas dans la base
        list_data = []
        for key in keys_inserted:
            if not key in data:
                list_data.append(key)
                                     
        if list_data != []:
            return list_data
        else: #S'il n'y a que des donnees connues
            keys_updated = 0
            for k in dictvalues_final:
                dval = dictvalues_final[k]
                try:
                    if modelname == Method or modelname == SpecificTreatment:
                        a  = modelname.objects.get(name = str(dval['Name']))
                        if a.description != str(dval['Description']):
                            setattr(a, 'description', str(dval['Description']))
                            a.save()
                            keys_updated = 1
                                
                    elif modelname == Trait:
                        a = modelname.objects.get(name = str(dval['Name']))
                        if a.abbreviation != str(dval['Abbreviation']):
                            setattr(a,'abbreviation', str(dval['Abbreviation']))
                            a.save()
                            keys_updated = 1
                                
                        if a.measure_unit != str(dval['Measure Unit']):
                            setattr(a,'measure_unit', str(dval['Measure Unit']))
                            a.save()
                            keys_updated = 1
                                
                        if a.comments != str(dval['Comments']):
                            setattr(a,'comments', str(dval['Comments']))
                            a.save()
                            keys_updated = 1
                    
                    elif modelname == Environment:
                        a  = modelname.objects.get(name = str(dval['Name']))
                        if str(a.date) != str(dval['Date']):
                            setattr(a,'date', str(dval['Date']))
                            a.save()
                            keys_updated = 1
                            
                        if a.location != str(dval['Location']):
                            setattr(a,'location', str(dval['Location']))
                            a.save()
                            keys_updated = 1
                            
                        if a.specific_treatment != SpecificTreatment.objects.get(name = str(dval['Specific treatment'])):
                            setattr(a,'specific_treatment', SpecificTreatment.objects.get(name = str(dval['Specific treatment'])))
                            a.save()
                            keys_updated = 1
                            
                        if a.description != str(dval['Description']):
                            setattr(a,'description', str(dval['Description']))
                            a.save()
                            keys_updated = 1
                    else:
                        return 'An error has occurred, please contact an administrator.'
                            
                except:
                    return '{0} exists in the database with other parameters.'.format(str(dval['Name'])) + ' No update was done.'
                
            if keys_updated == 0:
                return 'All the entries are identical to those previously inserted. No update was done.'