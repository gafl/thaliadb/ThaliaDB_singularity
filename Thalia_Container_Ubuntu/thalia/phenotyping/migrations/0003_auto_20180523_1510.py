# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-23 13:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('phenotyping', '0002_auto_20170119_1043'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phenotypicvalue',
            name='sourcedata',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='team.Documents'),
        ),
    ]
