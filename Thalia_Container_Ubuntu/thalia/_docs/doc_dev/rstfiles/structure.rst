=========================
Structure App Description
=========================

Structure models
================
.. automodule:: classification.models
    :members: 

Structure views
===============
.. automodule:: classification.views
    :members:
    
Structure tests
===============
.. automodule:: classification.tests
    :members:
    