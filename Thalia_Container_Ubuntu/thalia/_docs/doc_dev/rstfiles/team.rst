====================
Team App Description
====================

Team models
===========
.. automodule:: team.models
    :members:
    
Team forms
==========
.. automodule:: team.forms
    :members:  
    
Team managers
=============
.. automodule:: team.managers
    :members:  

Team views
==========
.. automodule:: team.views
    :members:
    
Team tests
==========
.. automodule:: team.tests
    :members: