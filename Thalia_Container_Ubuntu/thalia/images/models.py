# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
LICENCE"""

from django.db import models

from django.utils.translation import ugettext as _
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


class Image(models.Model):
    """
       The image model gives informations about images and their storage.
       
        :var CharField image_name: name of the image
        :var FileField image_path: storage path of the image
        :var DateField date: date when the image was taken
        :var CharField comment: comment about the image
        :var CharField footprint: footprint of the image in SHA-1 encoding
    """
    fs = FileSystemStorage(location=settings.MEDIA_ROOT)
        
    image_name = models.CharField(_("Name"), max_length=255, blank=True, null=True)
    image_path = models.FileField(_("Image path"), max_length=255, upload_to= "images", storage=fs)
    date = models.DateField(_("Date"), blank=True, null=True)
    comment = models.CharField(_("Comment"), max_length=255, blank=True, null=True)
    footprint = models.CharField(_("Footprint"), max_length=255)
    
    def __unicode__(self):
        return u"%s"%self.image_name
    def __str__(self):
        return "%s"%self.image_name


class ImageLink(models.Model):
    """
       The imagelink model enable to link images to entities.
       
        :var ForeignKey image : link to the image
        :var CharField entity_name: name of the considered entity 
        :var ForeignKey content_type: foreign key to the table of the considered entity
        :var PositiveIntegerField object_id: id of the entity in the table
    """
    
    image = models.ForeignKey(Image)
    entity_name = models.CharField(_("Name"), max_length=255, blank=True, null=True)


    limit = models.Q(app_label = 'accession', model = 'accession') \
            | models.Q(app_label = 'lot', model = 'seedlot') \
            | models.Q(app_label = 'phenotyping', model = 'environment')
            
            
    # Django Generic Foreign Key
    # https://docs.djangoproject.com/fr/1.11/ref/contrib/contenttypes/#generic-relations
    content_type = models.ForeignKey(ContentType, limit_choices_to = limit, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    entity = GenericForeignKey('content_type', 'object_id')
    
    def __str__(self):
        return self.entity_name


@receiver(pre_delete, sender=Image)
def image_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.image_path.delete(False)