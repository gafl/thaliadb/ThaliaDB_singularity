# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import io
import csv
import os

import hashlib

from django.db.models import Q
from django.contrib.auth.decorators import user_passes_test, login_required
from django.http import HttpResponse
# from tornado.test.concurrent_test import CapError
from django.conf import settings
from django.shortcuts import render
from django.views import generic
from django.db import transaction
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView
from django.http.response import HttpResponseRedirect

from commonfct.genericviews import get_fields
from team.views import write_csv_search_result
from classification.forms import *
from accession.models import Accession
from lot.models import Seedlot
from images.forms import MultipleImageForm
from phenotyping.models import Environment
from phenotyping.views import _get_data

# from lot.forms import SeedlotDataForm
from images.models import Image, ImageLink
from images.forms import ImageForm, ImageLinkForm, ImageManagementForm
from commonfct.genericviews import generic_management

from PIL import Image as ImagePil

from team.models import Project

from django_bootstrap3_form import form
from django_file_form.models import UploadedFile
from django_file_form.util import url_reverse as reverse


from images import forms

#================ IMAGES ==================#

@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def img_management(request):
    """
        View to manage Images.
    """
    list_attributes=['image_name', 'date', 'comment']
    template = 'images/image_generic.html'
    form = ImageForm()
    model = Image
    #data = Image.objects.all()
    data, nb_per_page = _get_data(request, model)
    title = "Image Management"
    filetype = 'images'
    all = Image.objects.all()
    fields_name, fields_label = (['image_name', 'date', 'comment'], ['Image Name', 'Date', 'Comment'])
    
       
    if request.method == "GET":
        return refine_search(request, form, data, template, list_attributes, model)
            
    if request.method == "POST":
        
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            image_name=form.cleaned_data['image_path']
            
            #Calcul du footprint de l'image chargée
            file = request.FILES["image_path"]            
            stream = file.read()
            crypt = hashlib.sha1()
            crypt.update(stream)
            footprint = crypt.hexdigest()
            
        
            #Si aucune image n'a ce footprint, on la créé
            if not Image.objects.filter(footprint=footprint):
                
                #check the file's format
                format = (str(image_name).split('.'))[1]
                accepted_formats = ["JPG", "jpg", "png", "PNG", "JPEG", "jpeg"]
                print("FORMAT : "+format)
                if format not in accepted_formats:
                    form = ImageForm()
                    error = "Not the good extension (only \"jpg, png, jpeg\")."
                    return render(request,template,{"admin":True, 'error':error, 'fields_name':fields_name, 'fields_label':fields_label, 'all': all, 'form': form, 'model': Image, 'list_attributes':list_attributes, 'special_edit':'document', "MEDIA_ROOT":settings.MEDIA_ROOT,'filetype':'image','typeid':0},"Images Management")

                #Create image
                newobject = form.save()
                

                newobject.image_name = form.cleaned_data['image_path']
                newobject.image_name = str(newobject.image_name).replace(" ","_")
                newobject.save()
                
                newobject.footprint = footprint
                newobject.save()                
                
                image = Image.objects.get(image_name=newobject.image_name)
                
                select_acc = form.cleaned_data['accession']
                select_seed = form.cleaned_data['seedlot']
                select_env = form.cleaned_data['environment']
                        
                #Ajout seedlot
                for seed in select_seed:
                    if not ImageLink.objects.filter(image=image, entity_name=seed.name):
                        l = ImageLink(image=image, entity=seed, entity_name=seed.name)
                        l.save()
                    
                #Ajout accession
                for acc in select_acc:
                    if not ImageLink.objects.filter(image=image, entity_name=acc.name):
                        l = ImageLink(image=image, entity=acc, entity_name=acc.name)
                        l.save()
                        
                #Ajout environment
                for env in select_env:
                    if not ImageLink.objects.filter(image=image, entity_name=env.name):
                        l = ImageLink(image=image, entity=env, entity_name=env.name)
                        l.save()
                
                #Reset le formulaire
                form = ImageForm() 
                return render(request,template,{"admin":True, 'success': image_name, 'fields_name':fields_name, 'fields_label':fields_label, 'all': all, 'form': form, 'model': Image, 'list_attributes':list_attributes, 'special_edit':'document', "MEDIA_ROOT":settings.MEDIA_ROOT,'filetype':'image','typeid':0},"Images Management")

            #image exists (name)      
            else:
                form = ImageForm()
                error = "Image \""+ str(image_name) + "\" already exists."
                return render(request,template,{"admin":True, 'error':error, 'fields_name':fields_name, 'fields_label':fields_label, 'all': all, 'form': form, 'model': Image, 'list_attributes':list_attributes, 'special_edit':'document', "MEDIA_ROOT":settings.MEDIA_ROOT,'filetype':'image','typeid':0},"Images Management")

        
    form = ImageForm() 

    return render(request,template,{"admin":True, 'fields_name':fields_name, 'fields_label':fields_label, 'all': all, 'form': form, 'model': Image, 'list_attributes':list_attributes, 'special_edit':'document', "MEDIA_ROOT":settings.MEDIA_ROOT},"Images Management")


#------------------------ TEST FORMULAIRES MULTIPLES --------------------------------------

# Vue des link

@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def imgLink_management(request):
    """
        View to manage Image Link (not used).
    """
    return generic_management(ImageLink, ImageLinkForm, request, 'team/team_generic.html',{"admin":True},"Images Link Management")


    
#-------------------------------------------------------------------------------------------


@login_required
@user_passes_test(lambda u: u.is_team_admin_user(), login_url='/login/')
def image_manager(request, pk):
    """
    Management of 1 image. Here we can see and change
    image's informations.
    
    :param int pk : primary key of the image
    
    :var Image image: image to manage
    :var list select_acc: accession to link 
    :var list select_seed: seedlot to link 
    :var list select_env: environment to link
    :var list list_links: all the created links
    """
    
    #Image sur laquelle on est
    image = Image.objects.get(id=pk)
    
    #Liste des liens avec cette image
    list_links = ImageLink.objects.filter(image=image)

    if request.method == "GET":
    
        #Liste des accessions et seedlots et environment associés à cette image
        list_accessions=[]
        list_seedlots=[]
        list_environments=[]
        
        for link in list_links:
            if(link.content_type.name == "accession"):
                list_accessions.append(Accession.objects.get(id=link.object_id))
            elif(link.content_type.name == "seedlot"):
                list_seedlots.append(Seedlot.objects.get(id=link.object_id))
            elif(link.content_type.name == "environment"):
                list_environments.append(Environment.objects.get(id=link.object_id))
        
        form_class = ImageManagementForm
        form = form_class(initial={'accession': list_accessions,
                                'seedlot': list_seedlots,
                                'environment': list_environments,
                                'date': image.date,
                                'comment' : image.comment})
        
        return render(request, "images/image_manager.html", {"admin":True, "image":image, "form":form})

    #Si on a rempli le formulaire d'Update
    form = ImageManagementForm(request.POST)

    if form.is_valid():
        select_acc = form.cleaned_data['accession']
        select_seed = form.cleaned_data['seedlot']
        select_env = form.cleaned_data['environment']
        date = form.cleaned_data['date']
        comment = form.cleaned_data['comment']
        
        list_links = []
        
        #Ajout seedlot
        for seed in select_seed:
            list_links.append(ImageLink.objects.filter(image=image, entity_name=seed.name))
            if not ImageLink.objects.filter(image=image, entity_name=seed.name):
                l = ImageLink(image=image, entity=seed, entity_name=seed.name)
                l.save()
            
        #Ajout accession
        for acc in select_acc:
            list_links.append(ImageLink.objects.filter(image=image, entity_name=acc.name))
            if not ImageLink.objects.filter(image=image, entity_name=acc.name):
                l = ImageLink(image=image, entity=acc, entity_name=acc.name)
                l.save()

        #Ajout environment
        for env in select_env:
            list_links.append(ImageLink.objects.filter(image=image, entity_name=env.name))
            if not ImageLink.objects.filter(image=image, entity_name=env.name):
                l = ImageLink(image=image, entity=env, entity_name=env.name)
                l.save()
        
        #Modification date et comment  
        image.date = date
        image.comment = comment
        image.save()
        
        #Suppression de liens    
        
        #Liste contenant les liens sélectionnés
        select_links = []
        for link in list_links:
            for l in link:
                select_links.append(l)
        
        all_links = ImageLink.objects.filter(image=image)
        
        #Si des liens non sélectionnés existe, on supprime
        for link in all_links:
            if link not in select_links:
                link.delete()
        
    return render(request, "images/image_manager.html", {"admin":True, "image":image, "form":form, "update":True})


#=========================== AFFICHAGE IMAGES =================================#


@login_required
def images_renderer(request, pk):
    """
    :param int pk: primary key of the image
    
    Create an HTTP Response with the image
    """
    
    image = Image.objects.get(id=pk)
    image_name = image.image_name

    stream = open(settings.MEDIA_ROOT+'images/'+image_name,'rb')
    #building and returning the response --> it returns a msexcel file (CSV format)
    response = HttpResponse(stream.read(), content_type='image')
    #response['Content-Disposition'] = 'attachment; filename="'+file_type+'.csv"'
    return response
    
    
    
#=========================== OTHERS FUNCTIONS =================================#   

def refine_search(request, form, data, template, list_attributes, model):
    """
    Other version of refine search for images
    """
    number_all = len(data)
    nb_per_page = 10
    names, labels = (['image_name', 'date', 'comment'], ['Image Name', 'Date', 'Comment'])
    tag_fields = {'all':data,
              'fields_name':names,
              'fields_label':labels,
              'admin':True,}
    dico_get={}
    or_and = None
    print("youhou")
    if "no_search" in request.GET:
        print("nono")

        uploadForm = MultipleImageForm()

        tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       'uploadForm':uploadForm})

        return render(request,template,tag_fields)
    
    elif "or_and" in request.GET:
        print("or_and")
        or_and=request.GET['or_and']
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
        length_dico_get = len(dico_get)
        nb_per_page = 10
        if or_and == "or":
            proj=[]
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Q(**{var_search : dico_type_data[data_type]})
            
        if or_and == "and":
            proj=[]
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        print("----------",var_search)
                        query &= Q(**{var_search : dico_type_data[data_type]})

        data = model.objects.filter(query).distinct()
        print(data)
        number_all = len(data)
        
    print("nothing")
    uploadForm = MultipleImageForm()

    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       'special_edit':'document',
                       'uploadForm':uploadForm,
                       'filetype':'image',
                       'typeid':0,}) 
    
    return render(request, template, tag_fields) 


    
    
def recode(string):
    encoding = ['ascii', 'utf-8','iso-8859-1','utf-16']
    for e in encoding :
        try :
            return string.decode(e)
        except :
            pass
    raise UnicodeDecodeError("%s can't be decoded"%string)


    
#------------------------ FORMULAIRES MULTIPLES --------------------------------------

# Vues importées de shinemas


TEMP_DIR = settings.TEMP_DIR

IMAGE_NAME = 'IMAGE_NAME'
DATE = 'DATE'
COMMENT = 'COMMENT'
ACCESSION = 'ACCESSION'
SEEDLOT = 'SEEDLOT'
ENVIRONMENT = 'ENVIRONMENT'


class BaseFormView(generic.FormView):
    """ This view manage the upload of images and parse index 
        file informations (if an index file is sent) 
    """
    template_name = 'images/images_upload.html'
    
    def get_success_url(self):
        return reverse('link_images')
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['admin'] = True
        return context
    
    def form_valid(self, form):
        infos_upload = {}
        size = (250, 250)        
        
        # Creation of thumbnails, and in the same time verification that it's image files
        for f in form.cleaned_data.get('input_file'):
            outfile = f.file_id + ".thumbnail"
            if f != outfile:
                try:
                    im = ImagePil.open(f)
                    im.thumbnail(size)
                    im.save(TEMP_DIR + outfile, "JPEG")
                except IOError:
                    messages.add_message(self.request, messages.ERROR, 'The file named : '+ str(f)+ ' is not an image.')  
                    return self.form_invalid(form)
                
        try:
            # If an index file (input_index_file) is submitted, it's parsed and infos are stored in a dict with images' details
            # Use of Sniffer function to deduce the format of the file
            # https://docs.python.org/3.4/library/csv.html#csv.Sniffer
            if form.cleaned_data.get('input_index_file'):
                
                csv_file = form.cleaned_data.get('input_index_file')
                stream_init = csv_file.read()
                stream = recode(stream_init) #we call the recode function to avoid encoding errors             
                f = io.StringIO(stream)
                dialect = csv.Sniffer().sniff(f.read())
                f = io.StringIO(stream)
                index_file = csv.DictReader(f, dialect = dialect) 
                
                
                dict_img = {}
                
                for f in form.cleaned_data.get('input_file'):
                    dict_img[str(f)] = f
                   
                list_files = dict_img.keys()
                
                # All the informations about the image are stored in list detail_img 
                # and next, the list is saved in dict upload_infos
                
                for line in index_file: 
                    if line[IMAGE_NAME] in list_files:
                        
                        details_img = []
                        details_img.append(dict_img[line[IMAGE_NAME]].file_id)
                        details_img.append(line[IMAGE_NAME])
                        details_img.append(line[DATE])
                        details_img.append(line[COMMENT])
                                
                        list_a = []
                        list_s = []
                        list_e = []
             
                        if line[ACCESSION]:
                            accession_list = line[ACCESSION].split(';')
                            for accession in accession_list:
                                a = accession.strip(' ')
                                list_a.append(a)
                               
     
                        if line[SEEDLOT]:
                            seedlot_list = line[SEEDLOT].split(';')
                            for seedlot in seedlot_list:
                                s = seedlot.strip(' ')             
                                list_s.append(s)
                                
                        if line[ENVIRONMENT]:
                            environment_list = line[ENVIRONMENT].split(';')
                            for environment in environment_list:
                                e = environment.strip(' ')             
                                list_e.append(e)
                                            
                        details_img.append(list_a)
                        details_img.append(list_s)
                        details_img.append(list_e)
                                
                        if line[IMAGE_NAME] not in infos_upload.keys():
                            infos_upload[line[IMAGE_NAME]] = details_img
                        else:
                            messages.add_message(self.request, messages.WARNING, 'There is more than one line for the image '+ line[IMAGE_NAME]+ ' in the index file.')  
                                
                    else:
                        messages.add_message(self.request, messages.WARNING, 'Image '+ line[IMAGE_NAME] + ' is in the index file but has not been submitted.')  

                    
                for file in list_files: 
                    if file not in infos_upload.keys():
                        messages.add_message(self.request, messages.WARNING, 'The image '+ file + ' is not in the index file.')
                        details_img = []
                        details_img.append(dict_img[file].file_id)
                        details_img.append(file)
                        for i in range(5):
                            details_img.append('')
                     
                        infos_upload[file] = details_img
            
            # If there's no index file, only images infos are stored    
            else:
                for f in form.cleaned_data.get('input_file'):   
                    details_img = []
                    details_img.append(f.file_id)
                    details_img.append(str(f))
                    for i in range(5):
                        details_img.append('')
                    
                    infos_upload[str(f)] = details_img
                    
        except:
            messages.add_message(self.request, messages.ERROR, 'There is an error in the index file.')  
            return self.form_invalid(form)

        form.save()
        print(infos_upload)
        # Dict infos_upload is save in the session's variable
        # https://docs.djangoproject.com/fr/1.11/topics/http/sessions/
        self.request.session['infos_upload'] = infos_upload
        
        return super(BaseFormView, self).form_valid(form)
    
    


class UploadedImagesView(BaseFormView):
    form_class = forms.MultipleImageForm

    
    

class LinkImagesView(generic.FormView):
    """ This view retrieve data of the BasicFormView, 
        pass data to the form and collect info in order to 
        create Image and ImageLink objects.  
    """  
    template_name = 'images/link_images.html'
    form_class = forms.LinkForm
    
    def get_success_url(self):
        return reverse('upload_summary')
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['admin'] = True
        context['TEMP_DIR'] = TEMP_DIR
        return context
    
    def get(self, request, *args, **kwargs):
        """ Handles GET requests and instantiates blank versions of the form
            and its formsets or if an index file was submitted, pre-filled 
            fields with data collected in the previous view.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        
        infos_upload = self.request.session['infos_upload'].copy()
        print("dans le get : {0}".format(infos_upload))

        liste_n = []    #name
        liste_d = []    #date
        liste_c = []    #comment
        liste_a = []    #accession
        liste_s = []    #seedlot
        liste_e = []   #environment
        
        liste_img = []
                
        for clef in infos_upload.keys():
            print(clef)
            val = infos_upload.get(clef)
            liste_img.append(val[0] + '.thumbnail')
            liste_n.append(val[1])
            liste_d.append(val[2])
            liste_c.append(val[3])
            
            #Get the list of the links
            print("1")
            print(liste_n)
            print(liste_d)
            print(liste_c)
            print(val[4])
            print(val[5])

            get_a = []
            get_a = val[4]            
            get_a2 = []
            
                           
            for a in get_a:
                try:
                    get_a2.append(Accession.objects.get(name=a))
                except:
                    get_a2.append(None)
            liste_a.append(get_a2)
            
            
            print("2")
            
            
            get_s = []
            get_s = val[5] 
            get_s2 = []
            for s in get_s:
                try:
                    get_s2.append(Seedlot.objects.get(name=s))
                except:
                    get_s2.append(None)    
            liste_s.append(get_s2)
            
            print("3")
            
            get_e = []
            get_e = val[6] 
            get_e2 = []
            for e in get_e:
                try:
                    get_e2.append(Environment.objects.get(name=e))
                except:
                    get_e2.append(None)    
            liste_e.append(get_e2)
            
            
        print("HERE")
  
            
        
        # Formsets are pre-filled with info of those lists  
        link_form = forms.LinkFormSet(initial=[{'accession': liste_a[i],
                                                'seedlot': liste_s[i],
                                                'environment': liste_e[i],
                                                'date': liste_d[i],
                                                'comment': liste_c[i]} for i in range(len(liste_d))])
        
        formset = zip(link_form, liste_n, liste_img)
        
        self.request.session['liste_n'] = liste_n
        self.request.session['liste_img'] = liste_img
        
        
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset,
                                  link_form=link_form))
        
    def post(self, request, *args, **kwargs):
        """ Handles POST requests, instantiating a form instance and its
            formsets with the passed POST variables and then checking them for
            validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
       
        link_form = forms.LinkFormSet(self.request.POST)
        
        liste_n = self.request.session['liste_n']
        liste_img = self.request.session['liste_img']
        formset = zip(link_form, liste_n, liste_img)
        
        if link_form.is_valid():
            return self.form_valid(form, link_form)
        else:
            return self.form_invalid(form, link_form, formset)
        
    def form_valid(self, form, link_form):
        """ Called if all forms are valid. Create Image and ImageLink 
            objects with infos of the formsets. Then redirects to the UploadSummary view.
        """
        self.object = form.save()
        link_form.instance = self.object
        
        infos_upload = self.request.session['infos_upload'].copy() 
        summary_upload = {}
        
        liste_n = self.request.session['liste_n']
        liste_img = self.request.session['liste_img']
        formset = zip(link_form, liste_n, liste_img)
        
        print(formset)
        
        liste_n = []
        liste_d = []
        liste_c = []
        liste_a = []
        liste_s = []
        liste_e = []

        liste_img = []
               
        if link_form.is_valid():     
            
            #for form, clef, name in zip(link_form, infos_upload, self.request.session['liste_n']):
            for form, name, image in formset:
                
                val = infos_upload.get(name)

                up_file = UploadedFile.objects.get(file_id=val[0])
                file_w_id = up_file.get_uploaded_file()
                
                date = str(form.cleaned_data.get('date')) 
                comment = form.cleaned_data.get('comment')          
                
                # Image's footprint is created.
                file = open(settings.MEDIA_ROOT+'/'+str(up_file.uploaded_file), "rb")
                stream = file.read()
                crypt = hashlib.sha1()
                crypt.update(stream)
                footprint = crypt.hexdigest()
                
                # We check that image is not already in database, 
                # if it is not, Image object is create.
                try:
                    img = Image.objects.get(footprint=footprint)
                    in_db = True
                except:
                    in_db = False
                    
                if not in_db:    
                    try:
                        img = Image.objects.create(
                            image_path=file_w_id,
                            image_name= val[1].replace(" ","_"),
                            date = date,
                            comment = comment,
                            footprint = footprint
                        )
                        
                        liste_d.append(date)
                        liste_c.append(comment)
                    
                    except:
                        messages.add_message(self.request, messages.ERROR, 'There is an error in the submitted informations for the image named '+ val[1])
                        return self.form_invalid(form, link_form, formset)
                else:
                    messages.add_message(self.request, messages.WARNING, 'Image '+ val[1] + ' is already in the database. New link has been added but other informations has not been modified.')
                    
                    liste_d.append(None)
                    liste_c.append(None)
                    
            # New ImageLink objects are created if they didn't already exist
                
                get_a = []
                for a in form.cleaned_data.get("accession"):
                    accession_obj = Accession.objects.get(name=a)
                    
                    a_qs = ImageLink.objects.filter(image=img, entity_name=a)
                    
                    #Check le type de l'entité du lien
                    a_exist=False
                    for i in a_qs:
                        if type(i.entity).__name__ == "Accession":
                            a_exist = True

                    if not a_exist:
                        new_link = ImageLink(image=img, entity=accession_obj, entity_name=a)
                        new_link.save()
                        get_a.append(str(a))
                    
                        
                liste_a.append(get_a)
                
                get_s = []
                for s in form.cleaned_data.get("seedlot"):
                    seedlot_obj = Seedlot.objects.get(name=s)
                    
                    s_qs = ImageLink.objects.filter(image=img, entity_name=s)
                    
                    #Check le type de l'entité du lien
                    s_exist=False
                    for i in s_qs:
                        if type(i.entity).__name__ == "Seedlot":
                            s_exist = True

                    if not s_exist:
                        new_link = ImageLink(image=img, entity=seedlot_obj, entity_name=s)
                        new_link.save()
                        get_s.append(str(s))
                    
                        
                liste_s.append(get_s)
                
                get_e = []
                for e in form.cleaned_data.get("environment"):
                    environment_obj = Environment.objects.get(name=e)
        
                    e_qs = ImageLink.objects.filter(image=img, entity_name=e)
                    
                    #Check le type de l'entité du lien
                    e_exist=False
                    for i in e_qs:
                        if type(i.entity).__name__ == "Environment":
                            e_exist = True

                    if not e_exist:
                        new_link = ImageLink(image=img, entity=environment_obj, entity_name=e)
                        new_link.save()
                        get_e.append(str(e))
                    
                        
                liste_e.append(get_e)
                
                if get_a or get_s or get_e :
                    liste_img.append(val[0] + '.thumbnail')
                    liste_n.append(val[1])
                else:
                    liste_img.append(None)
                    liste_n.append(None)

                form.save()
                
            summary_upload["liste_n"] = liste_n
            summary_upload["liste_d"] = liste_d
            summary_upload["liste_c"] = liste_c
            summary_upload["liste_a"] = liste_a
            summary_upload["liste_s"] = liste_s
            summary_upload["liste_e"] = liste_e
            summary_upload["liste_img"] = liste_img
                    
            self.request.session['summary_upload'] = summary_upload

            messages.add_message(self.request, messages.SUCCESS, 'The following images have been saved with those informations :')
            return HttpResponseRedirect(self.get_success_url())
        
        else:
            return self.form_invalid(form, link_form, formset)
    
    
    def form_invalid(self, form, link_form, formset):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=formset,
                                  link_form=link_form))
        


class UploadSummaryView(TemplateView):
    """ This view display all infos which were selected in the formsets.
    """
    template_name = 'images/upload_summary.html'
    
    def get_context_data(self, **kwargs):
        context = super(UploadSummaryView, self).get_context_data(**kwargs)
        
        summary_upload = self.request.session['summary_upload'].copy()
        
        formset = zip(summary_upload["liste_n"], summary_upload["liste_d"],\
                      summary_upload["liste_c"], summary_upload["liste_a"],\
                      summary_upload["liste_s"], summary_upload["liste_e"],\
                      summary_upload["liste_img"])
        
        context['formset'] = formset
        context['admin'] = True

        return context
    
    

    
