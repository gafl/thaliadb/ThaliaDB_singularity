# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms
import django_bootstrap3_form

from images.models import Image, ImageLink

from accession.models import Accession
from lot.models import Seedlot
from phenotyping.models import Environment

from dal import autocomplete
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.forms.formsets import formset_factory

from django_file_form.forms import FileFormMixin, UploadedFileField, MultipleUploadedFileField



class ImageForm(forms.ModelForm):
    
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete', attrs={'style': 'width:350px'})
                                            )
    seedlot = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete', attrs={'style': 'width:350px'})
                                            )
    environment = forms.ModelMultipleChoiceField(queryset=Environment.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='environment-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    class Meta:
        model = Image
        widgets = {'date': forms.TextInput(attrs = {'class':'datePicker'})}
        fields = ['image_path', 'date', 'comment']
    #date = forms.DateField(widget=forms.SelectDateWidget())
    
    
    
class ImageLinkForm(forms.ModelForm):
    class Meta:
        model = ImageLink
        fields = '__all__'

class ImageManagementForm(forms.Form):
    
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete', attrs={'style': 'width:350px'})
                                            )
    seedlot = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete', attrs={'style': 'width:350px'})
                                            )
    environment = forms.ModelMultipleChoiceField(queryset=Environment.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='environment-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    date = forms.DateField(widget=forms.TextInput(attrs = {'class':'datePicker'}))
    comment = forms.CharField(required=False)
    

    def __init__(self, *args, **kwargs):
        super(ImageManagementForm, self).__init__(*args, **kwargs)
        
    def clean(self):
        cleaned_data = super(ImageManagementForm, self).clean()
        accession = self.cleaned_data.get('accession')
        seedlot = self.cleaned_data.get('seedlot')
        environment = self.cleaned_data.get('environment')
         
        return cleaned_data
    
    def save(self):
        pass      


#------------ FORM SHINEMAS -------------#


class MultipleImageForm(FileFormMixin, django_bootstrap3_form.BootstrapForm):
    
    input_index_file = UploadedFileField()
    input_file = MultipleUploadedFileField()
    
    def save(self):
        pass     


class LinkForm(forms.Form, FileFormMixin):

    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete', attrs={'style': 'width:350px'})
                                            )
    seedlot = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete', attrs={'style': 'width:350px'})
                                            )
    environment = forms.ModelMultipleChoiceField(queryset=Environment.objects.all().order_by('name'),required=False,
                                             widget=autocomplete.ModelSelect2Multiple(url='environment-autocomplete', attrs={'style': 'width:350px'})
                                            )
    
    date = forms.DateField(widget=forms.TextInput(attrs = {'class':'datePicker'}))
    comment = forms.CharField(required=False)
    
    def clean(self):
        cleaned_data = super(LinkForm, self).clean()
        accession = self.cleaned_data.get('accession')
        seedlot = self.cleaned_data.get('seedlot')
        environment = self.cleaned_data.get('environment')
        
        if not accession and not seedlot and not environment:
            raise forms.ValidationError('Please select at least one entity.')
         
        return cleaned_data
    
    def __init__(self, *args, **kwargs):
        super(LinkForm, self).__init__(*args, **kwargs)
        
    def save(self):
        self.delete_temporary_files()
  
LinkFormSet = formset_factory(LinkForm, max_num=1)
