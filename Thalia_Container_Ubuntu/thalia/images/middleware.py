# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, time
from datetime import datetime
from django.conf import settings

TEMP_DIR = settings.TEMP_DIR

class EmptyTempUploadFile(object):
    """ This middleware enable to empty the temporary upload file where 
    images are placed when uploaded in the package.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        files=os.listdir(TEMP_DIR)
        fmt = '%Y-%m-%d %H:%M:%S'
        time_now = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')   
        for i in range(0,len(files)):
            
            time_created = datetime.fromtimestamp(os.path.getctime(TEMP_DIR+files[i])).strftime('%Y-%m-%d %H:%M:%S')
                
            tn = datetime.strptime(time_now, fmt)
            tc = datetime.strptime(time_created, fmt)
                
            delta = (tn - tc).seconds
            #Possibilité de mettre .days à la place
            if delta >= 3600:
                os.remove(TEMP_DIR+files[i])
                          
        return response
    