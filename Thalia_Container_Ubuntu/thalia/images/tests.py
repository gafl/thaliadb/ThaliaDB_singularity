"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.apps import apps

from images.forms import ImageForm, ImageLinkForm, ImageManagementForm

import datetime

class FormTest(TestCase):
    
    def setUp(self):
        
        #SetUp Image
        AccessionType = apps.get_model("accession", "accessiontype")
        self.acc_type1 = AccessionType.objects.create(name="acc_type1")
        Accession = apps.get_model("accession", "accession")
        self.accession1 = Accession.objects.create(name="accession1", type=self.acc_type1)
        
        
        #SetUp ImageLink
        Image = apps.get_model("images", "image")
        self.image1 = Image.objects.create(image_name="img1")
        
    def test_image_form(self):
                    
        #Test positif
        image_form1 = ImageForm(data={"accession":[self.accession1],
                                        "seedlot":None,
                                        "environment":None,
                                        "image_path":"fichier",
                                        "date":datetime.date(2018, 5, 5),
                                        "comment":"commentaire1"})
        
        self.assertTrue(image_form1.is_valid())

        #Test sans fichier image
        image_form2 = ImageForm(data={"accession":[self.accession1],
                                        "seedlot":None,
                                        "environment":None,
                                        "date":datetime.date(2018, 5, 5),
                                        "comment":"commentaire1"})

        self.assertFalse(image_form2.is_valid())
        
    def test_imageManagement_form(self):
                    
        #Test positif
        data_form = {"accession":[self.accession1],"seedlot":None, "environment":None, "date":datetime.date(2018, 5, 5),"comment":"commentaire1"}
        imageManagement_form1 = ImageManagementForm(data=data_form)
  
        self.assertTrue(imageManagement_form1.is_valid())        
        
        
        #Test accession à la place de seedlot
        data_form = {"accession":[self.accession1],"seedlot":[self.accession1], "environment":None, "date":datetime.date(2018, 5, 5),"comment":"test"}
        imageManagement_form2 = ImageManagementForm(data=data_form)
  
        self.assertFalse(imageManagement_form2.is_valid())
        
        #Test accession à la place de environment
        data_form = {"accession":[self.accession1],"seedlot":[], "environment":[self.accession1], "date":datetime.date(2018, 5, 5),"comment":"test"}
        imageManagement_form3 = ImageManagementForm(data=data_form)
  
        self.assertFalse(imageManagement_form3.is_valid())
        
        
    def test_imageLink_form(self):
        
        #Test positif    
        data_form = {"image":self.image1, "entity":self.accession1, "entity_name":self.accession1.name}
        imageLink_form1 = ImageLinkForm(data=data_form)
          
        self.assertTrue(imageLink_form1.is_valid())

        
"""
class ModelTest(TestCase):
    
    def setUp(self):
        
        #SetUp Image
        Method = apps.get_model("team", "method")
        self.method1 = Method.objects.create(name="method1", type=2, description="description1")
    
        Person = apps.get_model("team", "person")
        self.person1 = Person.objects.create(first_name="arthur", last_name="robieux")
        
        Project = apps.get_model("team", "project")
        self.project1 = Project.objects.create(name="project1", authors="arthur", start_date=datetime.date(2018, 5, 5))
          
    def test_save_image(self):
        
        Classification = apps.get_model("classification", "classification")
        c1 = Classification()
        
"""  
        