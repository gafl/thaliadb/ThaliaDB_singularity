from django.conf.urls import include, url

from images.views import images_renderer, img_management, imgLink_management, image_manager, UploadedImagesView
from images.views import UploadedImagesView, LinkImagesView, UploadSummaryView


urlpatterns = [
    url(r'^render/(?P<pk>[0-9]+)/$', images_renderer, name='imagerender'),
    url(r'^images/$', img_management, name='img_management'),
    url(r'^images/(?P<pk>[0-9]+)/$', image_manager, name='image_manager'),
   
    url(r'^upload/', include('django_file_form.urls')),
    url(r'^upload_images/$', UploadedImagesView.as_view(), name='upload_images'),
    url(r'^link_images/$', LinkImagesView.as_view(), name='link_images'),
    url(r'^upload_summary/$', UploadSummaryView.as_view(), name='upload_summary'),
]



