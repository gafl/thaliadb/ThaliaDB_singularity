from django.db import models
from django.apps import apps

from team.models import User, Project

class ClassificationManager(models.Manager):
    """
    Manager for classification module.
    """
    
    def by_username(self,username):
        """
        Returns only the classifications from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def create_classification(self, line):
        """
        Create a classification with a line containing all the informations.

        :param list line: classification's informations

        :var model Classification: classification's model
        :var model Method: method's model
        :var model Person: person's model
        :var model Project: project's model
        :var Classification new_classif: new classification
        """
        Classification = apps.get_model('classification', 'classification')
        Method = apps.get_model('team', 'method')
        Person = apps.get_model('team', 'person')
        Project = apps.get_model('team', 'project')
        
        name=line[0]
        date=line[1]
        reference=line[2]
        method = Method.objects.get(name=line[3])
        first_name = line[4].split(" ")[0]
        last_name = line[4].split(" ")[1]
        person = Person.objects.get(first_name=first_name, last_name=last_name)
        description = line[5]
        #If more than one project
        if "|" in line[6]:
            projects = Project.objects.filter(name__in=line[6].split("|"))
        else:
            projects = Project.objects.filter(name=line[6])
    
        #Classification creation               
        new_classif = Classification(name = name, date=date, reference = reference, method=method, person=person, description=description)
        new_classif.save()
        new_classif.projects = projects
        new_classif.save()