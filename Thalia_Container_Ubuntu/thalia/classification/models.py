# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    LICENCE
"""

from django.db import models
from classification.managers import ClassificationManager

class Classification(models.Model):
    """
    This class represents a classification with some attributes
    
    :var CharField name: classification's name
    :var DateField date: publication's date
    :var CharField reference: classification's reference
    :var ForeignKey method: method used to create classification
    :var ForeignKey person: person who creates the classification
    :var ManyToManyField projects: projects which contains the classification
    """
    name = models.CharField(max_length=100, unique=True)
    date = models.DateField()
    reference = models.CharField(max_length = 200, blank=True, null=True)
    method = models.ForeignKey('team.Method')
    person = models.ForeignKey('team.Person')
    description = models.TextField(blank=True, null=True)
    
    projects = models.ManyToManyField('team.Project')
    
    objects = ClassificationManager()
    
    def __str__(self):
        return self.name


class Classe(models.Model):
    """
    This class represents a group, a population

    :var CharField name: group's name
    :var TextField description: group's description
    :var ForeignKey classification: classification which contains the group
    """
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    classification = models.ForeignKey('Classification')
    
    class Meta:
        unique_together=("name","classification")
    
    def __str__(self):
        return self.name

class ClasseValue(models.Model):
    """
    This class represents a value of classification, linked with a seedlot and a group
    
    :var Charfield value: value for a couple Seedlot/Group
    :var ForeignKey seedlot: seedlot of the value
    :var ForeignKey classe: group of the value
    """
    value = models.CharField(max_length=100)
    seedlot = models.ForeignKey('lot.Seedlot')
    classe = models.ForeignKey('Classe')
    class Meta:
        unique_together=("seedlot","classe")
        index_together=[["seedlot","classe"],]
        
    def __str__(self):
        return self.value