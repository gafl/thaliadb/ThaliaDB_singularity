"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENCE"""

from django.test import TestCase
from django.db import models
from django.apps import apps
from django.db.utils import IntegrityError

import datetime

from classification.forms import ClassificationForm, ClasseForm, ClasseValueForm

class FormTest(TestCase):
    """ 
    Tests on classification module forms
    """
    
    def setUp(self):
        """
        Creation of some objects in order to test the module
        
        :var Method method1: an instance of method
        :var Person person1: an instance of person
        :var Project project1: an instance of project
        :var Classification classification2: an instance of classification
        :var Classe classe1: an instance of classe
        :var AccessionType acc_type1: an instance of AccessionType
        :var Accession accession1: an instance of accession
        :var SeedlotType seed_type1: an instance of SeedlotType
        :var Seedlot seedlot1: an instance of seedlot
        """
        
        #SetUp Classification
        Method = apps.get_model("team", "method")
        self.method1 = Method.objects.create(name="method1", type=2, description="description1")
    
        Person = apps.get_model("team", "person")
        self.person1 = Person.objects.create(first_name="arthur", last_name="robieux")
        
        Project = apps.get_model("team", "project")
        self.project1 = Project.objects.create(name="project1", authors="arthur", start_date=datetime.date(2018, 5, 5))
        
        #SetUp Classe
        Classification = apps.get_model("classification", "classification")
        self.classification2 = Classification.objects.create(name="classification2", date=datetime.date(2018, 5, 5), reference="ref1", method=self.method1, person=self.person1)

        
        #SetUp ClasseValue
        Classe = apps.get_model("classification", "classe")
        self.classe1 = Classe.objects.create(name="classe1", classification=self.classification2)
        
        AccessionType = apps.get_model("accession", "accessiontype")
        self.acc_type1 = AccessionType.objects.create(name="acc_type1")
        Accession = apps.get_model("accession", "accession")
        self.accession1 = Accession.objects.create(name="accession1", type=self.acc_type1)
        
        SeedlotType = apps.get_model("lot", "seedlottype")
        self.seed_type1 = SeedlotType.objects.create(name="type1")
        Seedlot = apps.get_model("lot", "seedlot")
        self.seedlot1 = Seedlot.objects.create(name="seedlot1", accession=self.accession1, type=self.seed_type1)
        
    def test_classification_form(self):
        """
        Tests on Classification form
        
        :var form classification_form1: positive test
        :var form classification_form2: test missing project
        :var form classification_form3: test wrong date format
        :var form classification_form4: test name's length
        :var form classification_form5: test reference's length
        """
                    
        #Test positif
        classification_form1 = ClassificationForm(data={"name":"classification1",
                                                  "date":datetime.date(2018, 5, 5),
                                                  "reference":"ref1",
                                                  "method":self.method1.id,
                                                  "person":self.person1.id,
                                                  "projects":[self.project1]})
        
        self.assertTrue(classification_form1.is_valid())
        
        #Test de l'absence d'un projet
        classification_form2 = ClassificationForm(data={"name":"classification1",
                                                  "date":datetime.date(2018, 5, 5),
                                                  "reference":"ref1",
                                                  "method":self.method1.id,
                                                  "person":self.person1.id})

        self.assertFalse(classification_form2.is_valid())
        
        #Test du mauvais format de date
        classification_form3 = ClassificationForm(data={"name":"classification1",
                                                  "date":"date",
                                                  "reference":"ref1",
                                                  "method":self.method1.id,
                                                  "person":self.person1.id,
                                                  "projects":[self.project1]})
        
        self.assertFalse(classification_form3.is_valid())
        
        #Test de la longueur du nom
        classification_form4 = ClassificationForm(data={"name":"01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891",
                                                  "date":datetime.date(2018, 5, 5),
                                                  "reference":"ref1",
                                                  "method":self.method1.id,
                                                  "person":self.person1.id,
                                                  "projects":[self.project1]})
        
        self.assertFalse(classification_form4.is_valid())
        
        #Test de la longueur de la référence
        classification_form5 = ClassificationForm(data={"name":"classification",
                                                  "date":datetime.date(2018, 5, 5),
                                                  "reference":"012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891",
                                                  "method":self.method1.id,
                                                  "person":self.person1.id,
                                                  "projects":[self.project1]})
        
        self.assertFalse(classification_form5.is_valid())
        
        
    def test_classe_form(self):
        """
        Test on class form
        
        :var form classe_form1: positive test
        :var form classe_form2: test name's length
        :var form classe_form3: test missing classification

        
        """
        
        #Test positif
        classe_form1 = ClasseForm(data={'name':"Classe1",
                                        'description':'Description',
                                        'classification':self.classification2.id})
        self.assertTrue(classe_form1.is_valid())
        #Test longueur du nom
        classe_form2 = ClasseForm(data={'name':"01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891",
                                        'description':'Description',
                                        'classification':self.classification2.id})
        
        self.assertFalse(classe_form2.is_valid())
        
        #Test absence d'une classification
        classe_form3 = ClasseForm(data={'name':"Classe1",
                                        'description':'Description'})
        
        self.assertFalse(classe_form3.is_valid())
   
    def test_classe_value_form(self):
        """
        Test on classe_value form
        
        :var form classe_value_form1: positive test
        :var form classe_value_form2: test missing seedlot
        :var form classe_value_form3: test missing classe
        """
        
        #Test positif
        classe_value_form1 = ClasseValueForm(data={'value':0.5,
                                                   'seedlot':self.seedlot1.id,
                                                   'classe':self.classe1.id})
        
        self.assertTrue(classe_value_form1.is_valid())
        
        #Test sans seedlot
        classe_value_form2 = ClasseValueForm(data={'value':0.5,
                                                   'classe':self.classe1.id})
        
        self.assertFalse(classe_value_form2.is_valid())
        
        #Test sans classe
        classe_value_form3 = ClasseValueForm(data={'value':0.5,
                                                   'seedlot':self.seedlot1.id})
                                                   
        
        self.assertFalse(classe_value_form3.is_valid())


class ModelTest(TestCase):
    """ 
    Tests on classification module model
    """
    def setUp(self):
        """
        Creation of some objects in order to test the module
        
        :var Method method1: an instance of method
        :var Person person1: an instance of person
        :var Project project1: an instance of project
        :var Classification classification2: an instance of classification
        :var Classe classe1: an instance of classe
        :var AccessionType acc_type1: an instance of AccessionType
        :var Accession accession1: an instance of accession
        :var SeedlotType seed_type1: an instance of SeedlotType
        :var Seedlot seedlot1: an instance of seedlot
        """
        
        #SetUp Classification
        Method = apps.get_model("team", "method")
        self.method1 = Method.objects.create(name="method1", type=2, description="description1")
    
        Person = apps.get_model("team", "person")
        self.person1 = Person.objects.create(first_name="arthur", last_name="robieux")
        
        Project = apps.get_model("team", "project")
        self.project1 = Project.objects.create(name="project1", authors="arthur", start_date=datetime.date(2018, 5, 5))
  
        #SetUp Classe
        Classification = apps.get_model("classification", "classification")
        self.classification2 = Classification.objects.create(name="classification2", date=datetime.date(2018, 5, 5), reference="ref1", method=self.method1, person=self.person1)

        
        #SetUp ClasseValue
        Classe = apps.get_model("classification", "classe")
        self.classe1 = Classe.objects.create(name="classe1", classification=self.classification2)
        
        AccessionType = apps.get_model("accession", "accessiontype")
        self.acc_type1 = AccessionType.objects.create(name="acc_type1")
        Accession = apps.get_model("accession", "accession")
        self.accession1 = Accession.objects.create(name="accession1", type=self.acc_type1)
        
        SeedlotType = apps.get_model("lot", "seedlottype")
        self.seed_type1 = SeedlotType.objects.create(name="type1")
        Seedlot = apps.get_model("lot", "seedlot")
        self.seedlot1 = Seedlot.objects.create(name="seedlot1", accession=self.accession1, type=self.seed_type1)
          
    def test_save_classification(self):
        """
        Test create and save classification
        
        :var Classification c1: instance of classification
        """
        Classification = apps.get_model("classification", "classification")
        c1 = Classification()
        
        #Test la modification du nom
        c1.name = "classification1"
        c1.date = datetime.date(2018, 5, 5)
        c1.method = self.method1
        c1.person = self.person1
        c1.save()
        self.assertEqual(c1.name, "classification1")
        c1.name = "lalala"
        c1.save()
        self.assertNotEqual(c1.name, "classification1")
        

    def test_save_classe(self):
        """
        Test create and save classe
        
        :var Classification c2: instance of classe
        """
        
        Classe = apps.get_model("classification", "classe")
        c2 = Classe()
        
        c2.name = "classe2"
        
        #Test l'erreur si pas de classification
        
        c2.classification = self.classification2
        c2.save()
        self.assertRaises(IntegrityError)

        self.assertEqual(c2.name, "classe2")

    def test_save_classe_value(self):
        """
        Test create and save classe_value
        
        :var Classification cv: instance of classe_value
        """
        ClasseValue = apps.get_model("classification", "classevalue")
        cv = ClasseValue()
        
        cv.seedlot = self.seedlot1
        cv.classe = self.classe1
        cv.value = "0.5"
        cv.save()

        self.assertEqual(cv.seedlot, self.seedlot1)
        self.assertEqual(cv.classe, self.classe1)
        self.assertEqual(cv.value, "0.5")


class ViewTest(TestCase):
    """ 
    Tests on classification module views
    """
    @classmethod
    def setUpTestData(cls):
        
        #SetUp Classification
        Method = apps.get_model("team", "method")
        cls.method1 = Method.objects.create(name="method1", type=2, description="description1")
    
        Person = apps.get_model("team", "person")
        cls.person1 = Person.objects.create(first_name="arthur", last_name="robieux")
        
        #SetUp Classe
        Classification = apps.get_model("classification", "classification")
        cls.classification1 = Classification.objects.create(name="classification1", date=datetime.date(2018, 5, 5), method=cls.method1, person=cls.person1)
        
        
        
    def test_1(self):
        self.assertEqual(1, 1)
