# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from classification.models import *
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect


class ClassificationForm(forms.ModelForm):
    """
    This class represents a form for a classification.
    
    :var model model: model of the form
    :var DateField date: date field with widget
    :var list fields: all the fields in a classification
    """
    class Meta:
        model = Classification
        fields = '__all__'
    date = forms.DateField(widget=forms.TextInput(attrs = {'class':'datePicker'}))
    
        
class ClasseForm(forms.ModelForm):
    """
    This class represents a form for a classe.
    
    :var model model: model of the form
    :var list fields: all the fields in a classification
    """
    class Meta:
        model = Classe
        fields = '__all__'
        
class ClasseValueForm(forms.ModelForm):
    """
    This class represents a form for a classe_value.
    
    :var model model: model of the form
    :var list fields: all the fields in a classification
    """
    class Meta:
        model = ClasseValue
        fields = '__all__'

class UploadClasseValueFileForm(forms.Form):
    """
    This class represents a form for add classe_values.
    
    :var ModelChoiceField classification: classification in which classe_values will be added
    :var FileField main_file: CSV file with classe_values
    """
    classification = forms.ModelChoiceField(queryset=Classification.objects.all(),empty_label="Choose Classification")
    main_file = forms.FileField(label='Main file')
    
class UploadFileForm(forms.Form):
    """
    This class represents a form for file uploads.
    
    :var FileField main_file: CSV file to upload
    """
    main_file = forms.FileField(label='Main file')
