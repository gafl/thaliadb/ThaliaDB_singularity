"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    LICENCE
"""

import json
import csv
from unidecode import unidecode

from django.db.models import Q
import ast
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet

from django.shortcuts import render
from django.db import models, IntegrityError
from django.http import HttpResponse,HttpResponseServerError, Http404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist

from accession.models import AccessionType, AccessionTypeAttribute, AccessionAttributePosition, Accession
from accession.forms import AccessionTypeForm, AccessionTypeAttForm, AccessionDataForm
from team.models import Project
from commonfct.genericviews import generic_management, get_fields
from commonfct.utils import recode
from commonfct.constants import ACCEPTED_TRUE_VALUES

from team.models import User, Method
from lot.models import Seedlot
from classification.models import Classification, Classe, ClasseValue
from classification.forms import ClassificationForm, ClasseForm, ClasseValueForm, UploadClasseValueFileForm, UploadFileForm
from team.forms import MethodForm
from phenotyping.forms import EnvironmentForm
from team.views import write_csv_search_result
from phenotyping.views import _get_data, _create_phenodata_from_file
from phenotyping.views import refine_search as refine_search2

from team.models import Method, Person
from accession.forms import UploadFileForm as UploadFileForm2

from classification.forms import *
from dataview.forms import ClasseViewerForm, ClassificationViewerForm, SeedlotViewerForm, DataviewClassificationVisuForm
from dataview.views import return_csv_classif, getClassificationData


#============================ VIEWS =======================#


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classificationhome(request):
    """
        Classification home.
    """
    return render(request,'classification/classification_base.html',{"admin":True})

#=============================


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification(request):
    """
        View to manage classifications
        
        :var string template: template for classification
        :var form form: form to create classification
        :var list list_attributes: list of classifications's attributes
    """
    template = 'classification/classification_generic.html'
    form = ClassificationForm()
    list_attributes=['name', 'date', 'reference', 'method', 'person', 'projects']
    model = Classification
    #classifications = Classification.objects.all()
    classifications, nb_per_page = _get_data(request, model)
    list_exceptions = ['id', 'classe']
    title = "Structure Management"
    filetype = 'classification'
    typeid = 0
    
    if request.method == "GET":
        return refine_search(request, form, classifications, template, list_attributes, list_exceptions, title, model, filetype, typeid)
    
    #CSV add (if add_csv exists)
    try:
        add_data = request.POST['add_csv']
        all = Classification.objects.all()
        names, labels = get_fields(form)
        return insertCSVdata(request, template, labels, model, form, all, names)
            
    except:
        print("EXCEPT")
        pass;
    
    #Add a classification
    return generic_management(Classification, ClassificationForm, request, template,{"list_attributes":list_attributes,"admin":True,},"Structure Management")

    #return render(request,template,{"admin":True, 'form': form, 'datas': classifications, 'title': title})
#=============================


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classe(request):
    """
        View to manage classes
        
        :var string template: template for groups
        :var form form: form to create groups
        :var list list_attributes: list of group's attributes
    """
    template = 'classification/classification_generic.html'
    list_attributes=['name', 'description', 'classification']
    form = ClasseForm()
    model = Classe
    #classes = Classe.objects.all()
    classes, nb_per_page = _get_data(request, model)
    list_exceptions = ['id', 'classevalue']
    title = "Group Management"
    filetype = 'classe'
    typeid = 0
    
    #Research & first page
    if request.method == "GET":
        return refine_search(request, form, classes, template, list_attributes, list_exceptions, title, model, filetype, typeid)
    
    #CSV add (if add_csv exists)
    try:
        add_data = request.POST['add_csv']
        all = Classe.objects.all()
        names, labels = get_fields(form)
        return insertCSVdata(request, template, labels, model, form, all, names)
            
    except:
        print("EXCEPT")
        pass;
    
    #Manual add
    return generic_management(Classe, ClasseForm, request, template,{"list_attributes":list_attributes,"admin":True,},"Group Management")
    
    
    #return render(request,'classification/classification_generic.html',{"admin":True, 'form': ClasseForm, 'title': title})
#=============================


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def method(request):
    """
        View to manage methods
        
        :var string template: template for methods
        :var form form: form to create methods
        :var list list_attributes: list of method's attributes
    """
    form = MethodForm()
    formf = UploadFileForm2()
    template = 'classification/classification_generic.html'
    list_attributes=['name', 'description']
    list_exceptions = ['id','phenotypicvalue','classification','type']
    title = "Method Management"
    model = Method
    methods, nb_per_page = _get_data(request, model)
    #methods = Method.objects.filter(type=2)
    filetype = 'method'
    typeid = 0
    if request.method == "POST":
        formf = UploadFileForm2(request.POST, request.FILES) 
        try:
            separator = request.POST['delimiter']
        except: 
            pass
        if formf.is_valid():
            names, labels = get_fields(form)
            tag_fields = {"list_attributes":list_attributes,
                          "admin":True,
                          'fields_name':names,
                          'fields_label':labels,
                          'title': title,
                          'creation_mode':True,
                          'form':form,
                          "formfile":formf,
                          }
            return _create_phenodata_from_file(request, template, tag_fields, separator, model)
        else:
            return generic_management(Method, MethodForm, request, 'classification/classification_generic.html',{"admin":True,"method_type":2,},"Method Management")

    return refine_search2(request, form, formf, methods, template, list_attributes, list_exceptions, title, model, filetype, typeid)



#=============================


@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification_data(request):
    """
        View to add structure's data
        
        :var list classe_values: all the classification values of the database (ClasseValue)
        :var form uploadForm: form to add ClasseValue
        :var string template: template for classification data
        :var int count: number of ClasseValue in the database
    """
    classe_values = ClasseValue.objects.all()
    uploadForm = UploadClasseValueFileForm()
    template = 'classification/classification_data.html'

    if request.method == "GET":
        count = ClasseValue.objects.count()
        if count <1:
            count = 'no classification data in database'

    if request.method == "POST":
        uploadForm = UploadClasseValueFileForm(request.POST, request.FILES)
        if uploadForm.is_valid():
            return getClassifData(request, template, uploadForm)
        
    return render(request, template, {"admin":True,'uploadForm':uploadForm, 'classe_values': classe_values, 'count': count})

#===========================




@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def classification_view(request):
    """
        Structure's dataview in the Admin menu.
        
        :var string template: template for dataview
        :var int n_page: page / step of the search in the dataview
        :var list select_classification: classification selected in the first step
        :var list classe_values: all the values in the classification
        :var Seedlot sl: seedlot in the classification
        :var int export_or_web: boolean to have the type of visualization
        :var int show_accession: boolean to show accession in the table
    """
    template = "classification/classification_view.html"
    
    #Page 2-3 : Choose seedlots/groups & show results
    if request.method=="POST":
        
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        #Choose Seedlots & groups
        if n_page == 2:
            select_classification = request.POST.get('classification')
            print(select_classification)
            print(Classification.objects.get(id=select_classification))
            
            visumodeform = DataviewClassificationVisuForm()             
            formclasse = ClasseViewerForm()
            formseedlot = SeedlotViewerForm()

            names = []
            classe_values = ClasseValue.objects.filter(classe__in=Classe.objects.filter(classification=select_classification).distinct())
            for cv in classe_values:
                sl = cv.seedlot
                names.append(sl.name)

            formseedlot.fields["seedlot"].queryset = Seedlot.objects.filter(name__in=list(set(names))).order_by('name')
            formclasse.fields["group"].queryset = Classe.objects.filter(classification=select_classification).distinct().order_by('name')

            return render(request,template,{'admin':True, "n_page":n_page, 'formclasse':formclasse, 'formseedlot':formseedlot, 'visumodeform': visumodeform, "classification":select_classification})
            
        #Results
        if n_page ==3:

            classification = request.POST.get('classification') #ID de la classification
            classification = Classification.objects.get(id=classification)
            classe = request.POST.getlist('classe')
            seedlot = request.POST.getlist('seedlot')
            formclasse = ClasseViewerForm(request.POST)
            formseedlot = SeedlotViewerForm(request.POST)
            export_or_web = request.POST.get('export_or_web')
            show_accession = request.POST.get('show_accession')

            
            if formclasse.is_valid():
                select_classe = formclasse.cleaned_data['group']
                if formseedlot.is_valid():
                    select_seedlot = formseedlot.cleaned_data['seedlot']
            
            
            #Recupérer les données en fonction des colonnes/lignes choisies
            df = getClassificationData(select_classe, select_seedlot, show_accession)
            
            #Création du tableau en html
            if df.empty:
                html=None
            else:
                html = df.to_html()    
                html = html.split('\n')
                print(html)       
                    
            ########### SANS PANDAS #############
                    
            #Création du header            
            fields_label = ["Seedlot"]
            if show_accession == "1":
                fields_label.append("Accession")
            for gp in select_classe:
                fields_label.append(gp.name)
                
            #Récupération des données
            values = []
            
            for seedlot in select_seedlot:
                line = []
                line.append(seedlot)
                if show_accession == "1":
                    line.append(seedlot.accession.name)
                for gp in select_classe:
                    cv = ClasseValue.objects.get(seedlot=seedlot, classe=gp)
                    line.append(cv.value)
                
                values.append(line)    
                    
            ######################################
                            
         
                    
            if export_or_web == "1":
                return return_csv_classif(request)
            else:
                list_seedlot=df.index
                list_groups=list(df) #Liste des groupes et autres colonnes (accessions)
                return render(request,"classification/classification_dataViewer.html",{'admin':True,"show_accession": show_accession, "values":values, "fields_label":fields_label, "html":html, 'export_or_web': export_or_web, "list_seedlot":select_seedlot, "list_groups":list_groups, "show_accession":show_accession, "classification":classification})

        
    #Page 1 : Choose classification
    formclassification = ClassificationViewerForm()
    classifications = Classification.objects.all().order_by('name')
    formclassification.fields['classification'].queryset=classifications
       
    n_page=1
    
    return render(request,template,{'admin':True, "n_page":n_page, 'formclassification':formclassification})



#=========================== CLASSIFICATION DATA FUNCTIONS ==================#

def getClassifData(request, template, uploadForm):
    """
    Read a CSV file containing structure's data, and create data in the database 
    (only if seedlots & classes in the file already exist).
    
    :param string template: template for add classification's data
    :param form uploadForm: form to associate data with a classification
    
    :var file file: CSV file containing structure's data
    :var dict dict_line: lines of the file
    :var dict dict_data: values for each seedlot/groups
    :var int errors: number of error when data are added
    :var int add_count: number of data added
    """
    
    #Get the file and the data
    file = request.FILES['main_file']

    #Get the selected classification
    if uploadForm.is_valid():
        select_classification = uploadForm.cleaned_data['classification']
            
    #Check the csv file
    filename = file.name
    filtetype = filename.split ('.')
    extension = filtetype[1]
    
    if extension == "csv":
        #Get the line in a dictionary (0: line1, 1: line2...)
        dictline = {}
        i = 0
        for line in file:
            dictline[i] = line.strip()
            dictline[i] = recode(dictline[i])   #Conversion
            i+=1
             
        #Dictionary contains {lot1 : {gp1 : val, gp2 : val}, lot2 : {gp1 : val, gp2 : val}...}
        dict_data = {}    
        groups = dictline[0]    #First line of the file = groups
        
        #List of groups (classes)
        if ';' in groups or ',' in groups:
            if ';' in groups:
                groups = groups.split(';')
            else:
                groups = groups.split(',')
                   
            #Manage the other lines (seedlot with values)
            for line in dictline.keys():
                if line != 0:
                    line = dictline[line]
                    if ';' in line:
                        line = line.split(';')
                    else:
                        line = line.split(',')
                                        
                    #Seedlot = key
                    dict_data[line[0]] = {}
                    seedlot = dict_data[line[0]]
                    
                    #Add groups as keys with their values
                    i=1
                    for group in groups[1:len(groups)]:
                        seedlot[group] = line[i]
                        i+=1       
            
            #Check is seedlots & groups are in the DB.
            errors = checkClassesSeedlots(dict_data, groups, select_classification)
            
            #If errors, show the errors's type
            if(len(errors) > 0):
                return render(request, template, {"admin":True,'uploadForm':uploadForm, 'error_message_seedlot': errors[1], 'error_message_groups': errors[0]})
            
            #Now, datas are in the dictionnary
            #Seedlots & Classes OK
            #We add classe_values in the database
            
            """
            GESTION DES DOUBLONS / SUPPRESSIONS / ECRASEMENT, d'un CSV
            """
            
            add_count = add_classification_values(dict_data, select_classification, request, template, uploadForm)
            

              
    #If not CSV
    else:
        error_message = 'Please choose a CSV file.'
        return render(request, template, {"admin":True,'uploadForm':uploadForm, 'error_message': error_message})
    
    return render(request, template, {"admin":True,'uploadForm':uploadForm,'add_count':add_count,'select_classification':select_classification})




def add_classification_values(dict_data, select_classification, request, template, uploadForm):
    """
    Add classification values in the database and return the number of values added.
    
    :param dict dict_data: contains classification data from the CSV file
    :param list select_classification: selected classification
    :param string template: template
    :param form uploadForm: form with structure and CSV file with data
    
    :var int add_count: nmber of values added in the database
    :var float somme: sum of the values for 1 seedlot
    :var string error_message: error message if the total in a line isn't 1
    
    :returns:  int -- number of values added
    """

    add_count = 0
    for sl in dict_data.keys():
        somme=0
        for group in dict_data[sl].keys():
            value = dict_data[sl][group]
            seedlot = Seedlot.objects.get(name=sl)
            classe = Classe.objects.get(name=group, classification=Classification.objects.get(name=select_classification))
            
            try:
#                 old_cv = ClasseValue.objects.get(seedlot=seedlot, classe=classe)
#                 old_cv.delete()
#                 cv = ClasseValue(value=value, seedlot=seedlot, classe=classe)
                cv = ClasseValue.objects.get(seedlot=seedlot, classe=classe)
                cv.value=value
                cv.save()
                add_count+=1
                somme += float(value)
                
            except ClasseValue.DoesNotExist :
                cv = ClasseValue(value=value, seedlot=seedlot, classe=classe)
                cv.save()
                add_count+=1
                somme += float(value)
        
        """
        Retravailler la vérification d'une ligne pour 1 seedlot.
        Pb : add_count prend le render alors que le render devrait s'afficher directement.
        
        #Check one seedlot = 1
        #Chercher une methode differente
        if somme > 1.01 or somme < 0.99:
            error_message = "Sum of : " + sl + " =/= 1."
            return render(request, template, {"admin":True,'uploadForm':uploadForm, 'error_message': error_message})
        """
        
    return add_count


def checkClassesSeedlots(dict_data, groups, select_classification):
    """
    Check if Seedlots & Groups are in the database
    
    :param dict dict_data: keys = seedlots in the file
    :param list groups: groups in the file
    :param list select_classification: selected classification
    
    :var list wrong_groups: missing groups in the database
    :var list wrong_seedlots: missing seedlots in the database
    :var list error_message_groups: all the errors for groups
    :var list error_message_seedlot: all the errors for seedlots
    
    :returns: list -- error messages for groups/seedlots which doesn't exist in the database
    """

    #Check datas exist in the DB
    #Check the groups in the database
    wrong_groups = []
    for group in groups[1:len(groups)]:                
        try:
            Classe.objects.get(name=group, classification=select_classification)
        except Classe.DoesNotExist :
            wrong_groups.append(group)
            
    error_message_groups = ''
    if(len(wrong_groups) > 0):
        error_message_groups = 'Classe(s) " '
        for w_group in wrong_groups:
            error_message_groups += (w_group + ' ')
        error_message_groups += (' " not in the classification " ' + select_classification.name + ' ".')

    #Check the seedlots exist
    wrong_seedlots = []
    for seedlot in dict_data.keys():
        try:
            Seedlot.objects.get(name=seedlot)
        except Seedlot.DoesNotExist :
            wrong_seedlots.append(seedlot)
    
    error_message_seedlot = ''
    if(len(wrong_seedlots) > 0):
        error_message_seedlot = 'Seedlot(s) " '
        for w_seedlot in wrong_seedlots:
            error_message_seedlot += (w_seedlot + ' ')
        error_message_seedlot += (' " not in the database.')

    #If error, return the error message with missing objects
    if len(error_message_groups)+len(error_message_seedlot) > 0:
        return [error_message_groups, error_message_seedlot]
    
    #0 Errors
    return []
    
#=========================== OTHERS FUNCTIONS =================================#   


#Classe & Classification CSV add
def insertCSVdata(request, template, labels, model, form, all, names):
    """
    Read a CSV file (like the header),
    and create data in the database 
    (for classe & classification)
    
    :param labels: names of the form's field
    :param model: model
    :param form: form
    :param all: all objects of the model
    :param names: names of the form's field
    """
    
    #Get the file and the data
    file = request.FILES['main_file']
    
    #Check the csv file
    filename = file.name
    filtetype = filename.split ('.')
    extension = filtetype[1]
    
    if extension == "csv":
        
        dictline = {}
        
        i = 0
        for line in file:
            dictline[i] = line.strip()
            dictline[i] = recode(dictline[i])   #Conversion
            i+=1
        
        header = dictline[0]
        #Get the header of the CSV
        if ';' in header or ',' in header:
            if ',' in header:
                header = header.split(',')
            if ';' in header:
                header = header.split(';')
        
        else:
            csv_error =  "No \",\" or \";\" as separator."
            return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

            
        if header != labels:
            print(labels)
            print(header)
            csv_error =  "Not the good header."
            return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

        add_count = 0
        for i in dictline.keys():
            if i != 0:
                print(dictline[i])
                if ',' in dictline[i]:
                    line = dictline[i].split(',')
                if ';' in dictline[i]:
                    line = dictline[i].split(';')
                
                #If we add Classe
                if model == Classe: 
                    #Check if the classe already exists
                    try:                        
                        Classe.objects.get(name = line[0], classification = Classification.objects.get(name=line[2]))
                        csv_error =  "Some classe(s) already exists."
                        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

                        """Peut créer une fonction pour reporter tous les éléments deja existents, et demander si on ajoute quand meme les nouveaux"""
                    #We add the class
                    except:
                        try:
                            new_classe = Classe(name = line[0], description = line[1], classification = Classification.objects.get(name=line[2]))
                            new_classe.save()
                            add_count += 1
                        except:
                            #Check the associate classification exists
                            csv_error =  "Assossiate Classification doesn't exist."
                            return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})
                          
                #If we add Classification          
                elif model == Classification:
                    #Check if the classification already exists
                    try:
                        Classification.objects.get(name = line[0])
                        csv_error =  "Some classification(s) already exists."
                        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

                        """Peut créer une fonction pour reporter tous les éléments deja existents, et demander si on ajoute quand meme les nouveaux"""
                    #We add the classification
                    except:
                        Classification.objects.create_classification(line)
                        add_count += 1
                
            
    else:
        csv_error = "Not a CSV file."
        return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'csv_error':csv_error})

    #If add is OK
    return render(request, template,{'form':form, "admin":True, 'all':all, 'fields_label': labels, 'fields_name': names, 'add_count': add_count})



def refine_search(request, form, data, template, list_attributes, list_exceptions, title, model, filetype, typeid):
    """
    Other version of refine search, with the possibility to add
    data with a CSV file.
    """
    number_all = len(data)
    nb_per_page = 10
    names, labels = get_fields(form)
    tag_fields = {'all':data,
              'fields_name':names,
              'fields_label':labels,
              'title': title, "admin":True,}
    dico_get={}
    or_and = None
    
    if "no_search" in request.GET:
        
        uploadForm = UploadFileForm()
        
        tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       'uploadForm':uploadForm})

        return render(request,template,tag_fields)
    
    elif "or_and" in request.GET:
        or_and=request.GET['or_and']
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
#                 if or_and in request.GET:
#                     dico_get[]
        length_dico_get = len(dico_get)
        nb_per_page = 10
        if or_and == "or":
            proj=[]
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query |= Q(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" )
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        if data_type == "person" or data_type == "method" or data_type == "classification":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Q(first_name__icontains=value)|Q(last_name__icontains=value))
                            elif data_type == "method":
                                data_filter = Method.objects.filter(name__icontains=value)
                            elif data_type == "classification":
                                data_filter = Classification.objects.filter(name__icontains=value)

                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query |= Q(**{var_search : search_value})
            
        elif or_and == "and":       
            query=Q()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query &= Q(projects__in=proj)
                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        data_type = data_type[0].lower()+data_type[1:]
                        var_search = str(data_type + "__icontains" ) 
                        if data_type == "person" or data_type == "method" or data_type == "classification":
                            var_search = str(data_type+"__in")
                            if data_type == "person":
                                data_filter = Person.objects.filter(Q(first_name__icontains=value)|Q(last_name__icontains=value))
                            elif data_type == "method":
                                data_filter = Method.objects.filter(name__icontains=value)
                            elif data_type == "classification":
                                data_filter = Classification.objects.filter(name__icontains=value)

                            data_id_list = []
                            for i in data_filter:
                                data_id_list.append(i.id)
                            search_value = data_id_list
                        query &= Q(**{var_search : search_value})
                        
        data = model.objects.filter(query).distinct()

        number_all = len(data)
        download_not_empty = False
        if data:
            download_not_empty = write_csv_search_result(data, list_exceptions)
        tag_fields.update({"download_not_empty":download_not_empty})
        
        
    uploadForm = UploadFileForm()

    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       "filetype":filetype,
                       'excel_empty_file':True,
                       "typeid":typeid,
                       'uploadForm':uploadForm}) 
    
    return render(request, template, tag_fields) 



