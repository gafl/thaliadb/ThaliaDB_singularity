#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url
from classification.views import classificationhome, classification, classe, classification_data, classification_view, method

urlpatterns = [
    url(r'^home/$', classificationhome, name='classificationhome'),                       
    url(r'^classification/$', classification, name='classification'),                       
    url(r'^classe/$', classe, name='classe'),                       
    url(r'^method/$', method, name='cl_method'),                       
    url(r'^data/$', classification_data, name='classification_data'),                       
    url(r'^view/$', classification_view, name='classification_view'),                
]