# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django import forms
import datetime
from lot.models import SeedlotType, SeedlotTypeAttribute, Seedlot
from accession.models import Accession
from genealogy_managers.models import Reproduction_method, Reproduction
from genealogy_accession.models import AccessionRelation
from genealogy_seedlot.models import SeedlotRelation
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect
from django.contrib.admin.widgets import FilteredSelectMultiple
from dal import autocomplete

class SeedlotHybridCrossForm(forms.Form):
    hybrid_seedlot = forms.ModelChoiceField(required=True,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='seedlot-autocomplete'))
    parent_male = forms.ModelChoiceField(required=True,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='seedlot-autocomplete'))
    parent_female = forms.ModelChoiceField(required=True,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='seedlot-autocomplete'))
    crossing_method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=3))
    site = forms.CharField(required=False)
    start_date = forms.DateField(initial=datetime.date.today)
    end_date = forms.DateField(initial=datetime.date.today)
    comments = forms.CharField(required=False)
    
    def create_hybrid_cross(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['crossing_method'], entity_type = 0, description = self.cleaned_data['comments'], start_date = self.cleaned_data['start_date'], end_date = self.cleaned_data['end_date'])
        male_relation = SeedlotRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_male'], child = self.cleaned_data['hybrid_seedlot'], parent_gender = "M",
                                                     site = self.cleaned_data['site'] )
        female_relation = SeedlotRelation.objects.create(reproduction=repro, parent = self.cleaned_data['parent_female'], child = self.cleaned_data['hybrid_seedlot'], parent_gender = "F",
                                                     site = self.cleaned_data['site'])
        return repro.id, male_relation.child, male_relation.parent, female_relation.parent, self.cleaned_data['crossing_method'].name, male_relation.site, repro.start_date, repro.end_date, repro.description
    
    
    
    
class MultiplicationForm(forms.Form):
    seedlot = forms.ModelChoiceField(required=True,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2(url='seedlot-autocomplete'))
    parent = forms.ModelMultipleChoiceField(required=True,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete'))
    multiplication_method = forms.ModelChoiceField(required=True,
                                  queryset=Reproduction_method.objects.filter(category=1))
    site = forms.CharField(required=False)
    start_date = forms.DateField(initial=datetime.date.today)
    end_date = forms.DateField(initial=datetime.date.today)
    comments = forms.CharField(required=False)
    
    
    def create_multiplication(self):
        repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['multiplication_method'], entity_type = 0, description = self.cleaned_data['comments'], start_date = self.cleaned_data['start_date'], end_date = self.cleaned_data['end_date'])
        for parent_i in self.cleaned_data['parent']:
            #repro = Reproduction.objects.create(reproduction_method = self.cleaned_data['multiplication_method'], entity_type = 0, description = self.cleaned_data['comments'], start_date = self.cleaned_data['start_date'], end_date = self.cleaned_data['end_date'])
            mult_relation = SeedlotRelation.objects.create(reproduction=repro, parent = parent_i, child = self.cleaned_data['seedlot'], parent_gender = "X",
                                                     site = self.cleaned_data['site'])
        return repro.id, mult_relation.child, mult_relation.parent, self.cleaned_data['multiplication_method'].name, mult_relation.site, repro.start_date, repro.end_date, repro.description
    
    
    
    
    