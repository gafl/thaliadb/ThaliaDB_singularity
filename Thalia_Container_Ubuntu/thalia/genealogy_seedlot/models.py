# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jsonfield import JSONField

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.encoding import force_bytes

from commonfct.constants import CROSSING_METHOD_TYPE


# Create your models here.

PARENT_GENDER = (
              ('M', 'Male'),
              ('F', 'Female'),
              ('X', 'Unknown'),
              )


class SeedlotRelation (models.Model):
    reproduction = models.ForeignKey('genealogy_managers.Reproduction',blank=True,null=True)
    parent = models.ForeignKey('lot.Seedlot', related_name='relations_as_parent')
    child = models.ForeignKey('lot.Seedlot',blank=True,null=True, related_name = 'relations_as_child')
    parent_gender =  models.CharField(max_length=2, choices=PARENT_GENDER, blank=True, null=True)
    site = models.CharField(blank= True, null=True, max_length = 100)
    
    def __str__(self):
        return "{0} {1} --> {2}".format(self.parent, self.parent_gender, self.child)
    
    class Meta:
        unique_together = ("parent","child","parent_gender")
        
    def clean(self):
        check_parent = []
        if self.parent_gender == "M":
            check_parent = self.reproduction.accessionrelation_set.filter(parent_gender="F")
        elif self.parent_gender == "F" :
            check_parent = self.reproduction.accessionrelation_set.filter(parent_gender="M")
        if check_parent:           
            if self.parent == check_parent[0].parent:
                errormsg = "Parents {0} can't be the same item".format(self.parent)
                raise ValidationError(errormsg)
            
        if self.parent == self.child:
            errormsg = "Parent {0} {1} and child {2} can't be the same item".format(self.parent, self.parent_gender, self.child)
            raise ValidationError(errormsg)

        if SeedlotRelation.objects.filter(parent=self.child, child=self.parent): ## si acc1 est le parent de acc2, acc2 ne peut pas devenir le parent de acc1
            errormsg = "{0} can't be the parent of {1}. {1} is already the parent of {0}.".format(self.parent, self.child)
            raise ValidationError(errormsg)
        
        if SeedlotRelation.objects.filter(parent=self.parent, child=self.child, parent_gender=self.parent_gender):
            #Verification pour autoriser l'update de l'element existant
            if SeedlotRelation.objects.filter(parent=self.parent, child=self.child, parent_gender=self.parent_gender)[0].id != self.id:
                errormsg = "The relation {0} {1} --> {2} already exists.".format(self.parent, self.parent_gender, self.child)
                raise ValidationError(errormsg)
    
    def save(self, *args, **kwargs):
        self.clean()
        super(SeedlotRelation,self).save(*args,**kwargs)