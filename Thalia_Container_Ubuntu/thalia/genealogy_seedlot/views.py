"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import
import django_mongoengine
#a mettre sinon pas de resultat a voir
from mongoengine import *
from django import forms
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render, render_to_response
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.datastructures import MultiValueDictKeyError
from django.db.transaction import TransactionManagementError
#necessaire sinon pb !!
from mongoengine.queryset.visitor import Q
from django.db.models import Q as Query_dj
import datetime
import ast ##convertir des strings en dict
from itertools import product
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import csv
import json
from django.db import models, IntegrityError, transaction
from django.conf import settings

from genealogy_managers.models import Reproduction, Reproduction_method
from genealogy_seedlot.models import SeedlotRelation
from genealogy_accession.models import AccessionRelation
from genealogy_managers.forms import MethodForm
from genealogy_accession.forms import AccessionHybridCrossForm
from genealogy_seedlot.forms import SeedlotHybridCrossForm, MultiplicationForm
from commonfct.utils import get_fields
from lot.models import Seedlot
from accession.models import Accession
from accession.forms import UploadFileForm
from commonfct.utils import recode
from phenotyping.views import write_csv_search_result

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Seedlothybridcross_management(request):
    seedlothybridform = SeedlotHybridCrossForm()
    template = "genealogy/hybridcross.html"
    names, labels = get_fields(seedlothybridform) 
    title = "Seedlot Hybrid Cross Management"
    formf = UploadFileForm()
    filetype = "seedlothybridcross"
    typeid = 0
    errormsg = False
    type_method = 3
        
    ##type method = 3 => sl crossing method
    model_relation = "SeedlotRelationCross"
    modelname = SeedlotRelation
    data_or_null = 0
    list_attributes=['hybrid seedlot', 'parent male', 'parent female', "crossing method", "site", "start date", "end date"]
    list_exceptions = []
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    print('dico: ',dico)
    classname = "SeedlotRelation"
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":seedlothybridform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"SeedlotRelation",
              'refine_search':True,
              'list_attributes':list_attributes,
              }
    if len(dico)==0:
        tag_fields.update({'all':False})
    if request.method == "POST":
        seedlothybridform = SeedlotHybridCrossForm(request.POST)
        formf = UploadFileForm(request.POST, request.FILES) 
        if seedlothybridform.is_valid():
            try:
                repro_id, child, parent_male, parent_female, repro_method, site, start_date, end_date, comments = seedlothybridform.create_hybrid_cross()
    
                #dico[child.name] = [child, parent_male, parent_female, repro_method, site, start_date, end_date, comments]
                dico[repro_id] = [child, parent_male, parent_female, repro_method, site, start_date, end_date, comments]
    #         
                tag_fields.update({"all":dico.items,
                                   "hybridform":seedlothybridform,
                                   })
                return render(request, template, tag_fields)
            except ValidationError as e:
                errormsg=e.message
        elif formf.is_valid():
            separator = request.POST['delimiter']
            return _create_genealogydata_from_file(request, template, tag_fields, separator, model_relation, modelname, type_method)
        
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in seedlothybridform.errors.items() ])
#     return render(request, template, tag_fields)
    return refine_search(request, seedlothybridform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Multiplication_management(request):
    multiplicationform = MultiplicationForm()
    template = "genealogy/hybridcross.html"

    names, labels = get_fields(multiplicationform) 
    title = "Seedlot Multiplication Management"
    
    formf = UploadFileForm()
    filetype = "seedlotmultiplication"
    typeid = 0
    errormsg = False
    type_method = 1 ##type method = 1 => multiplication method
    model_relation = "SeedlotRelationMultiplication"
    modelname = SeedlotRelation
    dico = {}
    dico_interm = {}
    data_or_null = 0
    list_attributes=['seedlot', 'parent', "multiplication method", "site", "start date", "end date"]
    list_exceptions = []
    dico = fill_dico(type_method, model_relation, modelname, data_or_null)
    classname = "SeedlotRelation"
    tag_fields = {'title':title,
              'fields_name':names,
              'fields_label':labels,
              "admin":True,
              "form":multiplicationform,
              "formfile":formf,
              'excel_empty_file':True,
              "creation_mode":True,
              'nb_per_page':50,
              'all':dico.items,
              "filetype":filetype,
              "typeid":typeid,
              "classname":"SeedlotRelation"
              }
    if len(dico)==0:
        tag_fields.update({'all':False})
    if request.method == "POST":
        multiplicationform = MultiplicationForm(request.POST)
        formf = UploadFileForm(request.POST, request.FILES) 
        if multiplicationform.is_valid():
            try:
                repro_id, child, parent, repro_method, site, start_date, end_date, comments = multiplicationform.create_multiplication()
                #dico[child.name] = [child, parent, repro_method, site, start_date, end_date, comments]
                dico[repro_id] = [child, parent, repro_method, site, start_date, end_date, comments]
                
                tag_fields.update({"all":dico.items,
                                   "hybridform":multiplicationform,
                                   })
                return render(request, template, tag_fields)
            except ValidationError as e:
                errormsg=e.message
        
        elif formf.is_valid():
            separator = request.POST['delimiter']
            return _create_genealogydata_from_file(request, template, tag_fields, separator, model_relation, modelname, type_method)
                
        else:
            errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in multiplicationform.errors.items() ])
    return refine_search(request, multiplicationform, formf, dico, template, list_attributes, list_exceptions, title, modelname, filetype, typeid, errormsg, model_relation, type_method, classname)

def _create_genealogydata_from_file(request, template, tag_fields, separator, model_relation, modelname, type_method):
    file_uploaded = _upload_file(request.FILES['file'], request, separator, model_relation, modelname, type_method)
    if type(file_uploaded) is str : #1er cas: l'insertion provoque des erreurs
        if file_uploaded.count(separator) > 0 :
            counter = file_uploaded.count(separator) + 1
            if counter == 1:
                errormsg = 'A {0} from your file is already in the database: '.format(str(model_relation)) + file_uploaded
            else:
                errormsg=  str(counter) + ' {0}s are already in your database: '.format(str(model_relation)) + file_uploaded
            tag_fields.update({'errormsg':errormsg})
            return render(request, template, tag_fields)
        else :
            tag_fields.update({'errormsg':file_uploaded})
            return render(request, template, tag_fields)
    elif type(file_uploaded) is list:
        #Cas où l'utilisateur tente un update mais que certaines données du CSV ne sont pas dans la BDD
        missing_data = file_uploaded[0]
        del file_uploaded[0]
        if file_uploaded !=[] and len(file_uploaded) ==1:
            missing_data= missing_data + ' ,' + file_uploaded[0]
        elif file_uploaded != [] and len(file_uploaded)>1:
            missing_data = missing_data + ' ,' +  '  ,'.join(file_uploaded)
        errormsg = 'Some {0}s of your file aren\'t in your database: '.format(str(model_relation)) + missing_data + ' .No update was done.'
        tag_fields.update({'errormsg':errormsg})
        return render(request, template, tag_fields)
    else:
        #Cas d'une insertion où d'un update fonctionnel
        inserted = True
        search_or_not = 0
        dico = fill_dico(type_method, model_relation, modelname, search_or_not)
        if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
            tag_fields.update({'all':dico.items, "msg_success":"All elements from your file have been inserted."})
        else: #L'utilisateur a cliqué sur le bouton update
            tag_fields.update({'all':dico.items, "msg_success":"All elements have been updated according to your file."})
        return render(request, template, tag_fields)

def _upload_file(files, request, separator, model_relation, modelname, type_method):
    if files != None:
        dictline = {}
        i = 0
        for line in files:
            #On récurère chaque ligne du fichier: la case i contiendra la i-1 ème ligne du fichier
            dictline[i] = line.strip()
            i+=1
        headers = recode(dictline[0])
        #la première ligne du tableau donne attributs du fichier; on les conserve dans un autre tableau et on efface la première ligne de dictvalues
        theaders = headers.split(separator)
        dictvalues_final = {}
        del dictline[0]
        t=0
        try:
            for key in dictline:
                #On récupère les valeurs correspondant aux clés
                v = recode(dictline[key])
                val = v.split(separator)
                dictvalues = {}
                for i in range(len(theaders)): 
                    #On remplit dictvalues avec les clés et valeur              
                    dictvalues[theaders[i]] = val[i]
                #On stocke le dictionnaire obtenu dans un autre dictionnaire: la case i contiendra la i+1 ème ligne du fichier
                dictvalues_final[t] = dictvalues
                t+=1
        except IndexError: #Cas où certaines lignes seraient vides, par exemple, si l'utilisateur est passé à la ligne après avoir inséré la dernière donnée de son .csv
            return 'There are empty lines in your file. Please fill or remove them.'
        else:
            if request.POST.get("submit"): #Cas d'une insertion de nouvelles données
                return _submit_upload_file(dictvalues_final, model_relation, modelname, type_method)
            elif request.POST.get("update"): #L'utilisateur a cliqué sur le bouton update
                return _update_upload_file(dictvalues_final, model_relation, modelname, type_method)
            else:
                database=Accession.objects.all().values_list('name')
                return 'ThaliaDB was unable to identify if you\'re trying to insert new data or to update former data.'
 
def _submit_upload_file(dictvalues_final, model_relation, modelname, type_method):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins un doublon, aucune donnée n'est insérée : soit l'utilisateur voulait faire un update, soit il ne s'est pas apercu des doublons
    Tout d'abord, on extrait les noms des accessions (clé primaire) de la BDD'''
    try:
        database = modelname.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = type_method)))
        keys_in_database=[i for i in database]
        #Maintenant, on récupère les noms des METHODS du CSV, contenues dans dictvalues_final
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
#         print(keys_inserted)
    except KeyError:
        return '1: The headers of your files do not correspond to those in the database. Please check them'
    else:
        #First we check if all the accessions or seedlots are in the db
        if model_relation == "AccessionRelationCross":
            entity = Accession
        else:
            entity = Seedlot
        try:
            i = 2
            for l in dictvalues_final :
                child = entity.objects.get(name=dictvalues_final[l]['Name'])
                if model_relation == "SeedlotRelationMultiplication":
                    parent = entity.objects.get(name=dictvalues_final[l]['Parent'])
                else:
                    parentm = entity.objects.get(name=dictvalues_final[l]['Parent Male'])
                    parentf = entity.objects.get(name=dictvalues_final[l]['Parent Female'])
                i+=1
        except:
            return 'A Seedlot or Accession at line '+ str(i) +' of the file is not in the database. Nothing have been inserted.'
        
        #On va maintenant comparer les deux listes
        #Ce hash stocke les donnees en commun entre le fichier et la base
        data = {} 
        for m in range(len(keys_inserted)):
            for n in range(len(keys_in_database)):
                if keys_inserted[m]==str(keys_in_database[n].child):
                    if not keys_inserted[m] in data:
                            data[keys_inserted[m]] = '1'
                            
        data_doublons = ''
        for key in data.keys():
            if data_doublons =='':
                data_doublons = keys_inserted[m]
            else:
                data_doublons = data_doublons + ' ;' + key
                    
        if set(keys_in_database) == set(keys_inserted):
            return 'All the methods in your CSV file are already in your database. Maybe you are trying to update former data ?'
        else:
            if data_doublons != '':
                return data_doublons
            else: #Cas de l'insertion
                jsonvalues={}
                try:
                    for k in dictvalues_final:
                        #On récupère la k+1 ème ligne du fichier
                        dval = dictvalues_final[k]
                        try :
                            if model_relation == "SeedlotRelationCross":
                                repro = Reproduction.objects.create(reproduction_method=Reproduction_method.objects.get(name=str(dval['Crossing Method'])),
                                                                    start_date = dval['Start Date'], end_date = dval['End Date'], description = str(dval['Comments']), entity_type=1)
                                a  = modelname.objects.create(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                              parent = Seedlot.objects.get(name = str(dval['Parent Male'])),
                                                              parent_gender = 'M',
                                                              reproduction = repro,
                                                              site = str(dval['Site'])
                                                              )
                                b = modelname.objects.create(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                              parent = Seedlot.objects.get(name = str(dval['Parent Female'])),
                                                              parent_gender = 'F',
                                                              reproduction = repro,
                                                              site = str(dval['Site'])
                                                              )
                            elif model_relation == "SeedlotRelationMultiplication":
                                repro = Reproduction.objects.create(reproduction_method=Reproduction_method.objects.get(name=str(dval['Multiplication Method'])),
                                                                    start_date = dval['Start Date'], end_date = dval['End Date'], description = str(dval['Comments']), entity_type=1)
                                a  = modelname.objects.create(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                              parent = Seedlot.objects.get(name = str(dval['Parent'])),
                                                              parent_gender = 'X',
                                                              reproduction = repro,
                                                              site = str(dval['Site'])
                                                              )
                            elif model_relation == "AccessionRelationCross":
                                if dval['First Production'] == "":
                                    dval['First Production']=None
                                repro = Reproduction.objects.create(reproduction_method=Reproduction_method.objects.get(name=str(dval['Crossing Method'])),
                                                                    description = str(dval['Comments']), entity_type=0)

                                a  = modelname.objects.create(child = Accession.objects.get(name = str(dval['Name'])),
                                                              parent = Accession.objects.get(name = str(dval['Parent Male'])),
                                                              parent_gender = 'M',
                                                              reproduction = repro,
                                                              synonymous = str(dval['Synonymous']),
                                                              first_production = dval['First Production']
                                                              )
                                b = modelname.objects.create(child = Accession.objects.get(name = str(dval['Name'])),
                                                              parent = Accession.objects.get(name = str(dval['Parent Female'])),
                                                              parent_gender = 'F',
                                                              reproduction = repro,
                                                              synonymous = str(dval['Synonymous']),
                                                              first_production = dval['First Production']
                                                              )
                        except IntegrityError as ie :
                            return str(ie)
                        
                        except ValidationError as ve:
                            return str(ve)
                        
                        except ObjectDoesNotExist as dne:
                            return str(dne)
                        
                except KeyError:
                    return '2: The headers of your files do not correspond to those in the database. Please check them and mind the uppercase and the lowercase.'
                
                except IntegrityError as ie:
                    return str(ie)


def _update_upload_file(dictvalues_final, model_relation, modelname, type_method):
    '''Dans ce cas, on va comparer les données du CSV et celles de la BDD via leur clé primaire
    S'il y a au moins une nouvelle donnee, rien n'est mis à jour
    '''
    try:
        database = modelname.objects.filter(reproduction__in=Reproduction.objects.filter(reproduction_method__in=Reproduction_method.objects.filter(category = type_method)))
        keys_in_database=[i for i in database]
        #Maintenant, on récupère les noms des METHODS du CSV, contenues dans dictvalues_final
        keys_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
#         print(keys_inserted)
    except KeyError:
        return '1: The headers of your files do not correspond to those in the database. Please check them'
    else:
        #First we check if all the accessions or seedlots are in the db
        if model_relation == "AccessionRelationCross":
            entity = Accession
        else:
            entity = Seedlot
        try:
            i = 2
            for l in dictvalues_final :
                child = entity.objects.get(name=dictvalues_final[l]['Name'])
                if model_relation == "SeedlotRelationMultiplication":
                    parent = entity.objects.get(name=dictvalues_final[l]['Parent'])
                else:
                    parentm = entity.objects.get(name=dictvalues_final[l]['Parent Male'])
                    parentf = entity.objects.get(name=dictvalues_final[l]['Parent Female'])
                i+=1
        except:
            return 'A Seedlot or Accession at line '+ str(i) +' of the file is not in the database. Nothing have been updated.'
        
        #On va maintenant comparer les deux listes
        #Ce hash stocke les donnees en commun entre le fichier et la base
        data = {} 
        for m in range(len(keys_inserted)):
            for n in range(len(keys_in_database)):
                if keys_inserted[m]==str(keys_in_database[n].child):
                        if not keys_inserted[m] in data:
                            data[keys_inserted[m]] = '1'
        #Cette liste stocke les donnees qui n'existent pas dans la base
        list_data = []
        for key in keys_inserted:
            if not key in data:
                list_data.append(key)
                                     
        if list_data != []:
            return list_data
        else: #S'il n'y a que des donnees connues
            keys_updated = 0
            for k in dictvalues_final:
                dval = dictvalues_final[k]
                try:
                    if model_relation == "AccessionRelationCross":
                        if dval['First Production'] == "":
                                    dval['First Production']=None
                        a  = modelname.objects.get(child = Accession.objects.get(name = str(dval['Name'])),
                                                   parent = Accession.objects.get(name = str(dval['Parent Male'])),
                                                   parent_gender = 'M'
                                                   )
                        
                        b = modelname.objects.get(child = Accession.objects.get(name = str(dval['Name'])),
                                                  parent = Accession.objects.get(name = str(dval['Parent Female'])),
                                                  parent_gender = 'F'
                                                  )
                        if a.synonymous != str(dval['Synonymous']):
                            setattr(a, 'synonymous', str(dval['Synonymous']))
                            setattr(b, 'synonymous', str(dval['Synonymous']))
                            a.save()
                            b.save()
                            keys_updated = 1
                            
                        if str(a.first_production) != dval['First Production']:
                            setattr(a, 'first_production', dval['First Production'])
                            setattr(b, 'first_production', dval['First Production'])
                            a.save()
                            b.save()
                            keys_updated = 1
                        
                        repro = Reproduction.objects.get(id = a.reproduction_id)
                        
                        if repro.reproduction_method != Reproduction_method.objects.get(name=str(dval['Crossing Method'])):
                            setattr(repro, 'reproduction_method', Reproduction_method.objects.get(name=str(dval['Crossing Method'])))
                            repro.save()
                            keys_updated = 1
                        if repro.description != str(dval['Comments']):
                            setattr(repro, 'description', str(dval['Comments']))
                            repro.save()
                            keys_updated = 1
                        
                    elif model_relation == "SeedlotRelationCross":
                                
                        a  = modelname.objects.get(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                   parent = Seedlot.objects.get(name = str(dval['Parent Male'])),
                                                   parent_gender = 'M'
                                                   )
                        
                        b = modelname.objects.get(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                  parent = Seedlot.objects.get(name = str(dval['Parent Female'])),
                                                  parent_gender = 'F'
                                                  )
                        
                        if a.site != str(dval['Site']):
                            setattr(a, 'site', str(dval['Site']))
                            setattr(b, 'site', str(dval['Site']))
                            a.save()
                            b.save()
                            keys_updated = 1
                            
                        repro = Reproduction.objects.get(id = a.reproduction_id)
                        
                        if repro.reproduction_method != Reproduction_method.objects.get(name=str(dval['Crossing Method'])):
                            setattr(repro, 'reproduction_method', Reproduction_method.objects.get(name=str(dval['Crossing Method'])))
                            repro.save()
                            keys_updated = 1
                        if str(repro.start_date) != dval['Start Date']:
                            setattr(repro, 'start_date', dval['Start Date'])
                            repro.save()
                            keys_updated = 1
                        if str(repro.end_date) != dval['End Date']:
                            setattr(repro, 'end_date', dval['End Date'])  
                            repro.save()
                            keys_updated = 1
                        if repro.description != str(dval['Comments']):
                            setattr(repro, 'description', str(dval['Comments']))
                            repro.save()
                            keys_updated = 1
                        
                    elif model_relation == "SeedlotRelationMultiplication":
                        
                        a  = modelname.objects.get(child = Seedlot.objects.get(name = str(dval['Name'])),
                                                   parent = Seedlot.objects.get(name = str(dval['Parent'])),
                                                   parent_gender = 'X'
                                                   )
                        if a.site != str(dval['Site']):
                            setattr(a, 'site', str(dval['Site']))
                            a.save()
                            keys_updated = 1
                            
                        repro = Reproduction.objects.get(id = a.reproduction_id)    
                            
                        if repro.reproduction_method != Reproduction_method.objects.get(name=str(dval['Multiplication Method'])):
                            setattr(repro, 'reproduction_method', Reproduction_method.objects.get(name=str(dval['Multiplication Method'])))
                            repro.save()
                            keys_updated = 1
                        if str(repro.start_date) != dval['Start Date']:
                            setattr(repro, 'start_date', dval['Start Date'])
                            repro.save()
                            keys_updated = 1
                        if str(repro.end_date) != dval['End Date']:
                            setattr(repro, 'end_date', dval['End Date'])   
                            repro.save()
                            keys_updated = 1
                        if repro.description != str(dval['Comments']):
                            setattr(repro, 'description', str(dval['Comments']))
                            repro.save()
                            keys_updated = 1
                    
                    else:
                        return 'An error has occurred, please contact an administrator.'
                            
                except:
                    return '{0} exists in the database with other parameters.'.format(str(dval['Name'])) + ' No update was done.'
            if keys_updated == 0:
                return 'All the entries are identical to those previously inserted. No update was done.'
                        
def fill_dico(type_method, model_relation, modelname, data_or_null):
    dico = {}
    dico_interm = {}
    ## pour n'afficher que les hybrides cross et pas les multiplications, il faut filtrer selon le type de methode utilisee
    if data_or_null == 0:
        print(type_method)
        repro_method_cross = Reproduction_method.objects.filter(category=type_method) ## type3 = sl cross method, type 2 = acc crossing method, type1=multiplication method
        repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
        all_relation = modelname.objects.filter(reproduction__in=repro_cross)
    else:
        name_child = []
        for i in data_or_null:
            name_child.append(i.child)
        all_relation = modelname.objects.filter(child__in = name_child)
    for i in all_relation:
        if i.reproduction.id not in dico_interm:
            dico_interm[i.reproduction.id]={}
        dico_interm[i.reproduction.id]['child'] = i.child.name
        if i.parent_gender == "M":
            if 'parent_male' in dico_interm[i.reproduction.id]:
                if type(dico_interm[i.reproduction.id]['parent_male']) == list:
                    dico_interm[i.reproduction.id]['parent_male'].append(i.parent.name)
                else:
                    dico_interm[i.reproduction.id]['parent_male'] = [dico_interm[i.reproduction.id]['parent_male']]
                    dico_interm[i.reproduction.id]['parent_male'].append(i.parent.name)
            else:
                dico_interm[i.reproduction.id]['parent_male'] = i.parent.name
        elif i.parent_gender == "F":
            if 'parent_female' in dico_interm[i.reproduction.id]:
                if type(dico_interm[i.reproduction.id]['parent_female']) == list:
                    dico_interm[i.reproduction.id]['parent_female'].append(i.parent.name)
                else:
                    dico_interm[i.reproduction.id]['parent_female'] = [dico_interm[i.reproduction.id]['parent_female']]
                    dico_interm[i.reproduction.id]['parent_female'].append(i.parent.name)
            else:
                dico_interm[i.reproduction.id]['parent_female'] = i.parent.name
        else:
            if 'parent' in dico_interm[i.reproduction.id]:
                if type(dico_interm[i.reproduction.id]['parent']) == list:
                    dico_interm[i.reproduction.id]['parent'].append(i.parent.name)
                else:
                    dico_interm[i.reproduction.id]['parent'] = [dico_interm[i.reproduction.id]['parent']]
                    dico_interm[i.reproduction.id]['parent'].append(i.parent.name)
            else:
                dico_interm[i.reproduction.id]['parent'] = i.parent.name
        if modelname == SeedlotRelation:
            dico_interm[i.reproduction.id]['repro_method'] = i.reproduction.reproduction_method.name
            dico_interm[i.reproduction.id]['comments']=i.reproduction.description
            dico_interm[i.reproduction.id]['site']=i.site
            dico_interm[i.reproduction.id]['start_date']=str(i.reproduction.start_date)
            dico_interm[i.reproduction.id]['end_date']=str(i.reproduction.end_date)
        else :
            dico_interm[i.reproduction.id]['repro_method'] = i.reproduction.reproduction_method.name
            dico_interm[i.reproduction.id]['comments']=i.reproduction.description
            dico_interm[i.reproduction.id]['synonymous']=i.synonymous
            dico_interm[i.reproduction.id]['first_production']=str(i.first_production)
    print(dico_interm)
    for i, j, in dico_interm.items():
        if model_relation == 'SeedlotRelationCross':
            dico[i]=[j['child'], j['parent_male'], j['parent_female'], j['repro_method'], j['site'], j['start_date'], j['end_date'], j['comments']]
        elif model_relation == 'SeedlotRelationMultiplication':
            if type(j['parent']) == list:
                parent = ' | '.join(j['parent'])
            else:
                parent = j['parent']
                
            dico[i]=[j['child'], parent, j['repro_method'], j['site'], j['start_date'], j['end_date'], j['comments']]
        else:
            if type(j['parent_male']) == list:
                parent_male = ' | '.join(j['parent_male'])
            else:
                parent_male = j['parent_male']
                
            if type(j['parent_female']) == list:
                parent_female = ' | '.join(j['parent_female'])
            else:
                parent_female = j['parent_female']
                
            dico[i]=[j['child'], parent_male, parent_female, j['repro_method'], j['synonymous'], j['first_production'], j['comments']]
            #dico[i]=[j['child'], j['parent_female'], j['parent_female'], j['repro_method'], j['synonymous'], j['first_production'], j['comments']]
    return dico

def refine_search(request, form, formf, data, template, list_attributes, list_exceptions, title, model, filetype, typeid, errormsg, model_relation, type_method,classname):
    number_all = len(data)
    if type(data)==dict:
        data=data.items
    nb_per_page = 10
    names, labels = get_fields(form)
    tag_fields = {'all':data,
              'fields_name':names,
              'fields_label':labels,
              'title': title, "admin":True,
              "errormsg":errormsg,
              "refine_search":True,
              "classname":classname,
              'admin':True}
    dico_get={}
    or_and = None
    if "no_search" in request.GET:
        tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       'all':data,
                       'excel_empty_file':True,
                       "formfile":formf,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       "filetype":filetype,
                       "typeid":typeid,
                       'form':form,
                       "list_attributes":list_attributes})
        return render(request,template,tag_fields)
     
    elif "or_and" in request.GET:
        or_and=request.GET['or_and']
        for i in range(1,10):
            data="data"+str(i)
            text_data="text_data"+str(i)
            if data in request.GET and text_data in request.GET:
                dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

        length_dico_get = len(dico_get)
        print("\ndico_get: ",dico_get)
        nb_per_page = 10
        if or_and == "or":
            proj=[]
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query |= Query_dj(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                elif "hybrid accession" in dico_type_data:
                    child_list = Accession.objects.filter(name__icontains=dico_type_data['hybrid accession'])
                    query |= Query_dj(child__in=child_list)
                elif "hybrid seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['hybrid seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "parent male" in dico_type_data :
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent male'])
                        query |= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent male'])
                        query |= Query_dj(parent__in=parent_list)
                elif "parent female" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent female'])
                        query |= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent female'])
                        query |= Query_dj(parent__in=parent_list)
                elif "parent" in dico_type_data:
                    parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent'])
                    query |= Query_dj(parent__in=parent_list)
                elif "crossing method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['crossing method']) ## type 2 = crossing method, type1=multiplication method
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query |= Query_dj(reproduction__in=repro_cross)
                elif "multiplication method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['multiplication method']) ## type 2 = crossing method, type1=multiplication method
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query |= Query_dj(reproduction__in=repro_cross)
                elif 'start date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['start date'])
                    query |= Query_dj(reproduction__in=date_repro)
                elif 'end date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['end date'])
                    query |= Query_dj(reproduction__in=date_repro)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query |= Query_dj(**{var_search : dico_type_data[data_type]})
             
        if or_and == "and":
            proj=[]
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        print('Projects empty')
                        query &= Query_dj(projects__isnull=True)
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                elif "hybrid accession" in dico_type_data:
                    child_list = Accession.objects.filter(name__icontains=dico_type_data['hybrid accession'])
                    query |= Query_dj(child__in=child_list)
                elif "hybrid seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['hybrid seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "seedlot" in dico_type_data:
                    child_list = Seedlot.objects.filter(name__icontains=dico_type_data['seedlot'])
                    query |= Query_dj(child__in=child_list)
                elif "parent male" in dico_type_data :
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent male'])
                        query &= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent male'])
                        query &= Query_dj(parent__in=parent_list)
                elif "parent female" in dico_type_data:
                    if model == AccessionRelation:
                        parent_list = Accession.objects.filter(name__icontains=dico_type_data['parent female'])
                        query &= Query_dj(parent__in=parent_list)
                    elif model == SeedlotRelation:
                        parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent female'])
                        query &= Query_dj(parent__in=parent_list)
                elif "parent" in dico_type_data:
                    parent_list = Seedlot.objects.filter(name__icontains=dico_type_data['parent'])
                    query &= Query_dj(parent__in=parent_list)
                elif "crossing method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['crossing method']) ## type 2 = crossing method, type1=multiplication method
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query &= Query_dj(reproduction__in=repro_cross)
                elif "multiplication method" in dico_type_data:
                    repro_method_cross = Reproduction_method.objects.filter(name__icontains=dico_type_data['multiplication method']) ## type 2 = crossing method, type1=multiplication method
                    repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
                    query &= Query_dj(reproduction__in=repro_cross)
                elif 'start date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['start date'])
                    query &= Query_dj(reproduction__in=date_repro)
                elif 'end date' in dico_type_data:
                    date_repro = Reproduction.objects.filter(start_date__icontains=dico_type_data['end date'])
                    query &= Query_dj(reproduction__in=date_repro)
                else:
                    for data_type, value in dico_type_data.items():
                        var_search = str( data_type + "__icontains" ) 
                        var_search = var_search.replace(' ','_')
                        query &= Query_dj(**{var_search : dico_type_data[data_type]})
        repro_method_cross = Reproduction_method.objects.filter(category=type_method) ## type3 = sl cross method, type 2 = acc crossing method, type1=multiplication method
        repro_cross = Reproduction.objects.filter(reproduction_method__in=repro_method_cross)
        query &= Query_dj(reproduction__in=repro_cross)
        data = model.objects.filter(query).distinct()
        number_all = len(data)
        dico = fill_dico(type_method, model_relation, model, data)
        download_not_empty = False
        if data:
            download_not_empty = write_csv_search_result(data, list_exceptions)
        tag_fields.update({"download_not_empty":False, 'all':dico.items})
    #Cas où veut afficher la base de données
    #accessions = _get_accessions(accessiontype, attributes, request)
    tag_fields.update({"search_result":dico_get,
                       "or_and":or_and,
                       "number_all":number_all,
                       'nb_per_page':nb_per_page,
                       'creation_mode':True,
                       'form':form,
                       "list_attributes":list_attributes,
                       "filetype":filetype,
                       'excel_empty_file':True,
                       "typeid":typeid,
                       "formfile":formf})  
    return render(request, template, tag_fields) 
