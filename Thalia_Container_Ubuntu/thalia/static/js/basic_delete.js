$(document).ready(
		function () {
			$('.delete').on('click',function() {
				object = $(this).next('[name=objectid]');
				classe = object.next('[name=classid]');

				if(confirm("Do you really want to delete "+$(this).attr('name')+" ?")){
					$.ajax({
						url:ROOT_URL.concat("genericdelete/"),
						type:"POST",
						data: {'objectid':object.val(),
								'classname':classe.val()},
						dataType: "json",
		                success: function(json) {
		                	alert(json.response);
		                },
		                error: function(jqXHR, textStatus, errorThrown) {
		                	alert("Deletion failed ! fichier basic_delete "+textStatus+"/ "+errorThrown);
		                }
					});
					
					$(this).closest('tr').remove();
					//cas particulier du password pour User
					$(this).closest('form').find("#id_login").closest('tr').after('<tr><th><label for="id_password">Password:</label></th><td><input id="id_password" maxlength="128" name="password" type="text"></td></tr>');
				}			
				
			});
		}
);

