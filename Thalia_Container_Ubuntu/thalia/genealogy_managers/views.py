# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import absolute_import
import django_mongoengine
#a mettre sinon pas de resultat a voir
from mongoengine import *
from django import forms
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render, render_to_response
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
#necessaire sinon pb !!
from mongoengine.queryset.visitor import Q
from django.db.models import Q as Query_dj
import datetime
import ast ##convertir des strings en dict
from itertools import product
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import csv
import json
from django.conf import settings

from accession.models import Accession
from lot.models import Seedlot
from commonfct.utils import get_fields
from commonfct.genericviews import generic_management
from genealogy_managers.models import Reproduction, Reproduction_method
from genealogy_managers.forms import MethodForm
from genealogy_accession.models import AccessionRelation
from genealogy_accession.forms import AccessionHybridCrossForm, OtherAccessionPedigreeForm
from genealogy_seedlot.models import SeedlotRelation
from genealogy_seedlot.forms import SeedlotHybridCrossForm, MultiplicationForm

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def genealogyhome(request):
    template = "genealogy/genealogyhome.html"
    return render(request, template, {'admin':True})

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def Method_management(request):
    methodform = MethodForm()
    template = "genealogy/method_management.html"
    names, labels = get_fields(methodform) 
    title = "Method Management"
    all = Reproduction_method.objects.all()
    errormsg = False
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "admin":True,
                  "form":methodform,
                  "creation_mode":True,
                  'nb_per_page':50,
                  "all":all
                  }
    if request.method == "POST":
        methodform = MethodForm(request.POST)
        if methodform.is_valid():
            try :
                name = request.POST['name']
                category = request.POST['category']
                description = request.POST['description']
                Reproduction_method.objects.create(name=name, category=category, description=description)
                all = Reproduction_method.objects.all()
                tag_fields.update({"all":all,
                                   "form":methodform,
                                   })
                return render(request, template, tag_fields)
    
            except Exception as e :
                errormsg = "ERROR"
        else:
            #errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in methodform.errors.items() ])
            return generic_management(Reproduction_method, MethodForm, request, 'genealogy/method_management.html',{"admin":True},"Method Management")
    tag_fields.update({"errormsg":errormsg})
    return render(request, template, tag_fields)

@login_required
@user_passes_test(lambda u: u.is_accession_admin_user(), login_url='/team/')
def genealogy_manager(request, pk):
    """
    Management of genealogy's informations of seedlot and accession.
    
    :param int pk : primary key of the reproduction
    
    :var Reproduction reproduction: reproduction to manage
    :var string template: template 
    :var Method method: method of the reproduction 
    :var string category: category of the method
    """
    
    template = "genealogy/genealogy_manager.html"
    #Reproduction qui est appellee
    reproduction = Reproduction.objects.get(id=pk)
    method = Reproduction_method.objects.get(id=reproduction.reproduction_method_id)
    category = str(method.category)
    
    #On definit le formulaire a utiliser en fonction de la category
    if category == '2':
        form_class = AccessionHybridCrossForm
        back_page = 'accessionhybridcross'
    elif category == '4':
        form_class = OtherAccessionPedigreeForm
        back_page = 'otheraccessionpedigree'
    elif category == '3':
        form_class = SeedlotHybridCrossForm
        back_page = 'seedlothybridcross'
    elif category == '1':
        form_class = MultiplicationForm
        back_page = 'multiplication'
    
    #Si on appelle l'edit : affichage du formulaire rempli avec les infos
    if request.method == "GET":
        
        if category == '2':
            type = 'Accession Hybrid'
            
            relation_m = AccessionRelation.objects.get(parent_gender='M', reproduction_id=reproduction.id)
            relation_f = AccessionRelation.objects.get(parent_gender='F', reproduction_id=reproduction.id)
            child = Accession.objects.get(id=relation_m.child_id)
            
            form = form_class(initial={'hybrid_accession':child,
                                       'parent_male':relation_m.parent_id,
                                       'parent_female':relation_f.parent_id,
                                       'crossing_method':method.id,
                                       'synonymous':relation_m.synonymous,
                                       'first_production':relation_m.first_production,
                                       'comments':reproduction.description})
        elif category == '4':            
            type = 'Accession'

            relations_m = AccessionRelation.objects.filter(parent_gender='M', reproduction_id=reproduction.id)
            list_rel_m = [rel_m.parent_id for rel_m in relations_m]
            relations_f = AccessionRelation.objects.filter(parent_gender='F', reproduction_id=reproduction.id)
            list_rel_f = [rel_f.parent_id for rel_f in relations_f]
            child = Accession.objects.get(id=relations_m[0].child_id)
            
            form = form_class(initial={'hybrid_accession':child,
                                       'parent_male':list_rel_m,
                                       'parent_female':list_rel_f,
                                       'crossing_method':method.id,
                                       'synonymous':relations_m[0].synonymous,
                                       'first_production':relations_m[0].first_production,
                                       'comments':reproduction.description})
            
        elif category == '3':
            type = 'Seedlot Hybrid'
            
            relation_m = SeedlotRelation.objects.get(parent_gender='M', reproduction_id=reproduction.id)
            relation_f = SeedlotRelation.objects.get(parent_gender='F', reproduction_id=reproduction.id)
            child = Seedlot.objects.get(id=relation_m.child_id)
            
            form = form_class(initial={'hybrid_seedlot':child,
                                       'parent_male':relation_m.parent_id,
                                       'parent_female':relation_f.parent_id,
                                       'crossing_method':method.id,
                                       'site':relation_m.site,
                                       'start_date':reproduction.start_date,
                                       'end_date':reproduction.end_date,
                                       'comments':reproduction.description})
            
        elif category == '1':
            type = 'Seedlot'
            
            relations_x = SeedlotRelation.objects.filter(parent_gender='X', reproduction_id=reproduction.id)
            list_rel_x = [rel_x.parent_id for rel_x in relations_x]
            child = Seedlot.objects.get(id=relations_x[0].child_id)
            
            form = form_class(initial={'seedlot':child,
                                       'parent':list_rel_x,
                                       'multiplication_method':method.id,
                                       'site':relations_x[0].site,
                                       'start_date':reproduction.start_date,
                                       'end_date':reproduction.end_date,
                                       'comments':reproduction.description})
            
            
        return render(request, template, {"admin":True, "back_page":back_page, "child":child, "type":type, "form":form})
    
    form = form_class(request.POST)
    
    #Si on clique sur update
    #Pour chaque objet on peut tout modifier sauf l'accession ou le seedlot enfant    
    if form.is_valid():
        
        if form_class == AccessionHybridCrossForm:
            type = 'Accession Hybrid'
            
            relation_m = AccessionRelation.objects.get(parent_gender='M', reproduction_id=reproduction.id)
            relation_f = AccessionRelation.objects.get(parent_gender='F', reproduction_id=reproduction.id)
            child = Accession.objects.get(id=relation_m.child_id)
            
            if form.cleaned_data['hybrid_accession'].id != child.id:
                error_msg = 'You can\'t modify the main '+ type +'.'
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
            relation_m.parent_id = form.cleaned_data['parent_male'].id
            relation_f.parent_id = form.cleaned_data['parent_female'].id
            reproduction.reproduction_method_id = form.cleaned_data['crossing_method'].id
            relation_m.synonymous = form.cleaned_data['synonymous']
            relation_f.synonymous = form.cleaned_data['synonymous']
            relation_m.first_production = form.cleaned_data['first_production']
            relation_f.first_production = form.cleaned_data['first_production']
            reproduction.description = form.cleaned_data['comments']
            
            try:
                relation_m.save()
                relation_f.save()
                reproduction.save()
            except Exception as e:
                error_msg = e
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
        
        elif form_class == OtherAccessionPedigreeForm:
            type = 'Accession'
            
            relations_m = AccessionRelation.objects.filter(parent_gender='M', reproduction_id=reproduction.id)
            list_rel_m = [rel_m.parent_id for rel_m in relations_m]
            relations_f = AccessionRelation.objects.filter(parent_gender='F', reproduction_id=reproduction.id)
            list_rel_f = [rel_f.parent_id for rel_f in relations_f]
            child = Accession.objects.get(id=relations_m[0].child_id)
            
            if form.cleaned_data['hybrid_accession'].id != child.id:
                error_msg = 'You can\'t modify the main '+ type +'.'
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
            select_pm = form.cleaned_data['parent_male']
            select_pf = form.cleaned_data['parent_female']
            
            list_pm = []
            list_pf = []
            #On cree les nouvelles relation s'il y en a
            try:
                for pm in select_pm:
                    list_pm.append(AccessionRelation.objects.filter(parent_gender='M', child_id=child.id, parent_id=pm.id, reproduction_id=reproduction.id))
                    if not AccessionRelation.objects.filter(parent_gender='M', child_id=child.id, parent_id=pm.id, reproduction_id=reproduction.id):
                        p = AccessionRelation(parent_gender='M', child_id=child.id, parent_id=pm.id, reproduction_id=reproduction.id, 
                                              first_production=form.cleaned_data['first_production'], synonymous=form.cleaned_data['synonymous'])
                        p.save()
                for pf in select_pf:
                    list_pf.append(AccessionRelation.objects.filter(parent_gender='F', child_id=child.id, parent_id=pf.id, reproduction_id=reproduction.id))
                    if not AccessionRelation.objects.filter(parent_gender='F', child_id=child.id, parent_id=pf.id, reproduction_id=reproduction.id):
                        p = AccessionRelation(parent_gender='F', child_id=child.id, parent_id=pf.id, reproduction_id=reproduction.id,
                                              first_production=form.cleaned_data['first_production'], synonymous=form.cleaned_data['synonymous'])
                        p.save()
                        
            except Exception as e:
                error_msg = e
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})   
                
            select_rel_m = []
            select_rel_f = []
            
            #On fait la liste des nouvelles accessions
            for rel_m in list_pm:
                for rm in rel_m:
                    select_rel_m.append(rm)
                    
            for rel_f in list_pf:
                for rf in rel_f:
                    select_rel_f.append(rf)
            #On recupere les relations presentes dans la base de donnees        
            all_pm = AccessionRelation.objects.filter(parent_gender='M', child_id=child.id)
            all_pf = AccessionRelation.objects.filter(parent_gender='F', child_id=child.id)
            
            #On compare les deux listes pour ne garder les relations du formulaire d'update, si elles existaient et sont conservees 
            #on les modifie avec les nouveaux parametres, si certaines ont ete enlevees, on supprime les relations dans la BD
            for pm in all_pm:
                if pm not in select_rel_m:
                    pm.delete()
                else:
                    pm.first_production=form.cleaned_data['first_production']
                    pm.synonymous=form.cleaned_data['synonymous']
                    pm.save()
                    
            for pf in all_pf:
                if pf not in select_rel_f:
                    pf.delete()
                else:
                    pf.first_production=form.cleaned_data['first_production']
                    pf.synonymous=form.cleaned_data['synonymous']
                    pf.save()
            
            #On sauvegarde les parametres associes a la reproduction        
            reproduction.reproduction_method_id = form.cleaned_data['crossing_method'].id
            reproduction.description = form.cleaned_data['comments']
            reproduction.save()
                   
        elif form_class == SeedlotHybridCrossForm:
            type = 'Seedlot Hybrid'
            
            relation_m = SeedlotRelation.objects.get(parent_gender='M', reproduction_id=reproduction.id)
            relation_f = SeedlotRelation.objects.get(parent_gender='F', reproduction_id=reproduction.id)
            child = Seedlot.objects.get(id=relation_m.child_id)
            
            if form.cleaned_data['hybrid_seedlot'].id != child.id:
                error_msg = 'You can\'t modify the main '+ type +'.'
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
            relation_m.parent_id = form.cleaned_data['parent_male'].id
            relation_f.parent_id = form.cleaned_data['parent_female'].id
            reproduction.reproduction_method_id = form.cleaned_data['crossing_method'].id
            relation_m.site = form.cleaned_data['site']
            relation_f.site = form.cleaned_data['site']
            reproduction.start_date = form.cleaned_data['start_date']
            reproduction.end_date = form.cleaned_data['end_date']
            reproduction.description = form.cleaned_data['comments']
            
            try:
                relation_m.save()
                relation_f.save()
                reproduction.save()
            except Exception as e:
                error_msg = e
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
        elif form_class == MultiplicationForm:
            type = 'Seedlot'
            
            relations_x = SeedlotRelation.objects.filter(parent_gender='X', reproduction_id=reproduction.id)
            list_rel_x = [rel_x.parent_id for rel_x in relations_x]
            child = Seedlot.objects.get(id=relations_x[0].child_id)
            
            if form.cleaned_data['seedlot'].id != child.id:
                error_msg = 'You can\'t modify the main '+ type +'.'
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
            select_px = form.cleaned_data['parent']
            
            list_px = []
            #On cree les nouvelles relation s'il y en a
            try:
                for px in select_px:
                    list_px.append(SeedlotRelation.objects.filter(parent_gender='X', child_id=child.id, parent_id=px.id, reproduction_id=reproduction.id))
                    if not SeedlotRelation.objects.filter(parent_gender='X', child_id=child.id, parent_id=px.id, reproduction_id=reproduction.id):
                        p = SeedlotRelation(parent_gender='X', child_id=child.id, parent_id=px.id, reproduction_id=reproduction.id, 
                                            site=form.cleaned_data['site'])
                        p.save()
            
            except Exception as e:
                error_msg = e
                return render(request, template, {'admin':True, "back_page":back_page, "child":child, "type":type, "form":form, "error_msg":error_msg})
            
            select_rel_x = []
            #On fait la liste des nouveaux seedlot
            for rel_x in list_px:
                for rx in rel_x:
                    select_rel_x.append(rx)
            
            #On recupere les relations presentes dans la base de donnees         
            all_px = SeedlotRelation.objects.filter(parent_gender='X', child_id=child.id)
            
            #On compare les deux listes pour ne garder les relations du formulaire d'update, si elles existaient et sont conservees 
            #on les modifie avec les nouveaux parametres, si certaines ont ete enlevees, on supprime les relations dans la BD
            for px in all_px:
                if px not in select_rel_x:
                    px.delete()
                else:
                    px.site=form.cleaned_data['site']
                    px.save()
                    
            #On sauvegarde les parametres associes a la reproduction         
            reproduction.multiplication_method_id = form.cleaned_data['multiplication_method'].id
            reproduction.start_date = form.cleaned_data['start_date']
            reproduction.end_date = form.cleaned_data['end_date']
            reproduction.description = form.cleaned_data['comments']
            
            reproduction.save()
            
    return render(request, template, {'admin':True, "update":True, "back_page":back_page, "child":child, "type":type, "form":form})
