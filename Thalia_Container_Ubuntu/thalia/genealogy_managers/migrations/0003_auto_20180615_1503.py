# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-15 13:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('genealogy_managers', '0002_auto_20180612_0944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reproduction_method',
            name='category',
            field=models.IntegerField(choices=[(1, 'Multiplication method'), (2, 'Accession Hybrid Crossing method'), (3, 'Seedlot Hybrid Crossing method'), (4, 'Other Accession Pedigree')], default=1),
        ),
    ]
