#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url
from genealogy_accession.views import Accessionhybridcross_management, Otheraccessionpedigree_management
from genealogy_managers.views import genealogyhome, Method_management, genealogy_manager
from genealogy_seedlot.views import Seedlothybridcross_management, Multiplication_management

urlpatterns = [
                      url(r'^accessionhybridcross/$', Accessionhybridcross_management, name='accessionhybridcross'),
                      url(r'^otheraccessionpedigree/$', Otheraccessionpedigree_management, name='otheraccessionpedigree'),
                      url(r'^seedlothybridcross/$', Seedlothybridcross_management, name='seedlothybridcross'),
                      url(r'^multiplication/$', Multiplication_management, name='multiplication'),
                      url(r'^home/$', genealogyhome, name='genealogyhome'),
                      url(r'^method/$', Method_management, name='method'),
                      url(r'^manager/(?P<pk>[0-9]+)/$', genealogy_manager, name='genealogy_manager'),
                       ]