"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach, Laetitia Courgey

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import ast

from django.apps import apps
from django.db import models
from django.db.models import Q

from team.models import User, Project
from operator import ior, iand


class SeedlotManager(models.Manager):
    """
    Manager for seedlot model.
    """
    def by_username(self,username):
        """
        Returns only the seedlots from projects linked to this user 

        :param string username : username

        :var int username_id : user's id
        :var list projects : projects linked to the user
        """
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        return self.filter(projects__in=projects).distinct()
    
    def refine_search(self, or_and, seedlottype, dico_get):
        
        Seedlot = apps.get_model('lot', 'seedlot')
        Accession = apps.get_model('accession', 'accession')
        SeedlotTypeAttribute = apps.get_model('lot','seedlottypeattribute')

        if or_and == "or" :
            myoperator = ior
        else :
            myoperator = iand
            
        proj=[]
        acc=[]
        query=Q()
        list_name_in=[]
        list_attr=[]
        for dat, dico_type_data in dico_get.items():
            if "projects" in dico_type_data:
                if dico_type_data['projects']=='':
#                     print('Projects empty')
                    query = myoperator(query, Q(projects__isnull=True))
                else:
                    proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                    query = myoperator(query, Q(projects__in=proj))
            elif 'accession' in dico_type_data:
                if dico_type_data['accession']=='':
                    query = myoperator(query, Q(accession__isnull=True))
                else:
                    acc =(Accession.objects.filter(name__icontains=dico_type_data['accession']))
                    query = myoperator(query, Q(accession__in=acc))
            elif 'parent_male' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent male']):
                    sl_relation = sl.relations_as_parent.filter()
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent female' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent female']):
                    sl_relation = sl.relations_as_parent.filter()
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            elif 'parent' in dico_type_data:
                parent_relation = []
                for sl in Seedlot.objects.filter(name__icontains=dico_type_data['parent']):
                    sl_relation = sl.relations_as_parent.filter()
                    for i in sl_relation:
                        parent_relation.append(i.child)
                query = myoperator(query, Q(name__in=parent_relation))
            else:
                for data_type, value in dico_type_data.items():
                    for i in SeedlotTypeAttribute.objects.filter(seedlot_type=seedlottype):
                        if str(data_type) == str(i):
#                                     print(str(i))
                            list_attr.append(str(i))
                    if data_type in list_attr:
                        id_attribute = str(SeedlotTypeAttribute.objects.get(attribute_name=data_type).id)
                        for i in Seedlot.objects.filter(type=seedlottype):
                            try:
#                                 print(value)
                                if value in ast.literal_eval(i.attributes_values)[id_attribute]:
#                                     print(str(i))
                                    list_name_in.append(str(i))
                            except:
                                if value in i.attributes_values[id_attribute]:
#                                             print(str(i))
                                    list_name_in.append(str(i))
                        query = myoperator(query, Q(name__in = list_name_in))
                    else:
                        var_search = str( data_type + "__icontains" ) 
                        query = myoperator(query, Q(**{var_search : dico_type_data[data_type]}))

        query = Q(query, type=seedlottype)  
        return Seedlot.objects.filter(query).distinct()

