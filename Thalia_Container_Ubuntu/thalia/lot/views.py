# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test

from django.db.models import Q

from lot.models import SeedlotType, SeedlotTypeAttribute, SeedlotAttributePosition, Seedlot
from lot.forms import SeedlotTypeAttForm, SeedlotTypeForm, SeedlotDataForm,\
    UploadFileForm
from accession.views import list_relation_append
from commonfct.genericviews import generic_management
from commonfct.utils import get_fields, recode
from genealogy_seedlot.models import SeedlotRelation
import json
import ast
import io
import operator
from django.forms.models import model_to_dict
from django.db.models.query import QuerySet
#import xlrd # pour file
#import xlwt
import csv
from accession.models import Accession
from team.models import Project 
from unidecode import unidecode
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from types import *
from django.http import HttpResponse,HttpResponseServerError, Http404
from django.db import IntegrityError

@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/login/')
def lothome(request):
    return render(request,'lot/seedlot_base.html',{"admin":True})

@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/login/')
def lottype(request):
    formatt = SeedlotTypeAttForm()
    response = generic_management(SeedlotType, SeedlotTypeForm, request, 'lot/seedlot_type.html',{'attrib_update':True,"admin":True, 'attributes':formatt,'classname':'Seedlot'},"Seedlot Type Management")
    if request.method == "POST":
        try :
            name = request.POST["name"]
            attribute = "attribute0"
            i = 0
            
            while attribute in request.POST:
                att_type = int(request.POST[attribute].split(';')[-1])
                att_name= ';'.join(request.POST[attribute].split(';')[:-1])
                fields_exist = [sl_field.name for sl_field in Seedlot._meta.get_fields()]
                
                for f in ['Parent Male','Parent Female','Parent X']:
                    fields_exist.append(f)

                for j in fields_exist:
                    if att_name.lower() == j.lower() :
                        error_field="The field '{0}' already exists, please change the attribute name.".format(att_name)
                        response = generic_management(SeedlotType, SeedlotTypeForm, request, 'lot/seedlot_type.html',{"name":name, "error_field":error_field,"admin":True, 'title':'Seedlot Type Management','attributes':formatt,'classname':'Seedlot'},"Seedlot Type Management")
                        return response
                seed_type = SeedlotType.objects.get(name=name)
                att, created = SeedlotTypeAttribute.objects.get_or_create(attribute_name=att_name, type=att_type)
                position_qs = seed_type.seedlotattributeposition_set.all().order_by('-position')
                if position_qs :
                    SeedlotAttributePosition.objects.create(attribute=att,type=seed_type,position=position_qs[0].position+1)
                else :
                    SeedlotAttributePosition.objects.create(attribute=att,type=seed_type,position=1)
                i+=1
                attribute = "attribute{0}".format(str(i))
        except Exception as e :
            print(e)
    return response

#===================================================

def uploadFile(files, form, attributess, seedtype, request, separator):
    """attributes = seedlottype.seedlottypeattribute_set.all()
    formattype = SeedlotDataForm(formstype=attributes)"""
#     print("sep2 === ",type(separator),"    ===  ",separator)
    if files != None:
        try:
            reader = csv.DictReader(io.StringIO(files.read().decode('utf-8')), delimiter=request.POST['delimiter'])
        except:
            try:
                reader = csv.DictReader(io.StringIO(files.read()), delimiter=request.POST['delimiter'])
            except:
                reader = csv.DictReader(io.BytesIO(files.read()), delimiter=request.POST['delimiter'])
        dictline = {}
        i = 0
        for line in files: 
            #On récurère chaque ligne du fichier: la case i contiendra la i-1 ème ligne du fichier
            dictline[i] = line.strip()
            i+=1
        headers = recode(dictline[0])
        #la première ligne du tableau donne attributs du fichier; on les conserve dans un autre tableau et on efface la première ligne de dictvalues
#         print(type(headers))
        theaders = headers.split(separator)
        dictvalues_final = {}
        del dictline[0]
        t=0
        try:
            for key in dictline:
                v = recode(dictline[key])
                val = v.split(separator)
                dictvalues = {}
                for i in range(len(theaders)): 
                    #On remplit dictvalues avec les clés et valeur              
                    dictvalues[theaders[i]] = val[i]
                #On stocke le dictionnaire obtenu dans un autre dictionnaire: la case i contiendra la i+1 ème ligne du fichier
                dictvalues_final[t] = dictvalues
                t+=1
        except IndexError: #Cas où certaines lignes seraient vides, par exemple, si l'utilisateur est passé à la ligne après avoir inséré la dernière donnée de son .csv
            return 'There are empty lines in your file. Please fill or remove them.'

        if request.POST.get("submit"): #Cas où l'utuilisateur veut insére rd enouvelles données
            #Si cette liste n'est pas vide, c'est qu'il manque des accessions
            seed_doublon=''
            try:
                database = Seedlot.objects.all().values_list('name')
                seeds_in_database=[i[0] for i in database]
                #Maintenant, on récupère les noms des seedlots du CSV, contenues dans dictvalues_final
                seeds_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
            except KeyError:
                return 'The headers of your files do not correspond to those in the database. Please check them'
            for i in range(len(seeds_inserted)):
                for j in range(len(seeds_in_database)):
                    if seeds_inserted[i]==seeds_in_database[j]:
                        seed_doublon ='  '.join([seed_doublon, seeds_inserted[i]])
            if set(seeds_in_database)==set(seeds_inserted): #Tus les lots du CSV sont déjà dans la BDD
                return 'All the seedlots you\'re trying to insert are already in your database. Maybe you were trying to update them instead ?'
            elif seed_doublon != '' and set(seeds_in_database) != set(seeds_inserted): #Certains lots du CSV sont déjà dans la BDD
                return seed_doublon
            else:
                jsonvalues={}
                try:
                    for k in dictvalues_final:
                        #print(('row', row)
                        dval = dictvalues_final[k]
                        #print(('newseedlot', newseedlot)
                        name = str(dval['Name'])
                        description = str(dval['Description'])
                        is_obsolete = False #par défaut, c'est false
                        if str(dval['Is Obsolete']) == 'True':
                            #Si le String Is Obsolete vaut False, on obtient un booléen False
                            is_obsolete = True
                        accession_name = str(dval['Accession'])
                        accession = Accession.objects.get(name=accession_name)
                        #print('seedlot Object name = ', accession.name
                        
                        jsonvalues = {}
                        for att in attributess:
                            value = dval[str(att.attribute_name)]
                            if value != None or value != '':
                                jsonvalues[att.id] = value
                            else:
                                jsonvalues[att.id]=''
                        newseedlot = Seedlot(name=name, accession=accession, type=seedtype, is_obsolete=is_obsolete, description=description, attributes_values=json.dumps(jsonvalues))
                        newseedlot.save()
                        try:
                            newseedlot.save()
                        except IntegrityError:
                            return newseedlot.name
                        project_list = str(dval['Projects']).split('|')
                        projects_tuple = Project.objects.all().values_list('name')
                        projects = [unidecode(i[0]) for i in projects_tuple]
                        if len(project_list) == 1:
                            project_name = unidecode(project_list[0])
                            if project_name != '' and project_name in projects:
                                project_name = project_list[0]
                                project = Project.objects.get(name=project_name)
                                newseedlot.projects.add(project)
                        else:
                            for l in range(len(project_list)):
                                project_name = unidecode(project_list[l])
                                if project_name != '' and project_name in projects:
                                    project_name = project_list[l]
                                    project = Project.objects.get(name=project_name)
                                    newseedlot.projects.add(project)
                        for proj_id in accession.projects.filter():
                            newseedlot.projects.add(proj_id)
                except KeyError:
                    return 'Your file\'s headers do not correspond to the entries of the database.'
        
        
        elif request.POST.get("update"): #Cas de l'update
            try:
                database = Seedlot.objects.filter(type=int(request.POST['type'])).values_list('name')
                seeds_in_database=[i[0] for i in database]
                #Maintenant, on récupère les noms des seedlots du CSV, contenues dans dictvalues_final
                seeds_inserted=[dictvalues_final[l]['Name'] for l in dictvalues_final]
            except KeyError:
                return 'The headers of your files do not correspond to those in the database. Please check them'
            #On va tout d'abord s'assurer que tous les lots du CSV sont bien dans la BDD
            common_seeds= [x for x in seeds_in_database if x in seeds_inserted] #Contient les lots présents dans le CSV et dans la BDD
            lacking_seeds = [x for x in seeds_inserted if x not in seeds_in_database] #Contient d'éventuels lots présents dans le CSV mais pas dans la BDD
            if common_seeds == []:
                return 'Your file share no seedlots with the database for this seedlot type. Please make sure you weren\'t trying to insert new data or seedlots existing for another type.'
            elif lacking_seeds != []:
                return lacking_seeds
            else:
                headers = reader.fieldnames
                attributes_names = [att.attribute_name for att in attributess]
                j = 0
                if attributes_names:
                    while headers[j] not in attributes_names:
                        j+=1
                static_headers = [headers[p] for p in range(j)]
                fields_names = []
                for field_csv in static_headers:
                    field_name = field_csv.lower()
                    field_name = field_name.replace(" ","_")
                    fields_names.append(field_name)
                for row in reader:
                    seed_to_update = Seedlot.objects.get(name=row['Name'])
                    seeds_updated = []
                    fields_values = [getattr(seed_to_update, field_to_update) for field_to_update in fields_names]
                    for field_csv, field, l in zip(static_headers, fields_names, range(len(fields_values))):
                        if field_csv == 'Accession':
                            accession_name = row['Accession']
                            accession_instance = Accession.objects.get(name=accession_name)
                            if seed_to_update.accession != accession_instance:
                                seed_to_update.accession = accession_instance
                                seeds_updated.append([seed_to_update, accession_name])
                                seed_to_update.save()
                        elif field_csv == 'Projects':
                            project_name = row['Projects']
                            current_project = seed_to_update.projects
                            if project_name != '' and current_project != None:
                                project = []
                                for proj in Project.objects.filter(name__in=[project_name]):
                                    project.append(proj.id)
                                if current_project != project:
                                    seed_to_update.projects.clear()
                                    for proj_to_add in project:
                                        seed_to_update.projects.add(proj_to_add)
                                    seeds_updated.append([seed_to_update, project_name])
                            elif project_name != '' and current_project == None:
                                project = Project.objects.get(name=project_name)
                                seed_to_update.projects.add(project)
                                seeds_updated.append([seed_to_update, project_name])
                            elif project_name == '' and current_project != None:
                                seed_to_update.projects.clear()
                                seeds_updated.append([seed_to_update, ' The project has been deteled'])
                        else:
                            field_type = Seedlot._meta.get_field(field).get_internal_type()
                            new_value = row[field_csv]
                            if field_type == 'BooleanField' and (new_value == 'False' or new_value == ''):
                                if fields_values[l] == True:
                                    setattr(seed_to_update, field, False)
                                    seeds_updated.append([seed_to_update, False])
                                    seed_to_update.save()
                            elif field_type == 'BooleanField' and new_value == 'True':
                                if fields_values[l] == False:
                                    setattr(seed_to_update, field, True)
                                    seeds_updated.append([seed_to_update, True])
                                    seed_to_update.save()
                            else:
                                if new_value != fields_values[l]:
                                    setattr(seed_to_update, field, new_value)
                                    seeds_updated.append([seed_to_update, new_value])
                    jsonvalues = {}
                    for att in attributess:
                        att_name = str(att.attribute_name)
                        value = row[att_name]
                        if value != None or value != '':
                            jsonvalues[att.id] = value
                        else:
                            jsonvalues[att.id] = ''
                    if type(seed_to_update.attributes_values) is dict:
                        attributes_in_database = seed_to_update.attributes_values
                    else:
                        attributes_in_database = json.loads(seed_to_update.attributes_values)
                    shared_items = set(jsonvalues.items()) & set(attributes_in_database.items())
                    if len(shared_items) < len(attributes_in_database):
                        seed_to_update.attributes_values = jsonvalues
                        seed_to_update.save()
                if seeds_updated == []:
                    return 'There were no changes in your CSV file compared to your database. No update was done.'
                    
#=============================

 
def _get_seedlots(seedlottype, attributes, request):
    seedlots_init = Seedlot.objects.filter(type=seedlottype).order_by('name')
    dico_relation={}
    seedlots=[]
    for i in seedlots_init:
        if SeedlotRelation.objects.filter(child=i):
            relation = SeedlotRelation.objects.filter(child=i)
            dico_relation[i]={}
            for j in relation:
                dico_relation[i][j.parent_gender]=j.parent
    for i in seedlots_init:
        list_relation = []
        for gender in ['M','F','X']:
            list_relation = list_relation_append("seedlot",gender, dico_relation, i, list_relation)
        seedlots.append([i,list_relation])  
    # Ici, on ne peut pas utiliser "seedlots.iterator()
    # car sur notre ModelForm les attributs dynamiques ne font pas partie
    # de la liste d'attributs du Model
    for seed in seedlots: 
        for att in attributes:
            if seed[0].attributes_values :
                if type(seed[0].attributes_values) is dict:
                    attributes_values = seed[0].attributes_values
                else:
                    attributes_values= json.loads(seed[0].attributes_values)
                #si l'id de l'attribut "att" est présent dans les clés de attributes_values
                if str(att.id) in attributes_values.keys() :
                    setattr(seed[0],att.attribute_name.lower().replace(' ','_'),attributes_values[str(att.id)])
                else :
                    setattr(seed[0],att.attribute_name.lower().replace(' ','_'),None)
            else :
                setattr(seed[0],att.attribute_name.lower().replace(' ','_'),'')
    #Adding Paginator        
    try:
        nb_per_page=request.GET['nb_per_page']     
        if nb_per_page=="all":
            nb_per_page = len(seedlots)
    except:
        nb_per_page=50
    paginator = Paginator(seedlots, nb_per_page)
    page = request.GET.get('page')
    try:
        seedlots = paginator.page(page)
    except PageNotAnInteger:
        # Si la page n'est pas un entier, on affiche la page un
        seedlots = paginator.page(1)
    except EmptyPage:
        # Si la page déborder, on affiche la dernière page
        seedlots = paginator.page(paginator.num_pages)
    return seedlots, nb_per_page 
#===================================================

@login_required
@user_passes_test(lambda u: u.is_seedlot_admin_user(), login_url='/login/')
def seedlotdata(request, type_id):
    # On récupère l'accessionType correspondant à l'id = type_id 
    seedlottype = SeedlotType.objects.get(id=int(type_id))
    # On récupère tous les attributs du type
    attributes = seedlottype.seedlottypeattribute_set.all()
    # On récupère tous les attributs du seedlot du type dans "formattype"
    formattype = SeedlotDataForm(formstype=attributes)
    
    register_formFile = UploadFileForm()
    
    names, labels = get_fields(formattype)
    names_genealogy = ['parent_male','parent_female','parent_x']
    labels_genealogy = ['Parent Male','Parent Female','Parent X']
    
    title = seedlottype.name+' Seedlot Management'
    tag_fields = {'title':title,
                  'fields_name':names,
                  'fields_label':labels,
                  "names_genealogy":names_genealogy,
                  "labels_genealogy":labels_genealogy,
                  "admin":True,
                  "refine_search":True,
                  }
        
    template = 'lot/seedlot_data.html'
    seedlots_all = Seedlot.objects.filter(type=seedlottype).distinct()
    number_all = len(seedlots_all)
    list_attributes=[]
    for i in ['name', 'accession', 'description', 'projects']:
        list_attributes.append(i)
    for i in SeedlotTypeAttribute.objects.filter(seedlot_type=seedlottype):
        #print(str(i.attribute_name))
        list_attributes.append(str(i.attribute_name))
    if request.method == "GET": 
        seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
        dico_get={}
        or_and = None
        if "no_search" in request.GET:
            return render(request, 'lot/seedlot_data.html', {"refine_search":True,"admin":True,"list_attributes":list_attributes,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'form':formattype,'creation_mode':True,"number_all":number_all, 'all':seedlots, 'fields_label':labels,'fields_name':names})

        elif "or_and" in request.GET:
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10

            seedlots_init = Seedlot.objects.refine_search(or_and, seedlottype, dico_get)
#             if or_and == "and":
#                 seedlots = Seedlot.objects.refine_search(operator.iand, seedlottype, **dico_get)
#             elif or_and == "or":
#                 seedlots = Seedlot.objects.refine_search(operator.ior, seedlottype, **dico_get)

            number_all = len(seedlots)
# <<<<<<< .working
#             dico = {}
#             download_not_empty = False
#             if seedlots:      
#                 list_col = [f.name for f in seedlots[0]._meta.get_fields()]
#                 try:
#                     ## si le dictionnaire contenant les attributs est vide, il est considéré comme un string, il faut donc le convertire en dictionnaire
#                     get_attr_values = ast.literal_eval(getattr(seedlots[0],"attributes_values")) 
#                 except:
#                     get_attr_values = getattr(seedlots[0],"attributes_values")
#                 ## pour avoir les attributs dans le bon ordre: sorted()
#                 for j in sorted(get_attr_values):
#                     list_col.append(SeedlotTypeAttribute.objects.get(id=j).attribute_name)
#                 for element_to_remove in ['id','type',"phenotypicvalue","classevalue","sample","attributes_values"]:
#                     list_col.remove(element_to_remove)
#                     
#                 with open("/tmp/result_search.csv", 'w', encoding='utf-8') as csv_file:
#                     writer = csv.writer(csv_file)
#                     writer.writerow(list_col)
#                     for i in seedlots:
#                         row=[]
#                         dico=model_to_dict(i)
#                         try:
#                             dico_attr_values = ast.literal_eval(getattr(i,"attributes_values"))
#                         except:
#                             dico_attr_values = getattr(i,"attributes_values")
#                         for id_attr, val_attr in dico_attr_values.items():
#                             dico[SeedlotTypeAttribute.objects.get(id=id_attr).attribute_name]=val_attr
#                         for key, value in dico.items():
#                             if not dico[key]:
#                                 dico[key]=""
#                             elif key == 'accession':
#                                 dico[key] = Accession.objects.get(id=dico[key]).name
#                             elif isinstance(dico[key], QuerySet):
#                                 list_qs = []
#                                 for queryset_i in dico[key]:
#                                     list_qs.append(queryset_i.name)
#                                 dico[key]=', '.join(list_qs)
#                         for col in list_col:
#                             row.append(dico[col])
#                         writer.writerow(row)    
#                         
#                 download_not_empty = True
#             tag_fields.update({"download_not_empty":download_not_empty})
# 
# =======
            dico = {}
            download_not_empty = False
            dico_relation={}
            seedlots=[]
            for sl in seedlots_init:
                dico_relation[sl]={}
                for r in sl.relations_as_child.all() :
                    dico_relation[sl][r.parent_gender] = r.parent
#                 if SeedlotRelation.objects.filter(child=i):
#                     relation = SeedlotRelation.objects.filter(child=i)
#                     dico_relation[i]={}
#                     for j in relation:
#                         dico_relation[i][j.parent_gender]=j.parent
            for i in seedlots_init:
                list_relation = []
                for gender in ['M','F','X']:
                    list_relation = list_relation_append("seedlot",gender, dico_relation, i, list_relation)
                seedlots.append([i,list_relation])    
            if seedlots:      
                list_col = [f.name for f in seedlots[0][0]._meta.get_fields()]
                try:
                    ## si le dictionnaire contenant les attributs est vide, il est considéré comme un string, il faut donc le convertire en dictionnaire
                    get_attr_values = ast.literal_eval(getattr(seedlots[0][0],"attributes_values")) 
                except:
                    get_attr_values = getattr(seedlots[0][0],"attributes_values")
                ## pour avoir les attributs dans le bon ordre: sorted()
                for j in sorted(get_attr_values):
                    list_col.append(SeedlotTypeAttribute.objects.get(id=j).attribute_name)
                for element_to_remove in ['id','relations_as_parent','relations_as_child','type',"phenotypicvalue","classevalue","sample","attributes_values"]:
                    list_col.remove(element_to_remove)
                
                
                    
                with open("/tmp/result_search.csv", 'w', encoding='utf-8') as csv_file:
                    writer = csv.writer(csv_file)
                    writer.writerow(list_col)
                    for i in seedlots_init:
                        row=[]
                        dico=model_to_dict(i)
                        try:
                            dico_attr_values = ast.literal_eval(getattr(i,"attributes_values"))
                        except:
                            dico_attr_values = getattr(i,"attributes_values")
                        for id_attr, val_attr in dico_attr_values.items():
                            dico[SeedlotTypeAttribute.objects.get(id=id_attr).attribute_name]=val_attr
                        for col in list_col:
                            if col not in dico.keys():
                                dico[col]=""
                        for key, value in dico.items():
#                             if not dico[key]:
#                                 dico[key]=""
#                             elif key == 'accession':
                            if key == 'accession':
                                dico[key] = Accession.objects.get(id=dico[key]).name
                            elif isinstance(dico[key], QuerySet):
                                list_qs = []
                                for queryset_i in dico[key]:
                                    list_qs.append(queryset_i.name)
                                dico[key]=', '.join(list_qs)
                        for col in list_col:
                            row.append(dico[col])
                        writer.writerow(row)    
                        
                download_not_empty = True
            tag_fields.update({"download_not_empty":download_not_empty})
# >>>>>>> .merge-right.r614
        tag_fields.update({'all':seedlots,
                           "search_result":dico_get,
                           "list_attributes":list_attributes,
                           'or_and':or_and,
                           'nb_per_page':nb_per_page,
                           "number_all":number_all,
                           'formfile':register_formFile,
                           'form':formattype,
                           'creation_mode':True,
                           'excel_empty_file':True,
                           'filetype':'seedlot',
                           'typeid': type_id})
        return render(request, template, tag_fields)#{'form':formattype,'creation_mode':True, 'all':seedlots, 'fields_label':labels,'fields_name':names})
    
    elif request.method == "POST":
        if "hiddenid" in request.POST.keys() :
            try :
                #on récupère l'objet déjà existant puis on utilise les fonctionnalités du formulaire pour l'updater
                instance = Seedlot.objects.get(id=request.POST['hiddenid'])
                form = SeedlotDataForm(instance=instance, data=request.POST, formstype=attributes)

                if form.is_valid():
                    instance = form.save()
                    jsonvalues = {}
                    for att in attributes:
                        jsonvalues[att.id] = request.POST[att.attribute_name.lower().replace(' ','_')]
                    instance.attributes_values = jsonvalues
                    instance.save()
                    seedlots,nb_per_page = _get_seedlots(seedlottype, attributes, request)                                 
                    tag_fields.update({'all':seedlots,
                                       "number_all":number_all,
                                       'nb_per_page':nb_per_page,"list_attributes":list_attributes,
                                       'form':form,
                                       'creation_mode':False,
                                       'object':instance,
                                       'hiddenid':instance.id})
                    return render(request,template,tag_fields)
                else :
                    #sinon on génère un message d'erreur qui sera affiché
                    errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in form.errors.items() ])
                    tag_fields.update({'form':form,
                                       'creation_mode':False,
                                       'hiddenid':instance.id,"list_attributes":list_attributes,
                                       'errormsg':errormsg})
                    return render(request,template,tag_fields)
            except ObjectDoesNotExist :
                tag_fields.update({'form':form,
                                    'creation_mode':True,
                                    'errormsg':"The object you try to update doesn't exist in the database"})
                return render(request,template,tag_fields)
                        
        else:
            formattype = SeedlotDataForm(data=request.POST,formstype=attributes)
            formf = UploadFileForm(request.POST, request.FILES)
            if formf.is_valid():
                separator = request.POST['delimiter']
                #uploadFile(request.FILES['file'], formf, attributes, seedlots, seedlottype)
                file_uploaded=uploadFile(request.FILES['file'], formf, attributes, seedlottype, request, separator)
                if isinstance(file_uploaded, str): # Cas où certains lots sont déjà dans la BDD
                    seedlots,nb_per_page = _get_seedlots(seedlottype, attributes, request) 
                    if file_uploaded == 'All the seedlots you\'re trying to insert are already in your database. Maybe you were trying to update them instead ?':
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots,"number_all":number_all, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':file_uploaded})   
                    elif file_uploaded == 'Your file share no seedlots with the database. Please make sure you weren\'t trying to insert new data.':
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':file_uploaded})
                    elif file_uploaded == 'There were no changes in your CSV file compared to your database. No update was done.':
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':file_uploaded})
                    elif file_uploaded == 'Your file\'s headers do not correspond to the entries of the database.':
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':file_uploaded})
                    else:            
                        errormsg = 'The following seedlots are already in your database: ' + file_uploaded
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':errormsg})                       
                elif isinstance(file_uploaded, list): #Cas où certaines accessions sont manquantes
                    seedlots,nb_per_page = _get_seedlots(seedlottype, attributes, request) 
                    if isinstance(file_uploaded[0], list):
                        accessions_not_in_database = ','.join(file_uploaded[0])
                        seeds_concerned = ','.join(file_uploaded[1])
                        errormsg = 'The file you try to upload relies on accessions which aren\'t in your database: ' + accessions_not_in_database + '. The seedlots concerned are: ' + seeds_concerned + '.'
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, "number_all":number_all, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':errormsg}) 
                    else:
                        lacking_seeds = file_uploaded[0]
                        del file_uploaded[0]
                        if file_uploaded != []:
                            if len(file_uploaded) == 1:
                                lacking_seeds = lacking_seeds + ' , ' + file_uploaded[0]
                            else:
                                lacking_seeds = lacking_seeds + ' , ' +' , '.join(file_uploaded)
                        errormsg = 'Some seedlots you\'re trying to update aren\'t in your database : ' + lacking_seeds
                        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, "number_all":number_all, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names, 'errormsg':errormsg})
                        
                else:    
                    seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)  
                    return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"admin":True,'formfile':register_formFile,'filetype':'seedlot','typeid': type_id,'excel_empty_file':True,'form':SeedlotDataForm(formstype=attributes),'creation_mode':True, 'all':seedlots, "number_all":number_all, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names})                       

                    
            if formattype.is_valid():
                newseedlot = formattype.save(commit=False)
                newseedlot.type = seedlottype
                jsonvalues = {}
                for att in attributes:
                    jsonvalues[att.id] = request.POST[att.attribute_name.lower().replace(' ','_')]
                        
                newseedlot.attributes_values = jsonvalues
                newseedlot.save()
                request_dict = dict(request.POST)
                if 'projects' in request_dict:
                    projectss = request_dict['projects']
                    if len(projectss) == 1:
                        project_id = projectss[0]
                        project = Project.objects.get(id=project_id)
                        newseedlot.projects.add(project)
                    else:
                        for i in range(len(projectss)):
                            project_id = projectss[i]
                            project = Project.objects.get(id=project_id)
                            newseedlot.projects.add(project)
                accession = Accession.objects.get(id=int(request.POST['accession']))            
                
                for proj_id in accession.projects.filter():
                    newseedlot.projects.add(proj_id)
                seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
                return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"object":newseedlot ,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'filetype':'seedlot','typeid': type_id,'formfile':register_formFile,'excel_empty_file':True,'form':formattype,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names})
            else :
                #Ajout 23/7/15
                errormsg = '<br />'.join(['{0} : {1}'.format(k,v.as_text()) for (k,v) in formattype.errors.items() ])
                seedlots, nb_per_page = _get_seedlots(seedlottype, attributes, request)
                #return generic_management(Seedlot, SeedlotDataForm, request, 'lot/lot_generic.html',{},"Seed lot Management")
                return render(request,template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'formfile':register_formFile, 'form':formattype,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names,'errormsg':errormsg})        
    else :
        return render(request, template, {"labels_genealogy":labels_genealogy,"refine_search":True,"list_attributes":list_attributes,"number_all":number_all, "admin":True,'formfile':register_formFile, 'form':SeedlotDataForm,'creation_mode':True, 'all':seedlots, 'nb_per_page':nb_per_page, 'fields_label':labels,'fields_name':names},"Seedlot Management")
 
 
#============================


