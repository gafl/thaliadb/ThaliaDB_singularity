# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from jsonfield import JSONField

from django.db import models

from commonfct.constants import ATTRIBUTE_TYPES
from django.utils.encoding import force_bytes
from lot.managers import SeedlotManager

# Create your models here.

class SeedlotType(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    
    def attribute_set(self):
        return self.seedlottypeattribute_set
    
    def __str__(self):
        return self.name
    
class Seedlot(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)
    accession = models.ForeignKey('accession.Accession', db_index=True)
    type = models.ForeignKey('lot.SeedlotType', db_index=True)
    is_obsolete = models.BooleanField(default= False)
    description = models.TextField(max_length=500, blank=True, null=True)
    attributes_values = JSONField(blank=True, null=True)
    objects = SeedlotManager() 
    
    #projects = models.ManyToManyField(Project)#blank=True, null=True
    projects = models.ManyToManyField('team.Project', blank=True)

    #images = models.ManyToManyField(Documents)

    def __str__(self):
        return self.name
    
    def relation_set(self):
        return self.seedlotrelation_set 
    
class SeedlotTypeAttribute(models.Model):
    
    attribute_name = models.CharField(max_length=100, blank=True)
    type = models.IntegerField(choices=ATTRIBUTE_TYPES, blank=True)
    seedlot_type = models.ManyToManyField('lot.SeedlotType', through="SeedlotAttributePosition")
    class Meta:
        unique_together = ("attribute_name","type")

    def natural_key(self):
        return (self.attribute_name, self.type)

    


class SeedlotAttributePosition(models.Model):
    attribute = models.ForeignKey('lot.SeedlotTypeAttribute')
    type = models.ForeignKey('lot.SeedlotType')
    position = models.IntegerField()
    
    class Meta:
        unique_together = ("type","position")
        
    
    
#TODO : Reste à implémenter les genealogies