# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms

from lot.models import SeedlotType, SeedlotTypeAttribute, Seedlot
from accession.models import Accession
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect
from django.contrib.admin.widgets import FilteredSelectMultiple
from dal import autocomplete

class SeedlotTypeForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = SeedlotType

class SeedlotTypeAttForm(forms.ModelForm):
    class Meta:
        model = SeedlotTypeAttribute
        exclude = ['seedlot_type']
        widgets = {'attribute_name': forms.TextInput(attrs = {'id':'attributes_autocomplete', 'required':False}),}

# Ajout 09/01/2015

class SeedlotDataForm(forms.ModelForm):
    class Meta:
        model = Seedlot
        exclude = ['attributes_values', 'type']
#         widgets = {'attribute_name': forms.TextInput(attrs = {'id':'attributes_autocomplete'}),}
    def __init__(self, *args, **kwargs):
        if 'formstype' in kwargs.keys():
            attrs = kwargs['formstype']
            if 'data' in kwargs.keys():
                if 'instance' in kwargs.keys():
                    super(SeedlotDataForm, self).__init__(data=kwargs['data'], instance=kwargs['instance'])
                else :
                    super(SeedlotDataForm, self).__init__(data=kwargs['data'])
            else :
                super(SeedlotDataForm, self).__init__()
            for at in attrs:
                self.fields[at.attribute_name.lower().replace(' ','_')] = forms.CharField(required = False)
                #self.attributes_val.append(at.attribute_name)
        else :
            if 'data' in kwargs.keys():
                super(SeedlotDataForm, self).__init__(data=kwargs['data'])
            else :
                super(SeedlotDataForm, self).__init__()

    
SepChoices=[(',',' , '),
            (';',' ; ')]          
                
class UploadFileForm(forms.Form):
    file = forms.FileField()
    delimiter = forms.CharField(label='Delimiter of the file ', initial=";")
    #delimiter = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=SepChoices, required=True, initial=",")
