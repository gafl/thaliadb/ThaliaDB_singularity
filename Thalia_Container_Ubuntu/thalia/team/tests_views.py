"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.db.models import Model, CharField
from django.forms import ModelForm
from django.test import TestCase, Client, RequestFactory
from django.core.urlresolvers import reverse
from commonfct.utils import get_fields
from team.models import User, Person
from team.views import authentication
from django.contrib import auth

# class TestModel(Model):
#     first_name = CharField(max_length=50)
#     last_name = CharField(max_length=50)
# 
# class TestForm(ModelForm):
#     class Meta:
#         model = TestModel

# class TeamViewsTestCase(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         #self.client = Client()
#         #self.factory = RequestFactory()
#         cls.person3 = Person.objects.create(first_name="Jane",last_name="Doe")
#         cls.user4 = User.objects.create(person=cls.person3, login="jdoe", password="mdpjdoe")
#         
#     def test_authentication(self):
#         self.client.login(login="jdoe",password="yannick")
#         user = auth.get_user(self.client)
#         self.assertFalse( user.is_authenticated())
#         
#         self.client.login(login="jdoe",password="mdpjdoe")
#         user = auth.get_user(self.client)
#         self.assertTrue( user.is_authenticated())
#         url2test = reverse('login')
#         response = self.client.get(url2test)
#         self.assertEqual(response.status_code,200)
#         
#         request = self.factory.post(url2test,{'username':'jdoe', 'password':'mdp_false'})
#         request.user = self.user4
#         response = authentication(request)
#         print(dir(response))
#         self.assertFalse(response.request.user.is_authenticated) #####NE MARCHE PAS !
#         self.assertEqual(response.status_code,200)
# 
#         request = self.factory.post('/',{'username':'jdoe', 'password':'mdpjdoe'})
#         request.user = self.user4
#         response = authentication(request)
#         self.assertTrue(response.request.user.is_authenticated)
#         self.assertEqual(response.status_code,200)
        
