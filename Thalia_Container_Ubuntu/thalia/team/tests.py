"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""

from django.db.models import Model, CharField
from django.forms import ModelForm
from django.test import TestCase

from commonfct.utils import get_fields
from team.models import User, Person

# class TestModel(Model):
#     first_name = CharField(max_length=50)
#     last_name = CharField(max_length=50)
# 
# class TestForm(ModelForm):
#     class Meta:
#         model = TestModel

class TeamUserTestCase(TestCase):
    """
      Class testing everything about the users
    """
    def setUp(self):
        """
          Function creating a test database containing some data defined here
        """
        person1 = Person.objects.create(first_name = "Yannick", last_name="De Oliveira")
        self.user1 = User.objects.create(person = person1, login="ydeoliveira",
                                        is_team_admin=True)
        
        person2 = Person.objects.create(first_name = "Jean", last_name="Dupont")
        self.user2 = User.objects.create(person = person2, login="jdupont")
        self.person3 = Person.objects.create(first_name = "Jane", last_name="Doe")
        
    
    def testUserFullName(self):
        """
          Function testing the function 'get_full_name'.
          Using the assertEqual function, the test pass if the function returns the right full name.
        """
        self.assertEqual(self.user1.get_full_name(), "Yannick De Oliveira")
        self.assertEqual(self.user2.get_full_name(), "Jean Dupont")

    def testUserAdmin(self):
        """
          Function testing the function 'is_superuser', check if user1 is a superuser and if user2 is not a superuser.
          Using assertTrue and assertFalse function, the test pass if the assertion (True or False) is correct.
        """
        self.assertTrue(self.user1.is_superuser())
        self.assertFalse(self.user2.is_superuser())
    
    def test_create_user_with_person(self):
        """
          Function testing the function 'create_user_with_person'.
          Check if it raises an error if there is no user (if null=true or if blank=true)
        """
        with self.assertRaises(ValueError):
            user=User.objects.create_user_with_person(None,"mdpjdoe",self.person3)
            
        with self.assertRaises(ValueError):
            user2=User.objects.create_user_with_person("","mdpjdoe",self.person3)
            
            
""" On teste les fonctions de commonfct dans team """
# class BasicTest(TestCase):
# 
#     def setUp(self):
#         pass
#     
#     def testGetFields(self):
# #         form = TestForm()
#         names, labels = get_fields(form)
#         self.assertEqual(labels, ["First name", "Last name"])
#         self.assertEqual(names, ["first_name", "last_name"])
        