# -*- coding: utf-8 -*-
"""
    LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
    
    This package reunites each form used in the app ''team'' and creating data such as persons, users etc.
"""

from django import forms

from team.models import Person, Institution, GenomeVersion, Project, Method, User, DataFile, Documents
from lot.models import Seedlot
from accession.models import Accession
from genotyping.models import Sample, GenotypingID, Experiment, Referential
from phenotyping.models import Environment, Trait
from classification.models import Classification
from dal import autocomplete
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.safestring import mark_safe
from commonfct.forms_utils import HorizontalRadioSelect

class PersonForm(forms.ModelForm):
    """
        Form based on the Person model to create persons
    """
    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        # change a widget attribute:
        self.fields['note_on_person'].widget.attrs["cols"] = 20
        self.fields['note_on_person'].widget.attrs["rows"] = 1
        self.fields['institutions'].widget.attrs['style'] = 'width: 180px;height: 120px;'
        self.fields['institutions'].queryset = Institution.objects.order_by('name')
    class Meta:
        fields = '__all__'
        model = Person

class InstitutionForm(forms.ModelForm):
    """
        Form based on the Institution model to create institutions
    """
    class Meta:
        fields = '__all__'
        model = Institution

class GenomeVersionForm(forms.ModelForm):
    """
        Form based on the GenomeVersion model to create genome versions
    """
    class Meta:
        fields = '__all__'
        model = GenomeVersion

class ProjectForm(forms.ModelForm):   
    """
        Form based on the Project model to create projects
    """
    class Meta:
        model = Project
        widgets = {'start_date': forms.TextInput(attrs = {'class':'datePicker'}),
                   'end_date': forms.TextInput(attrs = {'class':'datePicker'})
                   }
        fields = '__all__'

class MethodForm(forms.ModelForm):
    """
        Form based on the Method model to create methods
    """
    class Meta:
        model = Method
        exclude = ['type']
        
class DataFileForm(forms.ModelForm):
    """
        Form based on the DataFile model to create data files
    """
    class Meta:
        model = DataFile
        exclude = ['document']

class DocumentForm(forms.ModelForm):
    """
        Form based on the Documents model to create documents
    """
    class Meta:
        fields = '__all__'
        model = Documents

class UserCreateForm(forms.ModelForm):
    """
        Form based on the User model to create users
    """
    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['person'].queryset = Person.objects.order_by('last_name')
        
    class Meta:
        model = User
        fields = ['login','password','person','genome_version','is_team_admin','is_accession_admin','is_seedlot_admin','is_classification_admin','is_phenotyping_admin','is_genotyping_admin']

class UserUpdateForm(forms.ModelForm):
    """
        Form based on the User model to update users
    """
    class Meta:
        model = User
        fields = ['login','person','genome_version','is_team_admin','is_accession_admin','is_seedlot_admin','is_classification_admin','is_phenotyping_admin','is_genotyping_admin']

class SeedLotForm(forms.Form):
    """
        Form allowing the selection of a list of seedlots, using the autocompletion or in a list containing all the seedlots from the database.
    """
    name_seedlot = forms.ModelMultipleChoiceField(label='Select Seed Lot',required=False,
                                  queryset=Seedlot.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='seedlot-autocomplete'))
    seedlot = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all(),
                                                widget=FilteredSelectMultiple("Seedlot",False,attrs={'rows':'100','size':'20'}),
                                                required=False)
    
class SampleForm(forms.Form):
    """
        Form allowing the selection of a list of samples, using the autocompletion or in a list containing all the samples from the database.
    """
    name_sample = forms.ModelMultipleChoiceField(label='Select Sample',required=False,
                                  queryset=Sample.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='sample-autocomplete'))
    sample = forms.ModelMultipleChoiceField(queryset=Sample.objects.all(),
                                                widget=FilteredSelectMultiple("Sample",False,attrs={'rows':'100','size':'20'}),
                                                required=False)

class ClassificationForm(forms.Form):
    """
        Form allowing the selection of a list of classifications, using the autocompletion or in a list containing all the classifications from the database.
    """
    name_classification = forms.ModelMultipleChoiceField(label='Select Classification',required=False,
                                  queryset=Classification.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='classification-autocomplete'))
    classification = forms.ModelMultipleChoiceField(queryset=Classification.objects.all(),
                                                widget=FilteredSelectMultiple("Classification",False,attrs={'rows':'100','size':'10'}),
                                                required=False)
       
class ExperimentForm(forms.Form):
    """
        Form allowing the selection of a list of experiments, using the autocompletion or in a list containing all the experiments from the database.
    """
    name_xp = forms.ModelMultipleChoiceField(label='Select Experiment',required=False,
                                  queryset=Experiment.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='experiment-autocomplete'))
    experiment = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all(),
                                                widget=FilteredSelectMultiple("Experiment",False,attrs={'rows':'100','size':'10'}),
                                                required=False)
    
class GenotypingIDForm(forms.Form):
    """
        Form allowing the selection of a list of genotypingIDs, using the autocompletion or in a list containing all the genotypingIDs from the database.
    """
    name_genoID = forms.ModelMultipleChoiceField(label="Select Genotyping ID", required=False,
                                  queryset=GenotypingID.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='genotypingID-autocomplete'))
    genoID = forms.ModelMultipleChoiceField(label="GenoID",queryset=GenotypingID.objects.all(),
                                            widget=FilteredSelectMultiple("GenotypingID",False,attrs={'rows':'100','size':'20'}),
                                            required=False)
    
class UserForm(forms.Form):
    """
        Form allowing the selection of a list of users, using the autocompletion or in a list containing all the users from the database.
    """
    name_user = forms.ModelMultipleChoiceField(label="Select User", required=False,
                                  queryset=User.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='user-autocomplete'))
    user = forms.ModelMultipleChoiceField(label="User",queryset=User.objects.all(),
                                            widget=FilteredSelectMultiple("User",False,attrs={'rows':'100','size':'10'}),
                                            required=False)

class PhenoenvForm(forms.Form):
    """
        Form allowing the selection of a list of environments, using the autocompletion or in a list containing all the environments from the database.
    """
    name_env = forms.ModelMultipleChoiceField(label="Select Environment", required=False,
                                  queryset=Environment.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='environment-autocomplete'))
    environment = forms.ModelMultipleChoiceField(label="Environment",queryset=Environment.objects.all(),
                                                 widget=FilteredSelectMultiple("Environment",False,attrs={'rows':'100','size':'20'}),
                                                 required=False)
    
class PhenotraitForm(forms.Form):
    """
        Form allowing the selection of a list of traits, using the autocompletion or in a list containing all the traits from the database.
    """
    name_trait = forms.ModelMultipleChoiceField(label="Select Trait", required=False,
                                  queryset=Trait.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='trait-autocomplete'))
    trait = forms.ModelMultipleChoiceField(label="Trait",queryset=Trait.objects.all(),
                                                 widget=FilteredSelectMultiple("Trait",False,attrs={'rows':'100','size':'20'}),
                                                 required=False)    
    
class ReferentialForm(forms.Form):
    """
        Form allowing the selection of a list of referentials, using the autocompletion or in a list containing all the referentials from the database.
    """
    name_ref = forms.ModelMultipleChoiceField(label="Select Referential", required=False,
                                              queryset=Referential.objects.all(),
                                              widget=autocomplete.ModelSelect2Multiple(url='referential-autocomplete'))
    referential = forms.ModelMultipleChoiceField(label="Referential",queryset=Referential.objects.all(),
                                                 widget=FilteredSelectMultiple("Referential",False,attrs={'rows':"100","size":"20"}),
                                                 required=False)

choices_type_email=[('1','Support'),
                    ('2','Data Administrator')]
        
    
class ContactForm(forms.Form):
    """
        Contact form allowing the users to contact a competent person to answer their needs.
        
        :var list choices_type_email: list of tuples that defines the email recipient
    """
    subject = forms.CharField(max_length=100,required=False)
    message = forms.CharField(widget=forms.Textarea(attrs={'rows':5, 'cols':25}),max_length=1000, required=True)
    sender = forms.EmailField(required=False)
    cc_myself = forms.BooleanField(required=False)
    type_of_mail = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=choices_type_email, required=True, initial="1")
    
class UploadFileForm(forms.Form):
    """
        Form allowing the uploading of a file
    """
    file = forms.FileField(required=False)
    
choices_type_data = [('1','Accession'),
                     ('2','Seedlot'),
                     ('3','Sample'),
                     ('4','GenotypingID')]

class GlobalSearchBarForm(forms.Form):
    """
        Form allowing a global search among the database. TODO: for now, only the accessions can be searched with autocompletion. Something needs to be implemented
        so that when a type of data is checked, the autocompletion is made within this type of data.
        
        :var list choices_type_data: list of tuples that defines the different data types to display
    """
    search = forms.ModelChoiceField(required=True,queryset=Accession.objects.all(),widget=autocomplete.ModelSelect2(url='accession-autocomplete'))
    type_of_data = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=choices_type_data, required=True, initial='1')