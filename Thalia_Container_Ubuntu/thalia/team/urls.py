#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url
from team.views import teamhome, person_management, institution_management, genome_management, project_management, user_management, doc_management, file_manager
from team.views import link_data, user_failed, contact, help

urlpatterns = [
                       url(r'^home/$', teamhome, name='teamhome'),
                       url(r'^person/$', person_management, name='person_management'),
                       url(r'^institution/$', institution_management, name='institution_management'),
                       url(r'^genome/$', genome_management, name='genome_management'),
                       url(r'^project/$', project_management, name='project_management'),
                       url(r'^user/$', user_management, name='user_management'),
#                        url(r'^method/$', 'method_management', name='method_management'),

                       url(r'^link_data/$', link_data, name='link_data'),
                       url(r'^$', user_failed, name="user_failed"),
                       url(r'^contact/$', contact, name="contact"),
                       url(r'^help/$', help, name="help")
                       ]