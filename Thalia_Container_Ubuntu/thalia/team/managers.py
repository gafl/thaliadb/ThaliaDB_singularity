"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    LICENCE
"""


from django.db import models

from django.contrib.auth.models import BaseUserManager
from django.utils import timezone
#from django.db.models import get_model
from django.apps import apps

class UserManager(BaseUserManager):
    """
        This manager allows the creation of an user
    """
    def create_user_with_person(self, username, password, person, **extra_fields ):
        """
            This function creates an user from a person previously created
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(login=username, last_login=now, start_date=now, person=person, **extra_fields)
        user.set_password(password)
        user.save(using=self._db) 

        return user

    def create_user(self, username, password, first_name, last_name, **extra_fields):
        """
            This function creates a person if not created and calls the create_user_with_person function to create an user based on this person
        """
        person_class = apps.get_model('team', 'Person')
        person, created = person_class.objects.get_or_create(first_name=first_name, last_name=last_name)
        user = self.create_user_with_person(username, password, person, **extra_fields)
        return user
    
    def create_superuser(self, username, password, first_name, last_name, **extra_fields):
        """
            This function calls the create_superuser function and creates an user with all rights: it creates a superuser
        """
        u = self.create_user(username, password, first_name, last_name, **extra_fields)
        u.is_team_admin = True
        u.is_accession_admin = True
        u.is_seedlot_admin = True
        u.is_genotyping_admin = True
        u.is_phenotyping_admin = True
        u.is_classification_admin = True
        u.save(using=self._db)
        return u
    
    
    
    
    
    
    
