# -*- coding: utf-8 -*-
from django import template
from django.db.models.fields.files import FieldFile
register = template.Library()
from genotyping.models import LocusTypeAttribute
from accession.models import AccessionTypeAttribute, Accession
from lot.models import Seedlot, SeedlotTypeAttribute
from django.apps import apps
import ast
from django import template

register = template.Library()

@register.filter
def get_type(value):
    if type(value)==list or type(value) == tuple:
        true_false=True
    else:
        true_false=False
#     print(type(value))
    return true_false

# from accession.models import Accession

@register.filter
def to_class_name(value):
    return value.__class__.__name__

@register.simple_tag
def showattribute(value, attribute, app_label, model_name):
    attr_value = None
#     print('=====<<< value: ',value)
    try:
        attr_value = value.__getattribute__(attribute.lower().replace(' ','_'))
    except Exception:
        try:
            for i in value.attributes:
                if LocusTypeAttribute.objects.get(id=i.attribute_id).attribute_name==attribute:
                    attr_value=i.value
        except:
            model=apps.get_model(app_label, model_name)
            modelTypeAttribute = apps.get_model(app_label, str(model_name+'TypeAttribute'))
            try:
                for i in ast.literal_eval(value.attributes_values):
                    if modelTypeAttribute.objects.get(id=int(i)).attribute_name.replace(' ','_').lower()==attribute.lower():
                        attr_value=ast.literal_eval(model.objects.get(name=value).attributes_values)[i]
            except:
                for i in value.attributes_values:
                    if modelTypeAttribute.objects.get(id=i).attribute_name.replace(' ','_').lower()==attribute.lower():
                        attr_value=model.objects.get(name=value).attributes_values[i]
            #print(attr_value)
            #print(value)
            #print(attribute)
#     print(type(attr_value))
    if type(attr_value) is str :
        return attr_value
    elif attr_value is None :
        return ''
    elif type(attr_value) is FieldFile:
        link = "<a href=\"/filerender/{0}\">{1}</a>".format(attr_value.url,attr_value.url.split('/')[-1])
        return link
    else :
        try :
            msg = ''
            for att in attr_value.all() :
                msg += str(att)+', '
            return msg[:-2]
        except :
            try :
                if type(attr_value) is int :
                    try :
                        method_to_call = 'get_{0}_display'.format(attribute)
                        mymethod = getattr(value, method_to_call)
                        return mymethod()
                    except AttributeError:
                        return str(attr_value)
                else :
                    return str(attr_value)      
            except :
                return "Unexpected value" 
    
@register.filter      
def getkey(dictvalues, key):
    try:
        return dictvalues[key]
    except:
        return 'X'
    


