# -*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django import forms
from django.forms import ModelForm

from accession.models import Accession, AccessionType
from team.models import Project, User
#from cProfile import label
from django.contrib.admin.widgets import FilteredSelectMultiple
# Import pour aligner les radioboutons
from django.utils.safestring import mark_safe
from phenotyping.models import Trait, Environment
from genotyping.models import Referential, Experiment, Locus, Sample, GenotypingID
from classification.models import Classification, Classe
from django.forms.widgets import HiddenInput
from lot.models import Seedlot
# from dataview.views import AccessionAutocomplete
from dal import autocomplete
from commonfct.forms_utils import HorizontalRadioSelect

from django_bootstrap_typeahead.fields import * #autocompletion

class PhenoViewerAccessionForm (forms.Form):
    name = forms.ModelMultipleChoiceField(label='Select Accession',required=False,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'20'}),required=False)
    
class PhenoViewerAccessionForm2 (forms.Form):
#     name = forms.ModelMultipleChoiceField(label='Select Accession',required=False,
#                                   queryset=Accession.objects.all(),
#                                   widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all(),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'20'}))
    
class PhenoViewerTraitForm (forms.Form):
    trait = forms.ModelMultipleChoiceField(queryset=Trait.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Trait",False,attrs={'rows':'100','size':'15'}),required=True)
    
class PhenoViewerEnvironmentForm (forms.Form):
    environment = forms.ModelMultipleChoiceField(queryset=Environment.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Environment",False,attrs={'rows':'100','size':'15'}), required=True)
    
    
CHOICES = [('1', 'Accession',), 
           ('2', 'Seed lot',)]

MY_CHOICES = (
    ('2', 'Web browser'),
    ('1', 'Export'),
)
    
class DataviewPhenotypingValuesForm(forms.Form):
    missing_data = forms.CharField(max_length=30, initial='missing data')
#     check = forms.BooleanField(label="Export considering correspondence", required=False)
    export_or_view_in_web_browser = forms.ChoiceField(choices=MY_CHOICES)
    choose_visualization_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICES, initial="1")
    
    
#=====================================================================================
#===================== CLASSIFICATION ================================================


class ClassificationViewerForm (forms.Form):
    classification = forms.ModelChoiceField(queryset=Classification.objects.all(),empty_label="Choose Classification" ,required=True)
    
class ClasseViewerForm (forms.Form):
    group = forms.ModelMultipleChoiceField(queryset=Classe.objects.all(),
                                                widget=FilteredSelectMultiple("Classe",False,attrs={'rows':'100','size':'15'}), required=True)
    
class SeedlotViewerForm (forms.Form):
    seedlot = forms.ModelMultipleChoiceField(queryset=Seedlot.objects.all(),
                                                widget=FilteredSelectMultiple("Seedlot",False,attrs={'rows':'100','size':'15'}), required=True)    


YES_NO = [('2', 'No',), 
           ('1', 'Yes',)]


class DataviewClassificationVisuForm(forms.Form):
    export_or_web = forms.ChoiceField(choices=MY_CHOICES)
    show_accession = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=YES_NO, initial="2")

    
    
#=====================================================================================
#===================== GENOTYPING ====================================================

class GenoViewerAccessionForm2 (forms.Form):
#     name = forms.ModelMultipleChoiceField(label='Select Accession',required=False,
#                                   queryset=Accession.objects.all(),
#                                   widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all(),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'20'}),
                                                required=False)
    
    class Meta:
        model = Accession
        fields = ('__all__')

class GenoViewerAccessionForm (forms.Form):
    name = forms.ModelMultipleChoiceField(label='Select Accession',required=False,
                                  queryset=Accession.objects.all(),
                                  widget=autocomplete.ModelSelect2Multiple(url='accession-autocomplete'))
    accession = forms.ModelMultipleChoiceField(queryset=Accession.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'20'}),
                                                required=False)
    
    class Meta:
        model = Accession
        fields = ('__all__')
        
    
    
class GenoViewerLocusForm(forms.Form):
    locus_name = forms.ModelMultipleChoiceField(label='Enter locus name',
                                                queryset=Locus.objects.all(),
                                                widget=autocomplete.ModelSelect2Multiple(url='locus-autocomplete'))
    
    
class GenoViewerChrForm (forms.Form):
    genome_version = forms.CharField(required = False, label='Enter genome version') # widget=forms.TextInput(attrs={'required':"required"}),
    chromosome = forms.CharField(required = False, label='Enter chromosome')
    position_minimale = forms.IntegerField(required = False, label='Enter minimal position (optional)')
    position_maximale = forms.IntegerField(required = False, label='Enter maximal position (optional)')
#     position_minimale.widget.attrs["required"] = "required"
#     position_maximale.widget.attrs["required"] = "required"
#     chromosome.widget.attrs["required"] = "required"
#     genome_version.widget.attrs["required"] = "required"
    
class GenoViewerExperimentForm (forms.Form):
    experiment = forms.ModelMultipleChoiceField(queryset=Experiment.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Experiment",False,attrs={'rows':'100','size':'15'}),
                                                required=True)
    
class GenoViewerReferentialForm (forms.Form):
    referential = forms.ModelMultipleChoiceField(queryset=Referential.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Referential",False,attrs={'rows':'100','size':'15'}),
                                                required=False)
    
CHOICESACC = [('4', 'Genotyping ID'),
              ('3', 'Sample',), 
              ('2', 'Seed lot',), 
              ('1', 'Accession',),
              ]

CHOIX = [('1', 'Indiv level'),
         ('2', 'Frequency level')]
    
class DataviewGenotypingValuesForm(forms.Form):
    choose_visualization_mode = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOICESACC, required=True, initial="4")
    export_or_view_in_web_browser = forms.ChoiceField(choices=MY_CHOICES, required=False)
    ind_or_freq = forms.ChoiceField(widget=HorizontalRadioSelect(), choices=CHOIX, initial= "1",required=False, label='Individual or frequency level')
#     corr = forms.BooleanField(label="Export considering correspondence (only for microsat)", required=False)
    missing_data = forms.CharField(max_length=30, initial='missing data', required=False)

class UploadLocusFileForm(forms.Form):
    file = forms.FileField(label='Enter a file with loci', required=False)

class DtviewProjectForm(forms.Form):
    project = forms.ModelMultipleChoiceField(queryset=Project.objects.all(),
                                             widget=FilteredSelectMultiple("Project", False, attrs={'rows':'100','size':'15'}),
                                             required=False)
    
class FicheForm(forms.Form):
    fiche = forms.CharField(widget=forms.HiddenInput())
    
class QueryForm(forms.Form):
    query = forms.CharField(label='Enter a genoid', required=True)
    
class DtviewAccessionTypeForm (forms.Form):
    accession_type = forms.ModelMultipleChoiceField(queryset=AccessionType.objects.all().order_by('name'),
                                                widget=FilteredSelectMultiple("Accession",False,attrs={'rows':'100','size':'7'}),required=True)

choices_more_accession=[('1','Seed lot'),
              ('2','Sample')]


class DataviewAccession(forms.Form):
    more_info = forms.ChoiceField(widget=forms.CheckboxSelectMultiple(),choices=choices_more_accession, required=False, label='Other information to display')
  
choices_more_genoid=[('1','Sample'),
                     ('2','Seed lot'),
                     ('3','Accession')]

class DataviewGenoIDForm(forms.Form):
    more_info = forms.ChoiceField(widget=forms.CheckboxSelectMultiple(),choices=choices_more_genoid, required=False, label='Other information to display')
   
CHOICES_GENEALOGY_TYPES = [('1','Accession Cross'),
                           ('4','Other Accession Pedigree'),
                           ('2','Seedlot Cross'),
                           ('3','Seedlot Multiplication'),
                           ]

class DtviewGenealogyTypeForm(forms.Form):
    genealogy_types = forms.MultipleChoiceField(choices = CHOICES_GENEALOGY_TYPES,
                                                widget=FilteredSelectMultiple("Genealogy",False,attrs={'rows':'100','size':'7'}),required=True)
