# -*- coding: utf-8 -*-
"""LICENCE
    ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
LICENCE"""

from __future__ import absolute_import
import django_mongoengine
#a mettre sinon pas de resultat a voir
from mongoengine import *
from django import forms
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render, render_to_response
from dataview.forms import PhenoViewerAccessionForm, PhenoViewerTraitForm,\
    PhenoViewerEnvironmentForm, DataviewPhenotypingValuesForm,\
    GenoViewerAccessionForm, GenoViewerLocusForm, GenoViewerExperimentForm,\
    DataviewGenotypingValuesForm, GenoViewerReferentialForm, GenoViewerChrForm,\
    DataviewGenoIDForm, UploadLocusFileForm, DtviewProjectForm, FicheForm, DtviewAccessionTypeForm,DataviewAccession, GenoViewerAccessionForm2, PhenoViewerAccessionForm2,\
    ClassificationViewerForm, ClasseViewerForm, SeedlotViewerForm, DtviewGenealogyTypeForm,\
    DataviewClassificationVisuForm
from lot.models import Seedlot, SeedlotTypeAttribute 
from phenotyping.models import PhenotypicValue, Trait, Environment, SpecificTreatment
from genotyping.models import Sample, LocusType, Locus, Referential, Experiment, GenotypingID
from accession.models import Accession, AccessionType, AccessionTypeAttribute
from team.models import User, Project, Institution

from team.views import write_csv_search_result
from genealogy_accession.models import AccessionRelation
from genealogy_seedlot.models import SeedlotRelation
from classification.models import Classification, Classe, ClasseValue
from images.models import Image, ImageLink
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
#necessaire sinon pb !!
from mongoengine.queryset.visitor import Q
from django.db.models import Q as Query_dj
import datetime
import ast ##convertir des strings en dict
from itertools import product
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import csv
import json
from django.conf import settings

import re
import numpy as np
from django.db import IntegrityError
from django.db.models import Count
from collections import Counter

import pandas as pd
from operator import ior, iand

# Create your views here.
def dtgenohome(request):
    return render(request,'dataview/phenotyping_dtview_base.html',{'dataview':True,})

#=======================================VIEWER ==================================================

def return_csv_pheno(request):
    tmpfile=open('/tmp/view_pheno.csv','r')  
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename=view_pheno.csv'
    return response



#################################### CLASSIFICATION DATAVIEW #################################

def return_csv_classif(request):
    """
        Create a CSV file with selected structure's data.
        
        :var file tmpfile: temporary file with data
        :var HttpResponse response: HttpResponse for the temp file
    """
    tmpfile=open('/tmp/view_classif.csv','r')  
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename=view_classif.csv'
    return response
# @login_required
# @user_passes_test(lambda u: u.is_superuser(), login_url='/login/')

def classViewer_management(request):
    """
        Structure's dataview in the Dataview menu.
        
        :var string template: template for dataview
        :var int n_page: page / step of the search in the dataview
        :var list select_classification: classification selected in the first step
        :var list classe_values: all the values in the classification
        :var Seedlot sl: seedlot in the classification
        :var int export_or_web: boolean to have the type of visualization
        :var int show_accession: boolean to show accession in the table
    """
    if request.method=="POST":
        #Get the actual page number
        n_page = int(request.POST['n_page'])
        
        #We get the user's projects
        username = None
        if request.user.is_authenticated():
            username = request.user.login
        username_id=User.objects.get(login=username).id
        user_projects = Project.objects.filter(users=username_id)
        
        #Choose Classification
        if int(n_page) == 1:
            
            project = request.POST.getlist('project')
            request.session['projects'] = project
            formclassification = ClassificationViewerForm()
            formproject = DtviewProjectForm(request.POST)
            #Filter the query set in the Form
            formclassification.fields["classification"].queryset = Classification.objects.filter(projects__in=project).distinct().order_by('name')
            
            #Check le formulaire et récupération des projets dans une liste
            if formproject.is_valid():
                list_select_project = formproject.cleaned_data['project']
                
                #len() permet de checker si on a sélectionné au moins 1 projet
                if len(list_select_project) != 0:
                    return render(request,'dataview/classification_viewer.html',{'dataview':True, "n_page":n_page, 'formclassification': formclassification})
            
            
            
        #Choose Seedlots / Groups
        if int(n_page) == 2:
            
            classification = request.POST.get('classification')
            formclasse = ClasseViewerForm()
            formseedlot = SeedlotViewerForm()
            formclassification = ClassificationViewerForm(request.POST)
            visumodeform = DataviewClassificationVisuForm()
            
            #Get the names of the classes
            formclasse.fields["group"].queryset = Classe.objects.filter(classification=classification).distinct().order_by('name')

            names = []
            #ClasseValue ayant une classe appartenant à la classification
            classe_values = ClasseValue.objects.filter(classe__in=Classe.objects.filter(classification=classification).distinct())
            for cv in classe_values:
                sl = cv.seedlot
                names.append(sl.name)
            
            #Liste distincte des noms de seedlot
            #print(list(set(names)))
            
            #We get the projects linked to the classification
            classif_projects = Classification.objects.get(id=classification).projects.all()
            
            #We select the projects that the user can see and that are linked to the classification 
            #(intersection of the 2 querysets)
            project_qs_int = user_projects & classif_projects

            #We get the queryset of the selected projects
            projects = Project.objects.filter(id__in=request.session['projects'])
            
            #We refine the queryset by the projects that were selected at first
            project_qs = project_qs_int & projects.all()
            
            #Get the names of the seedlots in the classification
            formseedlot.fields["seedlot"].queryset = Seedlot.objects.filter(name__in=list(set(names)), projects__in=project_qs).distinct().order_by('name')
            
            #Filter the query set in the Form
            
            #Check le formulaire et récupération des classifications dans une liste
            if formclassification.is_valid():
                select_classification = formclassification.cleaned_data['classification']
                
                #len() permet de checker si on a sélectionné au moins 1 classification
                return render(request,'dataview/classification_viewer.html',{'dataview':True, "n_page":n_page, 'formclasse': formclasse, 'formseedlot': formseedlot, "visumodeform":visumodeform, "classification":classification})
            
        #Show the results
        if int(n_page) == 3:
            
            classification = request.POST.get('classification') #ID de la classification
            classification = Classification.objects.get(id=classification)
            classe = request.POST.getlist('classe')
            seedlot = request.POST.getlist('seedlot')
            formclasse = ClasseViewerForm(request.POST)
            formseedlot = SeedlotViewerForm(request.POST)
            export_or_web = request.POST.get('export_or_web')
            show_accession = request.POST.get('show_accession')
            
            if formclasse.is_valid():
                select_classe = formclasse.cleaned_data['group']
                if formseedlot.is_valid():
                    select_seedlot = formseedlot.cleaned_data['seedlot']
                    
            #Recupérer les données en fonction des colonnes/lignes choisies
            df = getClassificationData(select_classe, select_seedlot, show_accession)
            
            #Création du tableau en html
            if df.empty:
                html=None
            else:
                #html = df.to_html(index=False) 
                html = df.to_html()   
                html = html.split('\n')
                    
            print(html)
            
            ########### SANS PANDAS #############
                    
            #Création du header            
            fields_label = ["Seedlot"]
            if show_accession == "1":
                fields_label.append("Accession")
            for gp in select_classe:
                fields_label.append(gp.name)
                
            #Récupération des données
            values = []
            
            for seedlot in select_seedlot:
                line = []
                line.append(seedlot)
                if show_accession == "1":
                    line.append(seedlot.accession.name)
                for gp in select_classe:
                    cv = ClasseValue.objects.get(seedlot=seedlot, classe=gp)
                    line.append(cv.value)
                
                values.append(line)    
                    
            ######################################
            
            if export_or_web == "1":
                return return_csv_classif(request)
            else:
                list_seedlot=df.index
                list_groups=list(df) #Liste des groupes et autres colonnes (accessions)

                return render(request,'dataview/classification_dataview.html',{'dataview':True,"show_accession": show_accession, "values":values, "fields_label":fields_label, "html":html, 'export_or_web': export_or_web, 'list_seedlot':select_seedlot, 'list_groups':list_groups, 'classification':classification})

        
    #If GET, Choose Project
    formproject = DtviewProjectForm(request.POST)
    
    username = None
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    formproject.fields['project'].queryset=projects.order_by('name')
       
    n_page=0
    
    return render(request,'dataview/classification_viewer.html',{'dataview':True, "n_page":n_page, 'formproject':formproject,})

def getClassificationData(select_classe, select_seedlot, show_accession):
    """
        Create a dataframe with the filtered data (seedlots/groups/values). 
        
        :var list select_classe: selected groups in the classification
        :var list select_seedlot: selected seedlots in the classification
        :var boolean show_accession: add a column with accession ?
    """

    df_classif = pd.DataFrame()
    
    #For each classe/sedlot, get the value and update the table
    for classe in select_classe:
        for seedlot in select_seedlot:
            values = ClasseValue.objects.get(classe=classe, seedlot=seedlot)
            
            if show_accession == "1":
                df_classif.ix[seedlot.name, "Accession"] = seedlot.accession.name

            df_classif.ix[seedlot.name, classe.name] = values.value
            
    df_classif = df_classif.sort(axis=0)
    df_classif = df_classif.fillna('NA')      
    df_classif.to_csv("/tmp/view_classif.csv", sep=';')

    return df_classif

@login_required
def classPieChart(request, seedlot, classification):
    """
        Get the data for a seedlot in one classification. And create a Piechart.
        
        :var string template: template for classPieChart
        :var Seedlot seedlot: seedlot to show
        :var list classe_values: all the classe_values for the seedlot in the classification
        :var Classification classification: classification's name
    """
    template = "dataview/classification_piechart.html"

    #Get the Seedlot
    seedlot = Seedlot.objects.get(id=seedlot)
    #Get the CV of the Seedlot in this classification
    classe_values = ClasseValue.objects.filter(seedlot=seedlot, classe__in=Classe.objects.filter(classification=Classification.objects.get(id=classification)))
    #Get the name of the classification
    classification = (Classification.objects.get(id=classification)).name
    return render(request,template,{'dataview':True, "classe_values":classe_values, "seedlot_name":seedlot.name, "classification":classification})

def classMapViewer_management(request):
    """
        Manage the dataview with map. 
        
        :var string template: template for classMapViewer_management
        :var int n_page: page / step of the search in the dataview
        :var list project: all the projects with access
        :var list classification: classification selected in the first step
        :var list classe_values: all the values in the classification
        :var list list_groups: groups in the selected classification
        :var list list_lat: latitude for each accessions
        :var list list_long: longitude for each accessions
        :var Classe select_group: selected group for the map
    """
    template = "dataview/classification_map.html"
    
    if request.method=="POST" or 'id_classification' in request.GET.keys():
        #Get the actual page number
        if 'id_classification' in request.GET.keys():
            n_page = 2
        else:
            n_page = int(request.POST['n_page'])

        #We get the user's projects
        username = None
        if request.user.is_authenticated():
            username = request.user.login
        username_id=User.objects.get(login=username).id
        user_projects = Project.objects.filter(users=username_id)

        #Choose Classification
        if int(n_page) == 1:
            
            project = request.POST.getlist('project')
            request.session['projects'] = project
            formclassification = ClassificationViewerForm()
            formproject = DtviewProjectForm(request.POST)
            #Filter the query set in the Form
            formclassification.fields["classification"].queryset = Classification.objects.filter(projects__in=project).distinct().order_by('name')
            
            #Check le formulaire et récupération des projets dans une liste
            if formproject.is_valid():
                list_select_project = formproject.cleaned_data['project']
                
                #len() permet de checker si on a sélectionné au moins 1 projet
                if len(list_select_project) != 0:
                    return render(request,template,{'dataview':True, "n_page":n_page, 'formclassification': formclassification})
        
        #Show the map and select a group
        if int(n_page) == 2:
            if 'id_classification' in request.GET.keys():
                classification_id = request.GET['id_classification']
                request.session['projects'] = [proj.id for proj in user_projects]
            else:
                classification_id = request.POST.get('classification')
            classification = Classification.objects.get(id=int(classification_id))

            #print(classification)  
            
            
            #We get the projects linked to the classification
            classif_projects = classification.projects.all()
            
            #We select the projects that the user can see and that are linked to the classification 
            #(intersection of the 2 querysets)
            project_qs_int = user_projects & classif_projects

            #We get the queryset of the selected projects
            projects = Project.objects.filter(id__in=request.session['projects'])
            
            #We refine the queryset by the projects that were selected at first
            project_qs = project_qs_int & projects.all()
            
            #Get groups
            list_groups = Classe.objects.filter(classification=classification)
            
            #Get seedlots
            classe_values = ClasseValue.objects.filter(classe__in=list_groups)
            list_seedlots = []
            for cv in classe_values:
                if not cv.seedlot in list_seedlots:
                    if cv.seedlot.projects.all() & project_qs:
                        list_seedlots.append(cv.seedlot)
            
            #Seedlots names
            list_seedlots_names=[]
            for seedlot in list_seedlots:
                list_seedlots_names.append(seedlot.name)
            list_seedlots_names = json.dumps(list_seedlots_names)

            #Nb Seedlots
            nb_seedlots = []
            for i in range(0, len(list_seedlots)):
                nb_seedlots.append(i)
                    
            #Get Latitude & Longitude
            list_lat = []
            list_long = []
            
            for seedlot in list_seedlots:
                list_lat.append(seedlot.accession.latitude)
                list_long.append(seedlot.accession.longitude)
            
            list_lat = json.dumps(list_lat)
            list_long = json.dumps(list_long)

            
            #Selected group
            try:
                select_group = request.POST["group"]
                select_group = Classe.objects.get(id=select_group)
                
                #Get the classe_values for the group
                list_cv = []
                for seedlot in list_seedlots:
                    list_cv.append(ClasseValue.objects.get(classe=select_group, seedlot=seedlot).value)
                    
                list_cv = json.dumps(list_cv)
                
                return render(request,template,{'dataview':True, "n_page":n_page,"color":True, "list_lat":list_lat, "list_long":list_long, "classification":classification, "nb_seedlots": nb_seedlots, "list_cv":list_cv, "list_seedlots_names": list_seedlots_names, "list_groups": list_groups, "select_group": select_group})

            except:
                select_group = None
                list_cv=[]
                list_cv = json.dumps(list_cv)

            
            return render(request,template,{'dataview':True, "n_page":n_page, "color":False, "list_lat":list_lat, "list_long":list_long,"classification":classification, "nb_seedlots": nb_seedlots, "list_cv":list_cv,"list_seedlots_names": list_seedlots_names, "list_groups": list_groups, "select_group": select_group})
            

        
    #If GET, Choose Project
    formproject = DtviewProjectForm(request.POST)
    
    username = None
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    formproject.fields['project'].queryset=projects.order_by('name')
       
    n_page=0
    
    return render(request,template,{'dataview':True, "n_page":n_page, 'formproject':formproject,})







######################################## PHENOTYPING DATAVIEW #################################

def phenoViewer_management(request):
    project=[]
    form2 = PhenoViewerAccessionForm2(request.POST)
    formtrait = PhenoViewerTraitForm(request.POST)
    formenv = PhenoViewerEnvironmentForm(request.POST)
    formproject = DtviewProjectForm(request.POST)
    error_msg=""
    headername = None
    if request.method=="POST":
        projects_or_not=request.POST['projects_or_not']
        if projects_or_not=="0":
            form2 = PhenoViewerAccessionForm2()
            formtrait = PhenoViewerTraitForm()
            formenv = PhenoViewerEnvironmentForm()
            formproject = DtviewProjectForm(request.POST)
            visumodeform = DataviewPhenotypingValuesForm()
            projects_or_not=1
            project=request.POST.getlist('project')
            form2.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=project).distinct().order_by('name')
            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=project).distinct().order_by('name')
            return render(request,'dataview/phenotyping_viewer.html',{'dataview':True,"projects":project,'form2':form2,"projects_or_not":projects_or_not,'formtrait':formtrait, 'formenv':formenv, 'formproject':formproject, 'visumodeform':visumodeform})

        else:    
            if form2.is_valid() and formenv.is_valid() and formtrait.is_valid():
                if request.POST.getlist("name"):
                    id_autocomplete = request.POST.getlist("name")
                    listaccessions = Accession.objects.filter(id__in=id_autocomplete)   
                else:   
                    listaccessions = form2.cleaned_data['accession']
                listraits = formtrait.cleaned_data['trait']
                listenvs = formenv.cleaned_data['environment']   
                
                #============================
                mode = request.POST["choose_visualization_mode"]
                if mode == "1":
                    headername = 'Accession'
                else:
                    headername = 'Seedlot'     
                #===========================================================           
                if listaccessions!= None and listraits!= None and listenvs!= None :
                    df = getPhenodata(listaccessions, listraits, listenvs, headername)
                    if df.empty:
                        html=None
                    else:
                        html = df.to_html()    
                        html = html.split('\n')

                    pheno_html="dataview/phenotyping_dtview_base.html"
                    if request.POST["export_or_view_in_web_browser"]=="2":
                        return render(request, 'phenotyping/dtviewphenovalues.html', {'dataview':True,"pheno_html":pheno_html,"html":html})
                    else:
                        return return_csv_pheno(request)
                else:                
                    if listaccessions==None:
                        error_msg="Please choose at least one accession (step 1)."
                    elif listraits==None:
                        error_msg="Please choose at least one trait (step 2)."
                    elif listenvs==None:
                        error_msg="Please choose at least one environment (step 3)."
                    return render(request, 'dataview/phenotyping_viewer.html', {'dataview':True,'accesform':GenoViewerAccessionForm(), 'traitform':PhenoViewerTraitForm, 'enviform':PhenoViewerEnvironmentForm, 'visumodeform':DataviewPhenotypingValuesForm})       
                          
                    #========================================================
                    
            elif request.method=="POST":
                if not request.POST.getlist("name") and not request.POST.getlist('accession'):
                    error_msg="Please choose at least one accession (step 1)."
                elif not request.POST.getlist("trait"):
                    error_msg="Please choose at least one trait (step 2)."
                elif not request.POST.getlist('environment'):   
                    error_msg="Please choose at least one environment (step 3)."
    form2 = PhenoViewerAccessionForm2()
    formtrait = PhenoViewerTraitForm()
    formenv = PhenoViewerEnvironmentForm()
    visumodeform = DataviewPhenotypingValuesForm()
    username = None
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    formproject.fields['project'].queryset=projects.order_by('name')
    projects_or_not = 0
    if error_msg!="":
        projects_or_not=1
        project=request.POST['project']
        projects=project.split("'")
        project=[]
        for i in projects:
            if i.isdigit():
                project.append(i)        
        form2.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
    else:
        project=""
    return render(request,'dataview/phenotyping_viewer.html',{'dataview':True,"formproject":formproject,"formenv":formenv,"formtrait":formtrait,"form2":form2,'projects':project,'error_msg':error_msg,'formproject':formproject,"projects_or_not":projects_or_not,'accesform':GenoViewerAccessionForm(), 'visumodeform':visumodeform})

def getPhenodata(listaccessions, listraits, listenvs, headername):#headers
    datadict ={}
    traitslist = [exp for exp in listraits if exp != None]        
    envilist = [env for env in listenvs if env != None]
    acclist = [acc for acc in listaccessions if acc != None]
    headers = []
    headers.append(headername)   
    pheno_viewer=False 
    df_pheno=pd.DataFrame()
    for trait in traitslist:
        for e in envilist:
            tr = str(trait.name) +'\n'+ str(e.name)
            for acces in acclist:
                try:
                    seedlots=Seedlot.objects.filter(accession=acces)
                    for seed in seedlots:
                        phenos=PhenotypicValue.objects.filter(seedlot=seed,environment=e,trait=trait)
                        for pheno in phenos:
#                             print(acces,pheno.value)
                            if headername == 'Accession' or headername == "Accession_pheno":
                                df_pheno.ix[str(acces),tr]=str(pheno.value).replace(',', '.')
                            elif headername == "Geno_pheno":
                                sample=Sample.objects.filter(seedlot=seed)
                                for sple in sample:
                                    genoid=GenotypingID.objects.filter(sample=sple)
                                    for geno in genoid:
                                        df_pheno.ix[str(geno),tr]=str(pheno.value).replace(',', '.')                            
                            elif headername == "Sample_pheno":
                                sample=Sample.objects.filter(seedlot=seed)
                                for sple in sample:
                                    df_pheno.ix[str(sple),tr]=str(pheno.value).replace(',', '.')          
                            else: 
                                df_pheno.ix[str(seed),tr]=str(pheno.value).replace(',', '.')
                except:
                    pass
    df_pheno = df_pheno.fillna('NA')                    
    df_pheno.to_csv("/tmp/view_pheno.csv", sep=';')
    return df_pheno

def FileToCSV(headers, datadict):
    try:
        myFilePath = "/tmp/view_pheno.csv"
        with open(myFilePath, 'w') as myCSVFILE:
            csvWriter = csv.writer(myCSVFILE, dialect='excel', delimiter = ';')
            csvWriter.writerow(headers)
            for key, value in datadict.items():
                line=[]
                line.append(key)                      
                for h in headers[1:]:
                    check=0
                    for i,j in value.items():
                        if i==h:
                            line.append(j)
                            check=1
                    if check==0:
                        line.append('X')
                csvWriter.writerow(line)
    except IOError as errno:
        print("I/O error({0}): {1}".format(errno, errno))
    return  

#======================================GENOTYPING VALUES===================================================

def getGenodata(export,etape, projects_acc, chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode):
    listIdExp = []
    listIdRef = []
    listSamples = []
    samples = []
    sample_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    sd_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    acc_name = ["GENOME VERSION","CHROMOSOME","POSITION","REFERENTIAL","EXPERIMENT",]
    genotypingid_name = ["GENOME VERSION","CHROMOSOME","POSITION",]
    seed_lots = []
    ls_samples = []
    #print(listexperiments
#     header.append(str(headername))
    # si aucune accession n'est selectionne, ttes le st par defaut pareil pr exp et red
    if not listaccessions:
        listaccessions = Accession.objects.all(projects__in=projects_acc)
    if not listexperiments:
        listexperiments = Experiment.objects.all(projects__in=projects_acc)
    if not listreferentials:
        listreferentials = Referential.objects.all()   
    
    # recup des id des listes demandees par l'usr pr les request
#     print("proj2: "+str(projects_acc))
    for exp in listexperiments:
        listIdExp.append(exp.id)
    for ref in listreferentials:
        listIdRef.append(ref.id)
    for acc in listaccessions:
        acc_name.append(acc.name)
#         print(acc.name)
        seed = Seedlot.objects.filter(accession=acc,projects__in=projects_acc)
#         print(seed)
        if seed != None:
            for sd in seed:
#                 print(sd)
                seed_lots.append(sd)
                sd_name.append(sd.name)
                samples = Sample.objects.filter(seedlot=sd,projects__in=projects_acc)
#                 print(samples)
                if samples != None:
                    for sp in samples:
                        sample_name.append(sp.name)
                        listSamples.append(sp.id)
                        ls_samples.append(sp)
#                         print("proj3: "+str(projects_acc))
    # differentes req
#     print(listSamples)
    if genomeversion==None:
        genomeversion="3"
        
    if name!=None:     
        name = [x.decode() for x in name]  
    if chromosome==None:    
        #on recup tous les locus dont le nom se trouve dans le fichier donne
#         print(name)
        for i in name:
            listlocus=Locus.objects(name=i)
        listlocus = Locus.objects(name__in=name)
    else:  
        if chromosome != None and position_minimale != None and position_maximale != None:
            listlocus = Locus.objects(Q(positions__chromosome__in=chromosome) \
                & Q(positions__position__gte=position_minimale) \
                & Q(positions__position__lte=position_maximale) \
                & Q(positions__genomeversion__in=genomeversion))
        elif chromosome !=None:
            listlocus = Locus.objects(positions__chromosome__in=chromosome)
# listlocus=Locus.objects.all()
    listlocus2=listlocus.distinct('name')
    if len(listlocus2) > 300:
        df_pd_test = None
        nb_columns = 0
        
    else:
        locus_names = []
        for locus in listlocus2:
            locus_names.append(locus)
        listref = []
        i = 0
        dico_tableau={}
        d2 = datetime.datetime.now()
        
        import pymongo
        from pymongo import MongoClient
        client = MongoClient()
        db = client[settings.MONGODB_DATABASES['default']['name']]
        list_test_csv=[]
        list2=[]
        num_list=0
        dico2={}
        position_locus_counter=0
        request=[]
        dico_loc={}
        
        try:
            if etape=="1":
                df_pd_test=pd.read_csv('/tmp/view_geno.csv', index_col=0)
                nb_columns=len(locus_names)
                list_column_names=list(df_pd_test.columns.values)
                list_row_names=list(df_pd_test.index.values)
                dico_locus_column_names={}
                df_pd_etape1=pd.DataFrame(index=list_row_names)
                for i in listlocus2:
                    dico_locus_column_names[i]=[]
                    for k in list_column_names:
                        if i == k.replace(' ',''):
                            dico_locus_column_names[i].append(k)
                    if dico_locus_column_names[i]==[]:
                        del dico_locus_column_names[i]
                for key,value in dico_locus_column_names.items():
                    df_pd_etape1[key]=""
                    for v in value:
                        if v==value[-1]:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]
                        else:
                            df_pd_etape1[key]=df_pd_etape1[key]+df_pd_test[v]+"/"
                        df_pd_etape1.ix["Genome Version",key]=df_pd_test.loc["Genome Version",key]
                        df_pd_etape1.ix["Chromosome",key]=df_pd_test.loc["Chromosome",key]
                        df_pd_etape1.ix["Position",key]=df_pd_test.loc["Position",key]
                
                
                try:
                    df_pd_etape1=df_pd_etape1.T.sort('Position', ascending=True).T
                    df_pd_etape1=df_pd_etape1.dropna(axis=1, how='all')      
                except:
                    pass  
                return df_pd_etape1, len(listlocus), nb_columns               
            
            elif etape=="0":
                df_pd_etape2=pd.read_csv('/tmp/view_geno.csv', index_col=0)
                nb_columns=len(locus_names)
                try:
                    df_pd_etape2=df_pd_etape2.T.sort('Position', ascending=True).T
                    df_pd_etape2=df_pd_etape2.dropna(axis=1, how='all')  
                except:
                    pass      
                return df_pd_etape2, len(listlocus), nb_columns               
            
        except Exception as e:
            print("\n\n\n===>",e,"\n\n\n")
            pass
        
        #ACCESSION
        if mode != "4":
            name_test_csv=['']   
            geno = ''
            req_position = ''
            requete_geno = ''
            for loc in locus_names:
                
#                 requete_geno = db.locus.find_one({'$and':[{'genotyping':{"$ne":[]}},{'positions':{"$exists":True}},{'name':loc}]})
#                 if not requete_geno:
                req_position=db.locus.find({'$and':[{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                exp_list=[]
                ref_list=[]
                liste_loc=[loc]
#                 
#                 if len(requete_geno['genotyping']) != 0:
#                     list_test_csv.append({'':'Genome Version',loc:requete_geno['positions'][0]['genomeversion']})
#                     list_test_csv.append({'':'Chromosome',loc:requete_geno['positions'][0]['chromosome']})
#                     list_test_csv.append({'':'Position',loc:requete_geno['positions'][0]['position']})
#                     for i in range(len(requete_geno['genotyping'])):
#                             
# 
#                         if int(requete_geno['genotyping'][i]['sample_id']) in listSamples and int(requete_geno['genotyping'][i]['genotyping'][0]['experiment_id']) in listIdExp and int(requete_geno['genotyping'][i]['genotyping'][0]['referential_id']) in listIdRef :
#                             try:
#                                 list_test_csv.append({'':'Referential',loc:Referential.objects.get(id=requete_geno['genotyping'][i]['genotyping'][0]['referential_id']).name})
#                                 list_test_csv.append({'':'Experiment',loc:Experiment.objects.get(id=requete_geno['genotyping'][i]['genotyping'][0]['experiment_id']).name})
#                             except Exception as e:
#                                 print("erreur: ",e)
#                                 pass
#                             sample=Sample.objects.get(id=requete_geno['genotyping'][i]['sample_id']).name
#                             sample_id=Sample.objects.get(id=requete_geno['genotyping'][i]['sample_id']).id
#                             experiment_id=requete_geno['genotyping'][i]['genotyping'][0]['experiment_id']
#                             referential_id=requete_geno['genotyping'][i]['genotyping'][0]['referential_id']
#                             if experiment_id not in exp_list :
#                                 exp_list.append(experiment_id)
#                                 if len(exp_list)>1:
#                                     liste_loc.append(str(liste_loc[-1])+' ')
#                             if referential_id not in ref_list :
#                                 ref_list.append(referential_id)
#                                 if len(ref_list)>1:
#                                     liste_loc.append(str(liste_loc[-1])+' ')
#                             compteur=-1
#                             print("exp_list: ",exp_list)
#                             print("ref_list: ",ref_list)
#                             for i in exp_list:
#                                 print(Experiment.objects.get(id=i).name)
#                             for exp in exp_list:
#                                 print("exp: ",exp)
#                                 print("experiment_id: ",experiment_id)
#                                 if exp == experiment_id :
#                                     numero_locus=compteur
#                                 compteur = compteur + 1
#                             for ref in ref_list:
#                                 print("ref: ",ref)
#                                 print("referential_id: ",referential_id)
#                                 if ref == referential_id :
#                                     numero_locus=compteur
#                                 compteur = compteur + 1
#                             loc = liste_loc[numero_locus]
#                             print(numero_locus)
#                             if loc not in name_test_csv :
#                                 name_test_csv.append(loc)
#                             print(name_test_csv)
#                             seedlot=Seedlot.objects.get(sample=sample_id)
#                             accession=Accession.objects.get(seedlot=seedlot.id)
#                             test_csv=1
#                             list_test_csv[3][loc]=Referential.objects.get(id=referential_id).name
#                             list_test_csv[4][loc]=Experiment.objects.get(id=experiment_id).name
#                             if mode == "1":
#                                 create_list_csv(accession, list_test_csv, loc, requete_geno['genotyping'], i)
#                             elif mode == "2":
#                                 create_list_csv(seedlot, list_test_csv, loc, requete_geno['genotyping'], i)
#                             elif mode == "3":
#                                 create_list_csv(sample, list_test_csv, loc, requete_geno['genotyping'], i)
# #                             print(list_test_csv)
#                             
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    
                    for i in range(len(geno)):
                        genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'])
                        if position_locus_counter == 0:
                            try:
                                dico_genome_version={'':'Genome Version',loc:req_position['positions'][0]['genomeversion']}
                                dico_chromosome={'':'Chromosome',loc:req_position['positions'][0]['chromosome']}
                                dico_position={'':'Position',loc:req_position['positions'][0]['position']}
                                dico_referential={'':'Referential',loc:Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name}
                                dico_experiment={'':'Experiment',loc:Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name}
                                position_locus_counter=1
                                list_test_csv.append(dico_genome_version)
                                list_test_csv.append(dico_chromosome)
                                list_test_csv.append(dico_position)
                                list_test_csv.append(dico_referential)
                                list_test_csv.append(dico_experiment)
                            except Exception as e:
                                pass
                            
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']
#                         print(listSamples)

                        if int(geno[i]['sample_id']) in listSamples and int(geno[i]['genotyping'][0]['experiment_id']) in listIdExp and int(geno[i]['genotyping'][0]['referential_id']) in listIdRef :
                            sample=Sample.objects.get(id=geno[i]['sample_id']).name
                            sample_id=Sample.objects.get(id=geno[i]['sample_id']).id
                            experiment_id=geno[i]['genotyping'][0]['experiment_id']
                            referential_id=geno[i]['genotyping'][0]['referential_id']
                            if experiment_id not in exp_list :
                                exp_list.append(experiment_id) 
                                if len(exp_list)>1:
                                    liste_loc.append(str(liste_loc[-1])+' ')
# <<<<<<< .working
                            if referential_id not in ref_list :
                                ref_list.append(referential_id) 
                                if len(ref_list)>1:
                                    liste_loc.append(str(liste_loc[-1])+' ')
                            compteur=-1
# =======
#                             if referential_id not in ref_list :
#                                 ref_list.append(referential_id) 
#                                 if len(ref_list)>1:
#                                     liste_loc.append(str(liste_loc[-1])+' ')
#                             compteur=0
# >>>>>>> .merge-right.r614
                            for i in exp_list:
                                if i == experiment_id : 
                                    numero_locus=compteur
                                compteur = compteur + 1
# <<<<<<< .working
                            for i in ref_list:
                                if i == referential_id : 
                                    numero_locus=compteur
                                compteur = compteur + 1
                            loc = liste_loc[numero_locus]
# =======
#                             for i in ref_list:
#                                 if i == referential_id : 
#                                     numero_locus=compteur
#                                 compteur = compteur + 1
#                             loc=liste_loc[numero_locus]
# >>>>>>> .merge-right.r614
                            if loc not in name_test_csv :
                                name_test_csv.append(loc)
                            seedlot=Seedlot.objects.get(sample=sample_id)
                            accession=Accession.objects.get(seedlot=seedlot.id)
                            test_csv=1
                            list_test_csv[3][loc]=Referential.objects.get(id=referential_id).name
                            list_test_csv[4][loc]=Experiment.objects.get(id=experiment_id).name
                            if mode == "1":
                                create_list_csv(accession, list_test_csv, loc, geno, i)
                            elif mode == "2":
                                create_list_csv(seedlot, list_test_csv, loc, geno, i)
                            elif mode == "3":
# <<<<<<< .working
                                create_list_csv(sample, list_test_csv, loc, geno, i)
        
# =======
#                                 create_list_csv(Sample_name, list_test_csv, loc, geno, i)
# >>>>>>> .merge-right.r614
        #GENOTYPINGID
        else:
            name_test_csv=['','Accession','SeedLot','Sample','Experiment','Referential'] 
            dico2={}  
            for loc in locus_names:
                req_position=db.locus.find({"$and": [{'genotyping':[]},{'positions':{"$exists":True}},{'name':loc}]})
                geno=db.locus.find({"$and": [{'genotyping':{"$ne":[]}}, {'name':loc}]}).distinct('genotyping')
                if len(geno)!=0 and req_position.count()!=0:
                    req_position=req_position[0]
                    name_test_csv.append(loc)
                    for i in range(len(geno)):
                        if position_locus_counter == 0:
                            dico_genome_version={'':'Genome Version','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['genomeversion']}
                            list_test_csv.append(dico_genome_version)
                            dico_chromosome={'':'Chromosome','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['chromosome']}
                            list_test_csv.append(dico_chromosome)
                            dico_position={'':'Position','Accession':'--','SeedLot':'--','Sample':'--','Experiment':'--','Referential':'--',loc:req_position['positions'][0]['position']}
                            list_test_csv.append(dico_position)
                            position_locus_counter=1
                        else:
                            list_test_csv[0][loc]=req_position['positions'][0]['genomeversion']
                            list_test_csv[1][loc]=req_position['positions'][0]['chromosome']
                            list_test_csv[2][loc]=req_position['positions'][0]['position']
    
                        if geno[i]['sample_id'] in listSamples and geno[i]['genotyping'][0]['experiment_id'] in listIdExp and geno[i]['genotyping'][0]['referential_id'] in listIdRef :
                            try:
                                #genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'])
                                genotypingID=GenotypingID.objects.get(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                                dico={'':genotypingID,'Sample':geno[i]['sample_id'],'Experiment':genotypingID.experiment,'Referential':genotypingID.referential,loc:geno[i]['genotyping'][0]['genotype']}
                                test_csv=1
                                for k in range(len(list_test_csv)):
                                    if genotypingID in list_test_csv[k].values():
                                        num_list = k
                                        list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                        test_csv=0
                                if test_csv==1:
                                    seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
                                    accession=Accession.objects.get(seedlot=seedlot.id)
                                    dico_names={'Accession':accession,'SeedLot':seedlot,'':genotypingID,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':Experiment.objects.get(id=geno[i]['genotyping'][0]['experiment_id']).name, 'Referential':Referential.objects.get(id=geno[i]['genotyping'][0]['referential_id']).name ,loc:geno[i]['genotyping'][0]['genotype']}
                                    list_test_csv.append(dico_names)
                            except:
                                #genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'])
                                genotypingID=GenotypingID.objects.filter(sample=geno[i]['sample_id'],experiment=geno[i]['genotyping'][0]['experiment_id'],referential=geno[i]['genotyping'][0]['referential_id'])
                                test_csv=1
                                for genotyping_num in genotypingID:
                                    dico={'':genotyping_num,'Sample':geno[i]['sample_id'],'Experiment':geno[i]['genotyping'][0]['experiment_id'],'Referential':geno[i]['genotyping'][0]['referential_id'],loc:geno[i]['genotyping'][0]['genotype']}
                                    for k in range(len(list_test_csv)):
                                        if genotyping_num in list_test_csv[k].values():
                                            num_list = k
                                            list_test_csv[num_list][loc] = geno[i]['genotyping'][0]['genotype']
                                            test_csv=0
                                if test_csv==1:
                                    for genotyping_num in genotypingID:
                                        seedlot=Seedlot.objects.get(sample=geno[i]['sample_id'])
                                        accession=Accession.objects.get(seedlot=seedlot.id)
                                        dico_names={'Accession':accession,'SeedLot':seedlot,'':genotyping_num,'Sample':Sample.objects.get(id=geno[i]['sample_id']).name,'Experiment':genotyping_num.experiment, 'Referential':genotyping_num.referential ,loc:geno[i]['genotyping'][0]['genotype']}
                                        list_test_csv.append(dico_names)
        for i in list_test_csv:        
            for names in name_test_csv:  
                if not names in i:
                    i[names]='--'
        with open('/tmp/view_geno.csv','w') as f:                     
            writer=csv.DictWriter(f, fieldnames=name_test_csv)
            writer.writeheader()
            for i in list_test_csv:
                writer.writerow(i)
        df_pd_test=None       
        try:
            df_pd_test=pd.read_csv('/tmp/view_geno.csv', index_col=0)
            df_pd_test=(df_pd_test.T).sort('Position', ascending=True).T
        except:
            pass
        
        d2 = datetime.datetime.now()
    return df_pd_test, len(listlocus)

def create_list_csv(data_name, list_test_csv, loc, geno, i):
    test_csv=1
    for k in range(len(list_test_csv)):
        if data_name in list_test_csv[k].values():
            num_list = k
            list_test_csv[num_list][loc] = geno[int(i)]['genotyping'][0]['genotype']
            test_csv=0
    if test_csv==1:
        dico_names={'':data_name,loc:geno[int(i)]['genotyping'][0]['genotype']}
        list_test_csv.append(dico_names)

def genoViewer_management(request):
    project=[]
    template = 'dataview/dtviewgenovalues.html'
    formlocus = GenoViewerLocusForm(request.POST)
    formexp = GenoViewerExperimentForm(request.POST)
    formref = GenoViewerReferentialForm(request.POST)
    formchr = GenoViewerChrForm(request.POST)
    formfile = UploadLocusFileForm(request.POST,request.FILES)
    formproject = DtviewProjectForm(request.POST)
    visumodeform = DataviewGenotypingValuesForm(request.POST)
    if request.method == 'POST' or "or_and" in request.GET:        
        if "or_and" in request.GET:
            projects_or_not = '1'
        else:
            projects_or_not=request.POST['projects_or_not']
            
        if projects_or_not == '0':
            formacc = GenoViewerAccessionForm2()
            formlocus = GenoViewerLocusForm()
            formexp = GenoViewerExperimentForm()
            formref = GenoViewerReferentialForm()
            formchr = GenoViewerChrForm()
            formfile = UploadLocusFileForm()
            formproject = DtviewProjectForm(request.POST)
            project=request.POST.getlist('project')
            request.session['project_acc']=project
            formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
            formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=project).distinct().order_by('name')
            formref.fields["referential"].queryset = Referential.objects.filter(projects__in=project).distinct().order_by('name')
            visumodeform = DataviewGenotypingValuesForm()
            projects_or_not=1
            return render(request,'dataview/genotyping_viewer.html',{"refine_search":True,'dataview':True,'formproject':DtviewProjectForm(),"formacc":formacc,"visumodeform":visumodeform,
                                                                     "formlocus":formlocus,"formexp":formexp,"formref":formref,
                                                                     "formchr":formchr,"formfile":formfile,"projects_or_not":projects_or_not,"projects":project})
            
            
        elif projects_or_not == '1':
            if "or_and" in request.GET and "no_search" not in request.GET:
                mode = request.session['mode']
                dico_get={}
                or_and = None
                or_and=request.GET['or_and']
                for i in range(1,10):
                    data="data"+str(i)
                    text_data="text_data"+str(i)
                    if data in request.GET and text_data in request.GET:
                        if request.GET[data] == "experiment" and 'test' not in request.GET:
                            dico_get[data]={"name":request.GET[text_data]}
                        else:
                            dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                length_dico_get = len(dico_get)
                nb_per_page = 10
                df_test = pd.DataFrame.from_csv("/tmp/view_geno.csv",index_col=None)
                if or_and == 'and':
                    df_empty = 1
                    for dat, dico_type_data in dico_get.items():
                        for data_type, value in dico_type_data.items():
                            if mode != '4':
                                if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                    data_type = 'Unnamed: 0'
                            elif data_type == 'Genotyping ID':
                                data_type = 'Unnamed: 0'
                            data_type = data_type[0].upper()+data_type[1:]
                            if mode == "4":
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
                            else:
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) & df_or_and
                elif or_and == 'or':
                    df_empty = 1
                    for dat, dico_type_data in dico_get.items():
                        for data_type, value in dico_type_data.items():
                            if mode != '4':
                                if data_type == 'Accession' or data_type == 'Seed lot' or data_type == 'Sample':
                                    data_type = 'Unnamed: 0'
                            elif data_type == 'Genotyping ID':
                                data_type = 'Unnamed: 0'
                            data_type = data_type[0].upper()+data_type[1:]
                            if mode == "4":
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome") | df_test['Unnamed: 0'].str.contains("Position")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
                            else:
                                if df_empty == 1:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_test['Unnamed: 0'].str.contains("Genome Version") | df_test['Unnamed: 0'].str.contains("Chromosome")| df_test['Unnamed: 0'].str.contains("Position") | df_test['Unnamed: 0'].str.contains("Referential") | df_test['Unnamed: 0'].str.contains("Experiment")
                                    df_empty = 2
                                else:
                                    df_or_and = df_test[data_type].str.contains(value,case=False) | df_or_and
                df = df_test[df_or_and]
                list_attributes = list(df.columns.values)
                df = df.set_index('Unnamed: 0')
                df.index.names = [None] #permet d'avoir la premiere colonne du dataframe en index
                df.to_csv("/tmp/view_geno_refine_search.csv", index=False, encoding='utf-8')  
                html = df.to_html(index=True,justify={'center'})
                html = html.split('\n')
                html2 = refine_html(request, html, mode)
                if mode == '1':
                    nb_rows = len(df)-5
                    mode = 'Accession'
                    nb_marqueurs = len(df.columns)
                elif mode == '2':
                    nb_rows = len(df)-5
                    mode = 'Seed lot'
                    nb_marqueurs = len(df.columns)
                elif mode == '3':
                    nb_rows = len(df)-5
                    mode = 'Sample'
                    nb_marqueurs = len(df.columns)
                elif mode == '4':
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                for i in list_attributes : 
                    if "Unnamed:" in i:
                        list_attributes.remove(i)
                        list_attributes = [mode] + list_attributes
#<<<<<<< .working
                
                return render(request, template, {"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,'projects_or_not':projects_or_not,"or_and":or_and,})  
# =======
#                 return render(request, template, {"refine_search":True,"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,'projects_or_not':projects_or_not,"or_and":or_and,})  
# >>>>>>> .merge-right.r614
            
            elif "no_search" in request.GET:
                df = pd.DataFrame.from_csv('/tmp/view_geno.csv',index_col=None)
                list_attributes = list(df.columns.values)
                df = df.set_index('Unnamed: 0')
                df.index.names = [None]
                html = df.to_html(index=True,justify={'center'})
                html = html.split('\n')
                mode = request.session['mode']
                if mode == '1':
                    nb_rows = len(df)-5
                    mode = 'Accession'
                    nb_marqueurs = len(df.columns)
                elif mode == '2':
                    nb_rows = len(df)-5
                    mode = 'Seed lot'
                    nb_marqueurs = len(df.columns)
                elif mode == '3':
                    nb_rows = len(df)-5
                    mode = 'Sample'
                    nb_marqueurs = len(df.columns)
                elif mode == '4':
                    nb_rows = len(df)-3
                    mode = 'Genotyping ID'
                    nb_marqueurs = len(df.columns)-5
                for i in list_attributes : 
                    if "Unnamed:" in i:
                        list_attributes.remove(i)
                        list_attributes = [mode] + list_attributes
                html2 = refine_html(request, html, mode)
                return render(request, template, {"mode":mode,"nb_marqueurs": nb_marqueurs,"nb_rows":nb_rows,"list_attributes":list_attributes, "html":html2,'projects_or_not':projects_or_not})  
                
            if "projects_acc" in request.session:
                projects_acc_post=request.session['projects_acc']
                
            else:
                projects_acc_post = request.POST['project']
            projects_acc=[]
            for g in projects_acc_post:
                if g.isdigit() or g==',':
                    projects_acc.append(g)
            projects_acc="".join(projects_acc)
            projects_acc=projects_acc.split(',')
            formacc = GenoViewerAccessionForm2(request.POST)
            formexp = GenoViewerExperimentForm(request.POST)
            d1 = datetime.datetime.now()
            
            if "mode" not in request.POST:
                if not formexp.is_valid():
                    request.session['projects_acc']=projects_acc
                    formacc = GenoViewerAccessionForm2()
                    formexp = GenoViewerExperimentForm()
                    formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    formref.fields["referential"].queryset = Referential.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    message_error="Please enter some experiment. You may not have any right on the experiments. Please contact a competent person."
                    return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'message_error':message_error,'accesform':formacc, 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':formexp, 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                if not formacc.is_valid():
                    message_error="You don't have any right on the accessions. Please contact a competent person."
                    return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                if formacc.is_valid() and formexp.is_valid() and formref.is_valid():
                #cas ou l'on requete sur chromosome, region
                    name = []                
                    if formchr.is_valid() and not request.FILES:
                        chromosome = formchr.cleaned_data['chromosome']
                        genomeversion = formchr.cleaned_data['genome_version']
                        position_minimale = formchr.cleaned_data['position_minimale']
                        position_maximale = formchr.cleaned_data['position_maximale']
                        name = None
                        for i in [",",";","/"]:
                            if i in genomeversion:
                                genomeversion=genomeversion.split(i)
                                print("oui")
                            if i in chromosome:
                                chromosome=chromosome.split(i)
                        
                        if type(genomeversion)==str and len(genomeversion)==1:
                            genomeversion=genomeversion.split()
                        elif type(genomeversion)==list:
                            genomeversion=genomeversion
                        else:
                            message_error="Please use the right delimiters for the genome version."
                            return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                        if type(chromosome)==str:   
                            chromosome=chromosome.split()
                        elif type(chromosome)==list:
                            chromosome=chromosome
                        else:
                            message_error="Please use the right delimiters for the chromosome."
                            return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                    #cas ou on choisit une liste de locus
                    elif formlocus.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        name = formlocus.cleaned_data['locus_name']
                    
                    #file upload avec des noms de locus un par ligne
                    elif formfile.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        name = uploadFile(request.FILES['file'])
                        
#                         for line in request.FILES['file']:
#                             line = line.rstrip('\n')
#                             name.append(line)
                            
                    else:
                        message_error="Please enter some locus values."
                        return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})     
                    
                    etape="4"
                    listaccessions = formacc.cleaned_data['accession']
                    listaccessions_post=[i.name for i in listaccessions]
                    listexperiments = formexp.cleaned_data['experiment']
                    listexperiments_post = [i.name for i in listexperiments]
                    listreferentials = formref.cleaned_data['referential']
                    listreferentials_post = [i.name for i in listreferentials]
                    mode = request.POST["choose_visualization_mode"]
                    request.session['mode']=mode
                    export_or_view_in_web_browser = request.POST['export_or_view_in_web_browser']
                    ind_or_freq = request.POST['ind_or_freq']    
                
            elif "mode" in request.POST:
                ####### OK
                etape=request.POST['etape']
                mode=request.POST['mode']
                request.session['mode']=mode
                if mode=="Accession":
                    mode="1"
                elif mode=="Seed lot":
                    mode="2"
                elif mode=="Sample":
                    mode="3"
                elif mode=="Genotyping ID":
                    mode="4"
                ind_or_freq=request.POST["ind_or_freq"]
                export_or_view_in_web_browser=request.POST["export_or_view_in_web_browser"]
                chromosome_post=request.POST["chromosome"]
                chromosome=[]
                for c in chromosome_post:
                    if c.isdigit() or c==",":
                        chromosome.append(c)
                chromosome=''.join(chromosome)
                chromosome=chromosome.split(',')
                position_minimale=request.POST["position_minimale"]
                position_minimale=int(position_minimale)
                position_maximale=request.POST["position_maximale"]
                position_maximale=int(position_maximale)        
                genomeversion_post=request.POST["genomeversion"]
                genomeversion=[]
                for g in genomeversion_post:
                    if g.isdigit() or g==',':
                        genomeversion.append(g)
                genomeversion="".join(genomeversion)
                genomeversion=genomeversion.split(',')                
                listaccessions_post=request.POST["listaccessions"]
                listaccessions=listaccessions_post.strip('[').strip(']').split("'")
                listexperiments_post=request.POST["listexperiments"]
                listexperiments=listexperiments_post.strip('[').strip(']').split("'")
                listreferentials_post=request.POST["listreferentials"]
                listreferentials=listreferentials_post.strip('[').strip(']').split("'")
                for i in ['','u',', u']:
                    for j in listaccessions:
                        if i==j:
                            listaccessions.remove(j)
                    for j in listexperiments:
                        if i==j:
                            listexperiments.remove(j)
                    for j in listreferentials:
                        if i==j:
                            listreferentials.remove(j)        
                            
                listaccessions=Accession.objects.filter(name__in=listaccessions)
                name=request.POST["name"]
                listexperiments=Experiment.objects.filter(name__in=listexperiments)
                listreferentials=Referential.objects.filter(name__in=listreferentials)
                
            # niveau individu
            if ind_or_freq == ['1'] or ind_or_freq=="[u'1']" or ind_or_freq=="1":
                # export CSV
                if export_or_view_in_web_browser == '1':
                    export='export_csv'
                    df, nb_locus = getGenodata(export,etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode)
                    void = False
                    if df is not None:
                        if len(df)==0:
                            nb_rows = 0
                        else:
                            nb_rows = len(df)-5
                        if mode == '1':
                            nb_rows = len(df)-5
                            mode = 'Accession'
                            nb_marqueurs = len(df.columns)
                        elif mode == '2':
                            nb_rows = len(df)-5
                            mode = 'Seed lot'
                            nb_marqueurs = len(df.columns)
                        elif mode == '3':
                            nb_rows = len(df)-5
                            mode = 'Sample'
                            nb_marqueurs = len(df.columns)
                        elif mode == '4':
                            if len(df)==0:
                                nb_rows = 0
                            else:
                                nb_rows = len(df)-3
                            mode = 'Genotyping ID'
                            nb_marqueurs = len(df.columns)-5
#                     if df is not None:
                        df.to_csv('/tmp/view_geno.csv')

                        return render(request, 'dataview/dtviewgenovalues_csv.html', {'dataview':True, 'mode':mode})
                    else:
                        formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formref.fields["referential"].queryset = Referential.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        request.session['projects_acc']=projects_acc
                        projects_or_not = 1
                        message_error='nb loci found: ' + str(nb_locus) + '. Too many loci found: refine the query'
                        return render(request, 'dataview/genotyping_viewer.html', {'projects_or_not':projects_or_not,'dataview':True,'message_error':message_error,'formacc':formacc, 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':formexp, 'formref':formref, 'visumodeform':DataviewGenotypingValuesForm()})

                else:
                    export="table_web"
                    df, nb_locus = getGenodata(export,etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode)
                    # pour afficher le niveau
                    if df is not None:
                        if len(df)==0:
                            nb_rows = 0
                        else:
                            nb_rows = len(df)-5
                        if mode == '1':
                            mode = 'Accession'
                            nb_marqueurs = len(df.columns)
                        elif mode == '2':
                            mode = 'Seed lot'
                            nb_marqueurs = len(df.columns)
                        elif mode == '3':
                            mode = 'Sample'
                            nb_marqueurs = len(df.columns)
                        elif mode == '4':
                            if len(df)==0:
                                nb_rows = 0
                            else:
                                nb_rows = len(df)-3
                            mode = 'Genotyping ID'
                            nb_marqueurs = len(df.columns)-5
#                    if df is not None:
                        list_attributes = list(df.columns.values)
                        list_attributes = [mode] + list_attributes
                        if etape == "1":
                            df.to_csv('/tmp/view_geno_concat.csv')
                        else:
                            df.to_csv('/tmp/view_geno.csv')
                        html = df.to_html()    
                        html = html.split('\n')
                        
                        html2 = refine_html(request, html, mode)
                        if df.empty:
                            html = None
                            html2 = None
                        d2 = datetime.datetime.now()
                        if mode=="Accession" or mode=="Seed lot" or mode=="Sample":
                            type_table=1
                        elif mode=="Genotyping ID":
                            type_table=0
                        dico_genovalue={'mode':mode,
                                "nb_rows":nb_rows,
                                "ind_or_freq":ind_or_freq,
                                "export_or_view_in_web_browser":export_or_view_in_web_browser,
                                "chromosome":chromosome,
                                "position_minimale":position_minimale,
                                "position_maximale":position_maximale,
                                "genomeversion":genomeversion,
                                "listaccessions":listaccessions_post,
                                "name":name,
                                "html":html2,
                                "listexperiments":listexperiments_post,
                                "listreferentials":listreferentials_post,
                                "type_table":type_table,
                                "projects_or_not":projects_or_not,
                                "project":projects_acc,
                                "nb_marqueurs":nb_marqueurs,
                                "etape":etape,
                                'dataview':True,
                                "list_attributes":list_attributes, 
                                }
                        return render(request, 'dataview/dtviewgenovalues.html', dico_genovalue)
                        
                    else:
                        #formacc = GenoViewerAccessionForm2()
                        formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        formref.fields["referential"].queryset = Referential.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                        request.session['projects_acc']=projects_acc
                        projects_or_not = 1
                        message_error='nb loci found: ' + str(nb_locus) + '. Too many loci found: refine the query'
                        return render(request, 'dataview/genotyping_viewer.html', {'projects_or_not':projects_or_not,'dataview':True,'message_error':message_error,'formacc':formacc, 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':formexp, 'formref':formref, 'visumodeform':DataviewGenotypingValuesForm()})
                        #return HttpResponse('nb loci found: ' + str(nb_locus) + '<br> too many loci found, refine the query')
            # niveau frequency
            elif ind_or_freq == ['2']:
                return HttpResponse('Niveau frequency')

            elif ind_or_freq == ['1','2']:
                return HttpResponse('Niveau individu et frequency')
            
            
    else:
        formacc = GenoViewerAccessionForm2()
        formlocus = GenoViewerLocusForm()
        formexp = GenoViewerExperimentForm()
        formref = GenoViewerReferentialForm()
        formchr = GenoViewerChrForm()
        formfile = UploadLocusFileForm()
        formproject = DtviewProjectForm()
        username = None
        if request.user.is_authenticated():
            username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
        formproject.fields['project'].queryset=projects.order_by('name')
        visumodeform = DataviewGenotypingValuesForm()
    projects_or_not = 0

    return render(request,'dataview/genotyping_viewer.html',{'dataview':True,'formproject':formproject,"projects_or_not":projects_or_not})

def uploadFile(files):
    if files != None:
        tabline = []
        i=0
        for line in files:
            tabline.append(line.strip())
            i+=1
        return tabline   

# @login_required
# @user_passes_test(lambda u: u.is_superuser(), login_url='/login/')

def refine_html(request, html, mode):
    v=0
    if mode =="4":
        mode = "Genotyping ID"
    html2=[]
    for i in html:
        if "</tr>" in i or "</th>" in i:
            v=0
        
        if "<th>Referential</th>" in i and mode=="Genotyping ID":
            new_title='<th><center>Referential<br></center></th>'
            html2.append(new_title)
            v=2
        elif "<th>Experiment</th>" in i and mode=="Genotyping ID":
            new_title='<th><center>Experiment<br></center></th>'
            html2.append(new_title)
            v=2
        
#                         if mode == 'Genotyping ID':
        elif "<th>Position</th>" in i or "<th>Chromosome</th>" in i or "<th>Genome Version</th>" in i or "<th>Experiment</th>" in i or "<th>Referential</th>" in i:
            new_th="<th style='background-color:#848484; border:1px solid #848484;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
            html2.append(new_th)
            v=1
        elif v==1: 
            new_td="<td style='background-color:#BDBDBD; border:1px solid #848484;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
            html2.append(new_td)
        elif v==0:
            html2.append(i)
    return html2

def crossViewer_management(request):
    project=[]
    formacc = GenoViewerAccessionForm(request.POST)
    formlocus = GenoViewerLocusForm(request.POST)
    formexp = GenoViewerExperimentForm(request.POST)
    formref=GenoViewerReferentialForm(request.POST) 
    visumodeform = DataviewGenotypingValuesForm(request.POST)
    formtrait = PhenoViewerTraitForm(request.POST)
    formenv = PhenoViewerEnvironmentForm(request.POST)
    formproject=DtviewProjectForm()
    formchr = GenoViewerChrForm(request.POST)
    
    if request.method == 'POST':
        projects_or_not=request.POST['projects_or_not']
        if projects_or_not == '0':
            formacc = GenoViewerAccessionForm2()
            formlocus = GenoViewerLocusForm()
            formexp = GenoViewerExperimentForm()
            formref = GenoViewerReferentialForm()
            formchr = GenoViewerChrForm()
            formfile = UploadLocusFileForm()
            formtrait = PhenoViewerTraitForm()
            formenv = PhenoViewerEnvironmentForm()
            formproject = DtviewProjectForm(request.POST)
            project=request.POST.getlist('project')
            request.session['project_acc']=project
            formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=project).distinct().order_by('name')
            formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=project).distinct().order_by('name')
            formref.fields["referential"].queryset = Referential.objects.filter(projects__in=project).distinct().order_by('name')
            formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=project).distinct().order_by('name')
            formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=project).distinct().order_by('name')
            visumodeform = DataviewGenotypingValuesForm()
            projects_or_not=1
            return render(request,'dataview/crossview_viewer.html',{'dataview':True,'formproject':DtviewProjectForm(),"formacc":formacc,"visumodeform":visumodeform,
                                                                     "formlocus":formlocus,"formexp":formexp,"formref":formref,
                                                                     "formtrait":formtrait,"formenv":formenv,
                                                                     "formchr":formchr,"formfile":formfile,"projects_or_not":projects_or_not,"projects":project})
            
            
        elif projects_or_not == '1':
            if "projects_acc" in request.session:
                projects_acc_post=request.session['projects_acc']
            else:
                projects_acc_post = request.POST['project']
            projects_acc=[]
            for g in projects_acc_post:
                if g.isdigit() or g==',':
                    projects_acc.append(g)
            projects_acc="".join(projects_acc)
            projects_acc=projects_acc.split(',')
            formacc = GenoViewerAccessionForm2(request.POST)
            formexp = GenoViewerExperimentForm(request.POST)
            formref = GenoViewerReferentialForm(request.POST)
            formtrait = PhenoViewerTraitForm(request.POST)
            formenv = PhenoViewerEnvironmentForm(request.POST)
            d1 = datetime.datetime.now()
            if "mode" not in request.POST:
                etape="4"
                if not formexp.is_valid():
                    request.session['projects_acc']=projects_acc
                    formacc = GenoViewerAccessionForm2()
                    formexp = GenoViewerExperimentForm()
                    formtrait = PhenoViewerTraitForm()
                    formenv = PhenoViewerEnvironmentForm()
                    formacc.fields["accession"].queryset = Accession.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    formexp.fields["experiment"].queryset = Experiment.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    formref.fields["referential"].queryset = Referential.objects.filter(projects__in=projects_acc).distinct().order_by('name')
                    formtrait.fields["trait"].queryset = Trait.objects.filter(projects__in=project).distinct().order_by('name')
                    formenv.fields["environment"].queryset = Environment.objects.filter(projects__in=project).distinct().order_by('name')
                    message_error="Please enter some experiment. You may not have any right on the experiments. Please contact a competent person."
                    return render(request,'genotyping/crossview_viewer.html',{'dataview':True,'message_error':message_error,'accesform':formacc, "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':formexp, 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                if not formacc.is_valid():
                    message_error="Please enter some accession. You may not have any right on the accessions. Please contact a competent person."
                    return render(request,'genotyping/crossview_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), 'formchr':GenoViewerChrForm(),"formtrait":formtrait,"formenv":formenv, 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})

                if formacc.is_valid() and formexp.is_valid() and formref.is_valid() and formtrait.is_valid() and formenv.is_valid():
                #cas ou l'on requete sur chromosome, region
                    name = []                
                    if formchr.is_valid():
                        chromosome = formchr.cleaned_data['chromosome']
                        genomeversion = formchr.cleaned_data['genome_version']
                        position_minimale = formchr.cleaned_data['position_minimale']
                        position_maximale = formchr.cleaned_data['position_maximale']
                        name = None
                        for i in [",",";","/"]:
                            if i in genomeversion:
                                genomeversion=genomeversion.split(i)
                            if i in chromosome:
                                chromosome=chromosome.split(i)
                        if type(genomeversion)==str and len(genomeversion)==1: 
                            genomeversion=genomeversion.split()
                        elif type(genomeversion)==list:
                            genomeversion=genomeversion
                        else:
                            message_error="Please use the right delimiters for the genome version."
                            return render(request,'genotyping/crossview_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(),"formtrait":formtrait,"formenv":formenv, 'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                        if type(chromosome)==str: 
                            chromosome=chromosome.split()
                        elif type(chromosome)==list:
                            chromosome=chromosome
                        else:
                            message_error="Please use the right delimiters for the chromosome."
                            return render(request,'genotyping/crossview_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                    #cas ou on choisit une liste de locus
                    elif formlocus.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        name = formlocus.cleaned_data['locus_name']
                    #file upload avec des noms de locus un par ligne
                    elif formfile.is_valid():
                        chromosome = None
                        genomeversion = None
                        position_minimale = None
                        position_maximale = None
                        for line in request.FILES['file']:
                            line = line.rstrip('\n')
                            name.append(line)
                    else:
                        message_error="Please enter some locus values."
                        return render(request,'genotyping/crossview_viewer.html',{'dataview':True,'message_error':message_error,'accesform':GenoViewerAccessionForm(), "formtrait":formtrait,"formenv":formenv,'formchr':GenoViewerChrForm(), 'formlocus':GenoViewerLocusForm(), 'formfile':UploadLocusFileForm(), 'formexp':GenoViewerExperimentForm(), 'formref':GenoViewerReferentialForm(), 'visumodeform':DataviewGenotypingValuesForm()})
                    listaccessions = formacc.cleaned_data['accession']
                    listaccessions_post=[i.name for i in listaccessions]
                    listexperiments = formexp.cleaned_data['experiment']
                    listexperiments_post = [i.name for i in listexperiments]
                    listreferentials = formref.cleaned_data['referential']
                    listreferentials_post = [i.name for i in listreferentials]
                    listtraits = formtrait.cleaned_data["trait"]
                    listtraits_post=[i.name for i in listtraits]
                    listenvs = formenv.cleaned_data["environment"]
                    listenvs_post = [i.name for i in listenvs]
                    mode = request.POST["choose_visualization_mode"]
                    export_or_view_in_web_browser = request.POST['export_or_view_in_web_browser']
                    ind_or_freq = request.POST['ind_or_freq']    
                
            elif "mode" in request.POST:
                etape=request.POST['etape']
                mode=request.POST['mode']
                if mode=="Accession":
                    mode="1"
                elif mode=="Seed lot":
                    mode="2"
                elif mode=="Sample":
                    mode="3"
                elif mode=="Genotyping ID":
                    mode="4"
                ind_or_freq=request.POST["ind_or_freq"]
                export_or_view_in_web_browser=request.POST["export_or_view_in_web_browser"]
                chromosome_post=request.POST["chromosome"]
                chromosome=[]
                for c in chromosome_post:
                    if c.isdigit() or c==",":
                        chromosome.append(c)
                chromosome=''.join(chromosome)
                chromosome=chromosome.split(',')
                position_minimale=request.POST["position_minimale"]
                position_minimale=int(position_minimale)
                position_maximale=request.POST["position_maximale"]
                position_maximale=int(position_maximale)        
                genomeversion_post=request.POST["genomeversion"]
                genomeversion=[]
                for g in genomeversion_post:
                    if g.isdigit() or g==',':
                        genomeversion.append(g)
                genomeversion="".join(genomeversion)
                genomeversion=genomeversion.split(',')

                listaccessions_post=request.POST["listaccessions"]
                listaccessions=listaccessions_post.strip('[').strip(']').split("'")
                listexperiments_post=request.POST["listexperiments"]
                listexperiments=listexperiments_post.strip('[').strip(']').split("'")
                listreferentials_post=request.POST["listreferentials"]
                listreferentials=listreferentials_post.strip('[').strip(']').split("'")
                listtraits_post=request.POST["listtraits"]
                listtraits=listtraits_post.strip('[').strip(']').split("'")
                listenvs_post=request.POST['listenvs']
                listenvs=listenvs_post.strip('[').strip(']').split("'")
                
                for i in ['','u',', u']:
                    for j in listaccessions:
                        if i==j:
                            listaccessions.remove(j)
                    for j in listexperiments:
                        if i==j:
                            listexperiments.remove(j)
                    for j in listreferentials:
                        if i==j:
                            listreferentials.remove(j)  
                    for j in listtraits:
                        if i==j:
                            listtraits.remove(j)  
                    for j in listenvs:
                        if i==j:
                            listenvs.remove(j)        
                            
                listaccessions=Accession.objects.filter(name__in=listaccessions)
                name=request.POST["name"]
                listexperiments=Experiment.objects.filter(name__in=listexperiments)
                listreferentials=Referential.objects.filter(name__in=listreferentials)
                listtraits=Trait.objects.filter(name__in=listtraits)
                listenvs=Environment.objects.filter(name__in=listenvs)
                 
            # niveau individu
            if ind_or_freq == ['1'] or ind_or_freq=="[u'1']" or ind_or_freq=="1":
                # export CSV
                if export_or_view_in_web_browser == '1':
                    export='export_csv'
                    df_geno, nb_locus, nb_marqueurs = getGenodata(export, etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode)
                    
                    void = False
                    if mode == '1':
                        mode = 'Accession'
                        headername_pheno = "Accession_pheno"
                    elif mode == '2':
                        mode = 'Seed lot'
                        headername_pheno = "Seedlot_pheno"
                    elif mode == '3':
                        mode = 'Sample'
                        headername_pheno = "Sample_pheno"
                    elif mode == '4':
                        mode = 'Genotyping ID'
                        headername_pheno = "Geno_pheno"
                    
                    df_pheno = getPhenodata(listaccessions, listtraits, listenvs, headername_pheno)
                    message_error='nb loci found: ' + str(nb_locus) + '<br> too many loci found, refine the query' 
                            
                    if not df_pheno.empty and not df_geno[3:].empty:
                        df = pd.concat([df_geno,df_pheno], axis=1, join_axes=[df_geno.index])
                        df=df.fillna(value="-")
                    elif not df_geno[3:].empty:
                        df=df_geno
                    elif not df_pheno.empty:
                        df=df_pheno
                    elif df_pheno.empty and df_geno[3:].empty:
                        df=df_geno
                        no_values=1
                        
                    if df is not None:
                        df.to_csv('/tmp/view_crossview.csv')
                        if df.empty or no_values:
                            # envoi du variable au template pour dire qu'il n'y a pas de donnes
                            void = True
                        return render(request, 'dataview/dtviewcrossviewvalues_csv.html', {'dataview':True,'void':void, 'mode':mode})
                    else:
                        return render(request, 'dataview/dtviewcrossviewvalues.html', {'dataview':True,'message_error':message_error})
                else:
                    export="table_web"
                    df_geno, nb_locus, nb_marqueurs = getGenodata(export,etape, projects_acc,chromosome, position_minimale, position_maximale, genomeversion, listaccessions, name, listexperiments, listreferentials, mode)
                    no_values=0
                    if mode == '1':
                        mode = 'Accession'
                        headername_pheno = "Accession_pheno"
                    elif mode == '2':
                        mode = 'Seed lot'
                        headername_pheno = "Seedlot_pheno"
                    elif mode == '3':
                        mode = 'Sample'
                        headername_pheno = "Sample_pheno"
                    elif mode == '4':
                        mode = 'Genotyping ID'
                        headername_pheno = "Geno_pheno"

                    df_pheno = getPhenodata(listaccessions, listtraits, listenvs, headername_pheno) 
                    message_error='nb loci found: ' + str(nb_locus) + '<br> too many loci found, refine the query'
                    
                    if not df_pheno.empty and not df_geno[3:].empty:
                        df = pd.concat([df_geno,df_pheno], axis=1, join_axes=[df_geno.index])
                    elif not df_geno[3:].empty:
                        df=df_geno
                    elif not df_pheno.empty:
                        df=df_pheno
                    elif df_pheno.empty and df_geno[3:].empty:
                        df=df_geno
                        no_values=1
                    df=df.dropna(axis=1, how='all')
                    df=df.fillna(value="-")
                    if df is not None:
                        df.to_csv('/tmp/view_crossview.csv')
                        html = df.to_html(classes=["tablesorter"])    
                        html = html.split('\n')
                        v=0
                        count=0
                        count_geno_pheno=0
                        dico_geno_or_pheno={}
                        html2=[]
                        for i in html:
                            count_check=0
                            if "table" in i or "tbody" in i or "thead" in i or "<tr" in i:
                                count_geno_pheno=count_geno_pheno
                            else:
                                count_geno_pheno=count_geno_pheno+1 
                            if "</tr>" in i:
                                v=0
                                count_geno_pheno=0
                                count=0
                            if "<tr>" in i:
                                count=0
                            elif "<td>" in i:
                                count=count+1
                            if "<th>" in i and "</th>" not in i:
                                count_geno_pheno=count_geno_pheno-1
                                dico_geno_or_pheno[str(i)+"_"+str(count_geno_pheno)]=count_geno_pheno
                            else:
                                for key, value in dico_geno_or_pheno.items():
                                    if (count_geno_pheno-1)==value and "</th>" not in i:
                                        count_check=1

                                #gris
                            if mode=="Genotyping ID":
                                if "<th>Position</th>" in i or "<th>Chromosome</th>" in i or "<th>Genome Version</th>" in i:
                                    new_th="<th style='background-color:#D8D8D8; border:1px solid #A4A4A4;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
                                    html2.append(new_th)
                                    v=1
                                elif "<th>Accession</th>" in i or "<th>SeedLot</th>" in i or "<th>Sample</th>" in i or "<th>Experiment</th>" in i or "<th>Referential</th>" in i:
                                    new_th="<th style='background-color:#D8D8D8; border:1px solid #A4A4A4;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
                                    html2.append(new_th)
                                elif v==1 or count in range(1,6): 
                                    #gris
                                    if df_geno[3:].empty:
                                        new_td="<td style='background-color:#F5ECCE; border:1px solid #F7BE81;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                        html2.append(new_td)
                                    else: 
                                        new_td="<td style='background-color:#E6E6E6; border:1px solid #BDBDBD'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                        html2.append(new_td)
                                elif count_check==1:
                                    #orange pale/grisé
                                    new_td="<td style='background-color:#F5ECCE; border:1px solid #F7BE81;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                    html2.append(new_td)
                                else:
                                    html2.append(i)
                            
                            else:    
                                if "<th>Position</th>" in i or "<th>Chromosome</th>" in i or "<th>Genome Version</th>" in i or "<th>Experiment</th>" in i or "<th>Referential</th>" in i:
                                    new_th="<th style='background-color:#D8D8D8; border:1px solid #A4A4A4;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
                                    html2.append(new_th)
                                    v=1
                                elif "<th>Accession</th>" in i or "<th>SeedLot</th>" in i or "<th>Sample</th>" in i :
                                    new_th="<th style='background-color:#D8D8D8; border:1px solid #A4A4A4;'>"+str(i.replace("<th>","").replace("</th>",""))+"</th>"
                                    html2.append(new_th)
                                elif v==1 : 
                                    #gris
                                    if df_geno.empty:
                                        new_td="<td style='background-color:#F5ECCE; border:1px solid #F7BE81;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                        html2.append(new_td)
                                    else: 
                                        new_td="<td style='background-color:#E6E6E6; border:1px solid #BDBDBD'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                        html2.append(new_td)
                                elif count_check==1:
                                    #orange pale/grisé
                                    new_td="<td style='background-color:#F5ECCE; border:1px solid #F7BE81;'>"+str(i.replace("<td>","").replace("</td>",""))+"</td>"
                                    html2.append(new_td)
                                else:
                                    html2.append(i)

                        if df.empty:
                            html = None
                            html2 = None
                        d2 = datetime.datetime.now()
                        etape=1
                        if mode=="Accession" or mode=="Seed lot" or mode=="Sample":
                            type_table=1
                        elif mode=="Genotyping ID":
                            type_table=0
                        if no_values==1:
                            html2=None
                        dico_genovalue={'mode':mode,
                                "ind_or_freq":ind_or_freq,
                                "export_or_view_in_web_browser":export_or_view_in_web_browser,
                                "chromosome":chromosome,
                                "position_minimale":position_minimale,
                                "position_maximale":position_maximale,
                                "genomeversion":genomeversion,
                                "listaccessions":listaccessions_post,
                                "name":name,
                                "html":html2,
                                "listexperiments":listexperiments_post,
                                "listreferentials":listreferentials_post,
                                "type_table":type_table,
                                "projects_or_not":projects_or_not,
                                "project":projects_acc,
                                "listtraits":listtraits_post,
                                "listenvs":listenvs_post,
                                "nb_marqueurs":nb_marqueurs,
                                'dataview':True,
                                }
                        return render(request, 'dataview/dtviewcrossviewvalues.html', dico_genovalue)
                        
                    else:
                        return render(request, 'dataview/dtviewcrossviewvalues.html', {'dataview':True,'message_error':message_error})
            # niveau frequency
            elif ind_or_freq == ['2']:
                return HttpResponse('Niveau frequency')

            elif ind_or_freq == ['1','2']:
                return HttpResponse(message_error)
            
            
    else:
        formacc = GenoViewerAccessionForm2()
        formlocus = GenoViewerLocusForm()
        formexp = GenoViewerExperimentForm()
        formref = GenoViewerReferentialForm()
        formchr = GenoViewerChrForm()
        formfile = UploadLocusFileForm()
        formproject = DtviewProjectForm()
        formtrait = PhenoViewerTraitForm()
        formenv = PhenoViewerEnvironmentForm()
        username = None
        if request.user.is_authenticated():
            username = request.user.login
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
        formproject.fields['project'].queryset=projects.order_by('name')
        visumodeform = DataviewGenotypingValuesForm()
    projects_or_not = 0

    return render(request,'dataview/crossview_viewer.html',{'dataview':True,'formproject':formproject,"projects_or_not":projects_or_not})

def data_card(request):
    username = None
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    template="dataview/data_card.html"
# <<<<<<< .working
#     if request.method=="GET":    
#         if "trait" in request.GET:
# =======
    dico_search = {}
    if request.method=="GET":  
        if "search" in request.GET:
            dico_search[request.GET['type_data']]=request.GET['search']
            print(dico_search) 
            
        if 'classification' in request.GET:
            structure = request.GET['classification']
            type_data = 'structure'
              
            try:
                classification = Classification.objects.get(name=structure)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("structure",structure)})
            
            if classification.projects.filter(id__in=projects):
                type_data = "Structure"
                seedlot_list=[]
                classes = Classe.objects.filter(classification=classification)
                for classe in classes:
                    classe_values = ClasseValue.objects.filter(classe=classe)
                    for classe_value in classe_values:
                        seedlot_list.append(classe_value.seedlot)
                        
                projects_list=classification.projects.filter()
                classif_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        classif_projects.append(i.name)
                 
                dico={"name":classification.name,
                      "id_classification":classification.id,
                      "classif_name":classification.name,
                      "classif_date":classification.date,
                      "classif_reference":classification.reference,
                      "classif_method":classification.method,
                      "classif_person":classification.person,
                      "classif_seedlots":set(seedlot_list),
                      "classif_projects":classif_projects,
                      'dataview':True,
                      "type_data":type_data,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the structure {0}. Please contact a data administrator via the contact page.".format(structure)})
                          
        if 'genealogy' in request.GET:
            genealogy_child = request.GET['genealogy']
            parent = request.GET['parent']
            type_data = 'Genealogy'

            if parent == '': 
                #n'a pas de parent male donc est le resultat d'une multiplication (forcement seed lot)
                try:
                    child_entity = Seedlot.objects.get(name=genealogy_child)
                except:
                    return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("child",genealogy_child)})
            else:
                try :
                    child_entity = Accession.objects.get(name=genealogy_child)
                    parent_entity = Accession.objects.get(name=parent)
                    relation = AccessionRelation.objects.filter(child_id=child_entity.id,parent_id=parent_entity.id)
                except:
                    try:
                        child_entity = Seedlot.objects.get(name=genealogy_child)
                        parent_entity = Seedlot.objects.get(name=parent)
                        relation = SeedlotRelation.objects.filter(child_id=child_entity.id,parent_id=parent_entity.id)
                    except:
                        return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("child",genealogy_child)})
            
            print(child_entity.__class__)
            if child_entity.__class__ == Seedlot:
                for i in SeedlotRelation.objects.filter(child=child_entity):
                    method = i.reproduction.reproduction_method.name
                    site = i.site
                    start_date = i.reproduction.start_date
                    end_date = i.reproduction.end_date
                    comments = i.reproduction.description
                    synonymous = None
                    first_production = None
                    if i.parent_gender == "X":
                        model = "Seedlot multiplication"
                        parent = i.parent.name
                        parent_female = None
                        parent_male = None
                    else:
                        model = "Seedlot cross"
                        parent = None
                        if i.parent_gender == "F":
                            parent_female = i.parent.name
                        else:
                            parent_male = i.parent.name
            else:
                for i in AccessionRelation.objects.filter(child=child_entity):
                    model = 'Accession cross'
                    method = i.reproduction.reproduction_method.name 
                    synonymous = i.synonymous
                    if not synonymous:
                        synonymous =" "
                    first_production = i.first_production
                    comments = i.reproduction.description
                    start_date = None
                    end_date = None
                    parent = None
                    if i.parent_gender == "F":
                        parent_female = i.parent.name 
                    else:
                        parent_male = i.parent.name
                
            dico={"name_genealogy":child_entity.name,
                  "name":child_entity.name,
                  'type_data':type_data,
                      "parent_female":parent_female,
                      "parent_male":parent_male,
                      "model":model,
                      "method":method,
                      "synonymous":synonymous,
                      "first_production":first_production,
                      "comments":comments,
                      "start_date":start_date,
                      "end_date":end_date,
                      "parent":parent,
                      'dataview':True,
                      }
                
        elif "trait" in request.GET:
# >>>>>>> .merge-right.r614
            trait_pheno=request.GET["trait"]
            type_data="trait"
            try:
                trait=Trait.objects.get(name=trait_pheno)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("trait",trait_pheno)})
            
            if trait.projects.filter(id__in=projects):
                type_data="Trait"
                trait_abbreviation=trait.abbreviation
                trait_measure_unit=trait.measure_unit
                trait_comments=trait.comments
                projects_list=trait.projects.filter()
                trait_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        trait_projects.append(i.name)
                
                env_id = [pheno_val.environment_id for pheno_val in PhenotypicValue.objects.filter(trait=trait).distinct()]
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(trait=trait).distinct()]
                envlist = Environment.objects.filter(id__in=env_id).distinct()
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                
                dico={"name":trait_pheno,
                      "name_trait":trait_pheno,
                      "trait_abbreviation":trait_abbreviation,
                      "trait_measure_unit":trait_measure_unit,
                      "trait_comments":trait_comments,
                      "trait_projects":trait_projects,
                      "type_data":type_data,
                      "env_list":envlist,
                      "seedlot_list":seedlotlist,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the trait {0}. Please contact a data administrator via the contact page.".format(trait_pheno)})
            
        if "environment" in request.GET:
            environment_pheno=request.GET["environment"]
            type_data="environment"
            try:
                environment=Environment.objects.get(name=environment_pheno)
            except:
                return render(request, template, {'error':"ERROR! The {0} {1} doesn't exist.".format("environment",environment_pheno)})
            
            if environment.projects.filter(id__in=projects):
                type_data="Environment"
                environment_date=environment.date
                environment_location=environment.location
                environment_description=environment.description
                environment_specific_treatment=environment.specific_treatment
                projects_list=environment.projects.filter()
                environment_projects=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        environment_projects.append(i.name)
                        
                trait_id = [pheno_val.trait_id for pheno_val in PhenotypicValue.objects.filter(environment=environment).distinct()]
                sl_id = [pheno_val.seedlot_id for pheno_val in PhenotypicValue.objects.filter(environment=environment).distinct()]
                traitlist = Trait.objects.filter(id__in=trait_id).distinct()
                seedlotlist = Seedlot.objects.filter(id__in=sl_id).distinct()
                 
                dico={"name":environment_pheno,
                      "name_environment":environment_pheno,
                      "environment_date":environment_date,
                      "environment_location":environment_location,
                      "environment_description":environment_description,
                      "environment_specific_treatment":environment_specific_treatment,
                      "type_data":type_data,
                      "environment_projects":environment_projects,
                      "trait_list":traitlist,
                      "seedlot_list":seedlotlist,
                      'dataview':True,
                      "type_data":type_data,
                      }
                
                #Ici ajout des images, dans le même principe que la Map
                try:
                    env = Environment.objects.get(name=environment_pheno)
                    
                    #Liste des links et des images
                    images_link = ImageLink.objects.filter(entity_name=env.name)
                    images = []
                    for image_link in images_link:
                        if image_link.content_type.name == "environment":
                            images.append(image_link.image)

                    dico.update({"images":images})
                    dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                except:
                    pass;
                
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the environment {0}. Please contact a data administrator via the contact page.".format(environment_pheno)})
            
        if "genoid" in request.GET or "genoid" in dico_search:      
            try:          
                genotypingid=request.GET["genoid"]
                referential=request.GET["referential"]
            except:
                genotypingid = dico_search['genoid']
                referential=request.GET["referential"]
            type_data="GenotypingID"
            try:
# <<<<<<< .working
#                 genoid=GenotypingID.objects.get(id=genotypingid)
# =======
                genoid=GenotypingID.objects.get(name__iexact=genotypingid, referential=Referential.objects.get(name=referential))
# >>>>>>> .merge-right.r614
            except:
                genoid=GenotypingID.objects.filter(name__icontains=genotypingid, referential=Referential.objects.get(name=referential))
                if len(genoid) == 1:
                    genoid = GenotypingID.objects.get(name__icontains=genotypingid, referential=Referential.objects.get(name=referential))
                elif len(genoid) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("genotypingid",genotypingid)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("genotypingid",genotypingid)})
            
            if genoid.projects.filter(id__in=projects):
                sample_genoid=genoid.sample
                seedlot_genoid = sample_genoid.seedlot
                accession_genoid = seedlot_genoid.accession
                experiment_genoid=genoid.experiment
                referential_genoid=genoid.referential
                sentrixbarcode_a_genoid=genoid.sentrixbarcode_a
                sentrixposition_a_genoid=genoid.sentrixposition_a
                funding_genoid=genoid.funding
                projects_list=genoid.projects.filter()
                projects_genoid=[]
                projects_id=[]
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_genoid.append(i.name)
                dico={"type_data":type_data,
# <<<<<<< .working
#                       "name":GenotypingID.objects.get(id=genotypingid).name,
#                       "name_genoid":GenotypingID.objects.get(id=genotypingid).name,
# =======
                      "name":genoid.name,
                      "name_genoid":genoid.name,
#>>>>>>> .merge-right.r614
                      "sample_genoid":sample_genoid,
                      "seedlot_genoid":seedlot_genoid,
                      "accession_genoid":accession_genoid,
                      "experiment_genoid":experiment_genoid,
                      "referential_genoid":referential_genoid,
                      "sentrixbarcode_a_genoid":sentrixbarcode_a_genoid,
                      "sentrixposition_a_genoid":sentrixposition_a_genoid,
                      "funding_genoid":funding_genoid,
                      "projects_genoid":projects_genoid,
                      "dataview":True,}
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the genotypingID {0}. Please contact a data administrator via the contact page.".format(genotypingid)})
             
        if "accession" in request.GET or "accession" in dico_search:
            try:
                accession=request.GET["accession"]
            except:
                accession=dico_search['accession']
            type_data="Accession"
            try:
                acc=Accession.objects.get(name__iexact=accession)
            except:
# <<<<<<< .working
#                 return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("accession",accession)}) 
#             
# =======
                acc=Accession.objects.filter(name__icontains=accession)
                if len(acc) == 1:
                    acc = Accession.objects.get(name__icontains=accession)
                elif len(acc) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("accession",accession)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("accession",accession)})             
# >>>>>>> .merge-right.r614
            if acc.projects.filter(id__in=projects):
                type_acc = acc.type
                name = acc.name
                doi = acc.doi
                private_doi = acc.private_doi
                pedigree = acc.pedigree
                description = acc.description
                attributes = acc.attributes_values
                if type(attributes) != dict:
                    attributes=ast.literal_eval(attributes)
                
                attributes = list(attributes.items())
                attributes2=[]
                for i, j in sorted(attributes):
                    for nb in range(len(attributes)):
                        if i==attributes[nb][0]:
                            attributes2.append((AccessionTypeAttribute.objects.get(id=i).attribute_name,j))
    #                 images = acc.images
                projects_list=acc.projects.filter()
                projects_acc=[]
                projects_id=[]
                
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_acc.append(i.name)
                
                ##seedlots à montrer dans la fiche
                seedlots = set(Seedlot.objects.filter(accession=acc, projects__in=projects_id))
                seedlot_list=[]
                classification_list=[]
                for seedlot in seedlots:
                    seedlot_list.append(seedlot.name)
                    #Recherche de classification associee aux seed lots
                    classe_value = ClasseValue.objects.filter(seedlot=seedlot)
                    for value in classe_value:
                        classification_list.append(value.classe.classification)
                
                ##sample dans la fiche
                samples = set(Sample.objects.filter(seedlot__in=seedlots, projects__in=projects_id))
                sample_list=[]
                for sample in samples:
                    sample_list.append(sample.name)
                
                
                ##genoid dans la fiche
                genoids = set(GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id))
                genoid_list=[]
                experiment_list=[]
                for genoid in genoids:
                    genoid_list.append((genoid.name,genoid.referential))
                    experiment_list.append(genoid.experiment)
                
                try:    
                    parent_male_accession = AccessionRelation.objects.get(parent_gender = "M", child=acc).parent
                    parent_female_accession = AccessionRelation.objects.get(parent_gender = "F", child=acc).parent
                except ObjectDoesNotExist:
                    parent_male_accession = ""
                    parent_female_accession = ""
                
                dico={'name':acc.name,
                      'name_accession':acc.name,
                      'doi_acc':doi,
                      'private_doi':private_doi,
                      'pedigree_acc':pedigree,
                      'description_acc':description,
                      'attributes_acc':attributes2,
                      'projects_acc':projects_acc,
                      'seedlots_acc':seedlot_list,
                      'samples_acc':sample_list,
                      'genoid_acc':genoid_list,
                      'experiment_acc':set(experiment_list),
                      'classification_acc':set(classification_list),
                      "type_data":type_data,
                      'dataview':True,
                      "parent_male_accession":parent_male_accession,
                      "parent_female_accession":parent_female_accession,}
                                
                #Ajout Arthur : Affichage de la map dans la fiche des accessions (si possible)             
                try:
                    #Get accession
                    """
                    #Get accession type
                    acc_type = acc.type
                    #Get the attributes
                    latitude = acc.attributes_values[str(AccessionTypeAttribute.objects.get(attribute_name='latitude', accession_type=acc_type).id)]
                    longitude = acc.attributes_values[str(AccessionTypeAttribute.objects.get(attribute_name='longitude', accession_type=acc_type).id)]
                    latitude=latitude.replace(",",".")
                    longitude=longitude.replace(",",".")
                    """
                    latitude=acc.latitude
                    longitude=acc.longitude
                
                    dico.update({"latitude":latitude, "longitude":longitude, "acc":acc})
                except:
                    pass;
                
                
                #Ici ajout des images, dans le même principe que la Map
                try:                    
                    #Liste des links et des images
                    images_link = ImageLink.objects.filter(entity_name=acc.name)
                    images = []
                    for image_link in images_link:
                        if image_link.content_type.name == "accession":
                            images.append(image_link.image)

                    dico.update({"images":images})
                    dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                except:
                    pass;
                
                
                
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the accession {0}. Please contact a data administrator via the contact page.".format(accession)})

        if "experiment" in request.GET:
            experiment=request.GET["experiment"]
            type_data="Experiment"
            try:
                exp=Experiment.objects.get(name=experiment)
            except:
                return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("experiment",experiment)})    
                
            if exp.projects.filter(id__in=projects):
                institution_experiment=exp.institution
                person_experiment=exp.person
                date_experiment=exp.date
                comments_experiment=exp.comments
                projects_list=exp.projects.filter()
                projects_experiment=[]
                projects_id=[]
            
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_experiment.append(i.name)
#                 genoid=set(GenotypingID.objects.filter(experiment=exp, projects__in=projects_id))
#                 genoid_list=[]
#                 for i in genoid:
#                     genoid_list.append(i.name)
#                 genoid_experiment=', '.join(genoid_list)
                
                genoids = set(GenotypingID.objects.filter(experiment=exp, projects__in=projects_id))
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append((genoid.name,genoid.referential))
                
                dico={"name":experiment,
                      "name_experiment":experiment,
                      "institution_experiment":institution_experiment,
                      "person_experiment": person_experiment,
                      "date_experiment":date_experiment,
                      "comments_experiment":comments_experiment,
                      "projects_experiment":projects_experiment,
                      #"genoid_experiment":genoid_experiment,
                      "genoid_experiment":genoid_list,
                      "type_data":type_data,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the experiment {0}. Please contact a data administrator via the contact page.".format(experiment)})

# <<<<<<< .working
#         if "referential" in request.GET:
#             referential=request.GET["referential"]
#             type_data="Referential"
#             try:
#                 ref=Referential.objects.get(name=referential)
#             except:
#                 return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("referential",referential)})    
#                 
#             if ref.projects.filter(id__in=projects):
#                 person_referential=ref.person
#                 date_referential=ref.date
#                 comments_referential=ref.comments
#                 projects_list=ref.projects.filter()
#                 projects_referential=[]
#                 projects_id=[]
#                 
#                 for i in projects_list:
#                     if i in projects:
#                         projects_id.append(i.id)
#                         projects_referential.append(i.name)
#                 genoid=set(GenotypingID.objects.filter(referential=ref, projects__in=projects_id))
#                 genoid_list=[]
#                 for i in genoid:
#                     genoid_list.append(i.name)
#                 genoid_referential=', '.join(genoid_list)
#                 dico={"name":referential,
#                       "name_referential":referential,
#                       "person_referential": person_referential,
#                       "date_referential":date_referential,
#                       "comments_referential":comments_referential,
#                       "projects_referential":projects_referential,
#                       "genoid_referential":genoid_referential,
#                       "type_data":type_data,
#                       'dataview':True,
#                       }
#             else:
#                 return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the referential {0}. Please contact a data administrator via the contact page.".format(referential)})
# 
# 
#         if "seedlot" in request.GET:
#             seedlot=request.GET['seedlot']
# =======
        if "referential" in request.GET and "genoid" not in request.GET:
            referential=request.GET["referential"]
            type_data="Referential"
            try:
                ref=Referential.objects.get(name=referential)
            except:
                return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("referential",referential)})    
                
            if ref.projects.filter(id__in=projects):
                person_referential=ref.person
                date_referential=ref.date
                comments_referential=ref.comments
                projects_list=ref.projects.filter()
                projects_referential=[]
                projects_id=[]
                
                for i in projects_list:
                    if i in projects:
                        projects_id.append(i.id)
                        projects_referential.append(i.name)
#                 genoid=set(GenotypingID.objects.filter(referential=ref, projects__in=projects_id))
#                 genoid_list=[]
                
                genoids = set(GenotypingID.objects.filter(referential=ref, projects__in=projects_id))
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append((genoid.name,genoid.referential))
                
                
#                 for i in genoid:
#                     genoid_list.append(i.name)
#                 genoid_referential=', '.join(genoid_list)
                dico={"name":referential,
                      "name_referential":referential,
                      "person_referential": person_referential,
                      "date_referential":date_referential,
                      "comments_referential":comments_referential,
                      "projects_referential":projects_referential,
                      #"genoid_referential":genoid_referential,
                      "genoid_referential":genoid_list,
                      "type_data":type_data,
                      'dataview':True,
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the referential {0}. Please contact a data administrator via the contact page.".format(referential)})


        if "seedlot" in request.GET or "seedlot" in dico_search:
            try:
                seedlot=request.GET['seedlot']
            except:
                seedlot = dico_search['seedlot']
# >>>>>>> .merge-right.r614
            type_data="Seedlot"
            try:
                sl=Seedlot.objects.get(name__iexact=seedlot)
            except:
                sl=Seedlot.objects.filter(name__icontains=seedlot)
                if len(sl) == 1:
                    sl = Seedlot.objects.get(name__icontains=seedlot)
                elif len(sl) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("seedlot",seedlot)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("seedlot",seedlot)}) 
            if sl.projects.filter(id__in=projects):
                accession_sl=sl.accession.name
                type_sl=sl.type.name
                is_obsolete_sl=sl.is_obsolete
                description_sl=sl.description
                attributes=sl.attributes_values
                if type(attributes) != dict:
                    attributes=ast.literal_eval(attributes) ##conversion de la chaine de char en dictionnaire
                
                attributes = list(attributes.items())
                attributes2=[]
                for i, j in sorted(attributes):
                    for nb in range(len(attributes)):
                        if i==attributes[nb][0]:
                            attributes2.append((SeedlotTypeAttribute.objects.get(id=i).attribute_name,j))

                projects_seedlot=sl.projects.filter()
                projects_sl=[]
                projects_id=[]
                for i in projects:
                    if i in projects_seedlot:
                        projects_id.append(i.id)
                        projects_sl.append(i.name)
                samples = Sample.objects.filter(seedlot=sl, projects__in=projects_id)
#                 genoids = GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id)
                
                genoids = set(GenotypingID.objects.filter(sample__in=samples, projects__in=projects_id))
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append((genoid.name,genoid.referential))
                
                pheno_values = PhenotypicValue.objects.filter(seedlot=sl)
                
                environment_list = []
                trait_list = []
                for value in pheno_values:
                    environment_list.append(value.environment)
                    trait_list.append(value.trait)
                
                #On ajoute les classifications
                classification_list=[]
                classe_value = ClasseValue.objects.filter(seedlot=sl)
                for value in classe_value:
                    classification_list.append(value.classe.classification)
                
                try:
                    parent_male_sl = SeedlotRelation.objects.get(parent_gender = "M", child=sl).parent
                    parent_female_sl = SeedlotRelation.objects.get(parent_gender = "F", child=sl).parent
                    parent_x_sl = ""
                except:
                    try:
                        parent_male_sl = ""
                        parent_female_sl = ""
                        parent_x_sl = SeedlotRelation.objects.get(parent_gender = "X", child = sl).parent
                    except ObjectDoesNotExist:
                        parent_x_sl = ""
                
                dico={"name":sl.name,
                      "name_sl":sl.name,
                      "accession_sl":accession_sl,
                      "type_sl": type_sl,
                      "is_obsolete_sl":is_obsolete_sl,
                      "description_sl":description_sl,
                      "attributes_sl":attributes2,
                      "projects_sl":projects_sl,
                      "environment_sl":set(environment_list),
                      "trait_sl":set(trait_list),
                      "samples":set(samples),
                      #"genoid":set(genoids),
                      "genoid_list":genoid_list,
                      'classification_sl':set(classification_list),
                      "type_data":type_data,
                      'dataview':True,
                      'parent_male_sl':parent_male_sl,
                      'parent_female_sl':parent_female_sl,
                      'parent_x_sl':parent_x_sl
                      }
                
                
                
                                
                #Ici ajout des images, dans le même principe que la Map
                try:
                    seed = Seedlot.objects.get(name=seedlot)

                    #Liste des links et des images
                    images_link = ImageLink.objects.filter(entity_name=seed.name)
                    images = []
                    for image_link in images_link:
                        if image_link.content_type.name == "seedlot":
                            images.append(image_link.image)

                    dico.update({"images":images})
                    dico.update({"MEDIA_ROOT":settings.MEDIA_ROOT})
                except:
                    pass;
                
                
                
            else:
                return render(request, template,{'dataview':True,"error":"ERROR! You don't have any right on the seedlot {0}. Please contact a data administrator via the contact page.".format(seedlot)})
            
        if "sample" in request.GET or "sample" in dico_search:
            try:
                sample=request.GET['sample']
            except:
                sample = dico_search['sample']   
            type_data = "Sample"
            try:
                sple=Sample.objects.get(name__iexact=sample)
            except:
                sple=Sample.objects.filter(name__icontains=sample)
                if len(sple) == 1:
                    sple = Sample.objects.get(name__icontains=sample)
                elif len(sple) > 1:
                    return render(request, template,{"error":"The search for the {0} {1} returned more than one result. Please, refine your search".format("sample",sample)})
                else:
                    return render(request, template,{"error":"ERROR! The {0} {1} doesn't exist.".format("sample",sample)}) 
            if sple.projects.filter(id__in=projects):
                seedlot_sample = sple.seedlot.name
                accession_sample = sple.seedlot.accession.name
                description_sample = sple.description
                is_bulk_sample = sple.is_bulk
                is_obsolete_sample = sple.is_obsolete
                codelabo_sample = sple.codelabo
                tubename_sample = sple.tubename
                projects_sple = sple.projects.filter()
                projects_sample = []
                projects_id = []
                for i in projects:
                    if i in projects_sple:
                        projects_id.append(i.id)
                        projects_sample.append(i.name)

                genoids = set(GenotypingID.objects.filter(sample=sple, projects__in=projects_id))
                genoid_list=[]
                experiment_list=[]
                for genoid in genoids:
                    genoid_list.append((genoid.name,genoid.referential))
                    experiment_list.append(genoid.experiment)
                
                dico={"name":sple.name,
                      "name_sample":sple.name,
                      "seedlot_sample":seedlot_sample,
                      "accession_sample":accession_sample,
                      "description_sample":description_sample,
                      "is_bulk_sample":is_bulk_sample,
                      "is_obsolete_sample":is_obsolete_sample,
                      "codelabo_sample":codelabo_sample,
                      "tubename_sample":tubename_sample,
                      "projects_sample":projects_sample,
                      "genoid_list":genoid_list,
                      "experiment_list":set(experiment_list), 
                      "type_data":type_data,  
                      'dataview':True,              
                      }
            else:
                return render(request, template,{'dataview':True, "error":"ERROR! You don't have any right on the sample {0}. Please contact a data administrator via the contact page.".format(sample)})
        
        #Check if user is team manager, for images
        team_admin = request.user.is_team_admin
        if team_admin:
            dico.update({"team_admin":team_admin})

        return render(request, template, dico)

def structure_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Structure viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="classification"
    projects_or_not=1
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['structure', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            structures=_get_structures(request, list_projects)
            labels = ['Structure','Description', 'Project']
            dico={}
            for i in structures:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,i.description,', '.join(projects_to_show)]
            if dico:
                structures_all=Classification.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(structures_all)
                for i in structures_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,i.description,', '.join(projects_to_show)]    
                getPandasDf("/tmp/dtview_structure.csv",*labels, **dico_all)    
            dico=dico.items()
            projects_or_not=1
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":structures,
                          'projects_or_not':projects_or_not,
                          "structure":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            return render(request, template, tag_fields)        
                    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            struct_name_list = request.session["struct_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "structure":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10                
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "structure":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "structure":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})        
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=struct_name_list)   
            structures = Classification.objects.filter(query).distinct() 
            number_all = len(structures)
            list_exceptions = ['date','reference','method','person','classe']
            download_not_empty = False
            if structures:
                download_not_empty = write_csv_search_result(structures, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})  
            
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            structures=_get_structures(request, list_projects)     
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            structures=_get_structures(request, list_projects)
            struct_name_list=[]
            for i in structures : 
                struct_name_list.append(i.name)
            request.session["struct_name_list"] = struct_name_list
            
        if projects_or_not==1:
            labels = ['Structure','Description', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in structures:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.description,', '.join(projects_to_show)] 
            if dico:
                structures_all=Classification.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(structures_all)
                for i in structures_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.description,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_structure.csv",*labels, **dico_all)  
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":structures,
                          'projects_or_not':projects_or_not,
                          "structure":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
# <<<<<<< .working
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  
            
            
            
                    
def _get_structures(request, list_projects):
    structures=Classification.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(structures,50)
    page = request.GET.get('page')
    try:
        structures = paginator.page(page)
    except PageNotAnInteger:
        structures = paginator.page(1)
    except EmptyPage:
        structures = paginator.page(paginator.num_pages)
    return structures

def accession_viewer(request):
    username = None
    or_and = None
    if request.user.is_authenticated():
        username = request.user.login
    if "return" not in request.session:
        request.session['return']="no"
    formfiche=FicheForm()     
    list_attributes=[]
    for i in ['accession', 'type', 'pedigree', 'description', 'projects']:
        list_attributes.append(i)
    number_all=0
    tag_fields = {}
    if request.method == 'POST' or "page" in request.GET or "or_and" in request.GET: 
        dico_get={}
        if request.method == "POST":
            if "page" in request.GET:
                projects = request.session['projects']
                types = request.session['list_type']
                formfiche = FicheForm(request.POST)
                more = request.session['list_more']
                list_proj=[i for i in projects]
                list_projects=[]
                for i in list_proj:
                    list_projects.append(Project.objects.get(name=i))   
                types_encode=[i for i in types]
                list_type=[]
                for i in types_encode:
                    list_type.append(AccessionType.objects.get(name=i))
                list_more=[i for i in more]
                projects_allowed = request.session['projects_allowed']
                projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
                
            else:
                formproject = DtviewProjectForm(request.POST)
                formfiche = FicheForm(request.POST)
                accessiontypeform = DtviewAccessionTypeForm(request.POST)
                viewaccession = DataviewAccession(request.POST)
                if formproject.is_valid():
                    list_projects = formproject.cleaned_data['project']
                if list_projects==[]:
                    list_projects=request.POST["list_projects"]
                try:
                    projects_allowed = request.POST['projects_allowed']
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                except MultiValueDictKeyError:
                    projects_allowed = request.POST.getlist('projects_allowed')
                projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
                list_type = request.POST.getlist('accession_type')
                list_more = request.POST.getlist('more_info')
            
            list_projects2=[]
            for i in list_projects:
                list_projects2.append(i.id)
            request.session['list_projects2']=list_projects2
            request.session['list_more'] = list_more
            request.session["type_list_search"] = list_type
            request.session['projects_allowed'] = projects_allowed
            request.session['projects_allowed_id'] = projects_allowed_id
            if formfiche.is_valid():
                fiche = request.POST['fiche']
                type_data="Accession"
                if "--" in fiche:
                    fiche=fiche.replace("--","")
                    acc = Accession.objects.get(name=fiche)
                elif "seed:" in fiche:
                    fiche=fiche.replace("seed:", "")
                    seed = Seedlot.objects.get(name=fiche)
                    fiche = seed.accession.name
                    acc = Accession.objects.get(name=fiche)
                elif "sample:" in fiche:
                    fiche=fiche.replace("sample:","")
                    sample = Sample.objects.get(name=fiche)
                    seed_name = sample.seedlot.name
                    seed = Seedlot.objects.get(name=seed_name)
                    fiche = seed.accession.name
                    acc = Accession.objects.get(name=fiche)
                else:
                    acc = Accession.objects.get(name=fiche)
                type_acc = acc.type
                name = acc.name
                doi = acc.doi
                private_doi = acc.private_doi
                pedigree = acc.pedigree
                description = acc.description
                attributes = acc.attributes_values
                
                if attributes==u'{}' or type(attributes)==str:
                    attributes={}
                
                attributes = list(attributes.items())
                attributes2=[]
                for i, j in sorted(attributes):
                    for nb in range(len(attributes)):
                        if i==attributes[nb][0]:
                            attributes2.append((AccessionTypeAttribute.objects.get(id=i).attribute_name,j))
                projects_to_be_seen = acc.projects.filter()
                projects_to_show=[]
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                projects_fiche=Project.objects.filter(name__in=projects_to_show)
                
                projects_to_show_fiche=[]
                for i in projects_fiche:
                    projects_to_show_fiche.append(i.id)
                
                ##seedlots à montrer dans la fiche
                seedlots = Seedlot.objects.filter(accession=acc, projects__in=projects_to_show_fiche)
                seedlot_list=[]
                for seedlot in seedlots:
                    seedlot_list.append(seedlot.name)
                
                ##sample dans la fiche
                samples = Sample.objects.filter(seedlot__in=seedlots, projects__in=projects_to_show_fiche)
                sample_list=[]
                for sample in samples:
                    sample_list.append(sample.name)
                
                ##genoid dans la fiche
                genoids = GenotypingID.objects.filter(sample__in=samples, projects__in=projects_to_show_fiche)
                genoid_list=[]
                for genoid in genoids:
                    genoid_list.append(genoid.name)
                
                request.session['return']='return'
                projects = ', '.join(projects_to_show)
                dico_fiche={'genoids':set(genoid_list),
                            'samples':set(sample_list),
                            'seedlots':set(seedlot_list),
                            'name':name, 
                            "type_acc":type_acc, 
                            "doi":doi,
                            "private_doi":private_doi,
                            "pedigree":pedigree,
                            "description":description,
                            "attributes":attributes2,
                            "projects":projects_to_show,
                            "type_data":type_data,
                            'dataview':True,
                            "parent_male_accession":parent_male_accession,
                            "parent_female_accession":parent_female_accession,
                            }
                return render(request, 'dataview/accession_infos.html', dico_fiche)
                        
        
        elif "page" in request.GET:
            formfiche=FicheForm()
            projects = request.session['projects']
            types = request.session['list_type']
            
            more = request.session['list_more']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(name=i))   
            types_encode=[i for i in types]
            list_type=[]
            for i in types_encode:
                list_type.append(str(AccessionType.objects.get(name=i).id))
            list_more=[i for i in more]
            projects_allowed = request.session['projects_allowed']
            projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
            request.session['list_more'] = list_more
            request.session['type_list_search'] = list_type
            request.session['projects_allowed'] = projects_allowed
            request.session['projects_allowed_id'] = projects_allowed_id
                
        list_projects2=request.session['list_projects2']
        list_more = request.session['list_more']
        list_type = request.session['type_list_search']
        projects_allowed = request.session['projects_allowed']
        projects_allowed_id = request.session['projects_allowed_id']
        accessions = _get_accessions(request, list_projects2, list_type)
        all_accessions = list(set(Accession.objects.filter(projects__in=list_projects2, type_id__in=list_type).order_by('name')))
        #print(all_accessions)
        if 'or_and' in request.GET and "no_search" not in request.GET:
            or_and = None
            sample_refine = []
            projects_to_show = request.session['projects_to_show']
            acc_name_list = request.session['acc_name_list']    
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "accession":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                    
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            var_search = str(data_type + "__icontains" ) 
                            if data_type == "sample" or data_type == 'seedlot':
                                if data_type == "sample" :
                                    sample_ids = Sample.objects.filter(name__icontains = dico_type_data[data_type])
                                    seedlots_names = Seedlot.objects.filter(sample__in=sample_ids)
                                else : 
                                    seedlots_names = Seedlot.objects.filter(name__icontains = dico_type_data[data_type])
                                acc_names = Accession.objects.filter(seedlot__in=seedlots_names)
                                acc_name_filters = []
                                for i in acc_names:
                                    acc_name_filters.append(i.name)
                                acc_name_list2 = [] 
                                for acc_name in acc_name_list :
                                    if acc_name in acc_name_filters :
                                        acc_name_list2.append(acc_name)
                                search_value = acc_name_list2
                                var_search = str("name__in" ) 
                            elif data_type == "type":
                                acc_type = AccessionType.objects.filter(name__contains=value)
                                list_type_acc = []
                                for i in acc_type:
                                    list_type_acc.append(i.id)
                                search_value = list_type_acc
                                var_search = str(data_type + "__in" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            var_search = str(data_type + "__icontains" )
                            if data_type == "sample" or data_type == 'seedlot':
                                if data_type == "sample" :
                                    sample_ids = Sample.objects.filter(name__icontains = dico_type_data[data_type])
                                    seedlots_names = Seedlot.objects.filter(sample__in=sample_ids)
                                else : 
                                    seedlots_names = Seedlot.objects.filter(name__icontains = dico_type_data[data_type])
                                acc_names = Accession.objects.filter(seedlot__in=seedlots_names)
                                acc_name_filters = []
                                for i in acc_names:
                                    acc_name_filters.append(i.name)
                                acc_name_list2 = [] 
                                for acc_name in acc_name_list :
                                    if acc_name in acc_name_filters :
                                        acc_name_list2.append(acc_name)
                                search_value = acc_name_list2
                                var_search = str("name__in" ) 
                            elif data_type == "type":
                                acc_type = AccessionType.objects.filter(name__icontains=value)
                                list_type_acc = []
                                for i in acc_type:
                                    list_type_acc.append(i.id)
                                search_value = list_type_acc
                                var_search = str(data_type + "__in" ) 
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, type__in=list_type, name__in=acc_name_list)  
            accessions = Accession.objects.filter(query).distinct()
            all_accessions=accessions
            if list_more==[u'1'] or list_more == ['1']:
                number_all=len(Seedlot.objects.filter(accession__in=accessions).distinct())
            elif list_more==[u'2'] or list_more==[u'1',u'2'] or list_more == ['1','2'] or list_more == ['2']:
                seedlots = Seedlot.objects.filter(accession__in=accessions).distinct()
                seedlots_number = len(seedlots)
                sample_number = len(Sample.objects.filter(seedlot__in=seedlots).distinct())
                if seedlots_number > sample_number:
                    number_all = seedlots_number
                else:
                    number_all = sample_number
            else:
                number_all = len(accessions)
            
            list_exceptions = ['relations_as_parent','relations_as_child','seedlot','id','doi','private_doi','is_obsolete','attributes_values']
            download_not_empty = False
            if accessions:
                download_not_empty = write_csv_search_result(accessions, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})

            #list_exceptions = ['seedlot','id','doi','private_doi','is_obsolete','attributes_values','images']
            #download_not_empty = False
            #if accessions:
            #    download_not_empty = write_csv_search_result(accessions, list_exceptions)
            #tag_fields.update({"download_not_empty":download_not_empty})


        
        request.session['projects_to_show'] = []
        
        if list_more==[u'1'] or list_more == ['1']:
            list_attributes.append('seedlot')
            title = 'Accessions and Seedlots linked to the project(s)'
            labels = ['Accession','Seedlot', 'Type', 'Pedigree', 'Description', 'Projects']
            seedlot=[]
            dico={}
            
            for i in accessions:
                seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id)
                if not seedlot:
                    seedlot="-"
                for seed in seedlot:
                    projects_to_be_seen = i.projects.filter()
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    if seed=="-":
                        dico[str(i.name)+"--"]=[i.name,'--',i.type.name,i.pedigree,i.description,', '.join(projects_to_show)]
                    else:
                        dico["seed:"+str(seed.name)]=[i.name,seed.name,i.type.name,i.pedigree,i.description,', '.join(projects_to_show)]
            
        elif list_more==[u'2'] or list_more == ['2']:
            list_attributes.append('sample')
            title = 'Accessions and Samples linked to the project(s)'
            labels = ['Accession','Sample', 'Type', 'Pedigree', 'Description', 'Projects']
            seedlot=[]
            dico={}
            for i in accessions:
                projects_to_be_seen = i.projects.filter()
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id)
                if not seedlot:
                    seedlot="-"
                for seed in seedlot:
                    if seed=="-":
                        dico[str(i.name)+"--"]=[i.name,'--',i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                    else:
                        sample_all=Sample.objects.filter(seedlot=seed.id, projects__in=projects_allowed_id)
                        for sample in sample_all:
                            projects_to_be_seen = i.projects.filter()
                            dico["sample:"+str(sample.name)]=[i.name,sample.name,i.type,i.pedigree,i.description,', '.join(projects_to_show)]

        elif list_more==[u'1',u'2'] or list_more == ['1','2']:
            list_attributes.append('seedlot')
            list_attributes.append('sample')
            title = 'Accessions, Seedlots and Samples linked to the project(s)'
            labels = ['Accession','Seedlot', 'Sample','Type', 'Pedigree', 'Description', 'Projects']
            seedlot=[]
            dico={}
            for i in accessions:
                projects_to_be_seen = i.projects.filter()
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id).distinct()
                if not seedlot:
                    seedlot="-"
                for seed in seedlot:
                    if seed=="-":
                        dico[str(i.name)+"--"]=[i.name,'--',"--",i.type,i.pedigree,i.description, ', '.join(projects_to_show) ]
                        
                    else: 
                        sample_all=Sample.objects.filter(seedlot=seed.id, projects__in=projects_allowed_id)
                        
                        if not sample_all :
                            sample_all='-'
                        for sample in sample_all:
                            if sample=="-":
                                dico[str(i.name)+"--"]=[i.name,seed.name,"--",i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                            else:
                                dico["sample:"+str(sample.name)]=[i.name,seed.name,sample.name,i.type,i.pedigree,i.description,', '.join(projects_to_show) ]
                                
        else:
            title = 'Accessions linked to the project(s)'
            labels = ['Accession','Type', 'Pedigree', 'Description', 'Projects']
            dico={}
            for i in accessions:
                projects_to_be_seen = i.projects.filter()
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.type,i.pedigree,i.description,', '.join(projects_to_show)]
        if dico :
            accessions_all = list(set(Accession.objects.filter(projects__in=list_projects2, type_id__in=list_type).order_by('name')))
            if not number_all:
                number_all=len(accessions_all)
            dico_all={}
            acc_name_list=[]
            
            ####################################################################
            for i in accessions_all:
                acc_name_list.append(i.name)
                if list_more==[u'1']:
                    seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id)
                    if not seedlot:
                        seedlot="-"
                    for seed in seedlot:
                        if seed=="-":
                            dico_all[str(i.name)+"--"]=[i.name,'--',i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                        else:
                            dico_all["seed:"+str(seed.name)]=[i.name,seed.name,i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                
                elif list_more==[u'2']:
                    seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id)
                    if not seedlot:
                        seedlot="-"
                    for seed in seedlot:
                        if seed=="-":
                            dico_all[str(i.name)+"--"]=[i.name,'--',i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                        else:
                            sample_all=Sample.objects.filter(seedlot=seed.id, projects__in=projects_allowed_id)
                            for sample in sample_all:
                                projects_to_be_seen = i.projects.filter()
                                dico_all["sample:"+str(sample.name)]=[i.name,sample.name,i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                                
                elif list_more==[u'1',u'2']:
                    seedlot=Seedlot.objects.filter(accession=i.id, projects__in=projects_allowed_id)
                    if not seedlot:
                        seedlot="-"
                    for seed in seedlot:
                        if seed=="-":
                            dico_all[str(i.name)+"--"]=[i.name,'--',"--",i.type,i.pedigree,i.description, ', '.join(projects_to_show) ]
                        else:
                            sample_all=Sample.objects.filter(seedlot=seed.id, projects__in=projects_allowed_id)
                            if not sample_all:
                                sample_all='-'
                            for sample in sample_all:
                                if sample=="-":
                                    dico_all[str(i.name)+"--"]=[i.name,seed.name,"--",i.type,i.pedigree,i.description,', '.join(projects_to_show)]
                                else:
                                    dico_all["sample:"+str(sample.name)]=[i.name,seed.name,sample.name,i.type,i.pedigree,i.description,', '.join(projects_to_show) ]
                                        
                else:
                    dico_all[i.name]=[i.name,i.type,i.pedigree,i.description,', '.join(projects_to_show)]
            request.session['acc_name_list'] = acc_name_list            
            df=getPandasDf("/tmp/dtview_accession.csv",*labels, **dico_all)
        
        dico = dico.items()
        list_projects=[]
        for i in list_projects2:
            list_projects.append(Project.objects.get(id=i))
        tag_fields.update({'all':dico,
                      'title': title,
                      'fields_label': labels,
                      'creation_mode': True,
                      "pagination":accessions,
                      "projects":projects_allowed,
                      "list_projects":list_projects,
                      "fiche":"accession card",
                      "number_all":number_all,
                      'dataview':True,
                      "list_attributes":list_attributes,
                      'search_result':dico_get,
                      "or_and":or_and,
                      })
        list_proj=[(i.name) for i in list_projects]
        request.session['projects']=list_proj 
        request.session['projects_allowed']=projects_allowed
        list_typ=[]
        for i in list_type:
            if type(i)==str:
                list_typ.append(AccessionType.objects.get(id=i)) 
            else:
                list_typ=list_type               
        list_typ=[(i.name) for i in list_typ]
        request.session['list_type']=list_typ
        request.session['list_more']=list_more

        
        ###Ajout Arthur : Map affichant toutes les accessions sélectionnées

        print("Here is the map")
      
        accession_names = []

        """
        #le -- est présent quand on coche seedlot ou sample, on l'enlève
        #Bug : une accession avec un "--" ne fonctionnera pas.  
        for i in tag_fields['all']:
            i=list(i)
            i[0]= i[0].replace("--","")
            accession_names.append(i[0])
        """
        
        #all_accessions = liste des accessions correspondant à la recherche
        for i in all_accessions:
            accession_names.append(i)
        
        list_accessions = []
        list_accessions_names = []
        list_latitudes=[]
        list_longitudes = []
        
        for accession in accession_names:
     
            try:
                #Get accession
                acc = Accession.objects.get(name=accession)
                """ 
                #Get accession type
                acc_type = acc.type
                #Get the attributes
                latitude = acc.attributes_values[str(AccessionTypeAttribute.objects.get(attribute_name='latitude', accession_type=acc_type).id)]
                longitude = acc.attributes_values[str(AccessionTypeAttribute.objects.get(attribute_name='longitude', accession_type=acc_type).id)]
                latitude=latitude.replace(",",".")
                longitude=longitude.replace(",",".")
                """
                
                latitude=acc.latitude
                longitude=acc.longitude
                
                #Stockage des infos dans des listes pour l'affichage
                list_accessions.append(acc)
                list_accessions_names.append(acc.name)
                list_latitudes.append(latitude)
                list_longitudes.append(longitude)
            
            except:
                acc = None
                latitude = None
                longitude = None
        
        #Liste des accessions dans un string, séparé par des "_"
        string_accessions=""
        for acc in list_accessions:
            string_accessions += str(acc.id)
            string_accessions += "_"
        string_accessions = string_accessions[:-1]
        
        #Conversion des listes pour le template
        list_latitudes = json.dumps(list_latitudes)
        list_longitudes = json.dumps(list_longitudes)
        list_accessions_names = json.dumps(list_accessions_names)
        
        nb_accessions=list(range(0,len(list_accessions)))
        tag_fields.update({'string_accessions':string_accessions, 'list_accessions_names': list_accessions_names, 'list_latitudes':list_latitudes, 'list_longitudes':list_longitudes, 'nb_accessions':nb_accessions})
        
        ####################

        return render(request, 'dataview/dtview_tableau.html', tag_fields)
        
                    
    else:
        formproject = DtviewProjectForm()
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
        projects_allowed=', '.join([(v.name) for v in projects])
        projects_allowed = projects_allowed.split(', ')
        formproject.fields['project'].queryset=projects.order_by('name')
        accessiontypeform = DtviewAccessionTypeForm()
        viewaccession = DataviewAccession()
    request.session['return']="no"    
    return render(request,'dataview/choose_project.html', {'accession_viewer':True,'dataview':True,'projects':projects_allowed,'formproject':formproject,'accessiontypeform':accessiontypeform,'viewaccession':viewaccession})

def accessionMap(request, string_accessions):
    
    template = "dataview/accession_map.html"
    
    list_accessions_id = string_accessions.split("_")
    #Récupération des accessions
    list_accessions = []
    list_accessions_names = []
    list_latitudes=[]
    list_longitudes = []
    
    for id in list_accessions_id:
        acc = Accession.objects.get(id=id)
        
        latitude=acc.latitude
        longitude=acc.longitude
        
        #Stockage des infos dans des listes pour l'affichage
        list_accessions.append(acc)
        list_accessions_names.append(acc.name)
        list_latitudes.append(latitude)
        list_longitudes.append(longitude)

    #Conversion des listes pour le template
    list_latitudes = json.dumps(list_latitudes)
    list_longitudes = json.dumps(list_longitudes)
    list_accessions_names = json.dumps(list_accessions_names)
    
    nb_accessions=list(range(0,len(list_accessions)))
    
    return render(request,template,{'dataview':True, "list_accessions":list_accessions, "list_accessions_names":list_accessions_names, "list_latitudes":list_latitudes, "list_longitudes":list_longitudes, "nb_accessions":nb_accessions})

def _get_accessions(request, list_projects, list_type):
    accessions = list(set(Accession.objects.filter(projects__in=list_projects, type_id__in=list_type).order_by('name')))
    paginator = Paginator(accessions,50)
    
    page = request.GET.get('page')
    try:
        accessions = paginator.page(page)
    except PageNotAnInteger:
        accessions = paginator.page(1)
    except EmptyPage:
        accessions = paginator.page(paginator.num_pages)
    return accessions

def genoID_viewer(request):
    username = None
    or_and = None
    type_data="genoid"
    title="Genotyping ID viewer"
    if request.user.is_authenticated():
        username = request.user.login
    if "return" not in request.session:
        request.session['return']="no"
    formfiche=FicheForm()     
    list_attributes=[]
    for i in ['genotypingID', 'projects']:
        list_attributes.append(i)
    number_all=0
    tag_fields = {}
    if request.method == 'POST' or "page" in request.GET or "or_and" in request.GET: 
        dico_get={}
        if request.method == "POST":
            if "page" in request.GET:
                projects = request.session['projects']
                formfiche = FicheForm(request.POST)
                more = request.session['list_more']
                list_proj=[i for i in projects]
                list_projects=[]
                for i in list_proj:
                    list_projects.append(Project.objects.get(name=i))   
                list_more=[i for i in more]
                projects_allowed = request.session['projects_allowed']
                projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
            else:
                formproject = DtviewProjectForm(request.POST)
                formfiche = FicheForm(request.POST)
                if formproject.is_valid():
                    list_projects = formproject.cleaned_data['project']
                if list_projects==[]:
                    list_projects=request.POST["list_projects"]
                try:
                    projects_allowed = request.POST['projects_allowed']
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                except MultiValueDictKeyError:
                    projects_allowed = request.POST.getlist('projects_allowed')
                projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
                list_more = request.POST.getlist('more_info')
            
            list_projects2=[]
            for i in list_projects:
                list_projects2.append(i.id)
            request.session['list_projects2']=list_projects2
            request.session['list_more'] = list_more
            request.session['projects_allowed'] = projects_allowed
            request.session['projects_allowed_id'] = projects_allowed_id

        elif "page" in request.GET:
            formfiche=FicheForm()
            projects = request.session['projects']
            more = request.session['list_more']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(name=i))
            list_more=[i for i in more]
            projects_allowed = request.session['projects_allowed']
            projects_allowed_id=[Project.objects.get(name=i).id for i in projects_allowed]
            request.session['list_more'] = list_more
            request.session['projects_allowed'] = projects_allowed
            request.session['projects_allowed_id'] = projects_allowed_id

        list_projects2=request.session['list_projects2']
        list_more = request.session['list_more']
        projects_allowed = request.session['projects_allowed']
        projects_allowed_id = request.session['projects_allowed_id']
        genoID=_get_genoID(request, list_projects2)
        all_genoID = list(set(GenotypingID.objects.filter(projects__in=list_projects2).order_by('name')))

        if 'or_and' in request.GET and "no_search" not in request.GET:
            or_and = None
            projects_to_show = request.session['projects_to_show']
            genoid_name_list = request.session['genoid_name_list']
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "genotypingID":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]}
            length_dico_get = len(dico_get)
            nb_per_page = 10


            if or_and == "or":
                myoperator = ior
            else:
                myoperator = iand
                
            proj=[]
            query=Query_dj()
            list_name_in=[]
            list_attr=[]
            for dat, dico_type_data in dico_get.items():
                if "projects" in dico_type_data:
                    if dico_type_data['projects']=='':
                        query = myoperator(query, Query_dj(projects__isnull=True))
                    else:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query = myoperator(query, Query_dj(projects__in=proj))

                else:
                    for data_type, value in dico_type_data.items():
                        search_value = dico_type_data[data_type]
                        if data_type == "genotypingID":
                            data_type = "name"
                        var_search = str(data_type + "__icontains" )
                        if data_type == "sample":
                            search_value = [i.id for i in Sample.objects.filter(name__icontains=search_value)]
                            var_search = str(data_type + '__in')
                        if data_type == "seedlot":
                            search_value = [i.id for i in Sample.objects.filter(seedlot__name__icontains=search_value)]
                            var_search = str("sample" + '__in')
                        if data_type == "accession":
                            search_value = [i.id for i in Sample.objects.filter(seedlot__accession__name__icontains=search_value)]
                            var_search = str("sample" + '__in')
                        query = myoperator(query, Query_dj(**{var_search : search_value}))

            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=genoid_name_list)  
            #print("voila la query")
            #print(query)  
            genoID = GenotypingID.objects.filter(query).distinct()

            all_genoID=genoID
            number_all = len(genoID)
            if '1' in list_more or u'1' in list_more:
                list_exceptions = ['id','experiment','referential','sentrixbarcode_a','sentrixposition_a','funding']
            else:
                list_exceptions = ['id','sample','experiment','referential','sentrixbarcode_a','sentrixposition_a','funding']
            download_not_empty = False
            if genoID:
                download_not_empty = write_csv_search_result(genoID, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})     

        request.session['projects_to_show'] = []
        
        if list_more==[u'1'] or list_more == ['1']:
            list_attributes.append('sample')
            title = 'GenotypingID and Samples linked to the project(s)'
            labels = ['GenotypingID', 'Sample', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                samples = i.sample
                dico[i.name, i.referential] = [i.name,samples.name,', '.join(projects_to_show)]

        elif list_more==[u'2'] or list_more == ['2']: ## list more == "2" correspond à seedlot
            list_attributes.append('seedlot')
            title = 'GenotypingID and Seedlots linked to the project(s)'
            labels = ['GenotypingID', 'Seedlot', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                seedlots = i.sample.seedlot
                #dico[str(i.id)] = [i.name,seedlots.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,seedlots.name,', '.join(projects_to_show)]
                    
        elif list_more==[u'3'] or list_more == ['3']: ## list more == "3" correspond à accession
            list_attributes.append('accession')
            title = 'GenotypingID and Accessions linked to the project(s)'
            labels = ['GenotypingID', 'Accession', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                accessions = i.sample.seedlot.accession
                #dico[str(i.id)] = [i.name,accessions.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,accessions.name,', '.join(projects_to_show)]
            
        elif list_more==[u'1',u'2'] or list_more == ['1','2']: ##sample & seedlot
            list_attributes.append('sample')
            list_attributes.append('seedlot')
            title = 'GenotypingID, Samples and Seedlots linked to the project(s)'
            labels = ['GenotypingID', 'Sample', 'Seedlot', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                samples = i.sample
                seedlots = i.sample.seedlot
                #dico[str(i.id)] = [i.name,samples.name,seedlots.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,samples.name,seedlots.name,', '.join(projects_to_show)]
            
        elif list_more==[u'1',u'3'] or list_more == ['1','3']: ##sample & accession
            list_attributes.append('sample')
            list_attributes.append('accession')
            title = 'GenotypingID, Samples and Accessions linked to the project(s)'
            labels = ['GenotypingID', 'Sample', 'Accession', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                samples = i.sample
                accessions = i.sample.seedlot.accession
                #dico[str(i.id)] = [i.name,samples.name, accessions.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,samples.name, accessions.name,', '.join(projects_to_show)]

        elif list_more==[u'1',u'2',u'3'] or list_more == ['1','2','3']: ##sample & seedlot & accession
            list_attributes.append('sample')
            list_attributes.append('seedlot')
            list_attributes.append('accession')
            title = 'GenotypingID, Samples, Seedlots and Accessions linked to the project(s)'
            labels = ['GenotypingID', 'Sample', 'Seedlot', 'Accession', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                samples = i.sample
                seedlots = i.sample.seedlot
                accessions = i.sample.seedlot.accession
                #dico[str(i.id)] = [i.name,samples.name,seedlots.name, accessions.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,samples.name,seedlots.name, accessions.name,', '.join(projects_to_show)]
            
        elif list_more==[u'2',u'3'] or list_more == ['2','3']: ##seedlot & accession
            list_attributes.append('seedlot')
            list_attributes.append('accession')
            title = 'GenotypingID, Seedlots and Accessions linked to the project(s)'
            labels = ['GenotypingID','Seedlot', 'Accession', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                seedlots = i.sample.seedlot
                accessions = i.sample.seedlot.accession
                #dico[str(i.id)] = [i.name,seedlots.name,accessions.name,', '.join(projects_to_show)]
                dico[i.name, i.referential] = [i.name,seedlots.name,accessions.name,', '.join(projects_to_show)]

        else: #juste les genoID
            title = 'GenotypingID linked to the project(s)'
            labels = ['GenotypingID', 'Project']
            dico = {}
            for i in genoID:
                projects_to_show = get_projects_to_show(request,i,projects_allowed)
                dico[i.name, i.referential]=[i.name,', '.join(projects_to_show)]

        if dico :
            genoID_all=list(set(GenotypingID.objects.filter(projects__in=list_projects2)))
            if not number_all:
                number_all=len(genoID_all)
            dico_all={}
            genoid_name_list=[]

            for i in genoID_all:
                genoid_name_list.append(i.name)
                if list_more==[u'1'] or list_more == ['1']:
                    for i in genoID:
                        samples = i.sample
                        dico_all[str(i.id)] = [i.name,samples.name,', '.join(projects_to_show)]
                elif list_more==[u'2'] or list_more == ['2']: 
                    for i in genoID:
                        seedlots = i.sample.seedlot
                        dico_all[str(i.id)] = [i.name,seedlots.name,', '.join(projects_to_show)]
                elif list_more==[u'3'] or list_more == ['3']:
                    for i in genoID:
                        accessions = i.sample.seedlot.accession
                        dico_all[str(i.id)] = [i.name,accessions.name,', '.join(projects_to_show)]
                elif list_more==[u'3'] or list_more == ['3']: ## list more == "3" correspond à accession
                    for i in genoID:
                        accessions = i.sample.seedlot.accession
                        dico_all[str(i.id)] = [i.name,accessions.name,', '.join(projects_to_show)]
                elif list_more==[u'1',u'2'] or list_more == ['1','2']: ##sample & seedlot
                    for i in genoID:
                        samples = i.sample
                        seedlots = i.sample.seedlot
                        dico_all[str(i.id)] = [i.name,samples.name,seedlots.name,', '.join(projects_to_show)]
                elif list_more==[u'1',u'3'] or list_more == ['1','3']: ##sample & accession
                    for i in genoID:
                        samples = i.sample
                        accessions = i.sample.seedlot.accession
                        dico_all[str(i.id)] = [i.name,samples.name, accessions.name,', '.join(projects_to_show)]
                elif list_more==[u'1',u'2',u'3'] or list_more == ['1','2','3']: ##sample & seedlot & accession
                    for i in genoID:
                        samples = i.sample
                        seedlots = i.sample.seedlot
                        accessions = i.sample.seedlot.accession
                        dico_all[str(i.id)] = [i.name,samples.name,seedlots.name, accessions.name,', '.join(projects_to_show)]
                elif list_more==[u'2',u'3'] or list_more == ['2','3']: ##seedlot & accession
                    for i in genoID:
                        seedlots = i.sample.seedlot
                        accessions = i.sample.seedlot.accession
                        dico_all[str(i.id)] = [i.name,seedlots.name,accessions.name,', '.join(projects_to_show)]
                else:
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
            request.session['genoid_name_list'] = genoid_name_list
            getPandasDf("/tmp/dtview_genoid2.csv",*labels,**dico_all)
            
        dico = dico.items()
        list_projects=[]
        for i in list_projects2:
            list_projects.append(Project.objects.get(id=i))
        tag_fields.update({'all':dico,
                          'title': title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":genoID,
                          'projects_or_not':1,
                          "number_all":number_all,
                          "genoid":"1",
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
        list_proj=[(i.name) for i in list_projects]
        request.session['projects']=list_proj 
        request.session['projects_allowed']=projects_allowed
        request.session['list_more']=list_more

        return render(request, "dataview/GenoExp_viewer.html", tag_fields)

    else:
        formproject = DtviewProjectForm()
        if User.objects.get(login=username).is_superuser():
            projects = Project.objects.all()
        else:
            username_id=User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
        projects_allowed=', '.join([(v.name) for v in projects])
        projects_allowed = projects_allowed.split(', ')
        formproject.fields['project'].queryset=projects.order_by('name')
        accessiontypeform = DtviewAccessionTypeForm()
        viewaccession = DataviewAccession()
        
        tag_fields = {"type_data":type_data,
                      "DataviewGenoID":DataviewGenoIDForm(),
                      "genoid_viewer":True,
                      'dataview':True,
                      "projects":projects_allowed,
                      'formproject':formproject,
                      "projects_or_not":0,
                      "title":title,
#                       "search_result":dico_get,
                      "or_and":or_and,
                      }  
    request.session['return']="no" 
    return render(request, "dataview/choose_project.html", tag_fields)

def _get_genoID(request, list_projects):
    genoID=list(set(GenotypingID.objects.filter(projects__in=list_projects)))
    paginator = Paginator(genoID,50)
    page = request.GET.get('page')
    try:
        genoID = paginator.page(page)
    except PageNotAnInteger:
        genoID = paginator.page(1)
    except EmptyPage:
        genoID = paginator.page(paginator.num_pages)
    return genoID

def experiment_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Experiment viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="experiment"
    projects_or_not=1
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['experiment', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            experiments=_get_experiments(request, list_projects)
            labels = ['Experiment', 'Project']
            dico={}
            for i in experiments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                experiments_all=Experiment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(experiments_all)
                for i in experiments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_experiment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":experiments,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            exp_name_list = request.session["exp_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "experiment":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "experiment":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "experiment":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=exp_name_list)   
            experiments = Experiment.objects.filter(query).distinct() 
            number_all = len(experiments)
            list_exceptions = ['genotypingid','id','institution','person','date','comments']
            download_not_empty = False
            if experiments:
                download_not_empty = write_csv_search_result(experiments, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            experiments=_get_experiments(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            experiments=_get_experiments(request, list_projects)
            exp_name_list=[]
            for i in experiments : 
                exp_name_list.append(i.name)
            request.session["exp_name_list"] = exp_name_list
            
        if projects_or_not==1:
            labels = ['Experiment', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in experiments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)] 
            if dico:
                experiments_all=Experiment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(experiments_all)
                for i in experiments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_experiment.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":experiments,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
# <<<<<<< .working
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  
  
def referential_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Referential viewer"
    username = None
    number_all=0
    or_and = None
    dico_get={}
    type_data="referential"
    projects_or_not=1
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['referential', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            referentials=_get_referentials(request, list_projects)
            labels = ['Referential', 'Project']
            dico={}
            for i in referentials:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                referential_all=Referential.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(referential_all)
                print("-----------> NUMBER ALL:   ",number_all)
                for i in referential_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                            if k not in request.session['projects_to_show']:
                                request.session['projects_to_show'].append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_experiment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":referentials,
                          'projects_or_not':projects_or_not,
                          "experiment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          }
# =======
#                           "refine_search":True,
#                           })
# >>>>>>> .merge-right.r614
            
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            
            projects_to_show = request.session['projects_to_show']
            print('proj to show: ',projects_to_show)
            list_projects = request.session['list_projects']
            ref_name_list = request.session["ref_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "referential":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10
            
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "referential":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            print(data_type)
                            search_value = dico_type_data[data_type]
                            if data_type == "referential":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            print('proj to show: ',projects_to_show)
            print('ref_name_list: ',ref_name_list)
            
            query = Query_dj(query, projects__in=proj_allowed, name__in=ref_name_list)   
            print('query: ', query)
            referentials = Referential.objects.filter(query).distinct() 
            number_all = len(referentials)
            list_exceptions = ['id',"genotypingid",'person','date']
            download_not_empty = False
            if referentials:
                download_not_empty = write_csv_search_result(referentials, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            print("\n\n======+> TEST", list_projects)
            projects_or_not=1
            referentials=_get_referentials(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            referentials=_get_referentials(request, list_projects)
            ref_name_list=[]
            for i in referentials : 
                ref_name_list.append(i.name)
            request.session["ref_name_list"] = ref_name_list
            
        if projects_or_not==1:
            labels = ['Referential', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            for i in referentials:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)] 
            if dico:
                referentials_all=Referential.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                print("-----------> NUMBER ALL:   ",number_all)
                if number_all == 0:
                    number_all=len(referentials_all)
                for i in referentials_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_referential.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":referentials,
                          'projects_or_not':projects_or_not,
                          "referential":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
    
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  
    
def _get_referentials(request, list_projects):
    referentials=Referential.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(referentials,50)
    page = request.GET.get('page')
    try:
        referentials = paginator.page(page)
    except PageNotAnInteger:
        referentials = paginator.page(1)
    except EmptyPage:
        referentials = paginator.page(paginator.num_pages)
    return referentials
  
def environment_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Environment viewer"
    type_data="environment"
    username = None
    projects_or_not=1
    username = None
    or_and = None
    dico_get = {}
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['environment', 'date', 'location', 'specific treatment', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            environments=_get_environments(request, list_projects)
            labels = ['Environment', 'Date', 'Location', 'Specific treatment', 'Project']
            dico={}
            for i in environments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
            if dico:
                environments_all=Environment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(environments_all)
                for i in environments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_environment.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":environments,
                          'projects_or_not':projects_or_not,
                          "environment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            
            return render(request, template, tag_fields)
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            env_name_list = request.session['env_name_list']
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "environment":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera

            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            type_search = "__icontains"
                            if data_type == "environment":
                                data_type = "name"
                            elif data_type == "specific":
                                search_value = SpecificTreatment.objects.filter(name__icontains = dico_type_data[data_type])
                                data_type="specific_treatment"
                                type_search = "__in"
                            var_search = str(data_type + type_search )
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            type_search = "__icontains"
                            if data_type == "environment":
                                data_type = "name"
                            elif data_type == "specific":
                                search_value = SpecificTreatment.objects.filter(name__icontains = dico_type_data[data_type])
                                data_type="specific_treatment"
                                type_search = "__in"
                            var_search = str(data_type + type_search )
                            query &= Query_dj(**{var_search : search_value})
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=env_name_list)   
            environments = Environment.objects.filter(query).distinct() 
            number_all = len(environments)
            list_exceptions = ['phenotypicvalue','description','id']
            download_not_empty = False
            if environments:
                download_not_empty = write_csv_search_result(environments, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            environments=_get_environments(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            
            environments=_get_environments(request, list_projects)
            env_name_list=[]
            for i in environments:
                env_name_list.append(i.name)
            request.session['env_name_list']=env_name_list
        
        if projects_or_not==1:
            labels = ['Environment', 'Date', 'Location', 'Specific treatment', 'Project']
            dico={}
            request.session['projects_to_show']=[]
            
            for i in environments:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
            if dico:
                environments_all=Environment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(environments_all)
                for i in environments_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,i.date,i.location,i.specific_treatment,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_environment.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":environments,
                          'projects_or_not':projects_or_not,
                          "environment":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          "list_attributes":list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
        return render(request, template, {"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  

def _get_environments(request, list_projects):
    environments=Environment.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(environments,50)
    page = request.GET.get('page')
    try:
        environments = paginator.page(page)
    except PageNotAnInteger:
        environments = paginator.page(1)
    except EmptyPage:
        environments = paginator.page(paginator.num_pages)
    return environments

def trait_viewer(request):
    number_all=0
    template="dataview/GenoExp_viewer.html"
    formproject=DtviewProjectForm()
    title="Trait viewer"
    type_data="trait"
    username = None
    or_and = None
    dico_get={}
    number_all = 0
    projects_or_not=1
    if request.user.is_authenticated():
        username = request.user.login
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
    projects_allowed=', '.join([(v.name) for v in projects])
    projects_allowed = projects_allowed.split(', ')
    list_attributes=[]
    for i in ['trait', 'projects']:
        list_attributes.append(i)
    
    if "page" in request.GET:
        if request.session['projects']!=[]:
            projects = request.session['projects']
            list_proj=[i for i in projects]
            list_projects=[]
            for i in list_proj:
                list_projects.append(Project.objects.get(id=i))
            traits=_get_traits(request, list_projects)
            labels = ['Environment', 'Project']
            dico={}
            for i in traits:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                traits_all=Environment.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                number_all=len(traits_all)
                for i in traits_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_trait.csv",*labels, **dico_all)
            dico=dico.items()
            projects_or_not=1
            
            tag_fields = {'all':dico,
                          'fields_label': labels,
                          "title":title,
                          'creation_mode': True,
                          "pagination":traits,
                          'projects_or_not':projects_or_not,
                          "trait":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          'list_attributes':list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          "refine_search":True,
                          }
            
            return render(request, template, tag_fields)
    
    
    if request.method == 'POST' or "or_and" in request.GET :  
        tag_fields = {}
        if 'or_and' in request.GET and "no_search" not in request.GET:
            projects_to_show = request.session['projects_to_show']
            list_projects = request.session['list_projects']
            trait_name_list = request.session["trait_name_list"]
            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    if request.GET[data] == "trait":
                        dico_get[data]={"name":request.GET[text_data]}
                    else:
                        dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
            length_dico_get = len(dico_get)
            nb_per_page = 10
            if or_and == "or":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query |= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "trait":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" ) 
                            query |= Query_dj(**{var_search : search_value}) 
            
            if or_and == "and":
                proj=[]
                query=Query_dj()
                list_name_in=[]
                list_attr=[]
                for dat, dico_type_data in dico_get.items():
                    if "projects" in dico_type_data:
                        proj=(Project.objects.filter(name__icontains=dico_type_data['projects']))
                        query &= Query_dj(projects__in=proj)
                    else:
                        for data_type, value in dico_type_data.items():
                            search_value = dico_type_data[data_type]
                            if data_type == "trait":
                                data_type = "name"
                            var_search = str(data_type + "__icontains" )
                            query &= Query_dj(**{var_search : search_value})
            
            proj_allowed = Project.objects.filter(name__in=projects_to_show)
            query = Query_dj(query, projects__in=proj_allowed, name__in=trait_name_list)   
            traits = Trait.objects.filter(query).distinct() 
            number_all = len(traits)
            list_exceptions = ["phenotypicvalue","id","abbreviation","measure_unit","comments"]
            download_not_empty = False
            if traits:
                download_not_empty = write_csv_search_result(traits, list_exceptions)
            tag_fields.update({"download_not_empty":download_not_empty})
        
        elif "no_search" in request.GET:
            list_projects = request.session['list_projects']
            projects_or_not=1
            traits=_get_traits(request, list_projects)
            
        else:
            formproject=DtviewProjectForm(request.POST)
            formfiche = FicheForm(request.POST)
            if formproject.is_valid():
                list_projects_id = formproject.cleaned_data['project']
            list_projects = []
            for i in list_projects_id:
                list_projects.append(i.id)
            request.session['list_projects'] = list_projects
            traits=_get_traits(request, list_projects)
            trait_name_list=[]
            for i in traits : 
                trait_name_list.append(i.name)
            request.session["trait_name_list"] = trait_name_list
            

        if projects_or_not==1:
            labels = ['Trait', 'Project']
            request.session['projects_to_show']=[]
            dico={}
            for i in traits:
                projects_to_be_seen = i.projects.filter()
                projects_allowed = str(projects_allowed).strip('[]')
                projects_allowed = (projects_allowed.strip("'"))
                projects_allowed = projects_allowed.split("', '")
                projects_to_show = []
                for k in projects_to_be_seen:
                    k = (k.name)
                    if k in projects_allowed:
                        projects_to_show.append(k)
                        if k not in request.session['projects_to_show']:
                            request.session['projects_to_show'].append(k)
                dico[i.name]=[i.name,', '.join(projects_to_show)]
            if dico:
                traits_all=Trait.objects.filter(projects__in=list_projects).distinct()
                dico_all={}
                if number_all == 0:
                    number_all=len(traits_all)
                for i in traits_all:
                    projects_to_be_seen = i.projects.filter()
                    projects_allowed = str(projects_allowed).strip('[]')
                    projects_allowed = (projects_allowed.strip("'"))
                    projects_allowed = projects_allowed.split("', '")
                    projects_to_show = []
                    for k in projects_to_be_seen:
                        k = (k.name)
                        if k in projects_allowed:
                            projects_to_show.append(k)
                    dico_all[i.name]=[i.name,', '.join(projects_to_show)]
                getPandasDf("/tmp/dtview_trait.csv",*labels, **dico_all)   
            dico=dico.items()
            projects_or_not=1
            list_proj=[(i) for i in list_projects]
            request.session['projects']=list_proj
            tag_fields.update({'all':dico,
                          "title":title,
                          'fields_label': labels,
                          'creation_mode': True,
                          "pagination":traits,
                          'projects_or_not':projects_or_not,
                          "trait":"1",
                          "number_all":number_all,
                          'dataview':True,
                          "type_data":type_data,
                          'list_attributes':list_attributes,
                          "search_result":dico_get,
                          "or_and":or_and,
                          })
            
            return render(request, template, tag_fields)
       
    else:
        projects_or_not = 0    
        formproject.fields['project'].queryset=projects.order_by('name')
        return render(request, template, {"refine_search":True,"type_data":type_data,'dataview':True,"projects":projects_allowed,'formproject':formproject,"projects_or_not":projects_or_not, "title":title})  

def _get_traits(request, list_projects):
    traits=Trait.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(traits,50)
    page = request.GET.get('page')
    try:
        traits = paginator.page(page)
    except PageNotAnInteger:
        traits = paginator.page(1)
    except EmptyPage:
        traits = paginator.page(paginator.num_pages)
    return traits
  
def getPandasDf(name_of_file, *labels, **dico):
    df = pd.DataFrame(data=dico, index=None)
    df=df.transpose()
    df.columns=labels
    df.to_csv(name_of_file, index=False, encoding='utf-8')     
    return df

def _get_experiments(request, list_projects):
    experiments=Experiment.objects.filter(projects__in=list_projects).distinct()
    paginator = Paginator(experiments,50)
    page = request.GET.get('page')
    try:
        experiments = paginator.page(page)
    except PageNotAnInteger:
        experiments = paginator.page(1)
    except EmptyPage:
        experiments = paginator.page(paginator.num_pages)
    return experiments

def aseedsam_viewer(request):
    if request.method == 'POST':
        formproject = DtviewProjectForm(request.POST)
        if formproject.is_valid():
            projects_name = []
            list_projects = formproject.cleaned_data['project']
            for project in list_projects:
                projects_name.append(project.name)
            listgenoid = []
            header = ['Genotyping ID','Sample','Seed Lot', 'Accession']
            accessions = Accession.objects.filter(projects__in=list_projects)
            seedlots = Seedlot.objects.filter(accession__in=accessions)
            samples = Sample.objects.filter(seedlot__in=seedlots)
            genotypingid = GenotypingID.objects.filter(sample__in=samples)
            for g in genotypingid:
                listgenoid.append(g.name)
            index = range(len(listgenoid))
            df = pd.DataFrame(columns=header, index=index)

            i = 0
            for g in genotypingid:
                df.set_value(i, 'Genotyping ID', g.name)
                df.set_value(i, 'Sample', g.sample)
                df.set_value(i, 'Seed Lot', g.sample.seedlot)
                df.set_value(i, 'Accession', g.sample.seedlot.accession)

                i += 1
            df = df.dropna(how='all')

            html = df.to_html(index=False, justify={'center'}, col_space='150px',)
            if df.empty:
                html = None
            
            return render(request, 'dataview/dtview_aseedsam.html', {'dataview':True,}, locals())
    else:
        formproject = DtviewProjectForm()
    return render(request, 'dataview/choose_project.html', {'accession_viewer':True,'dataview':True,'formproject':formproject})

def project_viewer(request):
    if request.method == 'POST':
        if "project_fiche" not in request.POST:
            formfiche = FicheForm(request.POST)
        else:
            fiche=request.POST["project_fiche"]
            formfiche = FicheForm()
    else:
        formfiche = FicheForm()
    projects = _get_projects(request)
    title = 'All projects'
    names = ['name', 'authors', 'users', 'institutions']
    labels = ['Project', 'Authors', 'Users', 'institutions']
    
    tag_fields = {'all': projects,
                  'title': title,
                  'fields_name': names,
                  'fields_label': labels,
                  'creation_mode': True,
                  'dataview':True,
                  }    
    if formfiche.is_valid() or "project_fiche" in request.POST:
        print("form_fiche")
        if "project_fiche" not in request.POST:
            fiche = request.POST['fiche']
        project = Project.objects.get(name=fiche)
        name = project.name
        authors = project.authors
        start_date = project.start_date
        end_date = project.end_date
        description = project.description
        experiment= Experiment.objects.filter(projects=project)
        project_experiment=[]
        for exp in experiment:
            project_experiment.append(exp.name)
        fiche="project_card"
        if len(project.institutions.all())!=0:
            instit = project.institutions.get().name
        else:
            instit=""
             
        if len(project.linked_files.all())!=0:
            linked_files = project.linked_files.get().name
        else:
            linked_files=""
        return render(request, 'dataview/project_infos.html', locals())
    print("return 2 ")
    return render(request, 'dataview/dtview_tableau_projects.html', tag_fields)
    
def _get_projects(request):
    username = None
    if request.user.is_authenticated():
        username = request.user.login
    username_id=User.objects.get(login=username).id
    projects = Project.objects.filter(users=username_id)
    if User.objects.get(login=username).is_superuser():
        projects = Project.objects.all()
    
        # Adding Paginator        
    paginator = Paginator(projects, 50)
    page = request.GET.get('page')
    try:
        projects = paginator.page(page)
    except PageNotAnInteger:
        # If page number is not an integer then page one 
        projects = paginator.page(1)
    except EmptyPage:
        # invalid page number show the last
        projects = paginator.page(paginator.num_pages)    
    return projects

def genealogy_viewer(request):
    username = None
    or_and = None
    if request.user.is_authenticated():
        username = request.user.login
    if "return" not in request.session:
        request.session['return']="no"
    formfiche=FicheForm()     
    list_attributes=[]
    for i in ['accession', 'type', 'pedigree', 'description', 'projects']:
        list_attributes.append(i)
    number_all=0
    tag_fields = {}
    request.session['return']="no"
     
    if request.method=='POST':
        
        projects_id = []
        #if request.POST.getlist('project') != []:
        projects_id = request.POST.getlist('project')
        #else:
            #projects = Project.objects.filter(users=User.objects.get(login=username).id)
            #projects_id = [project.id for project in projects]
        accession_all = Accession.objects.filter(projects__in = projects_id)
        seedlot_all = Seedlot.objects.filter(projects__in = projects_id)
        dico_relation = {}
        title = ''
        fields_label = []
        method_cat = { 'Accession Cross':2,
                       'Other Pedigree':4,
                       'Seedlot Cross':3,
                       'Multiplication':1}
        
        if request.POST.getlist('genealogy_types') == ["1"]:
            title = 'Accession Cross Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Synonymous','First Production','Method','Description']
            for i in accession_all:
                try:
                    accession_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    accession_relation_method = accession_relation.reproduction.reproduction_method
                    #On teste pour ne recuperer que les accessions relations de cette categorie de methode
                    if accession_relation_method.category == method_cat['Accession Cross']:
                        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(accession_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append(accession_relation.synonymous)
                        dico_relation[i.name].append(str(accession_relation.first_production))
                        dico_relation[i.name].append(accession_relation.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(accession_relation.reproduction.description)
                except:
                    pass
            
        elif request.POST.getlist('genealogy_types') == ["2"]:
            title = 'Seedlot Cross Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Site','Start Date','End Date','Method','Description']
            for i in seedlot_all:
                try:
                    seedlot_cross = SeedlotRelation.objects.get(parent_gender = "M", child=i)
                    seedlot_cross_method = seedlot_cross.reproduction.reproduction_method
                    if seedlot_cross_method.category == method_cat['Seedlot Cross']:
                        parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=i).parent
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(seedlot_cross.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append(seedlot_cross.site)
                        dico_relation[i.name].append(str(seedlot_cross.reproduction.start_date))
                        dico_relation[i.name].append(str(seedlot_cross.reproduction.end_date))
                        dico_relation[i.name].append(seedlot_cross.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(seedlot_cross.reproduction.description)
                except:
                    pass
                
        elif request.POST.getlist('genealogy_types') == ["4"]:
            title = 'Other Accession Pedigree Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Synonymous','First Production','Method','Description']
            for i in accession_all:
                try:
                    accession_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    accession_relation_method = accession_relation.reproduction.reproduction_method
                    if accession_relation_method.category == method_cat['Other Pedigree']:
                        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(accession_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append(accession_relation.synonymous)
                        dico_relation[i.name].append(str(accession_relation.first_production))
                        dico_relation[i.name].append(accession_relation.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(accession_relation.reproduction.description)
                except:
                    pass
                
        elif request.POST.getlist('genealogy_types') == ["3"]:
            title = 'Seedlot Multiplication Dataview'
            fields_label = ['Name','Parent X','Site','Start Date','End Date','Method','Description']
            for i in seedlot_all:
                try:
                    seedlot_cross = SeedlotRelation.objects.get(parent_gender = "X", child=i)
                    seedlot_cross_method = seedlot_cross.reproduction.reproduction_method
                    if seedlot_cross_method.category == method_cat['Multiplication']:
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(seedlot_cross.parent.name)
                        dico_relation[i.name].append(seedlot_cross.site)
                        dico_relation[i.name].append(str(seedlot_cross.reproduction.end_date))
                        dico_relation[i.name].append(str(seedlot_cross.reproduction.end_date))
                        dico_relation[i.name].append(seedlot_cross.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(seedlot_cross.reproduction.description)
                except:
                    pass
        
        elif request.POST.getlist('genealogy_types') == ['2','3']:
            title = 'Seedlot Genealogy Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Parent X','Site','Start Date','End Date','Method','Description','Type of Reproduction']
            for i in seedlot_all:
                try:
                    try:
                        seedlot_cross = SeedlotRelation.objects.get(parent_gender = "M", child=i)
                        parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=i).parent
                        seedlot_type_repro = "Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(seedlot_cross.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append('')
                    except:
                        seedlot_cross = SeedlotRelation.objects.get(parent_gender = "X", child=i)
                        seedlot_type_repro = "Multiplication"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append('')
                        dico_relation[i.name].append('')                           
                        dico_relation[i.name].append(seedlot_cross.parent.name)
                    dico_relation[i.name].append(seedlot_cross.site)
                    dico_relation[i.name].append(str(seedlot_cross.reproduction.end_date))
                    dico_relation[i.name].append(str(seedlot_cross.reproduction.end_date))
                    dico_relation[i.name].append(seedlot_cross.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(seedlot_cross.reproduction.description)
                    dico_relation[i.name].append(seedlot_type_repro)
                except:
                    pass
        
        elif request.POST.getlist('genealogy_types') == ['1','2','3']:
            title = 'Genealogy Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Parent X','Method','Description','Type of Reproduction']
            for i in seedlot_all:
                try:
                    try:
                        genealogy_relation = SeedlotRelation.objects.get(parent_gender = "M", child=i)
                        parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=i).parent
                        type_repro = "Seedlot Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append('')
                    except:
                        genealogy_relation = SeedlotRelation.objects.get(parent_gender = "X", child=i)
                        type_repro = "Seedlot Multiplication"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append('')
                        dico_relation[i.name].append('')                           
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.description)
                    dico_relation[i.name].append(type_repro)
                except:
                    pass
            for i in accession_all:
                try:
                    genealogy_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    genealogy_relation_method = genealogy_relation.reproduction.reproduction_method
                    if genealogy_relation_method.category == method_cat['Accession Cross']:
                        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                        type_repro = "Accession Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append('')
                        dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(genealogy_relation.reproduction.description)
                        dico_relation[i.name].append(type_repro)
                except:
                    pass
            
        elif request.POST.getlist('genealogy_types') == ['1','2']:
            title = 'Cross Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Method','Description','Type of Reproduction']
            for i in seedlot_all:
                try:
                    genealogy_relation = SeedlotRelation.objects.get(parent_gender = "M", child=i)
                    parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=i).parent
                    type_repro = "Seedlot Cross"
                    dico_relation[i.name] = []
                    dico_relation[i.name].append(genealogy_relation.parent.name)
                    dico_relation[i.name].append(parent_female.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.description)
                    dico_relation[i.name].append(type_repro)
                except:
                    pass
            for i in accession_all:
                try:
                    genealogy_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    genealogy_relation_method = genealogy_relation.reproduction.reproduction_method
                    if genealogy_relation_method.category == method_cat['Accession Cross']:
                        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                        type_repro = "Accession Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(genealogy_relation.reproduction.description)
                        dico_relation[i.name].append(type_repro)
                except:
                    pass
        
        elif request.POST.getlist('genealogy_types') == ['1','3'] :
            title = 'Genealogy Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Parent X','Method','Description','Type of Reproduction']
            for i in seedlot_all:
                try:
                    genealogy_relation = SeedlotRelation.objects.get(parent_gender = "X", child=i)
                    type_repro = "Seedlot Multiplication"
                    dico_relation[i.name] = []
                    dico_relation[i.name].append('')
                    dico_relation[i.name].append('')                           
                    dico_relation[i.name].append(genealogy_relation.parent.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.description)
                    dico_relation[i.name].append(type_repro)
                except:
                    pass
            for i in accession_all:
                try:
                    genealogy_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    genealogy_relation_method = genealogy_relation.reproduction.reproduction_method
                    if genealogy_relation_method.category == method_cat['Accession Cross']:
                        parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                        type_repro = "Accession Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append('')
                        dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                        dico_relation[i.name].append(genealogy_relation.reproduction.description)
                        dico_relation[i.name].append(type_repro)
                except:
                    pass
         
        elif request.POST.getlist('genealogy_types') == ['1','4','2','3']:
            title = 'Genealogy Dataview'
            fields_label = ['Name','Parent Male','Parent Female','Parent X','Method','Description','Type of Reproduction']
            for i in seedlot_all:
                try:
                    try:
                        genealogy_relation = SeedlotRelation.objects.get(parent_gender = "M", child=i)
                        parent_female = SeedlotRelation.objects.get(parent_gender = "F", child=i).parent
                        type_repro = "Seedlot Cross"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                        dico_relation[i.name].append(parent_female.name)
                        dico_relation[i.name].append('')
                    except:
                        genealogy_relation = SeedlotRelation.objects.get(parent_gender = "X", child=i)
                        type_repro = "Seedlot Multiplication"
                        dico_relation[i.name] = []
                        dico_relation[i.name].append('')
                        dico_relation[i.name].append('')                           
                        dico_relation[i.name].append(genealogy_relation.parent.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.description)
                    dico_relation[i.name].append(type_repro)
                except:
                    pass
            for i in accession_all:
                try:
                    genealogy_relation = AccessionRelation.objects.get(parent_gender = "M", child=i)
                    genealogy_relation_method = genealogy_relation.reproduction.reproduction_method
                    if genealogy_relation_method.category == method_cat['Accession Cross']:
                        type_repro = "Accession Cross"
                    elif genealogy_relation_method.category == method_cat['Other Pedigree']:
                        type_repro = "Other Accession Pedigree"
                    parent_female = AccessionRelation.objects.get(parent_gender = "F", child=i).parent
                    dico_relation[i.name] = []
                    dico_relation[i.name].append(genealogy_relation.parent.name)
                    dico_relation[i.name].append(parent_female.name)
                    dico_relation[i.name].append('')
                    dico_relation[i.name].append(genealogy_relation.reproduction.reproduction_method.name)
                    dico_relation[i.name].append(genealogy_relation.reproduction.description)
                    dico_relation[i.name].append(type_repro)
                except:
                    pass 
         
        request.session['genealogy_types'] = request.POST.getlist('genealogy_types')
        request.session['dico_relation'] = dico_relation
        request.session['title'] = title
        request.session['projects_id'] = projects_id
        request.session['fields_label'] = fields_label
        list_attributes = fields_label
        if dico_relation != {}:
            df = pd.DataFrame.from_dict(dico_relation, orient='index') ## à changer !!!!
            ## read csv cf locus data genotyping view
            df.columns=fields_label[1:]
            df.to_csv('/tmp/dtview_genealogy.csv', encoding='utf-8')     
            return render(request,'dataview/genealogy_viewer.html', {'list_attributes':list_attributes,"refine_search":True,'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico_relation.items(),'fields_label':fields_label})
        else:
            df = pd.DataFrame(data=None, columns=fields_label[1:])
            df.to_csv('/tmp/dtview_genealogy.csv', encoding='utf-8')
            return render(request,'dataview/genealogy_viewer.html', {'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico_relation.items(),"number_all":number_all})

    else:
        if "or_and" in request.GET:
            genealogy_types = request.session['genealogy_types']
            data = request.session['dico_relation']
            title = request.session['title']
            projects_id = request.session['projects_id']
            fields_label = request.session['fields_label']
            list_attributes = fields_label
            form = None
            formf = None
            template = 'dataview/genealogy_viewer.html'
            df_test = pd.DataFrame.from_dict(data, orient='index')
            df_test.columns = fields_label[1:]

            dico_get={}
            or_and = None
            or_and=request.GET['or_and']
            for i in range(1,10):
                data="data"+str(i)
                text_data="text_data"+str(i)
                if data in request.GET and text_data in request.GET:
                    dico_get[data]={request.GET[data]:request.GET[text_data]} # format: dico_get[data1]={"name":"name_search"} obligation de mettre data1, data2 etc sinon, s'il y a plusieurs "name", ça écrasera
                    
            #                 if or_and in request.GET:
            #                     dico_get[]
            length_dico_get = len(dico_get)
            nb_per_page = 10

            if or_and == 'and':
                
                df_empty = 1
                
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if df_empty == 1:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False)
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False)
                            df_empty = 2
                        else:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False) & df_or_and
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False) & df_or_and
            elif or_and == 'or':
                df_empty = 1
                for dat, dico_type_data in dico_get.items():
                    for data_type, value in dico_type_data.items():
                        if df_empty == 1:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False)
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False)
                            df_empty = 2
                        else:
                            if data_type == "Name":
                                df_or_and = df_test.index.to_series().str.contains(value,case=False,na=False) & df_or_and
                            else:
                                df_or_and = df_test[data_type].str.contains(value,case=False,na=False) & df_or_and
            #print(df_or_and)
            df = df_test[df_or_and]
            dico_locus = {}
    #         print('test1')
            df.index.names = ['name']
            #df_test = df.to_dict('index')
            df.reset_index()
            df_test = df.to_dict()
            dico = {}
#             for i in df_test.keys():
#                 print(i)
#                 dico[i]=[]
#                 for j in fields_label[1:]:
#                     print(j)
#                     dico[i].append(df_test[i][j])

            for i in df_test[fields_label[1]].keys():
                #print(i)
                dico[i]=[]
                for j in fields_label[1:]:
                    #print(j)
                    dico[i].append(df_test[j][i])
            
            if 'no_search' in request.GET:
                dico_get = False
                    
            return render(request,'dataview/genealogy_viewer.html', {"search_result":dico_get,"or_and":or_and,"refine_search":True,'list_attributes':list_attributes,'title':title,'genealogy_viewer':True,'dataview':True,'dico_relation':dico.items,'fields_label':fields_label})

        formproject = DtviewProjectForm()
        #if User.objects.get(login=username).is_superuser():
            #projects = Project.objects.all()
        #else:
        username_id=User.objects.get(login=username).id
        projects = Project.objects.filter(users=username_id)
        projects_allowed=', '.join([(v.name) for v in projects])
        projects_allowed = projects_allowed.split(', ')
        formproject.fields['project'].queryset=projects.order_by('name')
        genealogytypeform = DtviewGenealogyTypeForm()
        
    return render(request,'dataview/choose_project.html', {'genealogy_viewer':True,'dataview':True,'projects':projects_allowed,'formproject':formproject,'genealogytypeform':genealogytypeform})

def return_csv(request, filename):
    tmpfile=open('/tmp/'+filename+'.csv','rb')
    response=HttpResponse(tmpfile.read(),content_type="application/vnd.ms-excel")
    response['Content-Disposition']='attachment; filename='+filename+'.csv'
    return response

from dal import autocomplete
 
class AccessionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Accession.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
        #    qs = Accession.objects.filter(projects__in=projects)
        # if User.objects.get(login=username).is_superuser():
        #     qs = Accession.objects.all()
        # else:       
            qs = Accession.objects.filter(projects__in=projects)
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class SeedlotAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Seedlot.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Seedlot.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Seedlot.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs
    
class SampleAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Sample.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Sample.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Sample.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class GenotypingIDAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return GenotypingID.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = GenotypingID.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = GenotypingID.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs
    
class ExperimentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Experiment.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Experiment.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Experiment.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class ReferentialAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Referential.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id = User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
            qs = Referential.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Referential.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs
    
class ClassificationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Classification.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id = User.objects.get(login=username).id
            projects = Project.objects.filter(users=username_id)
            qs = Classification.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Classification.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

# class ReferentialAutocomplete(autocomplete.Select2QuerySetView):
#     def get_queryset(self):
#         if not self.request.user.is_authenticated():
#             return Referential.objects.none()
#         username = None
#         if self.request.user.is_authenticated():
#             username = self.request.user
#             username_id = User.objects.get(login=username).id
#             projects = Project.objects.filter(users=username_id)
#             qs = Referential.objects.filter(projects__in=projects)
#         if User.objects.get(login=username).is_superuser():
#             qs = Referential.objects.all()
#             
#         if self.q:
#             qs = qs.filter(name__istartswith=self.q)
#             
#         return qs

class GenericAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        try :
            module_name = self.kwargs['module']
            class_name = self.kwargs['classname']
            mod = __import__(module_name)
            models = getattr(mod,'models')
            BaseClass = getattr(models, class_name)
        except :
            raise Http404
        if not self.request.user.is_authenticated():
            return BaseClass.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = BaseClass.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = BaseClass.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return User.objects.none()
        qs = User.objects.all()
        if self.q:
            qs=qs.filter(login__istartswith=self.q)
        return qs

class LocusAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Locus.objects.none()
        qs = Locus.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class EnvironmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Environment.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Environment.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Environment.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs

class TraitAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Trait.objects.none()
        username = None
        if self.request.user.is_authenticated():
            username = self.request.user
            username_id=User.objects.get(login=username).id
            projects=Project.objects.filter(users=username_id)
            qs = Trait.objects.filter(projects__in=projects)
        if User.objects.get(login=username).is_superuser():
            qs = Trait.objects.all()
        if self.q:
            qs=qs.filter(name__istartswith=self.q)
        return qs
    
def viewphenodata(request):
    return

def get_projects_to_show(request,i,projects_allowed):
    projects_to_be_seen = i.projects.filter()
    projects_allowed = str(projects_allowed).strip('[]')
    projects_allowed = (projects_allowed.strip("'"))
    projects_allowed = projects_allowed.split("', '")
    projects_to_show = []
    for k in projects_to_be_seen:
        k = (k.name)
        if k in projects_allowed:
            projects_to_show.append(k)
            if k not in request.session['projects_to_show']:
                request.session['projects_to_show'].append(k)
    return projects_to_show
