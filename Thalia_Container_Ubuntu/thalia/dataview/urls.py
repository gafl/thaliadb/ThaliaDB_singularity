#-*- coding: utf-8 -*-
"""ThaliaDB is developed by the ABI-SOFT team of GQE-Le Moulon INRA unit
    
    Copyright (C) 2017, Guy-Ross Assoumou, Lan-Anh Nguyen, Olivier Akmansoy, Arthur Robieux, Alice Beaugrand, Yannick De-Oliveira, Delphine Steinbach

    This file is part of ThaliaDB

    ThaliaDB is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.conf.urls import url

from dataview.views import AccessionAutocomplete, LocusAutocomplete,SeedlotAutocomplete,SampleAutocomplete, ExperimentAutocomplete, TraitAutocomplete, ReferentialAutocomplete, EnvironmentAutocomplete, GenotypingIDAutocomplete, UserAutocomplete, GenericAutocomplete, ClassificationAutocomplete
from dataview.views import dtgenohome, viewphenodata, genoViewer_management, crossViewer_management, referential_viewer, accession_viewer, data_card, genoID_viewer, experiment_viewer, trait_viewer, environment_viewer, phenoViewer_management, classViewer_management, classPieChart, accessionMap, classMapViewer_management, structure_viewer
from dataview.views import aseedsam_viewer, project_viewer, return_csv, genealogy_viewer
from commonfct.views import global_search, GlobalSearchAutocomplete


urlpatterns = [
                       url(r'^dthome/$', dtgenohome, name='dtgenohome'),
                       url(r'^phenoViewer/$', phenoViewer_management, name='phenoViewer_management'),
                       url(r'^viewpheno/$', viewphenodata,name='viewphenodata'),
                       url(r'^genoViewer/$', genoViewer_management,name='genoViewer_management'),
                       url(r'^classViewer/$', classViewer_management,name='classViewer_management'),
                       url(r'^classMapViewer/$', classMapViewer_management,name='classMapViewer_management'),
                       url(r'^classPieChart/(?P<classification>[\w]+)/(?P<seedlot>[\w]+)/$', classPieChart,name='classPieChart'),
                       url(r'^crossViewer/$', crossViewer_management,name='crossViewer_management'),
                       url(r'^accessionViewer/$', accession_viewer, name='accession_viewer'),
                       url(r'^accessionMap/(?P<string_accessions>[\w]+)/$', accessionMap,name='accessionMap'),
                       url(r'^data_card/$', data_card, name='data_card'),
                       url(r'^genoIDViewer/$', genoID_viewer, name='genoID_viewer'),
                       url(r'^experimentViewer/$', experiment_viewer, name='experiment_viewer'),
                       url(r'^structureViewer/$', structure_viewer, name='structure_viewer'),
                       url(r'^referentialViewer/$', referential_viewer, name='referential_viewer'),
                       url(r'^traitViewer/$', trait_viewer, name='trait_viewer'),
                       url(r'^environmentViewer/$', environment_viewer, name='environment_viewer'),
                       url(r'^aseedsamViewer/$', aseedsam_viewer, name='aseedsam_viewer'),
                       url(r'^projectViewer/$', project_viewer, name='project_viewer'),
                       url(r'^genealogyViewer/$', genealogy_viewer, name='genealogy_viewer'),
                       
                       url(r'^referential-autocomplete/$', ReferentialAutocomplete.as_view(), name="referential-autocomplete"),
                       url(r'^trait-autocomplete/$', TraitAutocomplete.as_view(), name='trait-autocomplete'),
                       url(r'^environment-autocomplete/$', EnvironmentAutocomplete.as_view(), name='environment-autocomplete'),
                       url(r'^user-autocomplete/$', UserAutocomplete.as_view(), name='user-autocomplete'),
                       url(r'^accession-autocomplete/$', AccessionAutocomplete.as_view(), name='accession-autocomplete'),
                       url(r'^seedlot-autocomplete/$', SeedlotAutocomplete.as_view(), name='seedlot-autocomplete'),
                       url(r'^sample-autocomplete/$', SampleAutocomplete.as_view(), name='sample-autocomplete'),
                       url(r'^genotypingID-autocomplete/$', GenotypingIDAutocomplete.as_view(), name='genotypingID-autocomplete'),
                       url(r'^experiment-autocomplete/$', ExperimentAutocomplete.as_view(), name='experiment-autocomplete'),
                       url(r'^locus-autocomplete/$', LocusAutocomplete.as_view(), name='locus-autocomplete'),
                       url(r'^classification-autocomplete/$', ClassificationAutocomplete.as_view(), name='classification-autocomplete'),
                       url(r'^globalsearch/$', global_search, name='globalsearch'),
                       url(r'^search-autocomplete/$', GlobalSearchAutocomplete.as_view(), name='search-autocomplete'),
                       
                       
                       url(r'^generic-autocomplete/(?P<module>[\w]+)/(?P<classname>[\w]+)/$', GenericAutocomplete.as_view(), name='generic-autocomplete'),
                       
                       url(r'^return_csv/(?P<filename>[\w]+)/$', return_csv,name='return_csv')
]