<div id='postgres'/> 


### Install container Postgresql

* Change directory for Postgres_Container

If you are in the ThaliaDB_Ctn directory made :

```
cd ./Postgres_Container
```

Or

```
cd /path/to/Postgres_Container
```

* Build container postgres with singularity

```
sudo singularity build ctn.img postgres.def
```

You can replace **"ctn"** by any name, but you have to make sure to have as extension of the container **".img"**

* Choose configuration settings

You can modify the parameters found in the **"parms.conf"** file in the **"config"** directory to choose the port number (choose a port number that is not used), the ip address or the directory (this directory must exist) to which you want the data to be stored. 

Warning: the value of the variable **"data_directory"** must be an absolute path, and must not be just the root (do not put '/'), but for example : /home

* Initialize Postgresql
```
sudo sh init_postgres.sh
```

* Run Postgresql

If you want to run postgresql run the script **"run_postgres.sh"**
```
sudo sh run_postgres.sh
```

* Stop Postgresql

If you want to stop postgresql run the script **"stop_postgres.sh"** 

```
sudo sh stop_postgres.sh
```

*******************