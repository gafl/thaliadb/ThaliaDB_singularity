 #recuperation du nom de l'image
nom_container_img=`ls | grep -o *\.img`

 # recuperation des parametres
port=`cat ./config/params.conf | grep "port = "| sed s/[^0-9]*//g`
data_directory_choice=`cat ./config/params.conf | grep "data_directory = "| grep -o \'.*\' | sed s/\'//g | sed -e 's/\/$//g'`
listen_addresses=`cat ./config/params.conf | grep "listen_addresses = "| grep -o \'.*\' | sed s/\'//g`


# modification du fichier de configuration selon les parametres
echo "$data_directory_choice $nom_container_img ----"
sed -e s/"port = [0-9]*"/"port = $port"/g -i ./config/postgresql.conf
sed -e s/"listen_addresses = '.*'"/"listen_addresses = \'$listen_addresses'"/g -i ./config/postgresql.conf
data_directory=$data_directory_choice/postgres_ctn/data
data_directory_temp=$(echo $data_directory | sed -e 's/\//\\\//g')
sed -e s/"data_directory = '.*'"/"data_directory = \'$data_directory_temp\'"/g -i ./config/postgresql.conf

#lancement de la configuration insterne de postgres
singularity run --app config -B ./scripts:/scripts,./config:/config,$data_directory_choice,./config/postgresql.conf:/etc/postgresql/10/main/postgresql.conf $nom_container_img

#sudo singularity shell -B /home/mmohamed,./config/postgresql.conf:/etc/postgresql/10/main/postgresql.conf ctn.img
