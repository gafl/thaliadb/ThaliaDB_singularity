port=`cat /etc/postgresql/10/main/postgresql.conf | grep "port = "| sed s/[^0-9]*//g`
data_directory=`cat /etc/postgresql/10/main/postgresql.conf | grep "data_directory = "| grep -o \'.*\' | sed s/\'//g`
listen_addresses=`cat /etc/postgresql/10/main/postgresql.conf | grep "listen_addresses = "| grep -o \'.*\'`

data_directory_choice=`cat /config/params.conf | grep "data_directory = "| grep -o \'.*\' | sed s/\'//g | sed -e 's/\/$//g'`


(ls $data_directory_choice/postgres_ctn/data/postgresql.conf || (rm -r $data_directory_choice/postgres_ctn && echo "Suppression data_directory [OK]")) || echo "Suppression data_directory [KO]"
mkdir -p $data_directory_choice/postgres_ctn  
mkdir -p $data_directory_choice/postgres_ctn/data && echo "Creation new data_directory [OK]"
mkdir -p $data_directory_choice/postgres_ctn/log


chown postgres $data_directory_choice/postgres_ctn
chown postgres $data_directory_choice/postgres_ctn/data
chown postgres $data_directory_choice/postgres_ctn/log
chmod 777 /var/run/postgresql
chmod 777 /var/run/postgresql/*


su - postgres -c "/usr/lib/postgresql/10/bin/initdb $data_directory_choice/postgres_ctn/data" && echo "Init Data [OK]"
su - postgres -c "service postgresql start"
su - postgres -c "psql -c \"CREATE DATABASE thalia;\""
su - postgres -c "psql -c \"CREATE USER thaliadbadmin WITH ENCRYPTED PASSWORD 'db';\""
su - postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE thalia TO thaliadbadmin;\""
su - postgres -c "psql -c \"ALTER USER thaliadbadmin WITH SUPERUSER CREATEDB;\""
su - postgres -c "service postgresql stop"

#su - postgres -c "service postgresql start"
#su - postgres -c "/usr/lib/postgresql/10/bin/pg_ctl -D $data_directory/postgres_ctn/data -p $port start"
# su - postgres -c "/usr/lib/postgresql/10/bin/createuser -s -p $port -w --no-password thaliadbadmin"
# su - postgres -c "/usr/lib/postgresql/10/bin/createdb -p $port -wU thaliadbadmin thalia"
# su - postgres -c "service postgresql stop"

#/usr/lib/postgresql/10/bin/createuser -s -p 5437 -w --no-password thaliadbadmin
#/usr/lib/postgresql/10/bin/pg_ctl -D /home/mmohamed/postgres_ctn/data -p 5437 start
#/usr/lib/postgresql/10/bin/createdb -p 5437 -wU thaliadbadmin thalia
#/usr/lib/postgresql/10/bin/initdb /home/mmohamed/postgres_ctn/data