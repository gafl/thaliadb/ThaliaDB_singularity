name_instance=instance_postgres
#nom_container_img=`ls | grep -o *\.img`
nom_container_img=ctn.img
data_directory_choice=`cat ./config/params.conf | grep "data_directory = "| grep -o \'.*\' | sed s/\'//g | sed -e 's/\/$//g'`

singularity instance start -B ./postgresql.log:/var/log/postgresql,./postgresql.log:/var/run/postgresql,$data_directory_choice,./config/postgresql.conf:/etc/postgresql/10/main/postgresql.conf $nom_container_img $name_instance



#singularity instance start -B /nosave/project/gafl/tools/containers/recipes/thaliadb/thaliadb_container/ThaliaDB_Ctn/Postgres_Container/postgresql.log:/var/run/postgresql,/nosave/project/gafl/tools/containers/recipes/thaliadb/data,./config/postgresql.conf:/etc/postgresql/10/main/postgresql.conf ctn.img instance_postgres

