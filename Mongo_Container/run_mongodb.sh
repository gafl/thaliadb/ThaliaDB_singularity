name_instance=instance_mongodb
#nom_container_img=`ls | grep -o *\.img`
nom_container_img=ctn.img
dbPath_choice=`cat ./config/params.conf | grep dbPath: | grep -o "\/.*" | sed -e 's/\/$//g'`
singularity instance start -B ./config:/config,$dbPath_choice,./config/mongod.conf:/etc/mongod.conf $nom_container_img $name_instance
