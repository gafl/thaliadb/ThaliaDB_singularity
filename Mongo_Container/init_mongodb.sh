#recuperation du nom de l'image
nom_container_img=`ls | grep -o *\.img`

port=`cat ./config/params.conf | grep port: | grep -o [0-9]*`
ip=`cat ./config/params.conf | grep bindIp: | grep -o [0-9.]*`
dbPath_choice=`cat ./config/params.conf | grep dbPath: | grep -o "\/.*" | sed -e 's/\/$//g'`

#port
#modif port dans mongod.conf
sed -e  "s/port: [0-9]*/port: $port/g" -i ./config/mongod.conf

# dbPath
#modif bindIp dans mongod.conf
sed -e  "s/bindIp: [0-9.]*/bindIp: $ip/g" -i ./config/mongod.conf

#dbPath
dbPath=$dbPath_choice/mongodb_ctn/data
dbPath_temp=$(echo $dbPath | sed -e 's/\//\\\//g')

#modif dbPath dans mongod.conf
sed -e  s/"dbPath: \/.*"/"dbPath: $dbPath_temp"/g -i ./config/mongod.conf

#lancement de la configuration insterne de postgres
singularity run --app config -B ./scripts:/scripts,./config:/config,./config:/config,$dbPath_choice,./config/mongod.conf:/etc/mongod.conf $nom_container_img
