<div id='mongodb'/> 

### Install container MongoDB

If you are in the ThaliaDB_Ctn directory make :

```
cd ./Mongo_Container
```

Or

```
cd /path/to/Mongo_Container
```

* Build container mongodb with singularity

```
sudo singularity build ctn.img mongodb.def
```
You can replace **"ctn"** by any name, but you have to make sure to have as extension of the container **".img"** 

* Choose configuration settings

You can modify the parameters found in the **"parms.conf"** file in the **"config"** directory to choose the port number (choose a port number that is not used), the ip address or the directory (this directory must exist) to which you want the data to be stored. 

Warning: the value of the variable **"dbPath"** must be an absolute path, and must not be just the root (do not put '/'), but for example : /home

* Initialize MongoDB
```
sudo sh init_mongodb.sh
```

* Run MongoDB

If you want to start mongodb run script **"run_mongodb.sh"** 
```
sudo sh run_mongodb.sh
```

* Stop MongoDB
if you want to stop mongodb run script **"stop_mongodb.sh"** 

```
sudo sh stop_mongodb.sh
```

*******************