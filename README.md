# ThaliaDB V3.4.1

*******
Contents 
 
 1. [Install ](#thalia)

*******

## Goal

The goal of the tutorial is to install ThaliaDB V3 (User interface) on a linux.

ThaliaDB V3 application uses Django framework. It is written in Python. 

It uses 2 databases system: 
* one relational : PostGreSQL. Django accepts also MySQL or Oracle (modifications to do in the settings file of ThaliaDB application, if you want to use it instead of PostGreSQL, not tested yet).
* one noSQL : MongoDB dedicated for high throughput data management (here to manage the **genotyping** data of **ThaliaDB**).

*******************

### Requirements

You must have the right to super user (sudo, root)

You must also have singularity of preference version 2.4.2, this is the version that was to test.

make : 
```
sudo apt-get install -y singularity-container
```
if you have a problem installing singularity, please follow : 
https://singularity.lbl.gov/install-linux 
or 
https://www.sylabs.io/guides/3.0/user-guide/installation.html
for installation


*******************

<div id="thalia"></div>

### Install

* Build container

Go to Thalia_Ctn directory

```
cd /path/to/Thalia_Ctn
```

And start 

```
sudo sh build_all.sh
```


* Choose configuration settings

You have to modify the file **"params.conf"** which is in the directory **"Thalia_Ctn/Postgres_Container/config/params.conf"** and **"Thalia_Ctn/Mongo_Container/config/params.conf"**  to choose the port number (choose a port number that is not used), the ip address or the directory (this directory must exist) to which you want the data to be stored. 

**Postgres**


<div class="text_bloc" >

```
port = 5437
listen_addresses = 'localhost'
data_directory = '/home'
```

</div>

**MongoDB**

<div class="text_bloc" >

```
port: 27020
bindIp: 127.0.0.1
dbPath: /home
```

</div>

Warning: the value of the variable **"data_directory"** or **"dbPath"** must be an absolute path without space, and must not be just the root (do not put '/'), but for example : /home

In the file **"Thalia_Ctn/Thalia_Container_Ubuntu/config/django-apps.conf"** you can choose on which port Apache will launch the application.

<div class="text_bloc" >

```
Listen 801
<VirtualHost *:801>
	ServerName example.com

	WSGIDaemonProcess thalia  python-path=/var/www/django-apps/thalia/
	WSGIScriptAlias / /usr/local/django-apps/apache/thalia.wsgi process-group=thalia application-group=%{GLOBAL}

	Alias /media/ /var/www/django-apps/thalia/media/
	Alias /static/ /var/www/django-apps/thalia/static/

	<Directory /usr/local/django-apps/apache/>
		<Files "*.wsgi">
			Require all granted
		</Files>
	</Directory>
</VirtualHost>
```

</div>

you just have to change:

<div class="text_bloc" >

```
Listen 801
<VirtualHost *:801>
...
```
</div>

Ports after Listen and virtualHost must be the same.

* Init container

```
sudo sh init_all.sh
```
* Start container

```
sudo sh run_all.sh
```

* Stop container

```
sudo sh stop_all.sh
```

follow this link :
http://127.0.0.1:801

port 801 is the default port if you want change just change the port number in the file **"Thalia_Ctn/Thalia_Container_Ubuntu/config/django-apps.conf"** and initialize all the container by launching :

```
sudo sh init_all.sh
```

* Login Thalia

By default login and password on **ThaliaDB** is :
<br/>
> **login** : thaliadbadmin

> **password** : PWDTOCHANGE

Enjoy :+1:

*******************