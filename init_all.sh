cd ./Postgres_Container
sh init_postgres.sh && echo [ OK ]

cd ../Mongo_Container
sh init_mongodb.sh && echo [ OK ]

cd ../Postgres_Container
sh run_postgres.sh && echo [ Run Ptgresql ]

cd ../Mongo_Container
sh run_mongodb.sh  && echo [ Run Mongo ]

cd ../Thalia_Container_Ubuntu
sh init_thalia.sh && echo [ OK ]

cd ../
sh ./Postgres_Container/stop_postgres.sh 
sh ./Mongo_Container/stop_mongodb.sh